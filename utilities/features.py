
from collections import defaultdict
# extension package
from scipy import interpolate
import numpy as np

# local packages
from peak_picker.TonalClass import tonal

def read(corpus, params):
    """
    read(corpus, params)
    Read whistles from the corpus.  Files in the corpus that do not match
    selection criteria will be removed from the corpus
    :param corpus:  Dictionary.  Keys are classes, values are lists of files
      from which whistles will be read.  Files will be removed from the
      corpus if they contain no whistles (side effect).
    :param params:  Dictionary corresponding to the experiment/features
      portion of the XML specification for the experiment.  See experiment
      specification for details.
    :return: dictionary with data arranged by category key.
        Each key has a list of lists.  The outer lists correspond to files,
        the innner lists are whistles within a file:
        "key" : [ [f0w0, f0w1, f0w2, ...], [f1w0, f1w1, ...], ...]


        Each file/whistle is a set of tonal descriptors arranged by time
        and then measurements. For example, a 20 ms whistle measured every
        2 ms would have 10 measurements.  If just the frequencies were returned,
        a 10x1 array would be returned.  If we computed the frequencies and their
        derivatives, a 20x2 (or 19x2 depending on how the first/last sample
        are handled) array would be returned.
    """
    data = defaultdict(list)

    for species in corpus.keys():
        noselections = []  # files with no selections will be removed
        for f in corpus[species]:
            (contours, starts) = get_features(f, minlen_s=params['minduration_s'])
            if len(contours) == 0:
                noselections.append(f)
                continue


            transform = params.get('transform', None)
            if transform == "zscorederiv":
                zcontours = []
                for contour in contours:
                    z = ztransform(contour)
                    # Append final vector with summary information:
                    # mean frequency and total of derivatives to give overall
                    # direction
                    mu = np.mean(contour)
                    muvec = np.array((mu,np.sum(z[1,:])),ndmin=2)
                    #zcontours.append(np.concatenate((z, muvec), axis=0))
                    zcontours.append(z)
                data[species].append(zcontours)
            else:
                data[species].append(contours)

        if len(noselections) > 0:
            # SIDE EFFECT:  No contours matching selection criteria
            for f in noselections:
                corpus[species].remove(f)

    return data


def get_features(filename, minlen_s=None, framerate_s=0.02):
    """get_features - Retrieve tonal contours
chmtv
    :param filename: Name of tonal annotation file
    :param minlen_s: Prune tonals < minlen_s
    :param framerate_s:  Resample every framerate_s
    :return: (freq_list, start_times) -
        freq_list - List of lists, each list are the frequencies for a tonal
        start_times - Start time relative to file (s) for each tonal
    """
    tonals = tonal(filename)
    freq_list = []
    times = []
    pruned = 0
    for t_item in tonals:
        t = t_item['Time']
        duration_s = t[-1] - t[0]
        if minlen_s is None or duration_s >= minlen_s:
            f = t_item['Freq']
            # Resample to specified sample rate
            n = int((t[-1] - t[0]) / framerate_s)
            new_t = np.linspace(t[0], t[-1], num=n)
            # Set to extrapolate, but should not be used.
            # Mainly set in case we want to round to the nearest
            # time step at some later date
            interpfn = interpolate.interp1d(t, f, kind='linear',
                                           fill_value='extrapolate')
            new_f = interpfn(new_t)
            freq_list.append(new_f)
            times.append(t[0])
        else:
            pruned += 1

    print("Retained %d of %d"%(len(freq_list), len(freq_list)+pruned))

    return freq_list, times


def non_ztransform(frequencies):
    """ztranform(frequencies) - Given a frequency list, z-transform data
    and compute modified z-transrom derivative as described in:
    Frasier, K. E., Henderson, E. E., Bassett, H. R., and Roch, M. A. (2016).
    "Automated identification of common subunits within delphinid vocalizations,"
    Mar. Mammal Sci. 32(3). 911-930.

    :param frequencies:
    :return:
    """
    # Transform frequencies to z-score
    # sigma = np.std(frequencies)
    # if np.isclose(sigma, 0):
    #     sigma = 1.0  # Flat whistle
    #
    # mu = np.mean(frequencies)
    # freq_z = (frequencies - mu) / sigma
    freq_z = frequencies

    # Compute first difference derivative and scale by standard deviation
    # We do not subtract the mean as sign is relevant for a derivative
    d = np.diff(frequencies)
    # sigma_d = np.std(d)
    # if np.isclose(sigma_d, 0):
    #     sigma_d = 1.0  # flat
    # dfreq_z = d / sigma_d
    dfreq_z = d

    # Create matrix of z-score frequencies and deriviatives
    # First frequency z-score is not returned so that we have
    # derivatives for all entries (could copy)
    result = np.transpose(np.vstack((freq_z[1:], dfreq_z)))

    return result


def ztransform(frequencies):
    """ztranform(frequencies) - Given a frequency list, z-transform data
    and compute modified z-transrom derivative as described in:
    Frasier, K. E., Henderson, E. E., Bassett, H. R., and Roch, M. A. (2016).
    "Automated identification of common subunits within delphinid vocalizations,"
    Mar. Mammal Sci. 32(3). 911-930.

    :param frequencies:
    :return:
    """

    # Transform frequencies to z-score
    sigma = np.std(frequencies)
    if np.isclose(sigma, 0):
        sigma = 1.0  # Flat whistle

    mu = np.mean(frequencies)
    freq_z = (frequencies - mu) / sigma

    # Compute first difference derivative and scale by standard deviation
    # We do not subtract the mean as sign is relevant for a derivative
    d = np.diff(frequencies)
    sigma_d = np.std(d)
    if np.isclose(sigma_d, 0):
        sigma_d = 1.0  # flat
    dfreq_z = d / sigma_d

    # Create matrix of z-score frequencies and deriviatives
    # First frequency z-score is not returned so that we have
    # derivatives for all entries (could copy)
    result = np.transpose(np.vstack((freq_z[1:], dfreq_z)))

    return result


def rocca(freq_contour, time_contour):
    '''
    ROCCA: feature extraction of whistle classification
    Oswald 2013
    :param frequencies:
    :return:
    '''
    rocca_fea = dict()
    rocca_fea['freq_beg'] = freq_contour[0]
    rocca_fea['freq_end'] = freq_contour[-1]
    rocca_fea['freq_min'] = freq_contour.min()
    rocca_fea['freq_max'] = freq_contour.max()
    rocca_fea['freq_range'] = rocca_fea['freq_max'] - rocca_fea['freq_min']
    rocca_fea['dur'] = time_contour[-1] - time_contour[0]
    rocca_fea['freq_mean'] = freq_contour.mean()
    rocca_fea['freq_median'] = np.median(freq_contour)
    rocca_fea['freq_std'] = freq_contour.std()
    rocca_fea['freq_spread'] = np.quantile(freq_contour, .75) - \
                               np.quantile(freq_contour, .25)
    ind_num = freq_contour.shape[0]
    rocca_fea['freq_quart'] = freq_contour[round(ind_num*.25)-1]
    rocca_fea['freq_half'] = freq_contour[round(ind_num*.50)-1]
    rocca_fea['freq_3quart'] = freq_contour[round(ind_num * .75)-1]
    rocca_fea['freq_center'] = (rocca_fea['freq_min'] + rocca_fea['freq_max']) / 2.
    rocca_fea['freq_bw'] = rocca_fea['freq_range'] / rocca_fea['freq_center']
    rocca_fea['freq_maxmin'] = freq_contour.max() / freq_contour.min()
    rocca_fea['freq_beg_end'] = rocca_fea['freq_beg'] / rocca_fea['freq_end']

    # sloop
    freq_contour_diff = np.diff(freq_contour)
    eps = np.finfo(float).eps
    try:
        rocca_fea['slope_beg'] = freq_contour_diff[0] + eps
    except:
        print()

    rocca_fea['slope_end'] = freq_contour_diff[-1] + eps
    rocca_fea['slope_min'] = freq_contour_diff.min() + eps
    rocca_fea['slope_max'] = freq_contour_diff.max() + eps
    rocca_fea['slope_range'] = rocca_fea['slope_max'] - rocca_fea['slope_min']
    rocca_fea['slope_mean'] = freq_contour_diff.mean()
    rocca_fea['slope_median'] = np.median(freq_contour_diff)
    rocca_fea['slope_std'] = freq_contour_diff.std()
    rocca_fea['slope_spread'] = np.quantile(freq_contour_diff, .75) - \
                                np.quantile(freq_contour_diff, .25)

    rocca_fea['slope_quart'] = freq_contour_diff[round(ind_num*.25)-1]
    rocca_fea['slope_half'] = freq_contour_diff[round(ind_num*.50)-1]
    rocca_fea['slope_3quart'] = freq_contour_diff[round(ind_num * .75)-1]
    rocca_fea['slope_center'] = (rocca_fea['slope_min'] + rocca_fea[
        'slope_max']) / 2. + eps
    rocca_fea['slope_bw'] = rocca_fea['slope_range'] / rocca_fea['slope_center'] + eps
    rocca_fea['slope_maxmin'] = rocca_fea['slope_max'] / rocca_fea['slope_min'] + eps
    rocca_fea['slope_beg_end'] = rocca_fea['slope_beg'] / rocca_fea['slope_end'] + eps

    # number of steps
    freq_contour_diff_abs = np.abs(freq_contour_diff)
    freq_contour_diff_abs_norm = np.abs((freq_contour_diff_abs - freq_contour_diff_abs.mean()))/(freq_contour_diff_abs.std()+eps)
    step = np.where(freq_contour_diff_abs_norm >= 2.0)[0]
    step = (step + 1.0) / freq_contour_diff_abs_norm.shape[0]
    rocca_fea['num_step'] = step.shape[0]
    if step.size == 0:
        rocca_fea['loc_step_mean'] = 0.
        rocca_fea['loc_step_std'] = 10.
    else:
        rocca_fea['loc_step_mean'] = step.mean()
        rocca_fea['loc_step_std'] = step.std() / step.mean()

    slope_pos_ind = np.where(freq_contour_diff >0)[0]
    rocca_fea['slope_pos_time_ratio'] = slope_pos_ind.shape[0] / freq_contour_diff.shape[0]
    rocca_fea['slope_pos_mag_ratio'] = freq_contour_diff[slope_pos_ind].sum() / (np.abs(freq_contour_diff).sum()+eps)

    # number of infections
    freq_contour_diff2 = np.diff(freq_contour_diff)
    freq_contour_diff2_norm = np.abs((freq_contour_diff2 - freq_contour_diff2.mean()))/(freq_contour_diff2.std()+eps)

    infection = np.where(freq_contour_diff2_norm >= 3.0)[0]
    infection = (infection+1.0) / freq_contour_diff2_norm.shape[0]
    rocca_fea['num_infection'] = infection.shape[0]
    if infection.size == 0:
        rocca_fea['loc_infection_mean'] = 0.
        rocca_fea['loc_infection_std'] = 10.
    else:
        rocca_fea['loc_infection_mean'] = infection.mean()
        rocca_fea['loc_infection_std'] = infection.std() / infection.mean()

    return rocca_fea
