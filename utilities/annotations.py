import os
from collections import defaultdict


def splitall(path):
    """splitall(path) - Split path into component parts
     from Ascher & Martelli's Python Cookbook
     O'Reilly Media, 2002.  ISBN: 0596001673
    """
    allparts = []
    while 1:
        parts = os.path.split(path)
        if parts[0] == path:  # sentinel for absolute paths
            allparts.insert(0, parts[0])
            break
        elif parts[1] == path: # sentinel for relative paths
            allparts.insert(0, parts[1])
            break
        else:
            path = parts[0]
            allparts.insert(0, parts[1])
    return allparts

def get_files(rootdir, extension_list = (".bin", ".ann")):
    """get_files(rootdir, extension_list)
    Retrieve annotation files separated by subdirectory.
    :param rootdir: Base directory to search
    :param extension_list: Children of root matching the extension
       will be added to the set of returned values.
    :return: Dictionary, keys are the top-level directory, usually
             the class (e.g. species).  Values are lists of files
             ending in an extension in the extension list.
    """

    corpus = defaultdict(list)

    # Find position of rootdirectory in path so that we can
    # identify the first child
    rootposn = len(splitall(rootdir)) - 1  # caution: splitall will have one
    # more element if the path ends with "/". E.g. /home/user and /home/user/
    # will have 3 and 4 elements, respectively.

    # Iterate over subdirectories and files in directory tree
    for directory, _subdirs, files in os.walk(rootdir):
        # Find directory name after root
        dirs = splitall(directory)
        if len(dirs) <= rootposn + 1:
            continue # In root directory, only process subdirectories
        key = dirs[rootposn+1]
        for f in files:
            if any([f.endswith(ext) for ext in extension_list]):
                # Ending matches extension list, add it with
                # top level directory as key
                corpus[key].append(os.path.join(directory, f))

    return corpus

def stratify_byday(corpus, features) :
    """stratify_byday(files)
    Stratify files by recording day
    :param files: List of files with timestamps in names
    :return: (groupN, membership)  groupN - Number of groups
        membership - Vector showing group membership
    """

    raise NotImplementedError("Not yet implemented") # todo

def stratify_byfile(corpus, features):
    """stratify_byfile(files)
    Stratify by file
    :param corpus: Corpus dictionary by class
       Each class has a list of associated files
    :param features: Feature dictionary by class
       Each class has lists of features, with features associated with
       a specific file in a list
    :param files: List of files
    :return: (groupN, membership)  groupN - Number of files
       membership - Vector showing membership [0, 1, 2, ... groupN-1]
    """

    # Notthing to do here, already arranged by file
    return
