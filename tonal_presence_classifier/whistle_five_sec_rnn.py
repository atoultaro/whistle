"""
Whistle presence classification
5-sec segments for training & testing
RNN
"""

import matplotlib.pyplot as plt
import numpy as np

from skimage.data import astronaut
from skimage.color import rgb2gray
from skimage.filters import sobel
from skimage.segmentation import felzenszwalb, slic, quickshift, watershed
from skimage.segmentation import mark_boundaries
from skimage.util import img_as_float
from skimage import io

import os
import glob
import warnings
import librosa
import librosa.display as display
import numpy as np

from keras.layers import Dense, Input, TimeDistributed, SimpleRNN, LSTM, GRU, Masking, Dropout
from keras.models import Sequential
from sklearn.model_selection import train_test_split
from keras.utils import to_categorical

from keras import metrics
from keras.callbacks import ModelCheckpoint, Callback, TensorBoard

from sklearn.metrics import confusion_matrix
from contextlib import redirect_stdout

import matplotlib
matplotlib.use('TkAgg')


def make_rnn_LSTM(data_shape, num_class):
    model_rnn = Sequential()
    model_rnn.add(LSTM(64, return_sequences=True, input_shape=data_shape)) # shape: time steps, features
    model_rnn.add(Dropout(0.5))
    model_rnn.add(LSTM(32, return_sequences=False))
    model_rnn.add(Dropout(0.5))
    model_rnn.add(Dense(num_class, activation='softmax'))
    print(model_rnn.summary())

    return model_rnn

def make_rnn_LSTM_easy(data_shape, num_class):
    model_rnn = Sequential()
    model_rnn.add(LSTM(32, return_sequences=True, input_shape=data_shape)) # shape: time steps, features
    model_rnn.add(Dense(16, activation='softmax'))
    print(model_rnn.summary())

    return model_rnn

class accuracy_history(Callback):
    """
    Callback function ro report classifier accuracy on-th-fly

    Args:
        keras.callbacks.Callback: keras callback
    """

    def on_train_begin(self, logs={}):
        self.acc = []
        self.val_acc = []

    def on_epoch_end(self, epoch, logs={}):
        self.acc.append(logs.get('acc'))
        self.val_acc.append(logs.get('val_acc'))

# img_path = r'/home/ys587/__Data/__whistle/__image_species_small/'
img_path = r'/home/ys587/__Data/__whistle/__image_species/'
log_dir = r'/home/ys587/__Data/__whistle/__log_dir'
species_id = {'bottlenose': 0, 'common': 1, 'spinner': 2, 'melonheaded': 3}
# input_shape: sample point (?), time (481), feature (144)

BATCH_SIZE = 256
EPOCHS = 500

fea_list = []
lab_list = []
num_dolphins = len(species_id)
for ss in species_id.keys():
    print(ss)
    img_list = glob.glob(os.path.join(img_path, ss,'*.png'))
    img_list.sort()

    for ii in img_list:
        fea_list.append((io.imread(ii).astype('float32')).T)
        lab_list.append(species_id[ss])

input_data = np.stack(fea_list, axis=0)

model_LSTM = make_rnn_LSTM([input_data.shape[1], input_data.shape[2]], num_dolphins)
# model_LSTM = make_rnn_LSTM_easy([input_data.shape[1], input_data.shape[2]], num_dolphins)

indices = np.arange(len(lab_list))
x_train, x_test, y_train, y_test, train_idx, test_idx = train_test_split(input_data, lab_list, indices, test_size=0.33, random_state=42)
y_train_onehot = to_categorical(y_train)
y_test_onehot = to_categorical(y_test)

model_name_format = 'epoch_{epoch:02d}_{val_acc:.4f}.hdf5'
check_path = os.path.join(log_dir, model_name_format)
checkpoint = ModelCheckpoint(check_path, monitor='val_acc', verbose=1, save_best_only=True, mode='max')
history = accuracy_history()
callbacks_list = [checkpoint, history, TensorBoard(log_dir=log_dir)]

# write out model
with open(os.path.join(log_dir, 'net_architecture.txt'), 'w') as f:
    with redirect_stdout(f):
        model_LSTM.summary()

model_LSTM.compile(loss='categorical_crossentropy', optimizer='Adam', metrics=['acc'])
CLASS_WEIGHT = {0: 2.5,
                1: 6,
                2: 10.,
                3: 1.}
history_log = model_LSTM.fit(x_train, y_train_onehot, validation_data=(x_test, y_test_onehot), batch_size=BATCH_SIZE,
               epochs=EPOCHS, verbose=1, callbacks=callbacks_list, class_weight=CLASS_WEIGHT)


# accuracy
loss, acc = model_LSTM.evaluate(x_test, y_test_onehot, batch_size=BATCH_SIZE)
print('acc: %.2f' % acc)
# model_LSTM.load_weights(os.path.join(log_dir, 'epoch_09_0.6823.hdf5'))
class_prob = model_LSTM.predict(x_test)  # predict_proba the same as predict
y_pred = np.argmax(class_prob, axis=1)
confu_mat = confusion_matrix(y_pred, y_test)
print(confu_mat)


####

if False:
    print('\nNumber of NARW calls in training set: ' +
          str(int(Y_train[:, 1].sum())))
    print('Number of NARW calls in testing set: ' +
          str(int(Y_test[:, 1].sum())))

    score = model.evaluate(X_test, Y_test, verbose=0)
    # print(model.metrics_names)
    print('\nTest loss:', score[0])
    print('Test F1 score:', score[1])

    class_prob = model.predict(X_test)  # predict_proba the same as predict
    print("\nAverage Precision Score: " +
          str(average_precision_score(Y_test, class_prob)))
    print("Area under the ROC curve: " +
          str(roc_auc_score(Y_test, class_prob)))

    # Y_pred = ((model.predict(X_test))[:,1]>0.5).astype(int)
    Y_pred = np.argmax(class_prob, axis=1)
    confu_mat = confusion_matrix(Y_pred, Y_test[:, 1].astype(int))

    print('\nConfuison matrix: ')
    print(confu_mat)
    print(classification_report(Y_test[:, 1].astype(int), Y_pred))

    with open(os.path.join(config.TRAIN_RESULT_PATH, 'ConfusionMatrix.txt'), 'w') as f2:
        with redirect_stdout(f2):
            print('\nTest loss:', score[0])
            print('Test F1 score:', score[1])
            print("Average Precision Score: " +
                  str(average_precision_score(Y_test, class_prob)))
            print("Area under the ROC curve: " +
                  str(roc_auc_score(Y_test, class_prob)))
            print('Confuison matrix: ')
            print(confu_mat)
            print(classification_report(Y_test[:, 1].astype(int), Y_pred))

    Y_test_label = Y_test[:, 1].astype(int)


