"""
Whistle feature extraction
"""
import matplotlib.pyplot as plt
import numpy as np

from skimage.data import astronaut
from skimage.color import rgb2gray
from skimage.filters import sobel
from skimage.segmentation import felzenszwalb, slic, quickshift, watershed
from skimage.segmentation import mark_boundaries
from skimage.util import img_as_float
from skimage import io

import os
import librosa
import librosa.display as display
import numpy as np

import matplotlib
matplotlib.use('TkAgg')


def power_law_calc(spectro_mat, nu1, nu2, gamma):
    dim_f, dim_t = spectro_mat.shape
    mu_k = [power_law_find_mu(spectro_mat[ff, :]) for ff in range(dim_f)]

    mat0 = spectro_mat ** gamma - np.array(mu_k).reshape(dim_f, 1) * np.ones(
        (1, dim_t))
    mat_a_denom = [(np.sum(mat0[:, tt] ** 2.)) ** .5 for tt in range(dim_t)]
    mat_a = mat0 / (np.ones((dim_f, 1)) * np.array(mat_a_denom).reshape(1, dim_t))
    mat_b_denom = [(np.sum(mat0[ff, :] ** 2.)) ** .5 for ff in range(dim_f)]
    mat_b = mat0 / (np.array(mat_b_denom).reshape(dim_f, 1) * np.ones((1, dim_t)))

    mat_a = mat_a * (mat_a > 0)  # set negative values into zero
    mat_b = mat_b * (mat_b > 0)
    power_law_mat = (mat_a**nu1)*(mat_b**nu2)
    # power_law_mat = (mat_a ** (2.0 * nu1)) * (mat_b ** (2.0 * nu2))
    # PowerLawTFunc = np.sum((mat_a**nu1)*(mat_b**nu2), axis=0)

    return power_law_mat


def power_law_find_mu(spectro_target):
    spec_sorted = np.sort(spectro_target)
    spec_half_len = int(np.floor(spec_sorted.shape[0]*.5))
    ind_j = np.argmin(spec_sorted[spec_half_len:spec_half_len*2] - spec_sorted[0:spec_half_len])
    mu = np.mean(spec_sorted[ind_j:ind_j+spec_half_len])

    return mu


N_FFT = 4096
# OFFSET = 334
OFFSET = 0.0
NU1 = 3.0
NU2 = 1.0
GAMMA = 1.0

WIN_STEP_SIZE = 5.0 # window size = step size, no overlap

species_id = {'bottlenose': 0, 'common': 1, 'spinner': 2, 'melonhead': 3}
species_name = {0: 'bottlenose', 1: 'common', 2: 'spinner', 3: 'melonhead'}

sound_file = r'/home/ys587/__Data/__whistle/5th_DCL_data_bottlenose/palmyra092007FS192-071011-193413.wav'  # bottlenose
# sound_file = r'/home/ys587/__Data/__whistle/5th_DCL_data_melon-headed/palmyra092007FS192-071004-024000.wav' # melon-head
# sound_file = r'/home/ys587/__Data/__whistle/5th_DCL_data_common/Qx-Dd-SCI0608-Ziph-060817-125009.wav' # common
# sound_file = r'/home/ys587/__Data/__whistle/5th_DCL_data_spinner/palmyra102006-061102-222000_4.wav' # spinner


# samples, samplerate = librosa.load(sound_file, offset=0, duration=30, sr=96000, mono=True)

if True:
    # demo of spectrogram, log-scale spectrogram & CQT spectrogram
    offset_curr = OFFSET
    fig, ax = plt.subplots(4, 1, figsize=(20, 10))

    sound_duration = librosa.get_duration(filename=sound_file, sr=96000)

    while offset_curr <= sound_duration - WIN_STEP_SIZE:
        print('offset: ' + str(offset_curr) )
        # sound_file = '/Users/ys587/__Data/Humpback/Total2007-2008.aif'
        samples, samplerate = librosa.load(sound_file, sr=96000, mono=True,
                                           offset=offset_curr,
                                           duration=WIN_STEP_SIZE)

        if False:
            spectro0 = np.abs(librosa.stft(samples, n_fft=N_FFT, hop_length=int(N_FFT/2))).astype('float64')

            # spectrogram original
            display.specshow(librosa.amplitude_to_db(spectro0, ref=np.max), y_axis='linear', x_axis='time', ax=ax[0],
                             sr=samplerate, hop_length=int(N_FFT/2))

            # spectrogram log-freq
            display.specshow(librosa.amplitude_to_db(spectro0, ref=np.max), y_axis='log', x_axis='time', ax=ax[1],
                             sr=samplerate, hop_length=int(N_FFT/2))

        # cqt
        spectro_cqt = np.abs(librosa.cqt(samples, sr=samplerate, hop_length=1000,
                                         n_bins=12*3*4, bins_per_octave=12*4, fmin=6000))
        spectro_cqt_db = librosa.amplitude_to_db(spectro_cqt, ref=np.max)
        display.specshow(spectro_cqt_db, y_axis='cqt_hz', x_axis='time', ax=ax[0],
                         bins_per_octave=12*4, hop_length=1000, sr=samplerate, fmin=6000)

        spectro_cqt_ud = np.flipud(spectro_cqt)
        spectro_cqt_pl = power_law_calc(spectro_cqt_ud, NU1, NU2, GAMMA)
        spectro_cqt_pl_db = librosa.amplitude_to_db(spectro_cqt_pl, ref=np.max)
        display.specshow(np.flipud(spectro_cqt_pl_db), y_axis='cqt_hz', x_axis='time', ax=ax[1],
                         bins_per_octave=12*4, hop_length=1000, sr=samplerate, fmin=6000)

        img = (spectro_cqt_pl - spectro_cqt_pl.min()) / (spectro_cqt_pl.max())

        # felzenszwalb #1
        segments_fz = felzenszwalb(img, scale=100, sigma=1.0, min_size=50)
        ax[2].imshow(mark_boundaries(img, segments_fz), aspect='auto')
        # ax[2].set_title("Felzenszwalbs's method")

        # felzenszwalb #2
        segments_fz_2 = felzenszwalb(img, scale=100, sigma=1.0, min_size=25)
        ax[3].imshow(mark_boundaries(img, segments_fz_2), aspect='auto')

        # find whistle
        time_span_sum = 0
        for ww in range(segments_fz.max()):
            ff, tt = np.where(segments_fz == ww+1)
            freq_low = ff.min()
            freq_high = ff.max()
            time_start = tt.min()
            time_stop = tt.max()

            freq_span = freq_high - freq_low + 1
            time_span = time_stop - time_start + 1
            if False:
                if time_span >= int(0.5*96):
                    print('The whistle length is: %.3f sec' % (float(time_span)*1000./samplerate))
                    print('Frequency boundaries: '+str(freq_low)+' and '+str(freq_high)+'; Time boundaries: ' +
                          str(time_start) + ' and '+str(time_stop)+'\n')
            if time_span >= int(0.1 * 96000./1000.):
                time_span_sum += time_span

        ax[3].set_title('time span sum: '+str(time_span_sum))
        # print('time span sum is: '+str(time_span_sum))

        plt.tight_layout()
        plt.pause(0.05)


        offset_curr += WIN_STEP_SIZE
    plt.show()
