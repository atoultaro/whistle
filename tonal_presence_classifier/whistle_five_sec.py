"""
Extract features and save them in grayscale png files
CQT constant-Q transform
GPL denoising
dB scale and normalization
"""
import matplotlib.pyplot as plt
import numpy as np

from skimage.data import astronaut
from skimage.color import rgb2gray
from skimage.filters import sobel
from skimage.segmentation import felzenszwalb, slic, quickshift, watershed
from skimage.segmentation import mark_boundaries
from skimage.util import img_as_float
from skimage import io

import os
import glob
import warnings
import librosa
import librosa.display as display
import numpy as np

import matplotlib
matplotlib.use('TkAgg')


def power_law_calc(spectro_mat, nu1, nu2, gamma):
    dim_f, dim_t = spectro_mat.shape
    mu_k = [power_law_find_mu(spectro_mat[ff, :]) for ff in range(dim_f)]

    mat0 = spectro_mat ** gamma - np.array(mu_k).reshape(dim_f, 1) * np.ones(
        (1, dim_t))
    mat_a_denom = [(np.sum(mat0[:, tt] ** 2.)) ** .5 for tt in range(dim_t)]
    mat_a = mat0 / (np.ones((dim_f, 1)) * np.array(mat_a_denom).reshape(1, dim_t))
    mat_b_denom = [(np.sum(mat0[ff, :] ** 2.)) ** .5 for ff in range(dim_f)]
    mat_b = mat0 / (np.array(mat_b_denom).reshape(dim_f, 1) * np.ones((1, dim_t)))

    mat_a = mat_a * (mat_a > 0)  # set negative values into zero
    mat_b = mat_b * (mat_b > 0)
    power_law_mat = (mat_a**nu1)*(mat_b**nu2)
    # power_law_mat = (mat_a ** (2.0 * nu1)) * (mat_b ** (2.0 * nu2))
    # PowerLawTFunc = np.sum((mat_a**nu1)*(mat_b**nu2), axis=0)

    return power_law_mat


def power_law_find_mu(spectro_target):
    spec_sorted = np.sort(spectro_target)
    spec_half_len = int(np.floor(spec_sorted.shape[0]*.5))
    ind_j = np.argmin(spec_sorted[spec_half_len:spec_half_len*2] - spec_sorted[0:spec_half_len])
    mu = np.mean(spec_sorted[ind_j:ind_j+spec_half_len])

    return mu


FLAG_VISUAL = True

N_FFT = 4096
# OFFSET = 334
OFFSET = 0.0
NU1 = 3.0
NU2 = 1.0
GAMMA = 1.0

WIN_STEP_SIZE = 5.0 # window size = step size, no overlap
SAMPLERATE_TARGET = 96000
HOP_CQT = 1000

species_id = {'bottlenose': 0, 'common': 1, 'spinner': 2, 'melonheaded': 3}
species_name = {0: 'bottlenose', 1: 'common', 2: 'spinner', 3: 'melonheaded'}

sound_path = r'/home/ys587/__Data/__whistle/__sound_species/'
img_output_path = r'/home/ys587/__Data/__whistle/__image_species_new/'
try:
    os.mkdir(img_output_path)
except FileExistsError:
    os.rename(img_output_path, img_output_path+'_prev')
    os.mkdir(img_output_path)


if FLAG_VISUAL:
    fig, ax = plt.subplots(3, 1, figsize=(20, 10))

for kk in species_id.keys():
    os.mkdir(os.path.join(img_output_path, kk))
    print(os.path.join(sound_path, kk))
    all_wav_files = glob.glob(os.path.join(sound_path, kk, '*.wav'))
    image_count_species = 0

    # all_wav_files = all_wav_files[:2] # DEBUG

    for ff in all_wav_files:
        print(ff)
        offset_curr = 0.0
        sound_duration = librosa.get_duration(filename=ff, sr=SAMPLERATE_TARGET)

        while offset_curr <= sound_duration - WIN_STEP_SIZE:
        # while offset_curr <= 3*WIN_STEP_SIZE:
            print('offset: ' + str(offset_curr))
            samples, samplerate = librosa.load(ff, sr=SAMPLERATE_TARGET, mono=True,
                                               offset=offset_curr,
                                               duration=WIN_STEP_SIZE)
            # cqt
            spectro_cqt = np.abs(librosa.cqt(samples, sr=samplerate, hop_length=HOP_CQT,
                                             n_bins=12 * 3 * 4, bins_per_octave=12 * 4, fmin=6000))

            spectro_cqt_ud = np.flipud(spectro_cqt)
            spectro_cqt_pl = power_law_calc(spectro_cqt_ud, NU1, NU2, GAMMA)

            img = (spectro_cqt_pl - spectro_cqt_pl.min()) / (spectro_cqt_pl.max() - spectro_cqt_pl.min())
            img2 = (spectro_cqt_ud - spectro_cqt_ud.min()) / (spectro_cqt_ud.max() - spectro_cqt_pl.min())

            # felzenszwalb #1
            segments_fz = felzenszwalb(img, scale=100, sigma=1.0, min_size=50)

            # find whistle
            time_span_sum = 0.0
            time_span_list = []
            for ww in range(segments_fz.max()):
                event_ff, event_tt = np.where(segments_fz == ww + 1)
                freq_low = event_ff.min()
                freq_high = event_ff.max()
                time_start = event_tt.min()
                time_stop = event_tt.max()

                freq_span = freq_high - freq_low + 1
                time_span = time_stop - time_start + 1
                if time_span >= int(0.2 * SAMPLERATE_TARGET/HOP_CQT):
                    time_span_sum += float(time_span)/(SAMPLERATE_TARGET/HOP_CQT)
                    time_span_list.append(float(time_span)/(SAMPLERATE_TARGET/HOP_CQT))

            if time_span_sum >= 1.0:
                img_filename = os.path.join(img_output_path, kk, kk+'_'+str(image_count_species)+'_contour.png')
                # img_filename2 = os.path.join(img_output_path, kk, kk + '_' + str(image_count_species) + '_spectro.png')
                print(img_filename)
                img_grayscale = (np.flipud(img)*256).astype("uint8")
                # img_spectro = (np.flipud(img2) * 256).astype("uint8")

                with warnings.catch_warnings():
                    warnings.simplefilter("ignore")
                    io.imsave(img_filename, img_grayscale)
                    # io.imsave(img_filename2, img_spectro)
                image_count_species += 1

            if FLAG_VISUAL:
                spectro_cqt_db = librosa.amplitude_to_db(spectro_cqt, ref=np.max)
                spectro_cqt_pl_db = librosa.amplitude_to_db(spectro_cqt_pl, ref=np.max)
                display.specshow(spectro_cqt_db, y_axis='cqt_hz', x_axis='time', ax=ax[0],
                                 bins_per_octave=12 * 4, hop_length=1000, sr=samplerate, fmin=6000)
                display.specshow(np.flipud(spectro_cqt_pl_db), y_axis='cqt_hz', x_axis='time', ax=ax[1],
                                 bins_per_octave=12 * 4, hop_length=1000, sr=samplerate, fmin=6000)
                ax[2].imshow(mark_boundaries(img, segments_fz), aspect='auto')
                ax[2].set_title('time span sum: ' + str(time_span_sum))
                plt.tight_layout()
                plt.pause(0.05)

            offset_curr += WIN_STEP_SIZE
        # plt.show()






