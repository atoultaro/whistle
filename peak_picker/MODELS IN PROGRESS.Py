#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 21 20:09:52 2018

@author: kpalmer
"""

# make sure aloways pred




import numpy as np
import matplotlib.pyplot as plt
import sys
import os


# import the dsp classes
# sys.path.append("/home/kpalmer/AnacondaProjects/dsp")
# sys.path.append('/home/kpalmer/AnacondaProjects/peak-picker-deep-net')

soundfile_avail = True
_default_reader = "SoundFile"

# Build the model 
from keras.models import Sequential  
from keras.layers.core import Dense, Activation  
from keras.layers.recurrent import LSTM
from keras.layers import BatchNormalization
from keras.regularizers import l2
import keras as K
import keras.backend as Kb
import tensorflow as tf

# Import features/classes 
from TonalClass_v1 import get_corpus, select_matching_files, get_features, \
get_feature_labels
from Batch_Generator_pos_examples_only import batch_generator
from buildmodels import build_model
from Data_augmentation_ import batch_dataAug

# Check keras/tensor flow can see the gosg-dang GPU
from tensorflow.python.client import device_lib
print(device_lib.list_local_devices())

###############################################################################
# Set directory of bin/ton files and associated wav files
# Top level directory, divided by species
data_dir = '/home/kpalmer/AnacondaProjects/data/dclmmpa2011/devel_data/'
data_by_species = dict()
for species in ['bottlenose', 'common', 'melon-headed', 'spinner']:
    data_by_species[species] = os.path.join(data_dir, species)
file_dirs = list(data_by_species.values())


data_dir = '/cache/quick_ssd/data/dclmmpa2011/devel_data/'
data_by_species = dict()
for species in ['SFrontalis', 'StenellaLongirostrisLongirostris', 
                'Tursiops truncatus-Palmyra',]:
    data_by_species[species] = os.path.join(data_dir, species)
file_dirs_valid = list(data_by_species.values())




###############################################################################
# Custom functions ###
##############################################################################
# Custom Loss
def magnify_ones_mean_squared_error(y_true, y_pred):
    'returns custom loss function mse times the binary mask to \
        place more emphasis on true positives'
    cross_e = Kb.abs(Kb.square(y_pred - y_true) *(y_true*Kb.constant(400)))
    return cross_e

#Custom metrics- from previous keras versions
def precision(y_true, y_pred):
    """Precision metric.

    Only computes a batch-wise average of precision.

    Computes the precision, a metric for multi-label classification of
    how many selected items are relevant.
    """
    true_positives = Kb.sum(Kb.round(Kb.clip(y_true * y_pred, 0, 1)))
    predicted_positives = Kb.sum(Kb.round(Kb.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + Kb.epsilon())
    return precision


def recall(y_true, y_pred):
    """Recall metric.

    Only computes a batch-wise average of recall.

    Computes the recall, a metric for multi-label classification of
    how many relevant items are selected.
    """
    true_positives = Kb.sum(Kb.round(Kb.clip(y_true * y_pred, 0, 1)))
    possible_positives = Kb.sum(Kb.round(Kb.clip(y_true, 0, 1)))
    recall = true_positives / (possible_positives + Kb.epsilon())
    return recall


###############################################################################
# Set up generators, frequency limits, class weights, and neurons 
##############################################################################

# Just try running on 4 frequency bins
df = 125 # delta frequncy assuming fs=192khz and len_ms =8
f1 = 8000 - df
f2 = 8000 + df

class_weight = {0 : 1., 1: 320.}

# bug - returning one more row than aniticpated #
generator_all = batch_generator(file_dirs=file_dirs, chunk_dur=2, batch_size=20,
                            adv_ms =3, len_ms = 8, freqs=(f1, f2), 
                            Only_Positive = False)

generator_pos = batch_generator(file_dirs=file_dirs, chunk_dur=2, batch_size=20,
                            adv_ms =3, len_ms = 8, freqs=(f1, f2), 
                            Only_Positive = True)

valid_generator =batch_generator(file_dirs_valid, chunk_dur=3, batch_size=20,
                                 adv_ms =3, len_ms = 8, freqs=(f1, f2), 
                                 Only_Positive = False)

n_neurons = [generator_all.get_example_dim()[2], 1]


###############################################################################
# Model that does NOT includes class weights and custom Loss fx
##############################################################################

with tf.device('/GPU:0'):

    model1 = Sequential()
    model1.add(LSTM(n_neurons[0],
                    batch_input_shape=generator_all.get_example_dim(),
                    return_sequences=True, 
                    stateful=False,
                    kernel_regularizer=l2(0.01)))
    model1.add(LSTM(1, return_sequences=True, stateful=False,
                    kernel_regularizer=l2(0.01)))
    #model1.add(LSTM(int(1), return_sequences=True, stateful=False))    
    #model1.add(Dense(1,     ))
#    model1.add(Dense(output_dim = 1,init ='uniform',activation = 'relu',
#                     input_dim=generator.get_example_dim()))
    model1.compile(loss=magnify_ones_mean_squared_error, optimizer='adam',
                   metrics=['accuracy', precision, recall])
    #Run 
    model1.fit_generator(generator_all, 
                         steps_per_epoch= generator_all.get_steps_per_epoch(),
                          epochs=1,  verbose=1,  
                          validation_data=generator_all,
                          validation_steps=generator_all.get_steps_per_epoch())
    
# serialize model to JSON
model1_json = model1.to_json()
with open("model1.json", "w") as json_file:
    json_file.write(model1_json)
# serialize weights to HDF5
model1.save_weights("model1.h5")
print("Saved model to disk")

###############################################################################
# Model that includes class weights
##############################################################################



with tf.device('/GPU:0'):

    model1 = Sequential()
    model1.add(LSTM(n_neurons[0],
                    batch_input_shape=generator_all.get_example_dim(),
                    return_sequences=True, 
                    stateful=False,
                    kernel_regularizer=l2(0.01)))
    model1.add(LSTM(1, return_sequences=True, stateful=False,
                    kernel_regularizer=l2(0.01)))
    #model1.add(LSTM(int(1), return_sequences=True, stateful=False))    
    #model1.add(Dense(1,     ))
#    model1.add(Dense(output_dim = 1,init ='uniform',activation = 'relu',
#                     input_dim=generator.get_example_dim()))
    model1.compile(loss='mse', optimizer='adam',
                   metrics=['accuracy', precision, recall])
    #Run 
    model1.fit_generator(generator_all, class_weight = class_weight,
                         steps_per_epoch= generator_all.get_steps_per_epoch(),
                          epochs=1,  verbose=1,  
                          validation_data=valid_generator,
                          validation_steps=valid_generator.get_steps_per_epoch())




###############################################################################
# partially connected layer after LSTM layer
##############################################################################

df = 125 # delta frequncy assuming fs=192khz and len_ms =8
f1 = 8000 - (4*df)
f2 = 8000 + (4*df)

generator_pos = batch_generator(file_dirs=file_dirs, chunk_dur=2, batch_size=20,
                            adv_ms =3, len_ms = 8, freqs=(f1, f2), 
                            Only_Positive = True)

_, n_steps, n_features = generator_pos.get_example_dim()

with tf.device('/GPU:0'):

    model1 = Sequential()
    model1.add(LSTM(n_neurons[0],
                    batch_input_shape=generator_all.get_example_dim(),
                    return_sequences=True, 
                    stateful=False,
                    kernel_regularizer=l2(0.01)))
    # output shape n_frequ bins in generator 
    model1.add(LocallyConnected1D(filters = 64, 
                                 kernel_size= 3,
                                 input_shape=(10, 32)))
    
    
    
    
    
    
    
    #model1.add(LSTM(int(1), return_sequences=True, stateful=False))    
    #model1.add(Dense(1,     ))
#    model1.add(Dense(output_dim = 1,init ='uniform',activation = 'relu',
#                     input_dim=generator.get_example_dim()))
    model1.compile(loss='mse', optimizer='adam',
                   metrics=['accuracy', precision, recall])
    #Run 
    model1.fit_generator(generator_all, class_weight = class_weight,
                         steps_per_epoch= generator_all.get_steps_per_epoch(),
                          epochs=1,  verbose=1,  
                          validation_data=valid_generator,
                          validation_steps=valid_generator.get_steps_per_epoch())
















# try with data augmentor 
    # Use the CPU only because SON OF A MONKEY'S UNCLE!!!

data_aug_generator= batch_dataAug(file_dirs, chunk_dur=2, batch_size=10,
                                  adv_ms =2, len_ms = 8,  freqs=(f1, f2))
valid_generator= batch_dataAug(file_dirs, chunk_dur=2, batch_size=10,
                               adv_ms =2, len_ms = 8,  freqs=(f1, f2))
 
n_neurons = [data_aug_generator.get_example_dim()[2], 1]

   
with tf.device('/cpu:0'):

    model1 = Sequential()
    model1.add(LSTM(n_neurons[0],
                    batch_input_shape=data_aug_generator.get_example_dim(),
                    return_sequences=True, 
                    stateful=False,
                    kernel_regularizer=l2(0.01)))
    model1.add(LSTM(1, return_sequences=True, stateful=False,
                    kernel_regularizer=l2(0.01)))

    model1.compile(loss=magnify_ones_mean_squared_error, optimizer='adam',
                   metrics=['accuracy'])
    #Run 
    model1.fit_generator(data_aug_generator, 
                         steps_per_epoch= data_aug_generator.get_steps_per_epoch(),
                          epochs=1,  verbose=1,  
                          validation_data=valid_generator,
                          validation_steps=valid_generator.get_steps_per_epoch())





#
#######################################################################
#
## Model based on Naithani, Gaurav, et al. 
##"Low-latency sound source separation using deep neural networks." 
##Signal and Information Processing (GlobalSIP),
## 2016 IEEE Global Conference on. IEEE, 2016.
######################################################################
#
#
#
#generator = batch_generator(testing_file_dirs, chunk_dur=3, batch_size=20, adv_ms =2,
#                 len_ms = 8, freqs=(4000, 15000))
#
#valid_generator =batch_generator(testing_file_dirs, chunk_dur=3, batch_size=20, adv_ms =2,
#                 len_ms = 8, freqs=(4000, 15000))
#
#
#
## account for unbalanced classes 
#n_neurons = [generator.get_example_dim()[2]]
#model1 = Sequential()
#model1.add(LSTM(n_neurons[0],
#                batch_input_shape=generator.get_example_dim(),
#                return_sequences=True, 
#                stateful=False))
#
## Output Layer
#model1.add(Dense(n_neurons[0], kernel_initializer='normal', activation='sigmoid'))
#
#
##Compile
#model1.compile(loss=magnify_ones_mean_squared_error, optimizer='adam',
#               metrics=['accuracy'])
#
##Run 
#model1.fit_generator(generator, steps_per_epoch= generator.get_steps_per_epoch(),
#                      epochs=1,  verbose=1)
#
#
#import scipy as sp
#(aa, bb) = next(valid_generator)
#result = model1.predict(bb, batch_size=20, verbose=0)
#plt.pcolormesh(bb[2,:,:])
#plt.pcolormesh(result[2,:,:])
#
#
#
#
###############################################################################
#
## Frequency restricted LSTM'
#
## frequency range of interest
#f=(4000,12000)
#f_width = 500 # filter bank width (hz)
#f_advance = 100 # filter bank advance (Hz)
#
#f0 = np.arange([f[0], f[1]-500], 20)
#f1 = f0+500
#
#
#generator_n = batch_generator(file_dirs, chunk_dur=3, batch_size=20, adv_ms =2,
#                 len_ms = 8, freqs=(f1[0], f2[0]]))
#
#
#
#
#
#
#f_neurons=[generator_n.get_example_dim()[2]]
#
#
#
#model_args =[(LSTM, [20], {'activation':'relu', 'input_dim': M}),
#     (LSTM, [20], {'activation':'relu', 'input_dim':20}),
#     (LSTM, [N], {'activation':'softmax', 'input_dim':20})]
#
#
#
#F_lstm04_06_khz =    build_model(]
#    
#F_lstm05_08_khz = Sequential()
#F_lstm06_10_khz = Sequential()
#F_lstm07_12_khz = Sequential()
#F_lstm08_14_khz = Sequential()
#F_lstm09_16_khz = Sequential()
#F_lstm10_18_khz = Sequential()
#F_lstm11_20_khz = Sequential()
#F_lstm12_22_khz = Sequential()
#F_lstm12_22_khz = Sequential()
#F_lstm12_22_khz = Sequential()
#F_lstm12_22_khz = Sequential()
#F_lstm12_22_khz = Sequential()
#F_lstm12_22_khz = Sequential()
#
#
#
#
#
#
#
#
#
#
#
#
#
#




#
#
#
#
#############################################################################
## Smaller model with sigmoid output
#
#generator = batch_generator(file_dirs, chunk_dur=2, batch_size=30, adv_ms =2,
#                 len_ms = 8)
#
#n_neurons = [generator.get_example_dim()[2]]
#
#model3  = Sequential()
#model3.add(LSTM(int(n_neurons[0]/2),
#                batch_input_shape=generator.get_example_dim(),
#                return_sequences=True, 
#                stateful=False))
#model3.add(LSTM(int(n_neurons[0]/3), return_sequences=True, stateful=False,
#                    kernel_regularizer=l2(0.01)))
#model3.add(Dense(n_neurons[0], kernel_initializer='normal', activation='sigmoid'))
#model3.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
#
## Look at the fit data
#model3.fit_generator(generator, steps_per_epoch= 1,
#                      epochs=400,  verbose=1)
#
#resul3 = model3.predict(bb, batch_size=2, verbose=0)
#
#plt.pcolormesh(resul3[1,:,:])
#plt.pcolormesh(aa[1,:,:])
#
#
#######################################################################
#
## Model based on Naithani, Gaurav, et al. 
##"Low-latency sound source separation using deep neural networks." 
##Signal and Information Processing (GlobalSIP),
## 2016 IEEE Global Conference on. IEEE, 2016.
######################################################################
#
#earlyStopping=keras.callbacks.EarlyStopping(monitor='val_loss', patience=0, verbose=0, mode='auto')
#
#model4  = Sequential()
#model3  = Sequential()
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#def create_smaller():
#	# create model
#	model = Sequential()
#	model.add(Dense(30, input_dim=60, kernel_initializer='normal', activation='relu'))
#	model.add(Dense(1, kernel_initializer='normal', activation='sigmoid'))
#	# Compile model
#	model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
#	return model
#
#
#
#
#estimators = []
#estimators.append(('standardize', StandardScaler()))
#estimators.append(('mlp', KerasClassifier(build_fn=create_smaller, epochs=100, batch_size=5, verbose=0)))
#pipeline = Pipeline(estimators)
#
#
#
#
#
#
#













