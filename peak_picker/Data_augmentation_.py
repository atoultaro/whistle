#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 24 13:18:58 2018

@author: kpalmer
"""

'''
Class for data augmentation using silbido whistle files and recordings

'''


from TonalClass_v1 import get_corpus, select_matching_files, MakeWhistleDF,\
 get_complete_frames, MakeBinaryMask_chunk, nearest_to

import numpy as np
import os
from FrameStream import AudioFrames
from DFTStream import DFTStream
import random 


################################################### 
# Noted issues - no spectrogram normalisation applied yet
##################################################

class batch_dataAug:
    def __init__(self, file_dirs, chunk_dur=10, batch_size=10, adv_ms =2,
                 len_ms = 8, freqs=None):
        
        ''' Class for generating data from tonal files and whitlse files
        Returns:
            target_batch
    
        '''
        # initial starting point
        self.current = 0
        self.file_dirs = file_dirs
        self.chunk_dur = chunk_dur
        self.batch_size = batch_size
        self.chunk_id, self.file_name, self.bin_name = self.generate_chunk_ids()
        self.N= len(self.chunk_id)
        self.example_ids = np.arange(len(self.chunk_id))
        self.adv_ms = adv_ms
        self.len_ms = len_ms
        self._epoch = 0    # current number of completed epochs
        self.iter_id =0 # counter for n in n in bach
        self.freqs = freqs
        framestream= AudioFrames(self.file_name[0], self.adv_ms, self.len_ms)
        self.ncol = int(self.len_ms*framestream.Fs/2000) 

            
        # declare example [spectrogram sections] dimensions
        framestream= AudioFrames(self.file_name[0], self.adv_ms, self.len_ms)
        self.nrow = get_complete_frames(framestream, self.adv_ms, self.len_ms, 
                                     chunk_dur = self.chunk_dur, Offset = 0)
        # frequency choppint
        if self.freqs is not None:
            self.f1 = freqs[0]
            self.f2= freqs[1] 
            _,idx_1 = nearest_to(DFTStream(framestream).bins_Hz, self.f1)
            _,idx_2 = nearest_to(DFTStream(framestream).bins_Hz, self.f2)
            self.ncol =idx_2+1-idx_1
            self.bins_hz = DFTStream(framestream).bins_Hz[idx_1:idx_2]
        else:
            self.ncol = int(self.len_ms*framestream.Fs/2000) #T*fs/2
            # Declare frequency bins in DFT  
            self.bins_hz =  DFTStream(framestream).bins_Hz        
        
        
    def __iter__(self):
        return self

    def __next__(self):
            "Return next minibatch (targets, features)"
            
            # Preallocate batch size
            target_batch= np.zeros([self.batch_size, self.nrow, self.ncol])
            feature_batch= np.zeros([self.batch_size, self.nrow, self.ncol])
            
            # Example indexes 
            batch_ids =self.example_ids[0:self.batch_size]


            # Create the batch of target and feature data from randomly
            # selected chunks
            for ii in range(self.batch_size):
    
                idx = batch_ids[ii]
                target_batch[ii,:,:], feature_batch[ii,:,:] = self.get_features_and_targets(idx)

                
                
                
                
            print(['Generating Sample ' + str() + 
                   ' of '+ str(self.batch_size) + 
                   ' and N ' + str(self.current) +
                   ' of ' + str(self.N)])
                   #' of ' + str(int(np.ceil(self.N  / self.batch_size)))])
                
            # advance the counter            
            self.iter_id+=1
    
            # advance the counter for each epoch
            self.current += self.batch_size
                
            # Return the batch! 
            return(target_batch, feature_batch)
            print('Returned ' + str(self.batch_size) + 'samples')
        


    def get_features_and_targets(self, idx):
        ''' Create features and target data for a chunk of sound and the
        associated whistle (bin or ton) file'''
        # load the sound file 
        framestream = AudioFrames(self.file_name[idx], self.adv_ms, self.len_ms)
        
        # advance the object iterator to the sample start 
        sample_start = framestream.Fs * self.chunk_id[idx] * self.chunk_dur
        framestream.seek_sample(sample_start)
        dftstream = DFTStream(framestream)
        
        # maximum number of frames
        max_frames = get_complete_frames(framestream, self.adv_ms, self.len_ms, 
                                         chunk_dur = self.chunk_dur, Offset = 0)
        
        ##########################################################3
        # features #
        ###########################################################
                
        # get the spectra
        spectra = []
        ii = 0
        for s in dftstream:
            # s is a tuple (spectrum, time offset s, timestamp)
            spectra.append(s[0])
            ii +=1
            # break once chunk duration has been reached
            if  ii >= max_frames:
                break
               
           # Check if frequency limits and clip
        if self.freqs is not None:
            f1 = self.freqs[0]
            f2= self.freqs[1]
            _,idx_1 = nearest_to(dftstream.bins_Hz, f1)
            _,idx_2 = nearest_to(dftstream.bins_Hz, f2)
            
            for jj in range(len(spectra)):
                spectra[jj] = spectra[jj][idx_1:idx_2+1]  
                
        # check if maximum number of frames equal to chunk size 0
        SampleCount = framestream.soundfileinfo.frames
        FrameShift = framestream.get_frameadv_samples()
        FrameLength = framestream.get_framelen_samples()
        FrameLastComplete = int(np.floor((SampleCount  
                                      - FrameLength + FrameShift) / FrameShift))

        # Tack on zeros 
        if FrameLastComplete < max_frames:
            dummy = spectra[0]
            dummy= np.subtract(dummy, dummy)
            n_dft_to_add = FrameLastComplete - max_frames
        
            for ii in range(n_dft_to_add):
                spectra.append(dummy)
                
        # Convert to an array
        spectra = np.asarray(spectra)
        framestream.fileobj.close()
        
    

        ##########################################################3
        # Labels/Targets #
        ###########################################################

        # Get whistle dataframe
        whistle_df = MakeWhistleDF(self.bin_name[idx])[0]

        #sampel start 
        start_s = sample_start/framestream.Fs
        stop_s = start_s + self.chunk_dur

        # binary mask to list to match format of features
        examples_labels = MakeBinaryMask_chunk(self.bin_name[idx], whistle_df, 
                                               framestream, 
                                               start_s = start_s,
                                               stop_s = stop_s,
                                               adv_ms = self.adv_ms,
                                               len_ms=self.len_ms,
                                               OnlyComplete= True,
                                               chunk_dur =self.chunk_dur)
        

        # pull a random binary file
        sim_idx =random.randint(0,len(self.bin_name)-1)
        bin_name_sim = self.bin_name[sim_idx]
        
        
        # check size, pull another if too small- arbritraty at the moment
        file_size = os.stat(bin_name_sim).st_size
        while file_size < 1000:
            file_size = os.stat(bin_name_sim).st_size
            bin_name_sim = random.choice(self.bin_name)
        
        
        whistle_df_sim = MakeWhistleDF(bin_name_sim)[0]
        
        # Pick a random start time, offset, and frequency shift
        start_time = random.choice(whistle_df_sim['Start_s'])
        offset = random.randint(-10,10)/100
        framestream = AudioFrames(self.file_name[sim_idx], self.adv_ms, self.len_ms)
        
        new_examples_labels = MakeBinaryMask_chunk(bin_name_sim, 
                                                   whistle_df_sim, 
                                                   framestream, 
                                                   start_s = offset + start_time,
                                                   stop_s = offset + start_time +self.chunk_dur,
                                                   adv_ms = self.adv_ms,
                                                   len_ms=self.len_ms,
                                                   OnlyComplete= True,
                                                   chunk_dur =self.chunk_dur)        
        # Close the streamer
        framestream.fileobj.close()      
        
        # Add to the binary mask
        examples_labels = np.clip(np.add(examples_labels, new_examples_labels), 0,1)
        
        # Check if frequency limits and clip
        if self.freqs is not None:
            examples_labels=examples_labels[:, idx_1:idx_2+1]
        
        # Yield rather than return for better memory performance
        yield examples_labels
        yield spectra
        
        
        
        
        
        
    def generate_chunk_ids(self):
        '''
        Generate three lists containing needed information to collect features
        and targets- could also be a dataframe in the future
        ' file names- list of filenames with associated whistle files
        ' binary file names- list of binary files with associated wav files
        ' chunk id''s for all training data provided in file_dirs'''
        file_name=[]
        bin_name =[]
        chunk_id =[]
        
        for ii in range(len(self.file_dirs)):
    
            # Get corpus of files 
            fnames_wavs = get_corpus(self.file_dirs[ii], filetype=".wav")  
            
            # Only wav files with whistle or tonal files
            fnames_wavs, fnames_whistles  = select_matching_files(fnames_wavs)
            
            # create lists for file name, chunk id [used for start time] and
            # binary file name
            for jj in range(len(fnames_wavs)):
                n_chunks = int(AudioFrames(fnames_wavs[jj], 2,8).soundfileinfo.duration/self.chunk_dur)
                chunk_id = chunk_id + np.arange(0, n_chunks).tolist()
                file_name =file_name+ [fnames_wavs[jj]] *n_chunks
                bin_name = bin_name + [fnames_whistles[jj]] *n_chunks
            
        return chunk_id, file_name, bin_name
    
    
    def get_N_examples(self):
        examplesN = len(self.chunk_id)  
        return examplesN
    def get_example_dim(self):
        return int(self.batch_size), int(self.nrow), int(self.ncol)
    def get_freq_bins(self):
        return self.bins_hz
    def get_steps_per_epoch(self):
        ' returns the number of batches per epoc for the specified parameters \
         batch_size, adv_ms, and len_ms'
        steps_per_epoch = int(np.ceil(self.N  / self.batch_size))  
        return steps_per_epoch
        
    

        
#############################################################################
## Debugging  #
#
#dir_name_spinner = '/home/kpalmer/AnacondaProjects/data/dclmmpa2011/devel_data/spinner'
#
#file_dirs=[dir_name_spinner]  
#df = 125 # delta frequncy assuming fs=192khz and len_ms =8
#f1 = 5000
#f2 = 5000 +(3 * df)
#
#data_aug = batch_dataAug(file_dirs, chunk_dur=3, batch_size=3, adv_ms =2,
#                 len_ms = 8, freqs=(f1, f2)) 
#
#[aa, bb] = next(data_aug)


        