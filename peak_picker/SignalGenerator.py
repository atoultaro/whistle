#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 13 12:03:56 2018

@author: kpalmer
"""

# Class for making time domain whistles from the whistle binary files

# TO BE DONE #
# 1) Output is on the 0-1 scale NOT what would be expected from a hydrophone



import os, sys

sys.path.append("/home/kpalmer/AnacondaProjects/dsp")
sys.path.append("/home/kpalmer/AnacondaProjects/dsp/dsp")
sys.path.append('/home/kpalmer/AnacondaProjects/silbido/src/Python/src/Tonals')
sys.path.append('/home/kpalmer/AnacondaProjects/peak-picker-deep-net')

from FrameStream import AudioFrames
from TonalClass_v1 import MakeWhistleDF, MakeBinaryMask_chunk, tonal, whistle_contour
from scipy.interpolate import interp1d, UnivariateSpline
import numpy as np
import soundfile as sf



class WhistleGeneratorTimeDomain():
    """
    class for simulating whistles in the time domain
    Inputs:
        fs = (int) sample frequency at which you would like to simulate the timedomain
        bin_file = (str) name of the binary whistle file  (*.ton or *.bin) to simulate
            time domain from
        f_shift = (int, None) +- frequency (Hz) you wish to shift the whistle
        file_output = (boolean, False) whether to output the signal as a wave
            file
        fname = (str, None) # name of the wavfile to output,
            if none use whistle file name
            
    Returns:
        wavefiles for each whistle in the binary file
    """
    def __init__(self, fs, bin_file, f_shift = None, 
                 file_output = False, fname = None):
        self.fs =  fs
        self.bin_file  = bin_file
        
        # Whether to write the file 
        self.file_output = file_output
        
        # frequency shift in hz
        self.f_shift = f_shift
        
        # name of the wavfile to output, if none use whistle file name
        self.fname = fname
        
        # Get the tonal file
        self.Tonal = iter(tonal(bin_file))
        
        # set iterator for whistle N in tonal file
        self.counter = 0 
            
        
    def __iter__(self):
        "iter(obj) - Return self as we know how to iterate"
        return self 

    def __next__(self):
        
        #update the counter
        self.counter += 1
        
        # get the whistle
        whistle = whistle_contour(next(self.Tonal), cs_type = 'spline')
        
        # new time range
        t = np.arange(min(whistle.Time), max(whistle.Time), 1/self.fs)
        
        # Shift the whistle if need be and/or re-create the poly
        if self.f_shift is None:
            poly = whistle.cs
        else:
            poly = UnivariateSpline(whistle.Time, whistle.Freq + f_shift, k=3)
            
        
        # grab the signal
        sig =self.sweep_poly(poly, t)
        
        # if outputting the file to wave
        if self.file_output is True:
            self.write_signal(sig)
    
        return (sig)
    
        
    def sweep_poly(self, poly, t):
        ''' 
        Returns: time series approximation of the binary whistle file
        
        Iinputs : t = (float) time series over which to simulate the sound
        poly : UnivariateSpline from whistle_contour() e.g. 
             whistle = whistle_contour(next(self.Tonal))
             poly = whistle.cs
        use the antiderivative of the spline polynomial to calculate the 
        phase of the signal at each time (t) 
        
        '''
        intpoly = poly.antiderivative(n =1)
        phase = 2 * np.pi * intpoly(t)
        sig = np.cos(phase)
        return (sig)
    
    def write_signal(self, sig):
        ''' output the signal to a wavfile '''
        
        if self.fname is None:
            # create a file name and add on the whistle ID
            #fname =  os.path.split(self.bin_file)[1][0:-4]
            fname = self.bin_file[0:-4] + '_' + str(self.counter) + '.wav'
        
        sf.write(fname, sig, self.fs)
        
    
    
    
    
    
############################################################################
## Testing/debugging #
############################################################################
#
#
#
#data_dir = '/home/kpalmer/AnacondaProjects/peak-picker-deep-net/TestBinFIles'
#fastq = [f for f in os.listdir(data_dir) if f.endswith('.bin') or f.endswith('.ton')]
#bin_name = data_dir + '/' + fastq[0]
#
#
#fs =96000*2
#
#sig_gen =  WhistleGeneratorTimeDomain(fs, bin_name, file_output = True)
#
#
#
#sig = next(sig_gen)
#sig1 = next(sig_gen)




    
            
    