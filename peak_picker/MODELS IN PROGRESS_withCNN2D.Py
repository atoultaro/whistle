#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 21 20:09:52 2018

@author: kpalmer
"""

# make sure aloways pred

import numpy as np
import matplotlib.pyplot as plt
import sys
import os


# import the dsp classes
# import the dsp classes

sys.path.append("/home/kpalmer/AnacondaProjects/dsp")
sys.path.append("/home/kpalmer/AnacondaProjects/peak-picker-deep-net")


soundfile_avail = True
_default_reader = "SoundFile"

# Build the model 
from keras.losses import binary_crossentropy, categorical_crossentropy
from keras.models import Sequential  
from keras.layers.core import Dense, Activation,Flatten,Lambda
#from keras.layers.recurrent import LSTM
from keras.layers import BatchNormalization,Conv2D, Add,MaxPooling2D,UpSampling2D
from keras.models import Model 
from keras.layers import Bidirectional, TimeDistributed, Input, RepeatVector,Reshape, Concatenate, concatenate
from keras.layers import Conv1D,MaxPooling1D, UpSampling1D,Softmax, Conv2DTranspose
from keras.regularizers import l2
import keras.backend as Kb
import tensorflow as tf
from keras.models import load_model
from Kernel_Constraint import Same
from keras.layers.cudnn_recurrent import CuDNNLSTM as LSTM
from keras.layers.cudnn_recurrent import CuDNNGRU as GRU
import keras



#from batch import batch_generator as batch_generator
from Batch_Generator_forCNN2D_old import batch_generator 
from batch import batch_generator as bg_new
from sklearn.metrics import precision_recall_curve

# Check keras/tensor flow can see the gosg-dang GPU
from tensorflow.python.client import device_lib
print(device_lib.list_local_devices())

###############################################################################
# Set directory of bin/ton files and associated wav files
# Top level directory, divided by species

data_dir = '/cache/kpalmer/quick_ssd/data/dclmmpa2011/devel_data/'
data_by_species = dict()
for species in ['bottlenose','melon-headed','common']: #'bottlenose', 'melon-headed'
    data_by_species[species] = os.path.join(data_dir, species)
file_dirs = list(data_by_species.values())



data_dir = '/cache/kpalmer/quick_ssd/data/dclmmpa2011/eval_data'
data_by_species = dict()
for species in ['Delphinus capensis', 'Delphinus delphis', 'Peponocephala electra',
                'StenellaLongirostrisLongirostris', 
                'Tursiops truncatus-Palmyra', 'Tursiops truncatus-SoCal']:
    data_by_species[species] = os.path.join(data_dir, species)
file_dirs_valid = list(data_by_species.values())





###############################################################################
# Custom functions ###
##############################################################################
# Custom Loss
def magnify_ones_mean_squared_error(y_true, y_pred):
    'returns custom loss function mse times the binary mask to \
        place more emphasis on true positives'
    cross_e = Kb.abs(Kb.square(y_pred - y_true) *(y_true*Kb.constant(550)))
    return cross_e


def magnify_ones_mean_squared_error1(y_true, y_pred):
    'returns custom loss function mse times the binary mask to \
        place more emphasis on true positives'
    cross_e = Kb.abs(Kb.square(y_pred - y_true)*(1+y_true*Kb.constant(550))) 
    return cross_e

def error(y_true, y_pred):
    cross_e = Kb.abs(-y_true*Kb.log(y_pred)*Kb.constant(100)+(1-y_true)*Kb.log(1-y_pred))
    return cross_e

#Custom metrics- from previous keras versions
def precision(y_true, y_pred):
    """Precision metric.

    Only computes a batch-wise average of precision.

    Computes the precision, a metric for multi-label classification of
    how many selected items are relevant.
    """
    true_positives = Kb.sum(Kb.round(Kb.clip(y_true * y_pred, 0, 1)))
    predicted_positives = Kb.sum(Kb.round(Kb.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + Kb.epsilon())
    return precision


def recall(y_true, y_pred):
    """Recall metric.

    Only computes a batch-wise average of recall.

    Computes the recall, a metric for multi-label classification of
    how many relevant items are selected.
    """
    true_positives = Kb.sum(Kb.round(Kb.clip(y_true * y_pred, 0, 1)))
    possible_positives = Kb.sum(Kb.round(Kb.clip(y_true, 0, 1)))
    recall = true_positives / (possible_positives + Kb.epsilon())
    return recall


###############################################################################
# Set up generators, frequency limits, class weights, and neurons 
##############################################################################
"""
# Just try running on 4 frequency bins
df = 125 # delta frequncy assuming fs=192khz and len_ms =8
f1 = 8000 - df
f2 = 8000 + df
"""

class_weight = {0 : 1., 1: 320.}

# bug - returning one more row than aniticpated #
generator_all = batch_generator(file_dirs=file_dirs, chunk_dur=2, batch_size=30,
                            adv_ms =2, len_ms = 8, Only_Positive = False)

generator_pos = batch_generator(file_dirs=file_dirs, chunk_dur=10, batch_size=30,
                            adv_ms =2, len_ms = 8, Only_Positive = True, verbose = True)

valid_generator =batch_generator(file_dirs_valid, chunk_dur=3, batch_size=30,
                                 adv_ms =2, len_ms = 8, Only_Positive = False)

n_neurons = [generator_all.get_example_dim()[2], 1]




# bug - returning one more row than aniticpated #

generator_pos_new = bg_new(file_dirs=file_dirs, chunk_dur=2, batch_size=20,
                            adv_ms =2, len_ms = 8, Only_Positive = True, verbose = 0)
generator_all_new = bg_new(file_dirs=file_dirs, chunk_dur=2, batch_size=20,
                            adv_ms =2, len_ms = 8, Only_Positive = True, verbose = 0)


n_neurons_new = [generator_pos_new.get_batch_dims()[2], 1]
#
#aa_old, bb_old = next(generator_pos)
#
aa_new, bb_new = next(generator_pos_new)



##%%
###########CONV2D Same layer#############
##Concatenate previous output and first input in 2 channels
##CNN layer+Dense layer to get a one-channel output
##The CNN and Dense layer are the same for each iteration
#
## working the best! :)
#
#
#nb_filters=20
#inputs=Input(batch_shape=generator_pos.get_example_dim()[0:4])
#SameCNN=Conv2D(nb_filters, [5,5], kernel_constraint=Same(),strides=(1, 1),
#               activation='relu',padding='same',
#               batch_input_shape=generator_pos.get_example_dim(),
#               data_format='channels_last')
#SameDense=Dense(1)
#
#
##First Conv
#concat=Concatenate(axis=3)([inputs,inputs]) #to have the same input size for CNN layer
#Conv=SameCNN(concat)
#Merged=SameDense(Conv) #to have one channel image output
#concat=Concatenate(axis=3)([Merged,inputs])
#
##Second Conv
#Conv=SameCNN(concat)
#Merged=SameDense(Conv)
#concat=Concatenate(axis=3)([Merged,inputs])
#
##Third Conv
#Conv=SameCNN(concat)
#Merged=SameDense(Conv)
#
## This was required when using the standard batch generatorthat produces
## 3 dimensional data as input to the RNN. After modifiying batch generator
## to produce 4 dimensional output the following lambda layers are no longer
## required
##Output=Lambda(lambda x: Kb.sum(x, axis=3))(Merged) #for 3D Batch Output
##Output=Lambda(lambda x: Kb.reshape(x,(Kb.shape(x)[0],Kb.shape(x)[1],Kb.shape(x)[2],1)))(Output)
#
#model1=Model(inputs,Merged)
#model1.compile(loss=magnify_ones_mean_squared_error1, optimizer='adam',
#             metrics=['accuracy',precision,recall])
#model1.fit_generator(generator_pos, steps_per_epoch=generator_pos.get_steps_per_epoch(),
#                    epochs=10,  verbose=0) #generator.get_steps_per_epoch() 
#
#model1.save('CNN_Same_Layer1_5x5.h5')



 Same thing new batch generator 

from keras.layers.normalization import BatchNormalization

nb_filters=20
inputs=Input(batch_shape=generator_pos_new.get_batch_dims()[0:4])
SameCNN=Conv2D(nb_filters, [10,10], kernel_constraint=Same(),strides=(1, 1),
               activation='relu',padding='same',
               batch_input_shape=generator_pos_new.get_batch_dims(),
               data_format='channels_last')
SameDense=Dense(1)


#First Conv
concat=Concatenate(axis=3)([inputs,inputs]) #to have the same input size for CNN layer
BN = BatchNormalization()(concat)
Conv=SameCNN(BN)
Merged=SameDense(Conv) #to have one channel image output
concat=Concatenate(axis=3)([Merged,inputs])

#Second Conv
BN = BatchNormalization()(concat)
Conv=SameCNN(BN)
Conv=SameCNN(concat)
Merged=SameDense(Conv)
concat=Concatenate(axis=3)([Merged,inputs])

#Third Conv
BN = BatchNormalization()(concat)
Conv=SameCNN(BN)
Merged=SameDense(Conv)

# This was required when using the standard batch generatorthat produces
# 3 dimensional data as input to the RNN. After modifiying batch generator
# to produce 4 dimensional output the following lambda layers are no longer
# required
#Output=Lambda(lambda x: Kb.sum(x, axis=3))(Merged) #for 3D Batch Output
#Output=Lambda(lambda x: Kb.reshape(x,(Kb.shape(x)[0],Kb.shape(x)[1],Kb.shape(x)[2],1)))(Output)

model1=Model(inputs,Merged)
model1.compile(loss=magnify_ones_mean_squared_error1, optimizer='adam',
             metrics=['accuracy',precision,recall])
model1.summary()

model1.fit_generator(generator_pos_new, steps_per_epoch=generator_pos_new.get_steps_per_epoch(),
                    epochs=3) #generator.get_steps_per_epoch() 

model1.save('CNN_Same_Layer1_10x10_batch_norm.h5')







#%%
##########CONV2D Same layer#############
# Modification of Kristel's model add a bidirectional RNN


# Same thing new batch generator 
nb_filters=20
inputs=Input(batch_shape=generator_pos_new.get_batch_dims()[0:4])
SameCNN=Conv2D(nb_filters, [10,10], kernel_constraint=Same(),strides=(1, 1),
               activation='relu',padding='same',
               batch_input_shape=generator_pos_new.get_batch_dims(),
               data_format='channels_last')

SameCNN2=Conv2D(nb_filters, [5,5], kernel_constraint=Same(),strides=(1, 1),
               activation='relu',padding='same',
               batch_input_shape=generator_pos_new.get_batch_dims(),
               data_format='channels_last')

SameCNN3=Conv2D(nb_filters, [1,1], kernel_constraint=Same(),strides=(1, 1),
               activation='relu',padding='same',
               batch_input_shape=generator_pos_new.get_batch_dims(),
               data_format='channels_last')


SameCNN4=Conv2D(nb_filters, [2,2], kernel_constraint=Same(),strides=(1, 1),
               activation='relu',padding='same',
               batch_input_shape=generator_pos_new.get_batch_dims(),
               data_format='channels_last')

Dropout_reg = Dropout(0.5)

BDN = GRU(768, batch_input_shape = generator_pos_new.get_batch_dims()[0:2],
                return_sequences=True, 
                stateful=False,
                kernel_regularizer=l2(0.01))


input2=generator_pos_new


SameDense0=Dense(1)
SameDnese1=Dense(1)
SameDnese2=Dense(1)
SameDnese3=Dense(1)
SameDnese4=Dense(1)


#First Conv
concat=Concatenate(axis=3)([inputs,inputs]) #to have the same input size for CNN layer
concat = Dropout(0.2)(concat)
BN = BatchNormalization()(concat)
Conv=SameCNN(BN)
Merged=SameDense0(Conv) #to have one channel image output
concat1=Concatenate(axis=3)([Merged,inputs])

#Second Conv
concat1 = Dropout(0.2)(concat1)
BN2 = BatchNormalization()(concat1)
Conv2=SameCNN2(BN2)
Merged2=SameDnese1(Conv2)
concat2=Concatenate(axis=3)([Merged2,inputs])

#Third Conv
BN3 = BatchNormalization()(concat2)
Conv3=SameCNN3(BN3)
Merged3=SameDnese2(Conv3)
concat3=Concatenate(axis=3)([Merged3,inputs])

#Third Conv
BN4 = BatchNormalization()(concat3)
Conv4=SameCNN3(BN4)
Merged4=SameDnese2(Conv4)


## Bi-directional RNN Block
#Output=Lambda(lambda x: Kb.sum(x, axis=3))(concat) #for 3D Batch Output
#Output=Lambda(lambda x: Kb.reshape(x,(Kb.shape(x)[0],Kb.shape(x)[1],Kb.shape(x)[2])))(Merged3)

Output=Lambda(lambda x: Kb.reshape(x,(x.shape[0],x.shape[1], -1)))(Merged3)
Output2 = BDN(Output)




# This was required when using the standard batch generatorthat produces
# 3 dimensional data as input to the RNN. After modifiying batch generator
# to produce 4 dimensional output the following lambda layers are no longer
# required
#Output=Lambda(lambda x: Kb.sum(x, axis=3))(Merged) #for 3D Batch Output
#Output=Lambda(lambda x: Kb.reshape(x,(Kb.shape(x)[0],Kb.shape(x)[1],Kb.shape(x)[2],1)))(Output)



CNN_BDLSTM_model=Model(inputs = [inputs], outputs = Merged3)


CNN_BDLSTM_model.compile(loss=magnify_ones_mean_squared_error1, optimizer='adam',
             metrics=['accuracy',precision,recall])
CNN_BDLSTM_model.summary()


with tf.device('/GPU:0'):
    
    CNN_BDLSTM_model.fit_generator(generator_pos_new,
                                   steps_per_epoch= generator_pos_new.get_steps_per_epoch(),
                        epochs=20,  verbose=1) #generator.get_steps_per_epoch() 

CNN_BDLSTM_model.save('CNN_model_10_5_1.h5')






#%%
# same as before, smaller neightbouerhood
from keras.layers.normalization import BatchNormalization

nb_filters=20
inputs=Input(batch_shape=generator_pos_new.get_batch_dims()[0:4])
SameCNN=Conv2D(nb_filters, [6,6], kernel_constraint=Same(),strides=(1, 1),
               activation='relu',padding='same',
               batch_input_shape=generator_pos_new.get_batch_dims(),
               data_format='channels_last')
SameDense=Dense(1)


#First Conv
concat=Concatenate(axis=3)([inputs,inputs]) #to have the same input size for CNN layer
BN = BatchNormalization()(concat)
Conv=SameCNN(BN)
Merged=SameDense(Conv) #to have one channel image output
concat=Concatenate(axis=3)([Merged,inputs])

#Second Conv
BN = BatchNormalization()(concat)
Conv=SameCNN(BN)
Conv=SameCNN(concat)
Merged=SameDense(Conv)
concat=Concatenate(axis=3)([Merged,inputs])

#Third Conv
BN = BatchNormalization()(concat)
Conv=SameCNN(BN)
Merged=SameDense(Conv)


#Fourth Conv
BN = BatchNormalization()(concat)
Conv=SameCNN(BN)
Merged=SameDense(Conv)



# This was required when using the standard batch generatorthat produces
# 3 dimensional data as input to the RNN. After modifiying batch generator
# to produce 4 dimensional output the following lambda layers are no longer
# required
#Output=Lambda(lambda x: Kb.sum(x, axis=3))(Merged) #for 3D Batch Output
#Output=Lambda(lambda x: Kb.reshape(x,(Kb.shape(x)[0],Kb.shape(x)[1],Kb.shape(x)[2],1)))(Output)

model1=Model(inputs,Merged)
model1.compile(loss='mse', optimizer='adam',
             metrics=['accuracy',precision,recall])
model1.summary()

model1.fit_generator(generator_pos_new, steps_per_epoch=generator_pos_new.get_steps_per_epoch(),
                    epochs=3) #generator.get_steps_per_epoch() 

model1.save('CNN_Same_Layer1_6x6_batch_norm.h5')


with tf.device('/GPU:0'):
    
    CNN_BDLSTM_model.fit_generator(generator_pos_new,
                                   steps_per_epoch= generator_pos_new.get_steps_per_epoch(),
                        epochs=10,  verbose=1) #generator.get_steps_per_epoch() 

CNN_BDLSTM_model.save('CNN_model_6_6_mse.h5')

#%%


#%%
# same as before, smaller neightbouerhood
from keras.layers.normalization import BatchNormalization
from keras.layers import LocallyConnected1D as LR1D

nb_filters=20
inputs=Input(batch_shape=generator_pos_new.get_batch_dims()[0:4])


SameCNN0=Conv2D(filters = nb_filters,  kernel_size= 110,strides=(1, 1),
               activation='relu',padding='same',
               batch_input_shape=generator_pos_new.get_batch_dims(),
               data_format='channels_last')

SameCNN2=Conv2D(nb_filters,  kernel_size= 110,strides=(1, 1),
               activation='relu',padding='same',
               batch_input_shape=generator_pos_new.get_batch_dims(),
               data_format='channels_last')


SameCNN3=Conv2D(nb_filters,  kernel_size= 110,strides=(1, 1),
               activation='relu',padding='same',
               batch_input_shape=generator_pos_new.get_batch_dims(),
               data_format='channels_last')

SameCNN4=Conv2D(nb_filters,  kernel_size= 110,strides=(1, 1),
               activation='relu',padding='same',
               batch_input_shape=generator_pos_new.get_batch_dims(),
               data_format='channels_last')





maxpool=MaxPooling2D(pool_size=(2, 2), 
                      padding='same',
                      data_format='channels_last',
                      trainable=False)

maxpool1=MaxPooling2D(pool_size=(2, 2), 
                      padding='same',
                      data_format='channels_last',
                      trainable=False)

maxpool2=MaxPooling2D(pool_size=(2, 2), 
                      padding='same',
                      data_format='channels_last',
                      trainable=False)

maxpool4=MaxPooling2D(pool_size=(2, 2), 
                      padding='same',
                      data_format='channels_last',
                      trainable=False)

LR1D_0 =LR1D()

SameDense0=Dense(350)

SameDense1=Dense(768)

SameDense2=Dense(1)



#First Conv
Conv=Conv2D(50, [3,3], strides=(1, 1), 
            padding='same',
            data_format='channels_last',
            trainable=True)(inputs)
maxp = maxpool0(Conv)

Conv1=Conv2D(10, [3,3], strides=(1, 1), 
            padding='same',
            data_format='channels_last',
            trainable=True)(maxp)
maxp1 = maxpool1(Conv1)

Conv2=Conv2D(4, [3,3], strides=(1, 1), 
            padding='same',
            data_format='channels_last',
            trainable=True)(maxp1)
maxp2 = maxpool2(Conv2)

dense_out1 = SameDense0(maxp2)
dense_out2 = SameDense1(dense_out1)
dense_out3 = SameDense2(dense_out2)



model1=Model(inputs,dense_out3)
model1.compile(loss=magnify_ones_mean_squared_error1, optimizer='adam',
             metrics=['accuracy',precision,recall])

model1.summary()




with tf.device('/GPU:0'):
    
    model1.fit_generator(generator_pos_new,
                                   steps_per_epoch= generator_pos_new.get_steps_per_epoch(),
                        epochs=10,  verbose=1) #generator.get_steps_per_epoch() 

CNN_BDLSTM_model.save('CNN_model_6_6_mse.h5')



#%%

#########################  CNN 2D 'SharpMask'   ###############################
#Architecture similar to that in 'Learning to Refine Object Segments' paper, Pedro O. Pinheiro,
# Tsung-Yi Lin, Ronan Collobert, Piotr Dollar, 2016.
#Outputs worse results than the same CNN Layer trained multiple times

#####FIRST STEP : TRAIN FEED FORWARD LAYERS ######

# run First step with trainable set to True

# Set trainable equal to false in first step

# run first and second step
#
#inputs=Input(batch_shape=generator_pos.get_example_dim()[0:4])
#
#conv2d=Conv2D(30, [3,3], strides=(1, 1), padding='same',data_format='channels_last',trainable=False)(inputs)
#maxpool1=MaxPooling2D(pool_size=(2, 2), padding='same',data_format='channels_last',trainable=False)(conv2d)
#
#conv2d=Conv2D(20, [3,3], strides=(1, 1), padding='same',data_format='channels_last',trainable=False)(maxpool1)
#maxpool2=MaxPooling2D(pool_size=(2, 2), padding='same',data_format='channels_last',trainable=False)(conv2d)
#
#conv2d=Conv2D(15, [3,3], strides=(1, 1), padding='same',data_format='channels_last',trainable=False)(maxpool2)
#maxpool3=MaxPooling2D(pool_size=(2, 2), padding='same',data_format='channels_last',trainable=False)(conv2d)
#
#output=Dense(10,trainable=False)(maxpool3)

# Uncomment this section for first step only #

#upsamp=UpSampling2D((8,8),data_format='channels_last')(output)
#model1=Model(inputs,upsamp)
#model1.compile(loss=magnify_ones_mean_squared_error1, optimizer='adam',
#             metrics=['accuracy',precision,recall])
#model1.fit_generator(generator_pos,steps_per_epoch=30,
#                    epochs=1,  verbose=1) #epochs=30, generator.get_steps_per_epoch() 

#model1.save('SharpMaskStep1.h5')


#model = load_model('SharpMaskStep1.h5', custom_objects={'magnify_ones_mean_squared_error1': magnify_ones_mean_squared_error1,'precision':precision,'recall':recall})



###SECOND STEP : FREEZE FEED FORWARD LAYERS########



#First Refinement module
maxpool3=Conv2D(10,[3,3], strides=(1, 1), padding='same',data_format='channels_last')(maxpool3)
concat=Concatenate(axis=-1)([output,maxpool3])
conv2d=Conv2D(10,[3,3], strides=(1, 1), padding='same',data_format='channels_last')(concat)
upsamp1=UpSampling2D(data_format='channels_last')(conv2d)

#Second Refinement module
maxpool2=Conv2D(10,[3,3], strides=(1, 1), padding='same',data_format='channels_last')(maxpool2)
concat=Concatenate(axis=-1)([upsamp1,maxpool2])
conv2d=Conv2D(10,[3,3], strides=(1, 1), padding='same',data_format='channels_last')(concat)
upsamp2=UpSampling2D(data_format='channels_last')(conv2d)

#Third Refinement module
maxpool1=Conv2D(10,[3,3], strides=(1, 1), padding='same',data_format='channels_last')(maxpool1)
concat=Concatenate(axis=-1)([upsamp2,maxpool1])
conv2d=Conv2D(10,[3,3], strides=(1, 1), padding='same',data_format='channels_last')(concat)
upsamp3=UpSampling2D(data_format='channels_last')(conv2d)
dense=Dense(1)(upsamp3)



model1=Model(inputs,dense)
model1.compile(loss=magnify_ones_mean_squared_error1, optimizer='adam',
             metrics=['accuracy',precision,recall])
model1.fit_generator(generator_pos,steps_per_epoch=2,
                    epochs=1,  verbose=1,workers=3) #epochs=30, generator.get_steps_per_epoch() 


model1.save('SharpMaskStep2Model.h5')








#%%
(aa, bb) = next(generator_pos_new) #valid_generator


#(bb, aa) = next(generator)#orderchanged
#%%
#result =model1.predict(aa, batch_size=20, verbose=0)
result=CNN_BDLSTM_model.predict(aa)
#%%

idx = 14
z_min, z_max = -np.abs(result[idx,:,:,0]).max(), np.abs(result[idx,:,:,0]).max()
plt.pcolormesh(result[idx,:,:,0], vmin=z_min, vmax=z_max)
plt.colorbar()


#%%
plt.pcolormesh(bb[1,:,:,0])
#%%
plt.pcolormesh(bb[1,:,:,0])

#%%
r=generator_all.get_example_dim()[1:3]
#%%
t=np.round(result)
plt.pcolormesh(t[1,:,:,0])

#%%
bb=0
while np.sum(np.sum(np.sum(bb)))<1:
    (aa, bb) = next(generator_all)


#%%
# Visualize weights
W = model1.layers[2].get_weights()
W=W[0]
#%%
for image in range (W.shape[2]):
    plt.figure()
    for i in range (W.shape[3]):
        plt.subplot(5,5,i+1)
        plt.pcolormesh(W[:,:,image,i],cmap='bwr')
#%%


#######   PRECISION RECALL CURVE ON ONE BATCH    ##############
(inp, test) = next(generator_all) #valid_generator
score=CNN_BDLSTM_model.predict(inp, batch_size=15, verbose=0)

score=score.reshape(score.shape[0]*score.shape[1]*score.shape[2]) #channels _last
test=test.reshape(test.shape[0]*test.shape[1]*test.shape[2])

    
precision, recall, thresholds= precision_recall_curve(test,score)
plt.fill_between(recall, precision, step='post', alpha=0.2,
                 color='b')
plt.xlabel('Recall')
plt.ylabel('Precision')

#%%
model = load_model('CNN_Same_Layer.h5', custom_objects={'magnify_ones_mean_squared_error1': magnify_ones_mean_squared_error1,'precision':precision,'recall':recall,'Kb':Kb})