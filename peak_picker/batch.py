'''
Created on Feb 14, 2018

batch - batch generators
Kaitlin Palmer and Kristelle Le Cam
'''
import numpy as np
import random
import math
import sys
import os
from scipy.signal import medfilt2d
from scipy.interpolate import interp1d

from pylru import lrucache  # least recently used cache
# from TonalClass_v1 import (get_corpus, select_matching_files,
#                           MakeWhistleDF, get_complete_frames,
#                           MakeBinaryMask_chunk, nearest_to, tonal,
#                           GetWhistelIds)
# from dsp.FrameStream import AudioFrames
from dsp.dftstream import DFTStream
import pandas as pd
import matplotlib.pyplot as plt

####################################################

sys.path.append("/home/kpalmer/AnacondaProjects/dsp")
sys.path.append("/home/kpalmer/AnacondaProjects/peak-picker-deep-net")

'''
Bug:  When Only_pos == False, returns too many features...
Scratch that. It seems to happen when using a validation directory file
thf something wrong in Tonal_v1 where locations are not being properly computed
'''
####################################################

def float_range(start, stop, increment):
    "float_range - range for floating point arithmetic"
    current = start
    while current < stop:
        yield current
        current += increment


class batch_generator():
    def __init__(self, file_dirs, batch_size=10, adv_ms =2,
                 len_ms = 8, randomise = True, verbose = False,
                 tonal_cache_size = 50,
                 normalize="mean", freqs=None, Only_Positive = True):
        
        '''batch_generator - 
        class for generating batches where file_dirs is the directory
        where labels (*.bin and *.ton) and audio data (*.wav) reside.
        
        NOTE: both wave files and tonal files must be stored together
        
        Chunk_dur : duration in seconds to chop up each sound file
        batch_size : number of chunks in each batch/minibatch
        adv_ms and len_ms: advance and length of the frame in milliseconds
        randomise: whether to shuffle the examples
        verbose (True|False): provide more feedback
        freqs: tuple indicating low and high frequency to include
        Only_positive: Restrict data generation to periods with whistles present        
        freqs - optional tuple for min and max frequency to include
        
        '''
        # initial starting point
        self.current = 0
        self.file_dirs = file_dirs
        self.chunk_dur = chunk_dur
        self.batch_size = batch_size
        self.Only_Positive = Only_Positive 
        self.whistle_cache = lrucache(tonal_cache_size)
        self.chunk_id, self.file_name, self.bin_name = self.generate_chunk_ids()
        self.N = len(self.chunk_id)
        self.example_ids = np.arange(len(self.chunk_id))
        self.adv_ms = adv_ms
        self.len_ms = len_ms
        self._epoch = 0    # current number of completed epochs
        self.iter_id =0 # counter for n in n in bach
        self.verbose = verbose
        self.normalize = "mean"
        self.freqs = freqs
        self.frames_counter = 0 
        self.randomise = randomise

        # Randomise
        if self.randomise is True:
            random.shuffle(self.example_ids)
            
        # declare example [spectrogram sections] dimensions
        framestream= AudioFrames(self.file_name[0], self.adv_ms, self.len_ms)
        self.nrow = get_complete_frames(framestream, self.adv_ms, self.len_ms, 
                                     chunk_dur = self.chunk_dur, Offset = 0)
        
        if self.nrow % 2:
            self.nrow = self.nrow -1

        # frequency restriction
        self.bins_Hz = DFTStream(framestream).bins_Hz
        if self.freqs is not None:
            # Builds range for matrix indices.  High bin is NOT included
            indices = []
            delta_Hz = 1.0 / (self.len_ms * 0.001)            
            for f in freqs:
                freq_bin = int(f / delta_Hz + 0.5)
                indices.append(freq_bin)
            # Expand the range
            self.freq_idx = [f for f in range(indices[0], indices[1])]
            self.ncol = len(self.freq_idx)
        else:
            self.freq_idx = None  # Use everything
            self.ncol = len(self.bins_Hz)
            
        # set up counter for number of positive samples
        self.pres = np.zeros((1, self.ncol))

    @property
    def epoch(self):
        return self._epoch
        
    def next_example_id(self):
        '__next_example_id() - return next chunk to process'
        
        if self.current >= self.N:
            self._epoch += 1
            self.current = 0
            print("Batch example crossed into epoch {}".format(self._epoch))
            if self.randomise is True:
                random.shuffle(self.example_ids)  # reshuffle
            
        start = self.current
        self.current += 1
        return start
            
        
        
    def __iter__(self):
        return self

    def __next__(self):
            "Return next minibatch (targets, features)"
            
            # Preallocate batch size
            target_batch= np.zeros([self.batch_size, 1]) #-1 for CNN 1D
            feature_batch= np.zeros([1,self.batch_size,self.nrow, self.ncol])
            

            if self.verbose:
                print("Generating {} example/layer pairs (epoch {}, {}%)".format(
                    self.batch_size, self._epoch, 100.0 *self.current / self.N))

            example_counter = 0
            while example_counter < self.batch_size:
                
                # get an example index
                batch_id = self.next_example_id()
                
                # Determine start and end time of chunks and retrieve
                # the tonal set that overlaps the start and end time                
                start_s, end_s = self.get_chunk_range(batch_id)
                tonals = self.get_tonals(self.bin_name[batch_id], start_s, end_s)
                
#                if self.Only_Positive == True and len(tonals) == 0:
#                    continue    # didn't find any...
#                    
                # Construct relevant portion of a spectrogram
                examples = self.get_features(batch_id)
                feature_batch[example_counter,:,:,0] = examples

                # Construct the labels and get number of targets set to 1
                labels, pos_bins_n = self.get_labels(tonals, start_s, end_s)
                target_batch[example_counter] = labels 

                if self._epoch == 0:
                    # on first epoch, count the number of frames & positives
                    self.frames_counter += self.nrow
                    self.pres = self.pres + pos_bins_n
                
                if self.verbose:
                    print('example {} has {} positive labels'.format(
                        example_counter, pos_bins_n))

                example_counter = example_counter + 1

            return(feature_batch, target_batch)


    def get_tonals(self, file, start_s=0, end_s=math.inf):
        """__get_tonals - Retrieve information about tonals in a file
        File should contain a silbido format tonal description
        Tonals returns are restricted to those that cross the specified
        interval        
        """
        try:
            tonals = self.whistle_cache[file]
        except KeyError:
            # Read whistles and store them
            tonals = []
            tonal_reader = tonal(file)
            for t in tonal_reader:
                tonals.append(t)            
            self.whistle_cache[file] = tonals # cache file
            
        # Kaitlin:  I think there was a bug at one point where some tonal
        # files might not be properly sorted... we should perhaps
        # check and fix this in the reader.  The code below will remedy
        # this by checking everything (less efficient)
        subset = []
        for t in tonals:
            tstart = t["Time"][0]
            tend = t["Time"][-1]
            # Keep t if there is overlap
            if (
                # block inside tonal
                (tstart < end_s and tend > start_s) or
                # tonal starts inside block
                (tstart > start_s and tstart < end_s) or
                # tonal ends inside block
                (tend > start_s and tend < end_s)
                ):
                subset.append(t)
            
        return subset
            
    def get_chunk_range(self, batch_idx):
        """__get_chunk_range(batch_idx)
        Return the putative start_s and end_s of a chunk
        The chunk may not actually be this long if the file stream ends 
        """        
        start_s = self.chunk_id[batch_idx] * self.chunk_dur
        return start_s, start_s + self.chunk_dur
            
    def get_features(self, batch_idx):
        """__get_features(batch_idx)
        Retrieve features associated with current batch
        Returns features, start time, end time
        """
                
        # load the sound file (this could be optimized, we open/close
        # the file multiple times.  Expensive...
        framestream = AudioFrames(self.file_name[batch_idx], self.adv_ms, self.len_ms)

        # advance the object iterator to the sample start 
        start_s = self.chunk_id[batch_idx] * self.chunk_dur
        sample_start = framestream.Fs * start_s
        framestream.seek_sample(sample_start)
        dftstream = DFTStream(framestream)
        
        # maximum number of frames
        max_frames = self.nrow
                    

            
        # Read the frames
        frames = []
        frame_idx = 0
        for s in dftstream: # s = (spectrum, offset_s, timestamp)
            if self.freq_idx is None:
                frames.append(s[0])
            else:
                frames.append(s[0][self.freq_idx])
            end_s = s[1]
            frame_idx = frame_idx + 1
            if frame_idx >= max_frames:
                break
        
        zeros = [0 for idx in range(len(frames[0]))]
        while frame_idx < max_frames:
            # File completed before chunk, zero pad
            frames.append(zeros)
            frame_idx = frame_idx + 1
            
        spectra = np.asarray(frames)  # prepare for tensor
        
        return spectra
            
    def get_labels(self, tonals, start_s, end_s):
        """__get_labels(tonals, start_s, end_s)
        Construct a spectrogram mask with 1s where tonals occur.
        tonals is expected to be a list of tonals that overlap
        start_s and end_s
        """
                  
        ms_to_s = .001  # milliseconds to seconds
        len_s = self.len_ms * ms_to_s
        adv_s = self.adv_ms * ms_to_s
        
        # frequency parameters
        bin_Hz = 1.0 / len_s
        if self.freq_idx is None:
            start_freq_bin = 0
        else:
            # Will need to shift frequency bins down by this much
            start_freq_bin = self.freq_idx[0]
        
             
        labels = np.zeros([self.nrow, self.ncol])
        count = 0
        for tonal in tonals:
            
            # inline function for finding nearest frame time
            nearest_frame_start = lambda time : round(time / adv_s)*adv_s
            
            # Interpolate over any gaps
            t = tonal["Time"]
            f = tonal["Freq"]
            frame_start = nearest_frame_start(t[0])
            frame_end = nearest_frame_start(t[-1])
            # Construct interpolation function to get values at frames.
            # Permit extrapolation as first and last frame may be slightly
            # beyond the tonal due to rounding
            # (Alternatively, we could use ceil on start and floor on end)
            try: 
                f_tonal = interp1d(t, f, fill_value = "extrapolate",
                                   bounds_error=False, kind='cubic', 
                                   assume_sorted=True)
            except ValueError as e:
                # Very occasionally, the times may not be sorted
                if np.any(np.diff(t) < 0) == True:
                    # oh-oh, chongo, not sorted
                    ordering = np.argsort(t)
                    # Permute 
                    t = tuple([t[i] for i in ordering])
                    f = tuple([f[i] for i in ordering])
                    # Check if permutation worked, there are sometimes dups
                    _, idx_unique =np.unique(t, return_index= True)
                    
                    t = tuple([t[i] for i in idx_unique])
                    f = tuple([f[i] for i in idx_unique])
                        
                    f_tonal = interp1d(t, f, 
                                       fill_value = "extrapolate",
                                       bounds_error=False, kind='cubic', 
                                       assume_sorted=True)
                #print(e)
            # Find times for this tonal that are within processing block
            times = np.array(
                [time for time in float_range(frame_start, frame_end, adv_s) 
                     if time >= start_s and time <= end_s]
                )
            if len(times) > 0:
                frame_idx = ((times - start_s) / adv_s).astype(int)
                freq_Hz = f_tonal(times)
                bin_idx = ((freq_Hz / bin_Hz) - start_freq_bin + 0.5).astype(int)
                for idx in range(len(frame_idx)):
                    try:
                        labels[frame_idx[idx],bin_idx[idx]] = 1
                        count += 1
                    except IndexError:
                        pass  # edge case outside of bounding box
                    
        return labels, count
        
    def generate_chunk_ids(self):
        '''
        Generate three lists containing needed information to collect features
        and targets- could also be a dataframe in the future
        ' file names- list of filenames with associated whistle files
        ' binary file names- list of binary files with associated wav files
        ' chunk id''s for all training data provided in file_dirs'''
        file_name=[]
        bin_name =[]
        chunk_id =[]
        
        for ii in range(len(self.file_dirs)):
    
            # Get corpus of files 
            fnames_wavs = get_corpus(self.file_dirs[ii], filetype=".wav")  
            
            # Only wav files with whistle or tonal files
            fnames_wavs, fnames_whistles  = select_matching_files(fnames_wavs)
            
            # I need the index to reference both binary and wave names...
            # I'm sure there is a scandalously simple way to do it....
            
            
            for kk in range(len(fnames_wavs)):
                
                # if only looking for positive examples, iterate through everythin
                if self.Only_Positive is True:
                    binary_file = fnames_whistles[kk]
                    tonals = self.get_tonals(binary_file)
                    max_t =0
                    
                    
                    # get the time of the last whislte
                    # this should be a lambda fx but it's being a pill
                    for t in tonals:
                        if max(t['Time'])> max_t:
                            max_t =max(t['Time'])
                    
                    n_steps_pos = int(np.ceil(max_t/self.chunk_dur) * self.chunk_dur)
                    
                    for jj in range(n_steps_pos):
                        st = jj *self.chunk_dur
                        et = st + self.chunk_dur 
                        
                        tonals = self.get_tonals(binary_file, st, et)
                        
                        if len(tonals)>0 or self.Only_Positive is False:
                            bin_name.append(binary_file)
                            file_name.append(fnames_wavs[kk])
                            chunk_id.append(jj)
                            
               # Otherwise just calculate the times of the chunk 
                else:
                    n_chunks = int(AudioFrames(fnames_wavs[kk], 2,8).soundfileinfo.duration/self.chunk_dur)
                    chunk_id = chunk_id + np.arange(0, n_chunks).tolist()
                    file_name =file_name+ [fnames_wavs[kk]] *n_chunks
                    bin_name = bin_name + [fnames_whistles[kk]] *n_chunks
            
   
        return chunk_id, file_name, bin_name
    
    def get_time_axis(self):
        """"get_time_axis() - Return a time axis for a batch element
        Axis is relative to start of example/label
        """
        adv_s = self.adv_ms / 1000.0
        return [t*adv_s for t in range(self.nrow)]
    
    def get_freq_axis(self):
        """get_freq_axis() - Return a frequency axis for a batch element
        """
        if self.freq_idx is not None:
            Hz = self.bins_Hz[self.freq_idx]
        else:
            Hz = self.bins_Hz
        return Hz
    
    def get_steps_per_epoch(self):
        ' returns the number of batches per epoc for the specified parameters \
         batch_size, adv_ms, and len_ms'
        steps_per_epoch = int(np.ceil(self.N  / self.batch_size))  
        return steps_per_epoch
    
    def get_N_examples(self):
        'Returns the number total number of examples in the dataset provided \
        approximately N_files / chunk_dur'
        examplesN = len(self.chunk_id)  
        return examplesN
    
    def get_epochs(self):
        'Returns the current epoch'
        return self._epoch
    
    def get_batch_dims(self):
        """get_batch_dims()
        Returns the dimensions of the batch, (batch size, rows, cols, channels)
        For audio data, rows are time and frequencies are columns        
        """
        return int(self.batch_size), int(self.nrow), int(self.ncol), 1
    
    def get_steps_per_epoch(self):
        """get_steps_epoch() - How many times must next be called for an epoch?
        Will not be an exact number unless batch_size divides evenly into
        the number of examples.  Rounds up, so usually some of the next
        epoch will be included in the last step
        """        
        return int(math.ceil(self.N / self.batch_size))
    
    def set_current(self, current):
        raise NotImplementedError

    def plot_matrix(self, m):
        "plot_matrix(m) - Given an example or label matrix, plot it"
        
        plt.pcolormesh(self.get_time_axis(), self.get_freq_axis(),
                       m.transpose())
        plt.colorbar(label="Magnitude")



## Set directory of bin/ton files and associated wav files
## Top level directory, divided by species
##
#data_dir = '/cache/kpalmer/quick_ssd/data/dclmmpa2011/devel_data/'
#data_by_species = dict()
#for species in ['bottlenose','melon-headed','common', 'spinner']: #'bottlenose', 'melon-headed'
#    data_by_species[species] = os.path.join(data_dir, species)
#file_dirs = list(data_by_species.values())

#
#counter = 0
#file_counter =0
#for dir_name in file_dirs:
#    fnames_wavs = get_corpus(dir_name, filetype=".wav")  
#    
#    # Only wav files with whistle or tonal files
#    fnames_wavs, fnames_whistles  = select_matching_files(fnames_wavs)
#    
#    for ff in fnames_whistles:
#        tonal_reader = tonal(ff)
#        print(ff)
#        file_counter +=1
#        while True:
#            try:
#                # get the tonal
#                next(tonal_reader)
#                counter +=1
#                print(str(counter))
#            except:
#                break
#
#
#    
#    
#    
#    for ff in fnames_whistles:
#        # get the tonal
#        tonal_reader = tonal(ff)
#        
#        try:
#            next(tonal_reader)
#            counter +=1
#        except EOFError:
#            print(ff) 
#            pass
#

#
#mm = bg_new(file_dirs, chunk_dur=5, batch_size=20, adv_ms = 5 ,
#                 len_ms = 8, 
#                 Only_Positive = True, verbose =0)
#aa, bb = next(mm)
#aa_new = aa.copy()
##
#
#for ii in range(mm.get_steps_per_epoch()):
#     aa_new, bb_new = next(mm)
#     print([str(ii) +' ' +  str(mm.epoch)])
#     if aa_new.shape != aa.shape:
#         print('shape mismatch')
#         break
#     
#




















