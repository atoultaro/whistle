# README #

Code repository for peak picking building neural networks 

Data used in modelling are from the DCLDE2011 

This repository contains functions and classes for creating neural networks for extracting peaks from spectrogram data
TonalClassV1 contains methods for reading binary files and creating target data (spectrogram with 0/1 output indicating the presence 
whistle energy)

datainputstream- required class for reading binary *.bin and *.ton files.

batch.py contains the batch_generator class which creates batches of features (spectrograms) and targets (binary outputs)
to feed to the neural networks. 

MODELS IN PROGRESS conatins code for building preliminary NN models. These are working examples, not finalised, and may or may not run. 
Feedback welcome.



