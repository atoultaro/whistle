#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 20 11:42:49 2018

@author: kristelle
"""
from keras.constraints import Constraint
import keras.backend as Kb

class Same(Constraint):
    """Weights are 4D [size1, size2,channels_number,kernels_number]
       Add a constraint for sharing the same weights across all channels
    """

    def __init__(self,axis=2):
        self.axis=2
        
    def __call__(self, w):    
        nb_channels=w.shape[self.axis]
        W=Kb.reshape(w[:,:,0,:],(Kb.shape(w)[0],Kb.shape(w)[1],1,Kb.shape(w)[3]))
        w=Kb.repeat_elements(W, nb_channels, self.axis)
        return w

    
