#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Extract features from Oswald's data

Created on 10/16/20
@author: atoultaro
"""
import warnings
warnings.filterwarnings('ignore', category=FutureWarning)

import os
import species_lib


# Oswald's data
deployment = ['STAR2000', 'STAR2003', 'STAR2006', 'HICEAS2002', 'PICEAS2005']
species_to_code = {'bottlenose': 'BD',
                   'longbeaked_common': 'LCD',
                   'shortbeaked_common': 'SCD',
                   # 'common': 'CD',
                   'striped': 'STR',
                   'spotted': 'SPT',
                   'spinner': 'SPIN',
                   'pilot': 'PLT',
                   'roughtoothed': 'RT',
                   'false_killer': 'FKW',
                   'melon-headed': 'MH', }

# species_list = ['BD', 'CD', 'STR', 'SPT', 'SPIN', 'PLT']  # six species
species_list = ['BD', 'LCD', 'SCD', 'STR', 'SPT', 'SPIN', 'PLT', 'RT', 'FKW']  # nine species

# species_id = {'BD': 0, 'CD': 1, 'STR': 2, 'SPT': 3, 'SPIN': 4, 'PLT': 5, 'NO': 6}
species_id = {'BD': 0, 'LCD': 1, 'SCD': 2, 'STR': 3, 'SPT': 4, 'SPIN': 5, 'PLT': 6, 'RT': 7, 'FKW': 8, 'NO': 9}
num_species = len(species_list) + 1  # noise included = 10

whistle_data_oswald = '/home/ys587/__Data/__whistle/__whistle_oswald'
work_path = '/home/ys587/__Data/__whistle/__domain_adaptation'
fea_type = 'pcen'
# fea_type = None

csv_oswald_sound = os.path.join(work_path, '__sound_info', 'oswald_encounter.csv')
csv_oswald_info = os.path.join(work_path, '__sound_info', 'oswald_soundinfo.csv')

df_sound_oswald, df_info_oswald = species_lib.df_sound_info_oswald(csv_oswald_sound, csv_oswald_info, species_to_code, whistle_data_oswald, deployment)
df_sound_oswald = df_sound_oswald[df_sound_oswald['species'].isin(species_list)]

# whistleness model
model_path = os.path.join(work_path, 'resnet34_expt_alldata_run0_f1/epoch_33_valloss_0.3726_valacc_0.9333.hdf5')

fea_out = os.path.join(work_path, '__feature_mat')
seltab_out = os.path.join(work_path, '__sound_seltab')
species_lib.extract_fea_oswald(df_sound_oswald, model_path,
                               fea_out,
                               seltab_out, fea_type=fea_type,
                               conf_whistle_thre_pos=0.9,
                               conf_whistle_thre_neg=0.4
                               )
