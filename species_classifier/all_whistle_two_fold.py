#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Two-fold cross-validation, given the features were extracted and placed in data
folders

Created on 6/9/20
@author: atoultaro
"""
import os
import glob
import sys
from math import floor, ceil

import numpy as np
import pandas as pd
import soundfile as sf
from sklearn.utils import shuffle
from keras.models import load_model
import librosa
import random

from species_classifier.driver_context_conv_audio_4fold import \
    all_data_train_validate, one_fold_validate
from cape_cod_whale.preprocess import bin_extract, contour_target_retrieve
from cape_cod_whale.classifier import metrics_two_fold

import species_lib

import warnings
warnings.filterwarnings('ignore', category=FutureWarning)


def main():
    curr_path = os.path.dirname(os.path.abspath(__file__))
    os.chdir(curr_path)

    flag_use_saved_fea = True

    # Oswald's data
    whistle_data = '/home/ys587/__Data/__whistle/__whistle_oswald'
    deployment = ['HICEAS2002', 'PICEAS2005', 'STAR2000', 'STAR2003', 'STAR2006']
    species_to_code = {'bottlenose': 'BD',
                       'longbeaked_common': 'CD',
                       'shortbeaked_common': 'CD',
                       'common': 'CD',
                       'striped': 'STR',
                       'spotted': 'SPT',
                       'spinner': 'SPIN',
                       'pilot': 'PLT',
                       'roughtoothed': 'RT',
                       'false_killer': 'FKW',
                       'melon-headed': 'MH',
                       }

    species_list = ['BD', 'MH', 'CD', 'STR', 'SPT', 'SPIN', 'PLT', 'RD', 'RT',
                    'WSD', 'FKW', 'BEL', 'KW', 'WBD', 'DUSK', 'FRA', 'PKW']  # remove LPLT, only have one file

    species_id = {'BD': 0, 'MH': 1, 'CD': 2, 'STR': 3, 'SPT': 4, 'SPIN': 5,
                  'PLT': 6, 'RD': 7, 'RT': 8, 'WSD': 9, 'FKW': 10, 'BEL': 11,
                  'KW': 12, 'WBD': 13, 'DUSK': 14, 'FRA': 15, 'PKW': 16,
                  'LPLT': 17, 'NO': 18}
    num_species = 18  # noise not included


    csv_oswald_sound = '/home/ys587/__Data/__whistle/__whistle_all/__sound_info/oswald_encounter.csv'
    csv_oswald_info = '/home/ys587/__Data/__whistle/__whistle_all/__sound_info/oswald_soundinfo.csv'
    df_sound_oswald, df_info_oswald = species_lib.df_sound_info_oswald(csv_oswald_sound, csv_oswald_info)

    # Gillispie's
    whistle_data = '/home/ys587/__Data/__whistle/__whistle_gillispie'
    csv_gillispie_sound = '/home/ys587/__Data/__whistle/__whistle_all/__sound_info/gillispie_encounter.csv'
    csv_gillispie_info = '/home/ys587/__Data/__whistle/__whistle_all/__sound_info/gillispie_soundinfo.csv'
    df_sound_gillispie, df_info_gillispie = species_lib.df_sound_info_gillispie\
        (csv_gillispie_sound, csv_gillispie_info, whistle_data=None)

    # DCLDE 2011, which has truth labels, hold on for the moment.
    whistle_data = '/home/ys587/__Data/__whistle/__sound_species'

    # whistle detection
    # setup
    model_path = '/home/ys587/__Data/__whistle/__log_dir_context/__new_results/__48k_fmin_3000_octave_3_binsperoctave_48_binstotal_144/resnet34_expt_all4pie_run0_f1'
    seltab_out = '/home/ys587/__Data/__whistle/__whistle_all/__sound_seltab'
    fea_out = '/home/ys587/__Data/__whistle/__whistle_all/__feature_mat'
    fea_part1_out = '/home/ys587/__Data/__whistle/__whistle_all/__feature_all/feature_label_part1.npz'
    fea_part2_out = '/home/ys587/__Data/__whistle/__whistle_all/__feature_all/feature_label_part2.npz'
    log_dir = "/home/ys587/__Data/__whistle/__log_dir_context/contour_temp_audio"

    conf = dict()
    conf['sample_rate'] = 48000
    conf['time_reso'] = 0.02
    conf['hop_length'] = int(conf['time_reso']*conf['sample_rate'])
    conf['win_size'] = 1.  # 1-s window
    conf['hop_size'] = 0.5
    conf['time_multi'] = floor(conf['win_size'] / conf['time_reso'])
    conf['time_multi_hop'] = floor(conf['hop_size'] / conf['time_reso'])
    conf['whistle_thre_min'] = 0.01  # for two-class classifier
    conf['whistle_thre_pos'] = 0.6
    conf['whistle_thre_neg'] = 0.1
    conf['contour_timethre'] = 20  # 0.4 s for dclde 2011
    conf['trained_class_num'] = 'two'

    # CQT
    conf['cqt_hop_size'] = int(conf['time_reso'] * conf['sample_rate'])
    conf['fmin'] = 3000.0
    conf['bins_per_octave'] = 48
    conf['n_bins'] = 144

    # CNN training
    conf['l2_regu'] = 0.01
    # conf['l2_regu'] = 0.001
    # conf['l2_regu'] = 0.2
    conf['dropout'] = 0.1
    conf['batch_size'] = 256
    # conf['batch_size'] = 32
    # conf['batch_size'] = 64
    conf['epoch'] = 200
    # conf['epoch'] = 10  # debug
    # conf['learning_rate'] = 1.0
    conf['learning_rate'] = 0.5
    conf['num_class'] = num_species

    conf['confusion_callback'] = False
    conf['spectro_dilation'] = False

    conf['numpy_data_output'] = True  # !!
    conf['numpy_data_use'] = not conf['numpy_data_output']
    conf['img_data_output'] = False  # output image of spectrogram data

    conf['IMG_T'] = conf['time_multi']
    conf['IMG_F'] = conf['n_bins']

    # model
    print('Loading the classifier')
    model_name = glob.glob(model_path + '/*.hdf5')[0]
    classifier_model = load_model(model_name)
    ######################################################
    if not flag_use_saved_fea:  # calculate features
        # Gillispie data detection & feature extraction
        # conf['whistle_thre_pos'] = 0.6
        # conf['whistle_thre_neg'] = 0.1

        for index, row in df_sound_gillispie.iterrows():
            # print('Species: ' + row['folder'])
            print('Species ' + str(index+1) + '/' + str(
                len(df_sound_gillispie)) + ': ' + row['folder'])

            wav_list = glob.glob(row['path'] + '/*.wav')
            wav_list.sort()
            whistle_time_start = []
            whistle_time_end = []
            whistle_score = []
            begin_path = []
            file_offset = []
            whistle_image_4d_pos_list = []
            whistle_image_4d_neg_list = []
            # for ww in [wav_list[0]]:
            for ww in wav_list:
                print(os.path.basename(ww))
                samples, _ = librosa.load(ww, sr=conf['sample_rate'])
                whistle_freq0 = np.abs(
                    librosa.pseudo_cqt(samples, sr=conf['sample_rate'],
                                       hop_length=conf['hop_length'],
                                       fmin=conf['fmin'],
                                       bins_per_octave=conf['bins_per_octave'],
                                       n_bins=conf['n_bins']))
                whistle_freq = np.zeros(whistle_freq0.shape)
                for rr in range(whistle_freq0.shape[0]):
                    whistle_freq[rr, :] = whistle_freq0[rr, :] - whistle_freq0[rr, :].mean()

                whistle_freq_list = []
                win_num = floor((whistle_freq.shape[1] - conf['time_multi']) / conf[
                    'time_multi_hop']) + 1  # 0.5s hop

                if win_num > 0:
                    for nn in range(win_num):
                        whistle_freq_curr = whistle_freq[:,
                                            nn * conf['time_multi_hop']:
                                            nn * conf['time_multi_hop'] + conf[
                                                'time_multi']]
                        whistle_freq_list.append(whistle_freq_curr)
                    if len(whistle_freq_list) >= 2:
                        whistle_image = np.stack(whistle_freq_list)
                    else:
                        whistle_image = np.expand_dims(whistle_freq_list[0], axis=0)
                    whistle_image_4d = np.expand_dims(whistle_image, axis=3)
                    predictions = classifier_model.predict(whistle_image_4d)

                    whistle_win_ind = \
                    np.where(predictions[:, 1] > conf['whistle_thre_min'])[0]
                    if whistle_win_ind.shape[0] >= 1:
                        # detected whistle start & end time
                        whistle_time_start_curr = whistle_win_ind * conf['hop_size']
                        whistle_time_start.append(whistle_time_start_curr)
                        whistle_time_end.append(
                            whistle_time_start_curr + conf['win_size'])
                        # detected whistle score
                        whistle_score.append(predictions[:, 1][whistle_win_ind])
                        begin_path.append([ww] * whistle_win_ind.shape[0])
                        file_offset.append(whistle_win_ind * conf['hop_size'])
                else:
                    continue

                # extract features here for both positive & negative classes
                whistle_win_ind_pos = \
                np.where(predictions[:, 1] > conf['whistle_thre_pos'])[0]
                whistle_win_ind_neg = \
                np.where(predictions[:, 1] < conf['whistle_thre_neg'])[0]
                fea_out_file = os.path.join(fea_out, row['species']+'_'+row['deployment']+'_'+os.path.splitext(os.path.basename(ww))[0]+'.npz')
                np.savez(fea_out_file, fea_pos=whistle_image_4d[whistle_win_ind_pos, :, :, :],
                         fea_neg=whistle_image_4d[whistle_win_ind_neg, :, :, :])

            # make sound selection table
            if len(whistle_time_start) >= 1:
                if len(whistle_time_start) >= 2:
                    whistle_time_start = np.concatenate(whistle_time_start)
                    whistle_time_end = np.concatenate(whistle_time_end)
                    whistle_score = np.concatenate(whistle_score)
                    begin_path = np.concatenate(begin_path)
                    file_offset = np.concatenate(file_offset)
                else:  # == 1
                    whistle_time_start = whistle_time_start[0]
                    whistle_time_end = whistle_time_end[0]
                    whistle_score = whistle_score[0]
                    begin_path = begin_path[0]
                    file_offset = file_offset[0]
                seltab_out_file = os.path.join(seltab_out, row['species'] + '_' + row[
                    'deployment'] + '.txt')
                species_lib.make_sound_sel_table(seltab_out_file, whistle_time_start,
                                     whistle_time_end, begin_path, file_offset,
                                     whistle_score, conf['whistle_thre_min'])

        # Oswald data detection & feature extraction
        # for index, row in df_sound_oswald.iterrows():
        # conf['whistle_thre_pos'] = 0.4
        # conf['whistle_thre_neg'] = 0.02
        # for index, row in df_sound_oswald[16:].iterrows():
        for index, row in df_sound_oswald.iterrows():
            print('Acoustic encounter '+str(index+1)+'/'+str(len(df_sound_oswald))+': '+row['folder'])

            wav_list = glob.glob(row['path']+'/*.wav')
            wav_list.sort()
            whistle_time_start = []
            whistle_time_end = []
            whistle_score = []
            begin_path = []
            file_offset = []
            whistle_image_4d_pos_list = []
            whistle_image_4d_neg_list = []

            # if row['encounter'] == 's132':
            # for ww in [wav_list[0]]:
            for ww in wav_list:
                print(os.path.basename(ww))
                # ww = os.path.join(row['path'], ww0)
                samples, _ = librosa.load(ww, sr=conf['sample_rate'])
                whistle_freq0 = np.abs(librosa.pseudo_cqt(samples, sr=conf['sample_rate'],
                                                         hop_length=conf['hop_length'],
                                                         fmin=conf['fmin'],
                                                         bins_per_octave=conf['bins_per_octave'],
                                                         n_bins=conf['n_bins']))
                whistle_freq = np.zeros(whistle_freq0.shape)
                for rr in range(whistle_freq0.shape[0]):
                    whistle_freq[rr, :] = whistle_freq0[rr, :] - whistle_freq0[
                                                                 rr, :].mean()

                whistle_freq_list = []
                win_num = floor( (whistle_freq.shape[1]-conf['time_multi'])
                                 / conf['time_multi_hop'])+1  # 0.5s hop

                if win_num > 0:
                    for nn in range(win_num):
                        whistle_freq_curr = whistle_freq[:, nn*conf['time_multi_hop']:
                                                            nn*conf['time_multi_hop']
                                                            +conf['time_multi']]
                        whistle_freq_list.append(whistle_freq_curr)
                    if len(whistle_freq_list) >= 2:
                        whistle_image = np.stack(whistle_freq_list)
                    else:
                        whistle_image = np.expand_dims(whistle_freq_list[0], axis=0)
                    whistle_image_4d = np.expand_dims(whistle_image, axis=3)
                    predictions = classifier_model.predict(whistle_image_4d)

                    whistle_win_ind = np.where(predictions[:, 1] > conf['whistle_thre_min'])[0]
                    if whistle_win_ind.shape[0] >= 1:
                        # detected whistle start & end time
                        whistle_time_start_curr  = whistle_win_ind * conf['hop_size']
                        whistle_time_start.append(whistle_time_start_curr)
                        whistle_time_end.append(whistle_time_start_curr + conf['win_size'])
                        # detected whistle score
                        whistle_score.append(predictions[:, 1][whistle_win_ind])
                        begin_path.append([ww]*whistle_win_ind.shape[0])
                        file_offset.append(whistle_win_ind * conf['hop_size'])
                else:
                    continue

                # extract features here for both positive & negative classes
                whistle_win_ind_pos = np.where(predictions[:, 1] > conf['whistle_thre_pos'])[0]
                whistle_win_ind_neg = np.where(predictions[:, 1] < conf['whistle_thre_neg'])[0]
                whistle_image_4d_pos_list.append(whistle_image_4d[whistle_win_ind_pos, :, :, :])
                whistle_image_4d_neg_list.append(whistle_image_4d[whistle_win_ind_neg, :, :, :])

            whistle_image_4d_pos = np.concatenate(whistle_image_4d_pos_list)
            whistle_image_4d_neg = np.concatenate(whistle_image_4d_neg_list)
            fea_out_file = os.path.join(fea_out, row['species'] + '_' + row[
                'deployment'] + '_' + row['encounter'] + '.npz')
            np.savez(fea_out_file, fea_pos=whistle_image_4d_pos, fea_neg=whistle_image_4d_neg)

            # make sound selection table
            if len(whistle_time_start) >= 1:
                if len(whistle_time_start) >= 2:
                    whistle_time_start = np.concatenate(whistle_time_start)
                    whistle_time_end= np.concatenate(whistle_time_end)
                    whistle_score = np.concatenate(whistle_score)
                    begin_path= np.concatenate(begin_path)
                    file_offset = np.concatenate(file_offset)
                else:  # == 1
                    whistle_time_start = whistle_time_start[0]
                    whistle_time_end = whistle_time_end[0]
                    whistle_score = whistle_score[0]
                    begin_path = begin_path[0]
                    file_offset = file_offset[0]
                seltab_out_file = os.path.join(seltab_out, row['species']+'_'+row['deployment']+'_'+row['encounter']+'.txt')
                species_lib.make_sound_sel_table(seltab_out_file, whistle_time_start,
                               whistle_time_end, begin_path, file_offset, whistle_score,
                               conf['whistle_thre_min'])

        # DCLDE 2011 data detection & feature extraction
        # Read species labels, filenames & extract time and frequency sequences
        # bin_dir = '/home/ys587/__Data/__whistle/__dclde2011_bin_file_test'
        bin_dir = '/home/ys587/__Data/__whistle/__dclde2011_bin_file'
        sound_dclde2011_dir = '/home/ys587/__Data/__whistle/__sound_species'
        species_name = ['bottlenose', 'common', 'spinner', 'melon-headed']

        for ss in species_name:
            contour_target, bin_wav_pair_curr = bin_extract(bin_dir, sound_dclde2011_dir, [ss])
            # read contours from bin files
            contour_target_list = contour_target_retrieve(contour_target,
                                                          bin_dir,
                                                          conf['time_reso'])
            whistle_image_list_pos = []
            whistle_image_list_neg = []
            for ff in range(len(contour_target_list)):
                filename = contour_target_list[ff][0]
                print('\n'+filename)
                # label_contour = contour_target_list[ff][1]
                file_contour = contour_target_list[ff][2]

                contour_target_ff, start_time, end_time, freq_low, freq_high = \
                    species_lib.contour_data(file_contour, conf['time_reso'])

                timesteps = ceil((end_time - start_time)/conf['time_reso'])+1
                print("Start time: "+str(start_time))
                print("Stop time: " + str(end_time))

                # spectrogram named whistle_freq for each file
                sound_path = os.path.join(sound_dclde2011_dir, ss, filename+'.wav')
                samples, _ = librosa.load(sound_path, sr=conf['sample_rate'], offset=start_time, duration=end_time-start_time+2.*conf['time_reso'])
                whistle_freq = np.abs(librosa.pseudo_cqt(samples,
                                                         sr=conf['sample_rate'],
                                                         hop_length=conf['cqt_hop_size'],
                                                         fmin=conf['fmin'],
                                                         bins_per_octave=conf['bins_per_octave'],
                                                         n_bins=conf['n_bins']))

                whistle_presence = np.zeros((int(timesteps)))
                for cc in contour_target_ff:
                    time_ind_start = int(floor((cc['Time'][0]-start_time)/conf['time_reso']))
                    for ii in range(cc['Time'].shape[0]):
                        whistle_presence[time_ind_start+ii] = 1.0

                # cut whistle_freq into segments for data samples
                size_time = int(conf['win_size']/conf['time_reso'])
                size_hop = int(conf['hop_size']/conf['time_reso'])
                for tt in range(floor((whistle_freq.shape[1]-size_time)/size_hop)):
                    whistle_image = whistle_freq[:, tt*size_hop:tt*size_hop+size_time]
                    whistle_presence_seg = whistle_presence[tt * size_hop:tt * size_hop + size_time]
                    if whistle_presence_seg.sum() >= conf['contour_timethre']:
                        whistle_image_list_pos.append(whistle_image)
                    elif whistle_presence_seg.sum() == 0.0:
                        whistle_image_list_neg.append(whistle_image)

                # whistle_image_pos = np.asarray(whistle_image_list_pos)
                if len(whistle_image_list_pos) >= 2:
                    whistle_image_pos = np.stack(whistle_image_list_pos)
                elif len(whistle_image_list_pos) == 1:
                    whistle_image_pos = np.expand_dims(whistle_image_list_pos[0], axis=0)
                else:
                    whistle_image_pos = np.zeros((conf['n_bins'], conf['time_multi'], 0))
                whistle_image_4d_pos = np.expand_dims(whistle_image_pos, axis=3)

                # whistle_image_neg = np.asarray(whistle_image_list_neg)
                if len(whistle_image_list_neg) >= 2:
                    whistle_image_neg = np.stack(whistle_image_list_neg)
                elif len(whistle_image_list_neg) == 1:
                    whistle_image_neg = np.expand_dims(whistle_image_list_neg[0], axis=0)
                else:
                    whistle_image_neg = np.zeros((conf['n_bins'], conf['time_multi'], 0))
                whistle_image_4d_neg = np.expand_dims(whistle_image_neg, axis=3)

                fea_out_file = os.path.join(fea_out, species_to_code[ss]+'_DCLDE2011_'+filename+'.npz')
                np.savez(fea_out_file, fea_pos=whistle_image_4d_pos, fea_neg=whistle_image_4d_neg)

    ######################################################
    # collect features & labels from multiple data sources
    print("Collecting features...")
    if os.path.exists(fea_part1_out) & os.path.exists(fea_part2_out):
        fea_part1_loadfile = np.load(fea_part1_out)
        fea_part1_4d = fea_part1_loadfile['fea_part1_4d']
        label_part1 = fea_part1_loadfile['label_part1']
        fea_part2_loadfile = np.load(fea_part2_out)
        fea_part2_4d = fea_part2_loadfile['fea_part2_4d']
        label_part2 = fea_part2_loadfile['label_part2']
    else:
        # collect all features
        species_fea_part1 = dict()
        species_fea_part2 = dict()
        for ss in species_list:
            print('Reading features from files ' + ss + ':')
            fea_species_list = glob.glob(os.path.join(fea_out, ss+'*.npz'))

            # split files in fea_species_list into training / validation data sets
            random.shuffle(fea_species_list)
            len_fea_files = int(ceil(len(fea_species_list)/2.))
            fea_species_part1_list = fea_species_list[:len_fea_files]
            fea_species_part2_list = fea_species_list[len_fea_files:]

            fea_pos_part1_list = []
            for ff in fea_species_part1_list:
                fea_curr = np.load(ff)
                fea_pos_part1_list.append(fea_curr['fea_pos'])
            if len(fea_pos_part1_list) >= 1:
                fea_pos_part1 = np.concatenate(fea_pos_part1_list, axis=0)
            elif len(fea_pos_part1_list) == 0:
                # fea_pos_part1 = []
                continue
            else:
                fea_pos_part1 = fea_pos_part1_list[0]
            species_fea_part1.update({ss: {'fea_pos': fea_pos_part1}})

            fea_pos_part2_list = []
            for ff in fea_species_part2_list:
                fea_curr = np.load(ff)
                fea_pos_part2_list.append(fea_curr['fea_pos'])
            if len(fea_pos_part2_list) >= 1:
                fea_pos_part2 = np.concatenate(fea_pos_part2_list, axis=0)
            elif len(fea_pos_part2_list) == 0:
                # fea_pos_part2 = []
                continue
            else:
                fea_pos_part2 = fea_pos_part2_list[0]
            species_fea_part2.update({ss: {'fea_pos': fea_pos_part2}})

        # combine features into part1 data sets
        print("combine features into part1 data sets")
        fea_part1_list = []
        label_part1_list = []
        for key, value in species_fea_part1.items():
            print('Combining features from '+key+':')
            fea_species = value['fea_pos']
            fea_part1_list.append(fea_species)
            label = [species_id[key]]*fea_species.shape[0]
            label_part1_list.append(label)
        fea_part1_4d = np.concatenate(fea_part1_list, axis=0)
        label_part1 = np.concatenate(label_part1_list, axis=0)
        del species_fea_part1
        del fea_part1_list
        del label_part1_list
        np.savez(fea_part1_out, fea_part1_4d=fea_part1_4d, label_part1=label_part1)

        # combine features into part2 data sets
        print("combine features into part2 data sets")
        fea_part2_list = []
        label_part2_list = []
        for key, value in species_fea_part2.items():
            print('Combining features from ' + key + ':')
            fea_species = value['fea_pos']
            fea_part2_list.append(fea_species)
            label = [species_id[key]] * fea_species.shape[0]
            label_part2_list.append(label)
        fea_part2_4d = np.concatenate(fea_part2_list, axis=0)
        label_part2 = np.concatenate(label_part2_list, axis=0)
        del species_fea_part2
        del fea_part2_list
        del label_part2_list
        np.savez(fea_part2_out, fea_part2_4d=fea_part2_4d,
                 label_part2=label_part2)

    # shuffle data
    print("Shuffle the features...")
    fea_part1_4d, label_part1 = shuffle(fea_part1_4d, label_part1,
                                        random_state=0)
    fea_part2_4d, label_part2 = shuffle(fea_part2_4d, label_part2,
                                        random_state=0)

    print("Classifier training...")
    conf = dict()
    # conf['log_dir'] = "/home/ys587/__Data/__whistle/__log_dir_context/contour_temp_audio"
    conf['save_dir'] = "/home/ys587/__Data/__whistle/__log_dir_context/audio_data_store_temp"
    conf['data_store'] = "/home/ys587/__Data/__whistle/__log_dir_context/audio_data_store/__four_class"
    # conf['bin_dir'] = bin_dir

    conf['species_name'] = species_list
    conf['species_id'] = species_id
    # conf['time_reso'] = 0.01  # 10 ms
    # conf['time_reso'] = 0.05  # 50 ms
    conf['time_reso'] = 0.02  # 20 ms

    # cepstral coefficient
    conf['sample_rate'] = 48000
    conf["num_class"] = len(species_list)

    conf['context_winsize'] = 1.0  # sec
    conf['context_hopsize'] = 0.5  # sec
    conf['contour_timethre'] = 10  # 0.2 s

    conf['fft_size'] = 4096
    conf['hop_size'] = int(conf['time_reso']*conf['sample_rate'])
    # audio
    conf['freq_ind_low'] = 0
    # conf['freq_ind_low'] = 20
    conf['freq_ind_high'] = 144

    conf['IMG_T'] = int(floor((conf['context_winsize'] / conf['time_reso'])))
    conf['IMG_F'] = conf['freq_ind_high'] - conf['freq_ind_low']
    conf['input_shape'] = (conf['IMG_F'], conf['IMG_T'], 1)

    conf['l2_regu'] = 0.01
    # conf['l2_regu'] = 0.001
    # conf['l2_regu'] = 0.2
    conf['dropout'] = 0.1
    # conf['batch_size'] = 128  # lstm_2lay
    # conf['batch_size'] = 32  # resnet 18, 34
    conf['batch_size'] = 64
    conf['epoch'] = 200
    # conf['epoch'] = 1  # debug
    conf['learning_rate'] = 1.0
    pie_num = 4
    conf['pie_num'] = pie_num

    conf['confusion_callback'] = False
    conf['spectro_dilation'] = False

    conf['numpy_data_output'] = False  # !!
    conf['numpy_data_use'] = not conf['numpy_data_output']
    conf['img_data_output'] = False  # output image of spectrogram data

    # add one more class 'noise': 4 species class + 1 noise class
    conf['class_noise'] = True  # add the fifth class: noise
    conf['input_shape'] = (conf['IMG_F'], conf['IMG_T'], 1)

    model_type = 'resnet34_expt'
    for rr in range(5):
        proj_name = model_type+'_os_vs_gilli_' + str(rr) + '_f1'
        print(proj_name)

        conf['log_dir'] = os.path.join(log_dir, proj_name)
        if not os.path.exists(conf['log_dir']):
            os.mkdir(conf['log_dir'])
        conf['num_filters'] = 16

        y_pred2, y_pred2_prob, best_model_path = one_fold_validate(
            model_type, fea_part1_4d, label_part1,
            fea_part2_4d, label_part2, conf, fold_id=1)

        y_pred1, y_pred1_prob, best_model_path = one_fold_validate(
            model_type, fea_part2_4d, label_part2,
            fea_part1_4d, label_part1, conf, fold_id=2)

        y_pred_tot = np.concatenate((y_pred1, y_pred2))
        label_total = label_part1.tolist() + label_part2.tolist()
        metrics_two_fold(label_total, y_pred_tot, conf['log_dir'],
                         'accuracy_total.txt', conf, mode='total')

    # for rr in range(2, 4):
    #     model_type = 'resnet34_expt'
    #     proj_name = model_type + '_mixdata_run' + str(rr)
    #     print(proj_name)
    #
    #     conf['log_dir'] = os.path.join(log_dir, proj_name)
    #     if not os.path.exists(conf['log_dir']):
    #         os.mkdir(conf['log_dir'])
    #     conf['num_filters'] = 16
    #     learning_rate_original = 0.5
    #     conf['learning_rate'] = learning_rate_original/(2.0**float(rr))
    #     # conf['learning_rate'] = conf['learning_rate'] / 2.0  # start with 0.5
    #
    #     best_model_path = all_data_train_validate(model_type, fea_train_4d_shuf,
    #                                               label_train_shuf, conf,
    #                                               conf['log_dir'])


if __name__ == "__main__":
    main()





