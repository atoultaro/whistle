#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Created on 6/4/20
@author: atoultaro
"""

import os
import glob
from math import floor, ceil
import numpy as np
import warnings
warnings.filterwarnings("ignore")
import librosa
from keras.models import load_model

from species_classifier.all_whistle_training import make_sound_sel_table

# detection on multi-channel sound
# sound_path = '/mnt/DCLDE/noaa-pifsc-bioacoustic'
sound_path = '/home/ys587/__Data/__whistle/__whistle_gillispie'
deployment = ['48kHz', '96kHz']

species_list = ['BD', 'MH', 'CD', 'STR', 'SPT', 'SPIN', 'PLT', 'RD', 'RT',
                'WSD', 'FKW', 'BEL', 'KW', 'WBD', 'DUSK', 'FRA', 'PKW', 'LPLT']
species_id = {'BD': 0, 'MH': 1, 'CD': 2, 'STR': 3, 'SPT': 4, 'SPIN': 5,
              'PLT': 6, 'RD': 7, 'RT': 8, 'WSD': 9, 'FKW': 10, 'BEL': 11,
              'KW': 12, 'WBD': 13, 'DUSK': 14, 'FRA': 15, 'PKW': 16,
              'LPLT': 17, 'NO': 18}
num_species = 18  # noise not included

dclde2020_outpath = '/home/ys587/__Data/__whistle/__whistle_dclde2020'

# trained model
model_detection_path = os.path.join(dclde2020_outpath, '__trained_model/detection/epoch_160_valloss_0.3137_valacc_0.9483.hdf5')
model_classification_path = os.path.join(dclde2020_outpath, '__trained_model/classification/epoch_160_valloss_0.1101_valacc_0.9815.hdf5')
model_detector = load_model(model_detection_path)
model_classifier = load_model(model_classification_path)

# seltab output
# seltab_out_path = os.path.join(dclde2020_outpath, '__seltab_out')
seltab_out_path = '/home/ys587/__Data/__whistle/__whistle_all/__validation_seltab'

conf = dict()
conf['sample_rate'] = 48000
conf['time_reso'] = 0.02
conf['hop_length'] = int(conf['time_reso']*conf['sample_rate'])
conf['shift_length'] = int(conf['hop_length']/2.)  # shift from 0. Used for validation of shift variance
conf['win_size'] = 1.  # 1-s window
conf['hop_size'] = 0.5
conf['time_multi'] = floor(conf['win_size'] / conf['time_reso'])
conf['time_multi_hop'] = floor(conf['hop_size'] / conf['time_reso'])
conf['whistle_thre_min'] = 0.01  # for two-class classifier
conf['whistle_thre_pos'] = 0.6  # used for training
# conf['whistle_thre_pos'] = 0.4
conf['whistle_thre_neg'] = 0.1
conf['contour_timethre'] = 20  # 0.4 s for dclde 2011

conf['trained_class_num'] = 'two'

# CQT
conf['cqt_hop_size'] = int(conf['time_reso'] * conf['sample_rate'])
conf['fmin'] = 3000.0
conf['bins_per_octave'] = 48
conf['n_bins'] = 144

for dd in deployment:
    sound_target = os.path.join(sound_path, dd)
    species_dir = os.listdir(sound_target)
    for ss in species_dir:
        wav_files = glob.glob(os.path.join(sound_target, ss+'/*.wav'))
        wav_files.sort()

        whistle_time_start = []
        whistle_time_end = []
        whistle_score = []
        begin_path = []
        file_offset = []
        chan_id = []
        class_id = []
        for ww in wav_files:
        # for ww in [wav_files[0]]:
            ww_basename = os.path.basename(ww)
            print(ww_basename)

            samples, _ = librosa.load(ww, sr=conf['sample_rate'], mono=False)

            for cc in range(samples.shape[0]):
                print('channel: '+str(cc))
                # samples_chan = samples[cc, :]
                samples_chan = samples[cc, conf['shift_length']:]
                whistle_freq0 = np.abs(
                    librosa.pseudo_cqt(samples_chan, sr=conf['sample_rate'],
                                       hop_length=conf['hop_length'],
                                       fmin=conf['fmin'],
                                       bins_per_octave=conf['bins_per_octave'],
                                       n_bins=conf['n_bins']))
                whistle_freq = np.zeros(whistle_freq0.shape)
                for rr in range(whistle_freq0.shape[0]):
                    whistle_freq[rr, :] = whistle_freq0[rr, :] - whistle_freq0[rr, :].mean()

                whistle_freq_list = []
                win_num = floor((whistle_freq.shape[1] - conf['time_multi']) / conf[
                    'time_multi_hop']) + 1  # 0.5s hop

                for nn in range(win_num):
                    whistle_freq_curr = whistle_freq[:,
                                        nn * conf['time_multi_hop']:
                                        nn * conf['time_multi_hop'] + conf[
                                            'time_multi']]
                    whistle_freq_list.append(whistle_freq_curr)

                if len(whistle_freq_list) >= 2:
                    whistle_image = np.stack(whistle_freq_list)
                else:
                    whistle_image = np.expand_dims(whistle_freq_list[0], axis=0)
                whistle_image_4d = np.expand_dims(whistle_image, axis=3)
                predictions_detection = model_detector.predict(whistle_image_4d)
                predictions_classification = model_classifier.predict(whistle_image_4d)  # <<==
                pred_class = np.argmax(predictions_classification, axis=1)

                whistle_win_ind = \
                    np.where(predictions_detection[:, 1] > conf['whistle_thre_min'])[0]
                if whistle_win_ind.shape[0] >= 1:
                    # detected whistle start & end time
                    whistle_time_start_curr = whistle_win_ind * conf['hop_size']
                    whistle_time_start.append(whistle_time_start_curr)
                    whistle_time_end.append(
                        whistle_time_start_curr + conf['win_size'])
                    # detected whistle score
                    whistle_score.append(predictions_detection[:, 1][whistle_win_ind])
                    begin_path.append([ww] * whistle_win_ind.shape[0])
                    file_offset.append(whistle_win_ind * conf['hop_size'])
                    chan_id.append([cc+1] * whistle_win_ind.shape[0])
                    class_id.append(pred_class[whistle_win_ind])

                    # consider put scores from multi-species later
        # make sound selection tables
        if len(whistle_time_start) != 0:
            whistle_time_start = np.concatenate(whistle_time_start)
            whistle_time_end = np.concatenate(whistle_time_end)
            whistle_score = np.concatenate(whistle_score)
            begin_path = np.concatenate(begin_path)
            file_offset = np.concatenate(file_offset)
            chan_id = np.concatenate(chan_id)
            class_id = np.concatenate(class_id)
            seltab_out_file = ss+'.txt'
            make_sound_sel_table(os.path.join(seltab_out_path,seltab_out_file), whistle_time_start,
                                     whistle_time_end, begin_path, file_offset,
                                     whistle_score, conf['whistle_thre_min'],
                                 chan=chan_id, class_id=class_id)
