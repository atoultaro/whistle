#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Peoduce dataset for Whistle Siamese Network
Created on 8/6/20
@author: atoultaro
"""
import os
import species_lib
import numpy as np


def npz_add_label_dataset(fea_part0_out_path, label_dataset_part0):
    fea_part0_loadfile = np.load(fea_part0_out_path)
    fea_part0_4d = fea_part0_loadfile['fea_part_4d']
    label_part0 = fea_part0_loadfile['label_part']
    np.savez(fea_part0_out_path, fea_part_4d=fea_part0_4d, label_part=label_part0,
             label_dataset_part=label_dataset_part0)
    return fea_part0_4d, label_part0, label_dataset_part0

species_list = ['BD', 'CD', 'STR', 'SPT', 'SPIN', 'PLT']  # six species
species_id = {'BD': 0, 'CD': 1, 'STR': 2, 'SPT': 3, 'SPIN': 4, 'PLT': 5}

work_place_path = '/home/ys587/__Data/__whistle/__siamese'
fea_all_out = os.path.join(work_place_path, '__feature_all')
fea_mat_path = os.path.join(work_place_path, '__pcen_nopulse_jul24')
fea_part2_out_path = os.path.join(fea_all_out, 'feature_label_gillispie.npz')
fea_part1_out_path = os.path.join(fea_all_out, 'feature_label_oswald.npz')

# gillispie
species_fea_part2 = species_lib.read_features_from_files(os.path.join(fea_mat_path, '__gillispie'), species_list)
del species_fea_part2['NO']  # remove noise class
fea_part2_4d, label_part2 = species_lib.combine_features_from_dict(species_fea_part2, fea_part2_out_path, species_id)
label_dataset_part2 = np.ones(label_part2.shape)  # label 1 for Gillispie data

_, _, _ = npz_add_label_dataset(fea_part2_out_path, label_dataset_part2)

# oswald
species_fea_part1 = species_lib.read_features_from_files(os.path.join(fea_mat_path, '__oswald'), species_list)
del species_fea_part1['NO']  # remove noise class
fea_part1_4d, label_part1 = species_lib.combine_features_from_dict(species_fea_part1, fea_part1_out_path, species_id)
label_dataset_part1 = np.ones(label_part1.shape)  # label 1 for Gillispie data

_, _, _ = npz_add_label_dataset(fea_part1_out_path, label_dataset_part1)


# fea_part2_loadfile = np.load(fea_part2_out)
# fea_part2_4d = fea_part2_loadfile['fea_part_4d']
# label_part2 = fea_part2_loadfile['label_part']
# np.savez(fea_part2_out, fea_part_4d=fea_part2_4d,
#          label_part=label_part2,
#          label_dataset_part=label_dataset_part2,
#          )
