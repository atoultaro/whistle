#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Created on 6/4/20
@author: atoultaro
"""
import os
import numpy as np
from keras.models import load_model
from sklearn.metrics import confusion_matrix
from sklearn.utils.multiclass import unique_labels
import matplotlib.pyplot as plt
import warnings
warnings.filterwarnings("ignore")

species_list = ['BD', 'MH', 'CD', 'STR', 'SPT', 'SPIN', 'PLT', 'RD', 'RT',
                    'WSD', 'FKW', 'BEL', 'KW', 'WBD', 'DUSK', 'FRA', 'PKW', 'LPLT']

# data: features & labels
fea_all_out = '/home/ys587/__Data/__whistle/__whistle_all/__feature_all/feature_label_all.npz'
fea_all_loadfile = np.load(fea_all_out)
fea_train_4d = fea_all_loadfile['fea_train_4d']
label_train = fea_all_loadfile['label_train']

dclde2020_outpath = '/home/ys587/__Data/__whistle/__whistle_dclde2020'
model_classification_path = os.path.join(dclde2020_outpath, '__trained_model/classification/epoch_160_valloss_0.1101_valacc_0.9815.hdf5')

model = load_model(model_classification_path)
y_pred_score = model.predict(fea_train_4d)
y_pred_class = np.argmax(y_pred_score, axis=1)

conf_mat = confusion_matrix(label_train, y_pred_class)


def plot_confusion_matrix(y_true, y_pred, classes,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'

    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    # Only use the labels that appear in the data
    # classes = classes[unique_labels(y_true, y_pred)]
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    fig, ax = plt.subplots(figsize=[15, 15])
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    return ax


np.set_printoptions(precision=2)

# Plot non-normalized confusion matrix
plot_confusion_matrix(label_train, y_pred_class, classes=species_list,
                      title='Confusion matrix, without normalization')

# Plot normalized confusion matrix
plot_confusion_matrix(label_train, y_pred_class, classes=species_list, normalize=True,
                      title='Normalized confusion matrix')

plt.show()