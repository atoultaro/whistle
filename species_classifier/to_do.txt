20200809
0. triplet-loss-teras!
1. long > 2.5 s or longer > 5 s
2. manual select template and apply t-sne?
3. LSTM-base Siamese network
4. features: delta spectrogram; remove PCEN
5. how to select triplet

20200722
1. split sounds in gillispie & oswald into two parts, respectively. Thus, total will be 4 parts. Apply extract_fea_oswald and extract_fea_gillspie twice each

2. generate two seltabs: one for pos and one for neg, instead of one. Remove conf_whistle_thre_pos_seltab & conf_whistle_thre_neg_seltab. Use conf_whistle_thre_pos & conf_whistle_thre_neg instead.

3. t-SNE & UMAP

4. Use Talos hyper parameters or keras-tuner

20200625

*How to measure whistleness
whistleness: apply a trained binary classifier to pick up whislte-presense windows

1 sec too short => 3 sec long

features:
DONE powerlaw, PCEN, and new one that removes echolocation

DONE powerlaw is a unit length => fail to train! Why? The need for hyperparameter tuning?

Need to retrain binary classifier with added info

DONE noise class

train with 2D + 1D

lcoal structure: whislte + promising

autoencoder for noise, for signal and for combination


20200622
larger window 5 sec through whistle_win_ind_pos
generate an list annotating the whistleness length (from 1 sec to 5 sec)

power-law: Done!

Between two data sets:
transfer learning
mix oswald & gillispie data
t-SNE showing a spcefic from two data sets

Expt later:
conf_whistle_thre_pos 0.6 too small; try 0.8

===============
OLD TASKS

DONE, 2-fold cross-validation.
Gillispie's data need to have a feature file for each sound file

DONE, understand the result of experiment with 0.25 sec shift.

DONE, build metadata tables for every data point and store dataset name, deployment name. Save waveform.

* how to use gps data? Can I draw a boat?

* run predictions on all dclde 2020



Date: May 17 2017

@jfbercher created a nbextension variable inspector. The source code can be seen here jupyter_contrib_nbextensions. For more information see the docs.

Install
User
pip install jupyter_contrib_nbextensions
jupyter contrib nbextension install --user
Virtual environment
pip install jupyter_contrib_nbextensions
jupyter contrib nbextension install --sys-prefix
Enable
jupyter nbextension enable varInspector/main