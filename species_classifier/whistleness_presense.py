"""

Created on 6/29/20
@author: atoultaro
"""
import os
import glob
from math import floor

from scipy.stats import entropy
import numpy as np
import librosa

# snd_seltab = '/home/ys587/__Data/__whistle/__cv_os_vs_gilli/__sound_seltab/STR_96kHz.txt'
sound_species_target = '/home/ys587/__Data/__whistle/__whistle_gillispie/96kHz/STR'

# species_lib.extract_fea_gillispie(df_sound_gillispie, model_path,
#                                       os.path.join(fea_out, '__gillispie'),
#                                       seltab_out, fea_type)

conf_samplerate = 48000
conf_hop_length = int(0.02*48000)
conf_fmin = 3000.0
conf_bins_per_octave = 48
conf_n_bins = 144
conf_time_multi = floor(1. / 0.02)  # floor(conf['win_size'] / conf['time_reso'])
conf_time_multi_hop = floor(0.5 / 0.02)  # floor(conf['hop_size'] / conf['time_reso'])

wav_list = glob.glob(sound_species_target + '/*.wav')
wav_list.sort()
wav_list = ['/home/ys587/__Data/__whistle/__whistle_gillispie/96kHz/STR/STR11.wav']

# classifier_model = load_model(model_name)
for ww in wav_list:
    print(os.path.basename(ww))
    samples, _ = librosa.load(ww, sr=conf_samplerate)
    whistle_freq = np.abs(librosa.pseudo_cqt(samples, sr=conf_samplerate,
                                             hop_length=conf_hop_length,
                                             fmin=conf_fmin,
                                             bins_per_octave=conf_bins_per_octave,
                                             n_bins=conf_n_bins))

    whistle_freq = whistle_freq[72:, :]

    win_num = floor((whistle_freq.shape[1] - conf_time_multi) / conf_time_multi_hop)  # 0.5s hop

    # whistle_freq_list = []
    if win_num > 0:
        entropy_val = np.zeros(win_num)
        for nn in range(win_num):
            print(nn)
            whistle_freq_curr = whistle_freq[:, nn * conf_time_multi_hop:
                                                nn * conf_time_multi_hop + conf_time_multi]
            entropy_val[nn] = entropy(whistle_freq_curr.mean(axis=1))


    #         whistle_freq_list.append(whistle_freq_curr)
    #     if len(whistle_freq_list) >= 2:
    #         whistle_image = np.stack(whistle_freq_list)
    #     else:
    #         whistle_image = np.expand_dims(whistle_freq_list[0], axis=0)
    #     whistle_image_4d = np.expand_dims(whistle_image, axis=3)
    #
    #     # predictions = classifier_model.predict(whistle_image_4d)

