#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Training a classifier by all three whistle data sources, including Oswald,
Gillispie & DCLDE2011.

Created on 5/4/20
@author: atoultaro
"""
import os
import glob
import sys
from math import floor, ceil

import numpy as np
import pandas as pd
import soundfile as sf
from sklearn.utils import shuffle
from keras.models import load_model
import librosa

from species_classifier.driver_context_conv_audio_4fold import \
    all_data_train_validate
from cape_cod_whale.preprocess import bin_extract, contour_target_retrieve

curr_path = os.path.dirname(__file__)
os.chdir(curr_path)


def sound_file_info(df_target):
    ''' Get sound information
    '''
    print('Retrieving sound information...')
    sound_file = []
    sound_samplerate = []
    sound_duration = []
    sound_channels = []
    sound_format = []
    sound_subtype = []
    for index, row in df_target.iterrows():
        print(row['path'])
        filelist_curr = glob.glob(row['path']+'/*.wav')
        try:
            for ff in filelist_curr:
                sound_file.append(ff)
                soundinfo = sf.info(ff)
                sound_samplerate.append(soundinfo.samplerate)
                sound_duration.append(soundinfo.duration)
                sound_channels.append(soundinfo.channels)
                sound_format.append(soundinfo.format)
                sound_subtype.append(soundinfo.subtype)
        except IOError:
            sys.exit('File '+filelist_curr[0]+' does not exist.')
        except IndexError:
            print(row['path']+' is empty.')
            continue

    df_target_info = pd.DataFrame(list(zip(sound_file, sound_samplerate,
                                   sound_duration,
                                   sound_channels,
                                   sound_format,
                                   sound_subtype)), columns=['file',
                                                            'samplerate',
                                                            'duration',
                                                            'channels',
                                                            'format',
                                                            'subtype'])

    return df_target_info


def make_sound_sel_table(seltab_output_path, begin_time, end_time, begin_path,
                         file_offset, score_arr, score_thre, chan=None,
                         class_id=None):
    assert(begin_time.shape[0] == score_arr.shape[0])
    event_num = score_arr.shape[0]

    data_dict = {
        'Selection': [ii+1 for ii in range(event_num)],
        'View': ['Spectrogram 1']*event_num,

        'Begin Time (s)': np.around(begin_time, decimals=2),
        'End Time (s)': np.around(end_time, decimals=2),
        'Low Freq (Hz)': [3000.0]*event_num,
        'High Freq (Hz)': [22000.0]*event_num,
        'Score': np.around(score_arr, decimals=4),
        'Score Thre': np.repeat(np.around(score_thre, decimals=3), event_num),
        'Begin Path': begin_path,
        'File Offset': file_offset
    }
    if chan is None:
        data_dict.update({'Channel': [1] * event_num})
    else:
        data_dict.update({'Channel': chan})
    if class_id is not None:
        data_dict.update({'Class_id': class_id})
        df_seltab0 = pd.DataFrame.from_dict(data_dict)
        col_name = ['Selection', 'View', 'Channel', 'Begin Time (s)',
                    'End Time (s)', 'Low Freq (Hz)', 'High Freq (Hz)', 'Score',
                    'Score Thre', 'Begin Path', 'File Offset', 'Class_id']
    else:
        df_seltab0 = pd.DataFrame.from_dict(data_dict)
        col_name = ['Selection', 'View', 'Channel', 'Begin Time (s)',
                    'End Time (s)', 'Low Freq (Hz)', 'High Freq (Hz)', 'Score',
                    'Score Thre', 'Begin Path', 'File Offset']
    # sort columns by the order of Raven's selection table
    df_seltab = df_seltab0[col_name]
    # sort rows by first, Begin Path and then, Begin
    # df_seltab = df_seltab.sort_values(by=['Begin Path', 'File Offset'])
    df_seltab = df_seltab.sort_values(by=['File Offset'])
    # df_seltab.update(pd.Series([ii+1 for ii in range(event_num)], name='Selection'))


    # write out selection table
    df_seltab.to_csv(seltab_output_path, sep='\t', mode='a', index=False)


def contour_data(file_contour, time_reso):
    print('Retrieving contours...')
    contour_target_ff = []
    len_contour = len(file_contour)
    print('len_contour: '+str(len_contour))
    time_min = 86400.0
    time_max = 0.0
    freq_high = 0.0
    freq_low = 192000.0
    # read contours into the var contour_target_ff
    for cc in range(len_contour):
        time_contour = file_contour[cc]['Time']
        freq_contour = file_contour[cc]['Freq']

        if time_contour.shape[0] > 1:
            new_start_time = round(time_contour[0]/time_reso)*time_reso
            new_step = ceil((time_contour[-1] - time_contour[0])/time_reso)
            time_contour_interp = np.arange(new_start_time, new_start_time+new_step*time_reso, time_reso)

            time_min = np.min((time_contour_interp[0], time_min))
            time_max = np.max((time_contour_interp[-1], time_max))

            freq_contour_interp = np.interp(time_contour_interp, time_contour,
                                            freq_contour)
            freq_high = np.max((np.max(freq_contour_interp), freq_high))
            freq_low = np.min((np.min(freq_contour_interp), freq_low))

            contour_target_ff_cc = dict()
            contour_target_ff_cc['Time'] = time_contour_interp
            contour_target_ff_cc['Freq'] = freq_contour_interp

            contour_target_ff.append(contour_target_ff_cc)

    return contour_target_ff, time_min, time_max, freq_low, freq_high


def main():
    curr_path = os.path.abspath('')
    os.chdir(curr_path)

    # Oswald's data
    whistle_data = '/home/ys587/__Data/__whistle/__whistle_oswald'
    deployment = ['HICEAS2002', 'PICEAS2005', 'STAR2000', 'STAR2003', 'STAR2006']
    species_to_code = {'bottlenose': 'BD',
                       'longbeaked_common': 'CD',
                       'shortbeaked_common': 'CD',
                       'common': 'CD',
                       'striped': 'STR',
                       'spotted': 'SPT',
                       'spinner': 'SPIN',
                       'pilot': 'PLT',
                       'roughtoothed': 'RT',
                       'false_killer': 'FKW',
                       'melon-headed': 'MH',
                       }

    species_list = ['BD', 'MH', 'CD', 'STR', 'SPT', 'SPIN', 'PLT', 'RD', 'RT',
                    'WSD', 'FKW', 'BEL', 'KW', 'WBD', 'DUSK', 'FRA', 'PKW', 'LPLT']
    species_id = {'BD': 0, 'MH': 1, 'CD': 2, 'STR': 3, 'SPT': 4, 'SPIN': 5,
                  'PLT': 6, 'RD': 7, 'RT': 8, 'WSD': 9, 'FKW': 10, 'BEL': 11,
                  'KW': 12, 'WBD': 13, 'DUSK': 14, 'FRA': 15, 'PKW': 16,
                  'LPLT': 17, 'NO': 18}
    num_species = 18  # noise not included

    csv_oswald_sound = '/home/ys587/__Data/__whistle/__whistle_all/__sound_info/oswald_encounter.csv'
    csv_oswald_info = '/home/ys587/__Data/__whistle/__whistle_all/__sound_info/oswald_soundinfo.csv'
    if os.path.exists(csv_oswald_sound) & os.path.exists(csv_oswald_info):
        df_sound_oswald = pd.read_csv(csv_oswald_sound)
        df_info_oswald = pd.read_csv(csv_oswald_info)
    else:
        data_raw = []
        for dd in deployment:
            print(dd)
            deploy_folder = os.path.join(whistle_data, dd)
            folder_namelist = os.listdir(deploy_folder)
            for ff in folder_namelist:
                print(ff)
                data_raw.append([dd, ff, os.path.join(whistle_data, dd, ff)])
        df_sound_oswald = pd.DataFrame(data_raw, columns=['deployment', 'folder', 'path'])
        # ger species name
        df_sound_oswald['species_name'] = df_sound_oswald['folder'].str.extract(r'([a-zA-Z_]*)\s')
        # get encounter name
        df_sound_oswald['encounter'] = df_sound_oswald['folder'].str.extract(r'\s([a-zA-Z]\d+)')
        # species to code
        df_sound_oswald['species'] = df_sound_oswald['species_name'].apply(lambda x: species_to_code[x])

        df_info_oswald = sound_file_info(df_sound_oswald)

        df_sound_oswald.to_csv(csv_oswald_sound, index=False)
        df_info_oswald.to_csv(csv_oswald_info, index=False)

    # Gillispie's
    whistle_data = '/home/ys587/__Data/__whistle/__whistle_gillispie'
    csv_gillispie_sound = '/home/ys587/__Data/__whistle/__whistle_all/__sound_info/gillispie_encounter.csv'
    csv_gillispie_info = '/home/ys587/__Data/__whistle/__whistle_all/__sound_info/gillispie_soundinfo.csv'
    if os.path.exists(csv_gillispie_sound) & os.path.exists(csv_gillispie_info):
        df_sound_gillispie = pd.read_csv(csv_gillispie_sound)
        df_info_gillispie = pd.read_csv(csv_gillispie_info)
    else:
        # 48 kHz
        folder_namelist = os.listdir(os.path.join(whistle_data, '48kHz'))
        data_raw = []
        for ff in folder_namelist:
            data_raw.append(['48kHz', ff, os.path.join(whistle_data, '48kHz', ff)])
        # 96 kHz
        folder_namelist = os.listdir(os.path.join(whistle_data, '96kHz'))
        for ff in folder_namelist:
            data_raw.append(['96kHz', ff, os.path.join(whistle_data, '96kHz', ff)])
        df_sound_gillispie = pd.DataFrame(data_raw, columns=['deployment', 'folder', 'path'])
        df_sound_gillispie['species'] = df_sound_gillispie['folder']

        df_info_gillispie = sound_file_info(df_sound_gillispie)

        df_sound_gillispie.to_csv('./gillispie_encounter.csv', index=False)
        df_info_gillispie.to_csv('./gillispie_soundinfo.csv', index=False)


    # DCLDE 2011, which has truth labels, hold on for the moment.
    whistle_data = '/home/ys587/__Data/__whistle/__sound_species'

    # whistle detection
    # setup
    model_path = '/home/ys587/__Data/__whistle/__log_dir_context/__new_results/__48k_fmin_3000_octave_3_binsperoctave_48_binstotal_144/resnet34_expt_all4pie_run0_f1'
    seltab_out = '/home/ys587/__Data/__whistle/__whistle_all/__sound_seltab'
    fea_out = '/home/ys587/__Data/__whistle/__whistle_all/__feature_mat'
    fea_all_out = '/home/ys587/__Data/__whistle/__whistle_all/__feature_all/feature_label_all.npz'
    log_dir = "/home/ys587/__Data/__whistle/__log_dir_context/contour_temp_audio"
    flag_use_saved_fea = True

    conf = dict()
    conf['sample_rate'] = 48000
    conf['time_reso'] = 0.02
    conf['hop_length'] = int(conf['time_reso']*conf['sample_rate'])
    conf['win_size'] = 1.  # 1-s window
    conf['hop_size'] = 0.5
    conf['time_multi'] = floor(conf['win_size'] / conf['time_reso'])
    conf['time_multi_hop'] = floor(conf['hop_size'] / conf['time_reso'])
    conf['whistle_thre_min'] = 0.01  # for two-class classifier
    conf['whistle_thre_pos'] = 0.6
    conf['whistle_thre_neg'] = 0.1
    conf['contour_timethre'] = 20  # 0.4 s for dclde 2011
    conf['trained_class_num'] = 'two'

    # CQT
    conf['cqt_hop_size'] = int(conf['time_reso'] * conf['sample_rate'])
    conf['fmin'] = 3000.0
    conf['bins_per_octave'] = 48
    conf['n_bins'] = 144

    # CNN training
    conf['l2_regu'] = 0.01
    # conf['l2_regu'] = 0.001
    # conf['l2_regu'] = 0.2
    conf['dropout'] = 0.1
    conf['batch_size'] = 256
    # conf['batch_size'] = 32
    # conf['batch_size'] = 64
    conf['epoch'] = 200
    # conf['epoch'] = 10  # debug
    # conf['learning_rate'] = 1.0
    conf['learning_rate'] = 0.5
    conf['num_class'] = num_species

    conf['confusion_callback'] = False
    conf['spectro_dilation'] = False

    conf['numpy_data_output'] = True  # !!
    conf['numpy_data_use'] = not conf['numpy_data_output']
    conf['img_data_output'] = False  # output image of spectrogram data

    conf['IMG_T'] = conf['time_multi']
    conf['IMG_F'] = conf['n_bins']

    # model
    print('Loading the classifier')
    model_name = glob.glob(model_path + '/*.hdf5')[0]
    classifier_model = load_model(model_name)

    ######################################################
    if not flag_use_saved_fea:  # calculate features
        # Gillispie data detection & feature extraction
        # conf['whistle_thre_pos'] = 0.6
        # conf['whistle_thre_neg'] = 0.1

        for index, row in df_sound_gillispie.iterrows():
            # print('Species: ' + row['folder'])
            print('Species ' + str(index+1) + '/' + str(
                len(df_sound_gillispie)) + ': ' + row['folder'])

            wav_list = glob.glob(row['path'] + '/*.wav')
            wav_list.sort()
            whistle_time_start = []
            whistle_time_end = []
            whistle_score = []
            begin_path = []
            file_offset = []
            whistle_image_4d_pos_list = []
            whistle_image_4d_neg_list = []
            # for ww in [wav_list[0]]:
            for ww in wav_list:
                print(os.path.basename(ww))
                samples, _ = librosa.load(ww, sr=conf['sample_rate'])
                whistle_freq0 = np.abs(
                    librosa.pseudo_cqt(samples, sr=conf['sample_rate'],
                                       hop_length=conf['hop_length'],
                                       fmin=conf['fmin'],
                                       bins_per_octave=conf['bins_per_octave'],
                                       n_bins=conf['n_bins']))
                whistle_freq = np.zeros(whistle_freq0.shape)
                for rr in range(whistle_freq0.shape[0]):
                    whistle_freq[rr, :] = whistle_freq0[rr, :] - whistle_freq0[rr, :].mean()

                whistle_freq_list = []
                win_num = floor((whistle_freq.shape[1] - conf['time_multi']) / conf[
                    'time_multi_hop']) + 1  # 0.5s hop

                if win_num > 0:
                    for nn in range(win_num):
                        whistle_freq_curr = whistle_freq[:,
                                            nn * conf['time_multi_hop']:
                                            nn * conf['time_multi_hop'] + conf[
                                                'time_multi']]
                        whistle_freq_list.append(whistle_freq_curr)
                    if len(whistle_freq_list) >= 2:
                        whistle_image = np.stack(whistle_freq_list)
                    else:
                        whistle_image = np.expand_dims(whistle_freq_list[0], axis=0)
                    whistle_image_4d = np.expand_dims(whistle_image, axis=3)
                    predictions = classifier_model.predict(whistle_image_4d)

                    whistle_win_ind = \
                    np.where(predictions[:, 1] > conf['whistle_thre_min'])[0]
                    if whistle_win_ind.shape[0] >= 1:
                        # detected whistle start & end time
                        whistle_time_start_curr = whistle_win_ind * conf['hop_size']
                        whistle_time_start.append(whistle_time_start_curr)
                        whistle_time_end.append(
                            whistle_time_start_curr + conf['win_size'])
                        # detected whistle score
                        whistle_score.append(predictions[:, 1][whistle_win_ind])
                        begin_path.append([ww] * whistle_win_ind.shape[0])
                        file_offset.append(whistle_win_ind * conf['hop_size'])
                else:
                    continue

                # extract features here for both positive & negative classes
                whistle_win_ind_pos = \
                np.where(predictions[:, 1] > conf['whistle_thre_pos'])[0]
                whistle_win_ind_neg = \
                np.where(predictions[:, 1] < conf['whistle_thre_neg'])[0]
                whistle_image_4d_pos_list.append(
                    whistle_image_4d[whistle_win_ind_pos, :, :, :])
                whistle_image_4d_neg_list.append(
                    whistle_image_4d[whistle_win_ind_neg, :, :, :])

            whistle_image_4d_pos = np.concatenate(whistle_image_4d_pos_list)
            whistle_image_4d_neg = np.concatenate(whistle_image_4d_neg_list)
            fea_out_file = os.path.join(fea_out, row['species'] + '_' + row[
                'deployment'] + '.npz')
            np.savez(fea_out_file, fea_pos=whistle_image_4d_pos,
                     fea_neg=whistle_image_4d_neg)

            # make sound selection table
            if len(whistle_time_start) >= 1:
                if len(whistle_time_start) >= 2:
                    whistle_time_start = np.concatenate(whistle_time_start)
                    whistle_time_end = np.concatenate(whistle_time_end)
                    whistle_score = np.concatenate(whistle_score)
                    begin_path = np.concatenate(begin_path)
                    file_offset = np.concatenate(file_offset)
                else:  # == 1
                    whistle_time_start = whistle_time_start[0]
                    whistle_time_end = whistle_time_end[0]
                    whistle_score = whistle_score[0]
                    begin_path = begin_path[0]
                    file_offset = file_offset[0]
                seltab_out_file = os.path.join(seltab_out, row['species'] + '_' + row[
                    'deployment'] + '.txt')
                make_sound_sel_table(seltab_out_file, whistle_time_start,
                                     whistle_time_end, begin_path, file_offset,
                                     whistle_score, conf['whistle_thre_min'])

        # Oswald data detection & feature extraction
        # for index, row in df_sound_oswald.iterrows():
        # conf['whistle_thre_pos'] = 0.4
        # conf['whistle_thre_neg'] = 0.02
        # for index, row in df_sound_oswald[16:].iterrows():
        for index, row in df_sound_oswald.iterrows():
            print('Acoustic encounter '+str(index+1)+'/'+str(len(df_sound_oswald))+': '+row['folder'])

            wav_list = glob.glob(row['path']+'/*.wav')
            wav_list.sort()
            whistle_time_start = []
            whistle_time_end = []
            whistle_score = []
            begin_path = []
            file_offset = []
            whistle_image_4d_pos_list = []
            whistle_image_4d_neg_list = []

            # if row['encounter'] == 's132':
            # for ww in [wav_list[0]]:
            for ww in wav_list:
                print(os.path.basename(ww))
                # ww = os.path.join(row['path'], ww0)
                samples, _ = librosa.load(ww, sr=conf['sample_rate'])
                whistle_freq0 = np.abs(librosa.pseudo_cqt(samples, sr=conf['sample_rate'],
                                                         hop_length=conf['hop_length'],
                                                         fmin=conf['fmin'],
                                                         bins_per_octave=conf['bins_per_octave'],
                                                         n_bins=conf['n_bins']))
                whistle_freq = np.zeros(whistle_freq0.shape)
                for rr in range(whistle_freq0.shape[0]):
                    whistle_freq[rr, :] = whistle_freq0[rr, :] - whistle_freq0[
                                                                 rr, :].mean()

                whistle_freq_list = []
                win_num = floor( (whistle_freq.shape[1]-conf['time_multi'])
                                 / conf['time_multi_hop'])+1  # 0.5s hop

                if win_num > 0:
                    for nn in range(win_num):
                        whistle_freq_curr = whistle_freq[:, nn*conf['time_multi_hop']:
                                                            nn*conf['time_multi_hop']
                                                            +conf['time_multi']]
                        whistle_freq_list.append(whistle_freq_curr)
                    if len(whistle_freq_list) >= 2:
                        whistle_image = np.stack(whistle_freq_list)
                    else:
                        whistle_image = np.expand_dims(whistle_freq_list[0], axis=0)
                    whistle_image_4d = np.expand_dims(whistle_image, axis=3)
                    predictions = classifier_model.predict(whistle_image_4d)

                    whistle_win_ind = np.where(predictions[:, 1] > conf['whistle_thre_min'])[0]
                    if whistle_win_ind.shape[0] >= 1:
                        # detected whistle start & end time
                        whistle_time_start_curr  = whistle_win_ind * conf['hop_size']
                        whistle_time_start.append(whistle_time_start_curr)
                        whistle_time_end.append(whistle_time_start_curr + conf['win_size'])
                        # detected whistle score
                        whistle_score.append(predictions[:, 1][whistle_win_ind])
                        begin_path.append([ww]*whistle_win_ind.shape[0])
                        file_offset.append(whistle_win_ind * conf['hop_size'])
                else:
                    continue

                # extract features here for both positive & negative classes
                whistle_win_ind_pos = np.where(predictions[:, 1] > conf['whistle_thre_pos'])[0]
                whistle_win_ind_neg = np.where(predictions[:, 1] < conf['whistle_thre_neg'])[0]
                whistle_image_4d_pos_list.append(whistle_image_4d[whistle_win_ind_pos, :, :, :])
                whistle_image_4d_neg_list.append(whistle_image_4d[whistle_win_ind_neg, :, :, :])

            whistle_image_4d_pos = np.concatenate(whistle_image_4d_pos_list)
            whistle_image_4d_neg = np.concatenate(whistle_image_4d_neg_list)
            fea_out_file = os.path.join(fea_out, row['species'] + '_' + row[
                'deployment'] + '_' + row['encounter'] + '.npz')
            np.savez(fea_out_file, fea_pos=whistle_image_4d_pos, fea_neg=whistle_image_4d_neg)

            # make sound selection table
            if len(whistle_time_start) >= 1:
                if len(whistle_time_start) >= 2:
                    whistle_time_start = np.concatenate(whistle_time_start)
                    whistle_time_end= np.concatenate(whistle_time_end)
                    whistle_score = np.concatenate(whistle_score)
                    begin_path= np.concatenate(begin_path)
                    file_offset = np.concatenate(file_offset)
                else:  # == 1
                    whistle_time_start = whistle_time_start[0]
                    whistle_time_end = whistle_time_end[0]
                    whistle_score = whistle_score[0]
                    begin_path = begin_path[0]
                    file_offset = file_offset[0]
                seltab_out_file = os.path.join(seltab_out, row['species']+'_'+row['deployment']+'_'+row['encounter']+'.txt')
                make_sound_sel_table(seltab_out_file, whistle_time_start,
                               whistle_time_end, begin_path, file_offset, whistle_score,
                               conf['whistle_thre_min'])

        # DCLDE 2011 data detection & feature extraction
        # Read species labels, filenames & extract time and frequency sequences
        # bin_dir = '/home/ys587/__Data/__whistle/__dclde2011_bin_file_test'
        bin_dir = '/home/ys587/__Data/__whistle/__dclde2011_bin_file'
        sound_dclde2011_dir = '/home/ys587/__Data/__whistle/__sound_species'
        species_name = ['bottlenose', 'common', 'spinner', 'melon-headed']

        for ss in species_name:
            contour_target, bin_wav_pair_curr = bin_extract(bin_dir, sound_dclde2011_dir, [ss])
            # read contours from bin files
            contour_target_list = contour_target_retrieve(contour_target,
                                                          bin_dir,
                                                          conf['time_reso'])
            whistle_image_list_pos = []
            whistle_image_list_neg = []
            for ff in range(len(contour_target_list)):
                filename = contour_target_list[ff][0]
                print('\n'+filename)
                # label_contour = contour_target_list[ff][1]
                file_contour = contour_target_list[ff][2]

                contour_target_ff, start_time, end_time, freq_low, freq_high = \
                    contour_data(file_contour, conf['time_reso'])

                timesteps = ceil((end_time - start_time)/conf['time_reso'])+1
                print("Start time: "+str(start_time))
                print("Stop time: " + str(end_time))

                # spectrogram named whistle_freq for each file
                sound_path = os.path.join(sound_dclde2011_dir, ss, filename+'.wav')
                samples, _ = librosa.load(sound_path, sr=conf['sample_rate'], offset=start_time, duration=end_time-start_time+2.*conf['time_reso'])
                whistle_freq = np.abs(librosa.pseudo_cqt(samples,
                                                         sr=conf['sample_rate'],
                                                         hop_length=conf['cqt_hop_size'],
                                                         fmin=conf['fmin'],
                                                         bins_per_octave=conf['bins_per_octave'],
                                                         n_bins=conf['n_bins']))

                whistle_presence = np.zeros((int(timesteps)))
                for cc in contour_target_ff:
                    time_ind_start = int(floor((cc['Time'][0]-start_time)/conf['time_reso']))
                    for ii in range(cc['Time'].shape[0]):
                        whistle_presence[time_ind_start+ii] = 1.0

                # cut whistle_freq into segments for data samples
                size_time = int(conf['win_size']/conf['time_reso'])
                size_hop = int(conf['hop_size']/conf['time_reso'])
                for tt in range(floor((whistle_freq.shape[1]-size_time)/size_hop)):
                    whistle_image = whistle_freq[:, tt*size_hop:tt*size_hop+size_time]
                    whistle_presence_seg = whistle_presence[tt * size_hop:tt * size_hop + size_time]
                    if whistle_presence_seg.sum() >= conf['contour_timethre']:
                        whistle_image_list_pos.append(whistle_image)
                    elif whistle_presence_seg.sum() == 0.0:
                        whistle_image_list_neg.append(whistle_image)

                # whistle_image_pos = np.asarray(whistle_image_list_pos)
                if len(whistle_image_list_pos) >= 2:
                    whistle_image_pos = np.stack(whistle_image_list_pos)
                elif len(whistle_image_list_pos) == 1:
                    whistle_image_pos = np.expand_dims(whistle_image_list_pos[0], axis=0)
                else:
                    whistle_image_pos = np.zeros((conf['n_bins'], conf['time_multi'], 0))
                whistle_image_4d_pos = np.expand_dims(whistle_image_pos, axis=3)

                # whistle_image_neg = np.asarray(whistle_image_list_neg)
                if len(whistle_image_list_neg) >= 2:
                    whistle_image_neg = np.stack(whistle_image_list_neg)
                elif len(whistle_image_list_neg) == 1:
                    whistle_image_neg = np.expand_dims(whistle_image_list_neg[0], axis=0)
                else:
                    whistle_image_neg = np.zeros((conf['n_bins'], conf['time_multi'], 0))
                whistle_image_4d_neg = np.expand_dims(whistle_image_neg, axis=3)

                fea_out_file = os.path.join(fea_out, species_to_code[ss]+'_DCLDE2011_'+filename+'.npz')
                np.savez(fea_out_file, fea_pos=whistle_image_4d_pos, fea_neg=whistle_image_4d_neg)
    ######################################################
    # collect features & labels from multiple data sources
    if os.path.exists(fea_all_out):
        fea_all_loadfile = np.load(fea_all_out)
        fea_train_4d = fea_all_loadfile['fea_train_4d']
        label_train = fea_all_loadfile['label_train']
    else:
        # collect all features
        species_fea = dict()
        for ss in species_list:
            print('Reading features from files ' + ss + ':')
            fea_pos_list = []
            fea_neg_list = []
            fea_species_list = glob.glob(os.path.join(fea_out, ss+'*.npz'))
            for ff in fea_species_list:
                fea_curr = np.load(ff)
                fea_pos_list.append(fea_curr['fea_pos'])
                # fea_neg_list.append(fea_curr['fea_neg'])
            if len(fea_pos_list) >= 1:
                fea_pos = np.concatenate(fea_pos_list, axis=0)
            elif len(fea_pos_list) == 0:
                fea_pos = []
                continue
            else:
                fea_pos = fea_pos_list[0]
            # if len(fea_neg_list) >= 1:
            #     fea_neg = np.concatenate(fea_neg_list, axis=0)
            # elif len(fea_neg_list) == 0:
            #     fea_neg  = []
            #     continue
            # else:
            #     fea_neg = fea_neg_list[0]
            # species_fea.update({ss: {'fea_pos': fea_pos, 'fea_neg': fea_neg}})
            species_fea.update({ss: {'fea_pos': fea_pos}})

        fea_list = []
        label_list = []
        for key, value in species_fea.items():
            print('Combining features from '+key+':')
            fea_species = value['fea_pos']
            fea_list.append(fea_species)
            label = [species_id[key]]*fea_species.shape[0]
            label_list.append(label)
            # fea_neg = value['fea_neg']
        fea_train_4d = np.concatenate(fea_list, axis=0)
        label_train = np.concatenate(label_list, axis=0)
        del species_fea
        del fea_list
        del label_list

        np.savez(fea_all_out, fea_train_4d=fea_train_4d, label_train=label_train)

    # shuffle data
    fea_train_4d_shuf, label_train_shuf = shuffle(fea_train_4d, label_train,
                                                  random_state=0)
    # train model
    # conf['IMG_T'] = fea_train_4d.shape[2]
    # conf['IMG_F'] = fea_train_4d.shape[1]
    conf['input_shape'] = (conf['IMG_F'], conf['IMG_T'], 1)

    for rr in range(0, 4):
        model_type = 'resnet34_expt'
        proj_name = model_type + '_mixdata_run' + str(rr)
        print(proj_name)

        conf['log_dir'] = os.path.join(log_dir, proj_name)
        if not os.path.exists(conf['log_dir']):
            os.mkdir(conf['log_dir'])
        conf['num_filters'] = 16
        learning_rate_original = 0.5
        conf['learning_rate'] = learning_rate_original/(2.0**float(rr))
        # conf['learning_rate'] = conf['learning_rate'] / 2.0  # start with 0.5

        best_model_path = all_data_train_validate(model_type, fea_train_4d_shuf,
                                                  label_train_shuf, conf,
                                                  conf['log_dir'])


if __name__ == "__main__":
    main()





