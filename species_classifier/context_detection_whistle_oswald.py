#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Apply a trained classifier for detecting whistles on Oswald data

Created on 4/21/20
@author: atoultaro
"""
import os
import glob
from math import floor
import numpy as np

import pandas as pd
from keras.models import load_model
import librosa

# from upcall.run_detector_dsp import make_sound_stream
from upcall.run_detector_dsp import run_detector_1day_parallel_fft


def make_sel_table(seltab_output_path, box_time, score_arr, score_thre):
    assert(box_time.shape[0] == score_arr.shape[0])
    event_num = score_arr.shape[0]
    data_dict = {
        'Selection': [ii+1 for ii in range(event_num)],
        'View': ['Spectrogram 1']*event_num,
        'Channel': [1]*event_num,
        'Begin Time (s)': np.around(box_time[:, 0], decimals=2),
        'End Time (s)': np.around(box_time[:, 1], decimals=2),
        'Low Freq (Hz)': [2000.0]*event_num,
        'High Freq (Hz)': [50000.0]*event_num,
        'Score': np.around(score_arr, decimals=4),
        'Score Thre': np.repeat(np.around(score_thre, decimals=3), event_num)
    }
    df_seltab0 = pd.DataFrame.from_dict(data_dict)
    col_name = ['Selection', 'View', 'Channel', 'Begin Time (s)',
                'End Time (s)', 'Low Freq (Hz)', 'High Freq (Hz)', 'Score',
                'Score Thre']
    df_seltab = df_seltab0[col_name]

    # write out selection table
    df_seltab.to_csv(seltab_output_path, sep='\t', mode='a', index=False)


sound_dir = '/home/ys587/__Data/__whistle/__whistle_oswald/PICEAS2005'
seltab_out = '/home/ys587/__Data/__whistle/__whistle_oswald/__whistle_seltab/PICEAS2005'
# 5-class model
# model_path = '/home/ys587/__Data/__whistle/__log_dir_context/__new_results/__win_1s_overlap_p5s_contour_p2s_timereso_p02/resnet_cifar10_expt_run1_f1/fold1'
# 2-class model
model_path = '/home/ys587/__Data/__whistle/__log_dir_context/__new_results/__win_1s_overlap_p5s_contour_p2s_timereso_p02_alldata/resnet_cifar10_expt_alldata_run0_f1'

conf = dict()
conf['sample_rate'] = 192000
conf['time_reso'] = 0.02
conf['hop_length'] = int(conf['time_reso']*conf['sample_rate'])
conf['win_size'] = 1.  # 1-s window
conf['hop_size'] = 1.0
conf['time_multi'] = floor(conf['win_size'] / conf['time_reso'])
# time_multi = 50  # 1-s windows with 1-s step => 1s = time_multi * time_reso
# conf['whistle_thre'] = 0.99  # for five-class classifier
conf['whistle_thre'] = 0.01  # for two-class classifier
conf['trained_class_num'] = 'two'

# model
print('Loading the classifier')
model_name = glob.glob(model_path + '/*.hdf5')[0]
classifier_model = load_model(model_name)

# run prediction
ae_list = os.listdir(sound_dir)  # ae: acoustic encounter
ae_list.sort()
for ae in ae_list:  # e.g. spotted s280
    ae_dir = os.path.join(sound_dir, ae)
    print(ae_dir)

    ae_sound_list = os.listdir(ae_dir)
    ae_sound_list.sort()

    if True:
        for ss in ae_sound_list:
            # print(ss)
            sound_path = os.path.join(ae_dir, ss)  # e.g. spotted s280/HICEAS021119-144218.wav
            print(sound_path)

            samples, _ = librosa.load(sound_path, sr=conf['sample_rate'])
            whistle_freq = np.abs(librosa.pseudo_cqt(samples, sr=conf['sample_rate'],
                                                     hop_length=conf['hop_length'],
                                                     fmin=4000.0, bins_per_octave=36,
                                                     n_bins=144))

            whistle_freq_list = []
            # win_num = floor(whistle_freq.shape[1]/ time_multi)  # no overlapping
            win_num = floor(whistle_freq.shape[1] / conf['time_multi'])

            for nn in range(win_num):
                whistle_freq_curr = whistle_freq[:, nn*conf['time_multi']:(nn+1)*conf['time_multi']]
                whistle_freq_list.append(whistle_freq_curr)
            whistle_image = np.stack(whistle_freq_list)
            whistle_image_4d = np.expand_dims(whistle_image, axis=3)
            predictions = classifier_model.predict(whistle_image_4d)
            print()

            if conf['trained_class_num'] is 'five':
                predictions_bin = np.stack((predictions[:, -1], predictions[:, :-1].sum(axis=1)), axis=-1)
                whistle_win_ind = np.where(predictions_bin[:, 0] < conf['whistle_thre'])[0]
                # detected whistle start & end time
                whistle_time_start = whistle_win_ind*conf['win_size']
                whistle_time_end = whistle_time_start + conf['win_size']
                whistle_time = (np.stack((whistle_time_start, whistle_time_end))).T
                # detected whistle score
                whistle_score = predictions_bin[:, 1][whistle_win_ind]

                # make selection table
                seltab_out_file = os.path.join(seltab_out, ae+'_'+os.path.splitext(ss)[0]+'.txt')
                make_sel_table(seltab_out_file, whistle_time, whistle_score, 1 - conf['whistle_thre'])
            elif conf['trained_class_num'] is 'two':
                whistle_win_ind = np.where(predictions[:, 1] > conf['whistle_thre'])[0]
                # detected whistle start & end time
                whistle_time_start = whistle_win_ind*conf['win_size']
                whistle_time_end = whistle_time_start + conf['win_size']
                whistle_time = (np.stack((whistle_time_start, whistle_time_end))).T
                # detected whistle score
                whistle_score = predictions[:, 1][whistle_win_ind]

                # make selection table
                seltab_out_file = os.path.join(seltab_out, ae+'_'+os.path.splitext(ss)[0]+'.txt')
                make_sel_table(seltab_out_file, whistle_time, whistle_score, 1 - conf['whistle_thre'])

            print('')


    # date_format = "%y%m%d-%H%M%S"
    # sample_stream = make_sound_stream(ae_dir, format_str=date_format)
    #
    # N_read = int(sample_stream.get_all_stream_fs()[0] * conf['win_size'])
    # N_prev = 0  # no overlap
    # run_detector_1day_parallel_fft(sample_stream,
    #             model,
    #             seltab_out,
    #             filter_args,
    #             N_read=N_read,
    #             N_previous= N_prev,
    #             date_format=date_format,
    #             ScoreThre=0.05,
    #             max_streams=40000, # make it big to utilize GPU
    #             OverlapRatioThre=0.4,
    #             SelectionTableName = 'temp',
    #             SNDSel= False,
    #             config = None)

    # # make prediction on the sound stream of an acoustic encounter
    # sample_stream = make_sound_stream(ae_dir, format_str="%y%m%d-%H%M%S")
    # current_date = sample_stream.get_current_timesamp().date()
    #
    # # samps = sample_stream.get_all_stream_samps()
    # N_read = int(sample_stream.get_all_stream_fs()[0]*conf['win_size'])
    # N_prev = 0  # no overlap
    # # sec_advance = float((N_read + N_previous) / sample_stream.get_all_stream_fs()[0])
    # samps = sample_stream.read(N=N_read, previousN=N_prev)[0]

    # make prediction on each sound file individually