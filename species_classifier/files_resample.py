#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Created on 6/2/20
@author: atoultaro
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
# change sampling rate of files in folders

Created on 3/23/20
@author: atoultaro
"""
import os
import glob
import soundfile as sf
from math import floor, ceil
import numpy as np
import multiprocessing as mp
from itertools import repeat
import librosa

import warnings
warnings.filterwarnings("ignore")
# import librosa
# from keras.models import load_model


def resample_files(file_in, file_out, target_samplerate):
    samples, _ = librosa.load(file_in, sr=target_samplerate, mono=False)
    # samples = np.asfortranarray(samples)
    # librosa.output.write_wav(file_out, samples.T, target_samplerate)
    sf.write(file_out, samples.T, target_samplerate, subtype='PCM_16')


# detection on multi-channel sound
# sound_path_in = '/mnt/DCLDE/noaa-pifsc-bioacoustic'
# sound_path_in = '/mnt/DCLDE/noaa-pifsc-bioacoustic-test'
# sound_path_out = '/mnt/DCLDE/noaa-pifsc-bioacoustic-test-48k'
sound_path_in = '/mnt/DCLDE/noaa-pifsc-bioacoustic'
sound_path_out = '/mnt/DCLDE/noaa-pifsc-bioacoustic-48k'
deployment = ['1705', '1706']
target_samplerate = 48000

for dd in deployment:
    file_in_list = []
    file_out_list = []

    sound_dir_out = os.path.join(sound_path_out, dd)
    if not os.path.exists(sound_dir_out):
        os.mkdir(sound_dir_out)

    sound_dir_in = os.path.join(sound_path_in, dd)
    file_folder_to_do = glob.glob(sound_dir_in+'/*.wav')
    file_folder_exist = glob.glob(sound_dir_out + '/*.wav')
    # find files mismatch
    file_folder = [x for x in file_folder_to_do if os.path.join(sound_dir_out, os.path.basename(x)) not in file_folder_exist]
    file_folder.sort()

    for ff in file_folder:  # for ff in file_folder:
        print(ff)
        ff = os.path.basename(ff)
        file_in = os.path.join(sound_dir_in, ff)
        file_out = os.path.join(sound_dir_out, ff)
        file_in_list.append(file_in)
        file_out_list.append(file_out)

    cpu_num = os.cpu_count() - 1
    pool_fea = mp.Pool(processes=cpu_num)
    pool_fea.starmap(resample_files, zip(file_in_list, file_out_list, repeat(target_samplerate)), chunksize=1)
    pool_fea.close()
    pool_fea.join()

# resample_files(file_in_list[0], file_out_list[0], target_samplerate)

if False:  # single cpu
    for dd in deployment:
        sound_dir_out = os.path.join(sound_path_out, dd)
        if not os.path.exists(sound_dir_out):
            os.mkdir(sound_dir_out)

        sound_dir_in = os.path.join(sound_path_in, dd)
        file_folder = os.listdir(sound_dir_in)
        file_folder.sort()

        for ff in file_folder:  #for ff in file_folder:
            print(ff)
            file_in = os.path.join(sound_dir_in, ff)
            file_out = os.path.join(sound_dir_out, ff)

            # librosa.core.resample(y, orig_sr, target_sr, res_type='kaiser_best',
            #                       fix=True, scale=False, **kwargs)[source]
            samples, _ = librosa.load(file_in, sr=target_samplerate, mono=False)
            samples = np.asfortranarray(samples)
            librosa.output.write_wav(file_out, samples.T, target_samplerate)

        if False:  # sox
            # sox_command = 'sox -r 200000 -b 16 -c 1 -e signed-integer -t raw "' + file_target + '" "' + file_target_out + '"'
            # sox_command = 'sudo sox -r 48000 "' + file_in + '" "' + file_out + '"'
            # sox_command = 'sudo sox "' + file_in + '" -r 250000 -b 16 -e signed-integer "' + file_out + '"'
            sox_command = 'sudo sox "' + file_in + '" -r 250000 "' + file_out + '"'
            print(sox_command)
            sudoPassword = 'nlgUcs20'
            # command = 'mount -t vboxsf myfolder /home/myuser/myfolder'
            p = os.system('echo %s|sudo -S %s' % (sudoPassword, sox_command))

        print()


if False:
    import os
    import sox

    sound_dir_in = '/home/ys587/__Data/__whistle/__whistle_oswald/STAR2000_temp'
    sound_dir_out = '/home/ys587/__Data/__whistle/__whistle_oswald/STAR2000_new'

    if not os.path.exists(sound_dir_out):
        os.mkdir(sound_dir_out)
    species_folder = os.listdir(sound_dir_in)

    for ss in species_folder:
    # for ss in [species_folder[0]]:
        # ss = '\"'+ss+'\"'
        # ss_fullpath = os.path.join(sound_dir_in, '\''+ss+'\'')
        ss_fullpath = os.path.join(sound_dir_in, ss)
        # ss_fullpath = os.path.join(sound_dir_in, 'bottlenose_s352_temp')
        print(ss_fullpath)

        ss_fullpath_out = os.path.join(sound_dir_out, ss)
        if not os.path.exists(ss_fullpath_out):
            os.mkdir(ss_fullpath_out)

        encounter_file_list = os.listdir(ss_fullpath)
        # print(encounter_file_list)

        # for ff in encounter_file_list[0]:
        for ff in encounter_file_list:
            file_target = os.path.join(ss_fullpath, ff)
            file_target_out = os.path.join(ss_fullpath_out, ff+'.wav')
            print(file_target)

            sox_command = 'sox -r 200000 -b 16 -c 1 -e signed-integer -t raw "'+file_target+'" "'+file_target_out+'"'
            os.system(sox_command)
            # sox -r 150000 -b 16 -c 1 -e signed-integer -t raw ./Acst0953.b02 ./Acst0953.b02.wav

            # tfm = sox.Transformer()
            # tfm.set_input_format(file_type=raw)
            # tfm.set_output_format(file_type='wav', rate=150000, channels=1,
            #                       encoding='signed-integer',
            #                       bits=16)
            # # tfm.build(file_target, file_target_out, extra_args=['-t raw'])
            # tfm.build(file_target, file_target_out)


