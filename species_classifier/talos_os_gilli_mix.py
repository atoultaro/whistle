#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Created on 7/24/20
@author: atoultaro
"""

import os
from math import floor

import pandas as pd
import numpy as np
from sklearn.utils import shuffle
import warnings
warnings.filterwarnings('ignore', category=FutureWarning)

from keras.callbacks import EarlyStopping
from keras.utils import to_categorical
from keras.losses import categorical_crossentropy
from keras.optimizers import Adam, Adadelta, SGD, Adagrad, Nadam

import talos

from species_classifier.driver_context_conv_audio_4fold import \
    one_fold_validate_fit_only, one_fold_validate_fit_only_talos, \
    validate_on_files
from cape_cod_whale.preprocess import bin_extract, contour_target_retrieve
from cape_cod_whale.classifier import metrics_two_fold
from species_classifier import species_lib
from species_classifier.build_model import resnet34_expt_talos

curr_path = os.path.dirname(os.path.abspath(__file__))
os.chdir(curr_path)

flag_use_saved_fea = True
expt_flag = 'expt1'
# fea_type = 'cqt'
# fea_type = 'pcen'
fea_type = 'pcen_nopulse'
# fea_type = 'powerlaw'
# fea_type = 'powerlawsym'

# work_path = '/home/ys587/__Data/__whistle/__cv_os_vs_gilli'
work_path = '/home/ys587/__Data/__whistle/__os_gilli_mix'

# Oswald's data
deployment = ['HICEAS2002', 'PICEAS2005', 'STAR2000', 'STAR2003', 'STAR2006']
species_to_code = {'bottlenose': 'BD', 'longbeaked_common': 'CD',
                   'shortbeaked_common': 'CD', 'common': 'CD',
                   'striped': 'STR', 'spotted': 'SPT', 'spinner': 'SPIN',
                   'pilot': 'PLT', 'roughtoothed': 'RT', 'false_killer': 'FKW',
                   'melon-headed': 'MH', }

species_list = ['BD', 'CD', 'STR', 'SPT', 'SPIN', 'PLT']  # six species

# species_id = {'BD': 0, 'CD': 1, 'STR': 2, 'SPT': 3, 'SPIN': 4, 'PLT': 5}
species_id = {'BD': 0, 'CD': 1, 'STR': 2, 'SPT': 3, 'SPIN': 4, 'PLT': 5, 'NO': 6}
# num_species = 6  # noise not included
num_species = 7  # noise included

# fea_out = '/home/ys587/__Data/__whistle/__cv_os_vs_gilli/__feature_mat'
# fea_out = os.path.join(work_path, '__feature_mat_'+fea_type)
# fea_out = os.path.join(work_path, '__feature_mat', '__'+fea_type)
fea_out = os.path.join(work_path, '__feature_mat', '__pcen_nopulse_two_folds')
# fea_out = '/home/ys587/__Data/__whistle/__cv_os_vs_gilli/__feature_mat_cqt'
# fea_out = '/home/ys587/__Data/__whistle/__cv_os_vs_gilli/__feature_mat_pcen'

# seltab_out = '/home/ys587/__Data/__whistle/__cv_os_vs_gilli/__sound_seltab'
seltab_out = os.path.join(work_path, '__sound_seltab')

whistle_data_oswald = '/home/ys587/__Data/__whistle/__whistle_oswald'
# csv_oswald_sound = '/home/ys587/__Data/__whistle/__cv_os_vs_gilli/__sound_info/oswald_encounter.csv'
# csv_oswald_info = '/home/ys587/__Data/__whistle/__cv_os_vs_gilli/__sound_info/oswald_soundinfo.csv'
csv_oswald_sound = os.path.join(work_path, '__sound_info', 'oswald_encounter.csv')
csv_oswald_info = os.path.join(work_path, '__sound_info', 'oswald_soundinfo.csv')
df_sound_oswald, df_info_oswald = species_lib.df_sound_info_oswald(csv_oswald_sound, csv_oswald_info, species_to_code, whistle_data_oswald, deployment)

whistle_data_gillispie = '/home/ys587/__Data/__whistle/__whistle_gillispie'
# csv_gillispie_sound = '/home/ys587/__Data/__whistle/__cv_os_vs_gilli/__sound_info/gillispie_encounter.csv'
# csv_gillispie_info = '/home/ys587/__Data/__whistle/__cv_os_vs_gilli/__sound_info/gillispie_soundinfo.csv'
csv_gillispie_sound = os.path.join(work_path, '__sound_info', 'gillispie_encounter.csv')
csv_gillispie_info = os.path.join(work_path, '__sound_info', 'gillispie_soundinfo.csv')
df_sound_gillispie, df_info_gillispie = species_lib.df_sound_info_gillispie\
    (csv_gillispie_sound, csv_gillispie_info, whistle_data_gillispie)

# generate a new df_sound_gillispie with pre-defined list of species
df_sound_oswald = df_sound_oswald[df_sound_oswald['species'].isin(species_list)]
df_sound_gillispie = df_sound_gillispie[df_sound_gillispie['species'].isin(species_list)]

# fea_part_out = '/home/ys587/__Data/__whistle/__cv_os_vs_gilli/__feature_all'
fea_part_out = os.path.join(work_path, '__feature_all')
fea_part1_out = os.path.join(fea_part_out, '__pcen_nopulse_two_folds', 'fold1.npz')
fea_part2_out = os.path.join(fea_part_out, '__pcen_nopulse_two_folds', 'fold2.npz')
# fea_part1_out = '/home/ys587/__Data/__whistle/__cv_os_vs_gilli/__feature_all/feature_label_oswald.npz'
# fea_part2_out = '/home/ys587/__Data/__whistle/__cv_os_vs_gilli/__feature_all/feature_label_gillispie.npz'

# model_path = '/home/ys587/__Data/__whistle/__log_dir_context/__new_results/__48k_fmin_3000_octave_3_binsperoctave_48_binstotal_144/resnet34_expt_all4pie_run0_f1/epoch_160_valloss_0.3137_valacc_0.9483.hdf5'

######################################################
# collect features & labels from multiple data sources
print("Collecting features...")
fea_out_fold1 = os.path.join(fea_out, '__fold1')
fea_out_fold2 = os.path.join(fea_out, '__fold2')
if os.path.exists(fea_part1_out) & os.path.exists(fea_part2_out) & flag_use_saved_fea:
    print('...reading features from files')
    fea_part1_loadfile = np.load(fea_part1_out)
    fea_part1_4d = fea_part1_loadfile['fea_part_4d']
    label_part1 = fea_part1_loadfile['label_part']

    fea_part2_loadfile = np.load(fea_part2_out)
    fea_part2_4d = fea_part2_loadfile['fea_part_4d']
    label_part2 = fea_part2_loadfile['label_part']
else:
    # collect all features
    print('Processing features of fold 1...')
    species_fea_part1 = species_lib.read_features_from_files(fea_out_fold1, species_list)
    if len(species_fea_part1) == 0:
        raise('No feature individual files are present. Need to compute features.')
    fea_part1_4d, label_part1 = species_lib.combine_features_from_dict(species_fea_part1, fea_part1_out, species_id)

    print('Processing features of fold 2...')
    species_fea_part2 = species_lib.read_features_from_files(fea_out_fold2, species_list)
    if len(species_fea_part2) == 0:
        raise('No feature individual files are present. Need to compute features.')
    fea_part2_4d, label_part2 = species_lib.combine_features_from_dict(species_fea_part2, fea_part2_out, species_id)

# shuffle data
print("Shuffle the features...")
fea_part1_4d, label_part1 = shuffle(fea_part1_4d, label_part1,
                                    random_state=0)
fea_part2_4d, label_part2 = shuffle(fea_part2_4d, label_part2,
                                    random_state=0)

print("Classifier training...")
log_dir = "/home/ys587/__Data/__whistle/__log_dir_context/contour_temp_audio"


def resnet34_talos(fea_part_4d_train, label_part_train,
                   fea_part_4d_val, label_part_val,
                   params):
    conf = dict()

    conf['species_name'] = species_list
    conf['species_id'] = species_id
    conf['time_reso'] = 0.02  # 20 ms

    # cepstral coefficient
    conf['sample_rate'] = 48000
    conf["num_class"] = num_species
    # conf['num_filters'] = 16

    conf['context_winsize'] = 1.0  # sec
    conf['context_hopsize'] = 0.5  # sec
    conf['contour_timethre'] = 10  # 0.2 s

    conf['fft_size'] = 4096
    conf['hop_size'] = int(conf['time_reso'] * conf['sample_rate'])
    conf['img_t'] = int(floor((conf['context_winsize'] / conf['time_reso'])))
    conf['img_f'] = 64
    conf['input_shape'] = (conf['img_f'], conf['img_t'], 1)

    # conf['l2_regu'] = 0.01
    conf['dropout'] = 0.1
    conf['batch_size'] = 64
    conf['epoch'] = 200
    conf['learning_rate'] = 0.01
    conf['patience'] = 50

    model = resnet34_expt_talos(conf, params)

    early_stop = EarlyStopping(monitor='val_loss', mode='min', verbose=1,
                               patience=conf['patience'])

    # model compile
    model.compile(loss=categorical_crossentropy,
                  # optimizer=Adadelta(lr=conf['learning_rate']),
                  # optimizer=Adadelta(lr=lr),
                  optimizer=params['optimizer'](lr=params['learning_rate'],
                                                # amsgrad=params['amsgrad'],
                                                beta_1=params['beta_1'],
                                                beta_2=params['beta_2'],
                                                epsilon=params['epsilon'],
                                                ),
                  # optimizer=Adam(lr=conf['learning_rate']),
                  metrics=['accuracy'])
    model.summary()

    label_part_train = to_categorical(label_part_train)
    label_part_val = to_categorical(label_part_val)
    count_species1 = label_part_train.sum(axis=0).tolist()
    conf["class_weight"] = (
                max(count_species1) / np.array(count_species1)).tolist()

    # callback_list = [checkpoint, TensorBoard(log_dir=log_dir1), early_stop]
    callback_list = [early_stop]

    out = model.fit(fea_part_4d_train, label_part_train,
                    batch_size=params['batch_size'], epochs=params['epoch'],
                    verbose=1,
                    validation_data=[fea_part_4d_val, label_part_val],
                    callbacks=callback_list,
                    class_weight=conf["class_weight"])

    return out, model


# hyperparameters
params = {
    # 'learning_rate': [1.0, 0.1],
    'batch_size': [128],
    'l2_regu': [0.5, 0.1],
    'num_filters': [32],
    'optimizer': [Adam],
    'learning_rate': [0.001, 0.01, 0.1],
    'amsgrad': [False, True],
    'beta_1': [0.9, 0.8],
    'beta_2': [0.999],
    'epsilon': [1e-7],
    'epoch': [300],
}

scan_object = talos.Scan(fea_part1_4d, label_part1,
                         val_split=0.2,
                         params=params,
                         model=resnet34_talos,
                         experiment_name='whistle_hyper'
                         )

report = talos.Analyze(scan_object)
report.data.to_csv('/home/ys587/__Data/__whistle/__os_gilli_mix/talos_expt_new.csv')

# label_part2 = to_categorical(label_part2)
e = talos.Evaluate(scan_object)
e2 = e.evaluate(fea_part2_4d, label_part2, task='multi_class', metric='loss')

talos.Predict('scan_object')

# if expt_flag == 'expt1':
#     # two-fold cross-validation. Trained on gillispie & test on oswald; and vice versa.
#     model_type = 'resnet34_expt'
#     # model_type = 'birdnet_5layers'
#     # model_type = 'lenet_dropout_input_conv'
#
#     proj_name = model_type+'_'+fea_type+'_os_vs_gilli_test_onfile_run' + str(rr) + '_f1'
#     print(proj_name)
#
#     conf['log_dir'] = os.path.join(log_dir, proj_name)
#     if not os.path.exists(conf['log_dir']):
#         os.mkdir(conf['log_dir'])
#     conf['num_filters'] = 16
#
#     best_model_path1 = one_fold_validate_fit_only_talos(
#         model_type, fea_part1_4d, label_part1, conf, fold_id=1)
    # df_validate_2, y_pred2, y_pred_prob_2, y_truth2 = validate_on_files(best_model_path1, fea_out_fold2, conf, fold_id=1)
    #
    # best_model_path2 = one_fold_validate_fit_only(
    #     model_type, fea_part2_4d, label_part2, conf, fold_id=2)
    # df_validate_1, y_pred1, y_pred_prob_1, y_truth1 = validate_on_files(best_model_path2, fea_out_fold1, conf, fold_id=2)
    #
    # y_pred_tot = np.concatenate((y_pred1, y_pred2))
    # label_total = y_truth1.tolist() + y_truth2.tolist()
    # metrics_two_fold(label_total, y_pred_tot, conf['log_dir'],
    #                  'accuracy_total.txt', conf, mode='total')
