#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
two-fold cross-validation between the two different data sets
Testing on each clips


Created on 6/16/20
@author: atoultaro
"""

import os
import sys
from math import floor, ceil

sys.path.append('..')
import numpy as np
from sklearn.utils import shuffle

from species_classifier.driver_context_conv_audio_4fold import \
    one_fold_validate
from cape_cod_whale.classifier import metrics_two_fold
import species_lib

import warnings
warnings.filterwarnings('ignore', category=FutureWarning)

curr_path = os.path.dirname(os.path.abspath(__file__))
os.chdir(curr_path)

flag_use_saved_fea = False
expt_flag = 'expt3'
# fea_type = 'cqt'
# fea_type = 'pcen'
fea_type = 'pcen_nopulse'
# fea_type = 'powerlaw'
# fea_type = 'powerlawsym'

# work_path = '/home/ys587/__Data/__whistle/__cv_os_vs_gilli'
work_path = '/home/ys587/__Data/__whistle/__os_gilli_mix'

# Oswald's data
deployment = ['HICEAS2002', 'PICEAS2005', 'STAR2000', 'STAR2003', 'STAR2006']
species_to_code = {'bottlenose': 'BD', 'longbeaked_common': 'CD',
                   'shortbeaked_common': 'CD', 'common': 'CD',
                   'striped': 'STR', 'spotted': 'SPT', 'spinner': 'SPIN',
                   'pilot': 'PLT', 'roughtoothed': 'RT', 'false_killer': 'FKW',
                   'melon-headed': 'MH', }

species_list = ['BD', 'CD', 'STR', 'SPT', 'SPIN', 'PLT']  # six species

# species_id = {'BD': 0, 'CD': 1, 'STR': 2, 'SPT': 3, 'SPIN': 4, 'PLT': 5}
species_id = {'BD': 0, 'CD': 1, 'STR': 2, 'SPT': 3, 'SPIN': 4, 'PLT': 5, 'NO': 6}
# num_species = 6  # noise not included
num_species = 7  # noise included

# fea_out = '/home/ys587/__Data/__whistle/__cv_os_vs_gilli/__feature_mat'
# fea_out = os.path.join(work_path, '__feature_mat_'+fea_type)
fea_out = os.path.join(work_path, '__feature_mat', '__'+fea_type)
# fea_out = '/home/ys587/__Data/__whistle/__cv_os_vs_gilli/__feature_mat_cqt'
# fea_out = '/home/ys587/__Data/__whistle/__cv_os_vs_gilli/__feature_mat_pcen'

# seltab_out = '/home/ys587/__Data/__whistle/__cv_os_vs_gilli/__sound_seltab'
seltab_out = os.path.join(work_path, '__sound_seltab')

whistle_data_oswald = '/home/ys587/__Data/__whistle/__whistle_oswald'
# csv_oswald_sound = '/home/ys587/__Data/__whistle/__cv_os_vs_gilli/__sound_info/oswald_encounter.csv'
# csv_oswald_info = '/home/ys587/__Data/__whistle/__cv_os_vs_gilli/__sound_info/oswald_soundinfo.csv'
csv_oswald_sound = os.path.join(work_path, '__sound_info', 'oswald_encounter.csv')
csv_oswald_info = os.path.join(work_path, '__sound_info', 'oswald_soundinfo.csv')
df_sound_oswald, df_info_oswald = species_lib.df_sound_info_oswald(csv_oswald_sound, csv_oswald_info, species_to_code, whistle_data_oswald, deployment)

whistle_data_gillispie = '/home/ys587/__Data/__whistle/__whistle_gillispie'
# csv_gillispie_sound = '/home/ys587/__Data/__whistle/__cv_os_vs_gilli/__sound_info/gillispie_encounter.csv'
# csv_gillispie_info = '/home/ys587/__Data/__whistle/__cv_os_vs_gilli/__sound_info/gillispie_soundinfo.csv'
csv_gillispie_sound = os.path.join(work_path, '__sound_info', 'gillispie_encounter.csv')
csv_gillispie_info = os.path.join(work_path, '__sound_info', 'gillispie_soundinfo.csv')
df_sound_gillispie, df_info_gillispie = species_lib.df_sound_info_gillispie\
    (csv_gillispie_sound, csv_gillispie_info, whistle_data_gillispie)

# generate a new df_sound_gillispie with pre-defined list of species
df_sound_oswald = df_sound_oswald[df_sound_oswald['species'].isin(species_list)]
df_sound_gillispie = df_sound_gillispie[df_sound_gillispie['species'].isin(species_list)]

# fea_part_out = '/home/ys587/__Data/__whistle/__cv_os_vs_gilli/__feature_all'
fea_part_out = os.path.join(work_path, '__feature_all')
fea_part1_out = os.path.join(fea_part_out, '__'+fea_type, 'feature_label_oswald.npz')
fea_part2_out = os.path.join(fea_part_out, '__'+fea_type, 'feature_label_gillispie.npz')
# fea_part1_out = '/home/ys587/__Data/__whistle/__cv_os_vs_gilli/__feature_all/feature_label_oswald.npz'
# fea_part2_out = '/home/ys587/__Data/__whistle/__cv_os_vs_gilli/__feature_all/feature_label_gillispie.npz'

# model_path = '/home/ys587/__Data/__whistle/__log_dir_context/__new_results/__48k_fmin_3000_octave_3_binsperoctave_48_binstotal_144/resnet34_expt_all4pie_run0_f1/epoch_160_valloss_0.3137_valacc_0.9483.hdf5'
# model_path = '/home/ys587/__Data/__whistle/__log_dir_context/__new_results/__pcen_nopulse_unit_vector/resnet34_expt_alldata_run0_f1/epoch_199_valloss_0.0003_valacc_1.0000.hdf5'
model_path = '/home/ys587/__Data/__whistle/__log_dir_context/__new_results/__pcen_nopulse_unit_vector2/resnet34_expt_alldata_run0_f1/epoch_33_valloss_0.3726_valacc_0.9333.hdf5'
######################################################
# collect features & labels from multiple data sources
print("Collecting features...")
if os.path.exists(fea_part1_out) & os.path.exists(fea_part2_out) & flag_use_saved_fea:
    print('...reading features from files')
    fea_part1_loadfile = np.load(fea_part1_out)
    fea_part1_4d = fea_part1_loadfile['fea_part_4d']
    label_part1 = fea_part1_loadfile['label_part']

    fea_part2_loadfile = np.load(fea_part2_out)
    fea_part2_4d = fea_part2_loadfile['fea_part_4d']
    label_part2 = fea_part2_loadfile['label_part']
else:
    # Extract features
    print('...extracting features')
    species_lib.extract_fea_gillispie(df_sound_gillispie, model_path,
                                      os.path.join(fea_out, '__gillispie'),
                                      seltab_out, fea_type)
    species_lib.extract_fea_oswald(df_sound_oswald, model_path,
                                   os.path.join(fea_out, '__oswald'),
                                   seltab_out, fea_type)

    # collect all features
    # part 1: oswald
    print('Processing features of Oswald data...')
    species_fea_part1= species_lib.read_features_from_files(os.path.join(fea_out, '__oswald'), species_list)
    if len(species_fea_part1) == 0:
        raise('No feature individual files are present. Need to compute features.')
    fea_part1_4d, label_part1 = species_lib.combine_features_from_dict(species_fea_part1, fea_part1_out, species_id)

    # part 2: gillispie
    print('Processing features of Gillispie data...')
    species_fea_part2 = species_lib.read_features_from_files(os.path.join(fea_out, '__gillispie'), species_list)
    if len(species_fea_part2) == 0:
        raise('No feature individual files are present. Need to compute features.')
    fea_part2_4d, label_part2 = species_lib.combine_features_from_dict(species_fea_part2, fea_part2_out, species_id)

# shuffle data
print("Shuffle the features...")
fea_part1_4d, label_part1 = shuffle(fea_part1_4d, label_part1,
                                    random_state=0)
fea_part2_4d, label_part2 = shuffle(fea_part2_4d, label_part2,
                                    random_state=0)

print("Classifier training...")
log_dir = "/home/ys587/__Data/__whistle/__log_dir_context/contour_temp_audio"

conf = dict()
# conf['save_dir'] = "/home/ys5087/__Data/__whistle/__log_dir_context/audio_data_store_temp"
# conf['data_store'] = "/home/ys587/__Data/__whistle/__log_dir_context/audio_data_store/__four_class"

conf['species_name'] = species_list
conf['species_id'] = species_id
# conf['time_reso'] = 0.01  # 10 ms
# conf['time_reso'] = 0.05  # 50 ms
conf['time_reso'] = 0.02  # 20 ms

# cepstral coefficient
conf['sample_rate'] = 48000
# conf["num_class"] = len(species_list)
conf["num_class"] = num_species

conf['context_winsize'] = 1.0  # sec
conf['context_hopsize'] = 0.5  # sec
conf['contour_timethre'] = 10  # 0.2 s

conf['fft_size'] = 4096
conf['hop_size'] = int(conf['time_reso'] * conf['sample_rate'])
# audio
# conf['freq_ind_high'] = 144

conf['img_t'] = int(
    floor((conf['context_winsize'] / conf['time_reso'])))
# conf['img_f'] = conf['freq_ind_high'] - conf['freq_ind_low']
# conf['img_f'] = 128
conf['img_f'] = 64
conf['input_shape'] = (conf['img_f'], conf['img_t'], 1)

# conf['l2_regu'] = 0.01
# conf['l2_regu'] = 0.005
# conf['l2_regu'] = 0.001  # good for **.1
conf['l2_regu'] = 0.1
# conf['l2_regu'] = 0.0
conf['dropout'] = 0.1
# conf['batch_size'] = 128  # lstm_2lay
# conf['batch_size'] = 32  # resnet 18, 34
conf['batch_size'] = 64
conf['epoch'] = 200
# conf['epoch'] = 1  # debug
conf['learning_rate'] = 1.0
# conf['learning_rate'] = 5.0
# conf['learning_rate'] = 10.0
conf['patience'] = 50

conf['confusion_callback'] = False
conf['spectro_dilation'] = False

conf['numpy_data_output'] = False  # !!
conf['numpy_data_use'] = not conf['numpy_data_output']
conf['img_data_output'] = False  # output image of spectrogram data

# add one more class 'noise': 4 species class + 1 noise class
conf['class_noise'] = True  # add the fifth class: noise
conf['input_shape'] = (conf['img_f'], conf['img_t'], 1)

# # unit length & upper half
# fea_part1_4d = fea_part1_4d[:, conf['img_f']:, :, :] + np.finfo(float).eps
# fea_part2_4d = fea_part2_4d[:, conf['img_f']:, :, :] + np.finfo(float).eps
# for ii in range(fea_part1_4d.shape[0]):
#     fea_sum = np.abs(fea_part1_4d[ii, :, :, :]).sum()
#     if fea_sum:
#         fea_part1_4d[ii, :, :, :] = fea_part1_4d[ii, :, :, :]/fea_sum
#     else:
#         fea_part1_4d[ii, :, :, :] = np.zeros((fea_part1_4d.shape[1], fea_part1_4d.shape[2], 1))
#
# for ii in range(fea_part2_4d.shape[0]):
#     fea_sum = np.abs(fea_part2_4d[ii, :, :, :]).sum()
#     if fea_sum:
#         fea_part2_4d[ii, :, :, :] = fea_part2_4d[ii, :, :, :]/fea_sum
#     else:
#         fea_part2_4d[ii, :, :, :] = np.zeros((fea_part2_4d.shape[1], fea_part2_4d.shape[2], 1))

if expt_flag == 'expt1':
    # two-fold cross-validation. Trained on gillispie & test on oswald; and vice versa.
    model_type = 'resnet34_expt'
    # model_type = 'birdnet_5layers'
    # model_type = 'lenet_dropout_input_conv'
    for rr in range(5):
        proj_name = model_type+'_'+fea_type+'_os_vs_gilli_run' + str(rr) + '_f1'
        print(proj_name)

        conf['log_dir'] = os.path.join(log_dir, proj_name)
        if not os.path.exists(conf['log_dir']):
            os.mkdir(conf['log_dir'])
        conf['num_filters'] = 16

        y_pred2, y_pred2_prob, best_model_path1 = one_fold_validate(
            model_type, fea_part1_4d, label_part1, fea_part2_4d,
            label_part2, conf, fold_id=1)

        y_pred1, y_pred1_prob, best_model_path2 = one_fold_validate(
            model_type, fea_part2_4d, label_part2, fea_part1_4d,
            label_part1, conf, fold_id=2)

        y_pred_tot = np.concatenate((y_pred1, y_pred2))
        label_total = label_part1.tolist() + label_part2.tolist()
        metrics_two_fold(label_total, y_pred_tot, conf['log_dir'],
                         'accuracy_total.txt', conf, mode='total')
elif expt_flag == 'expt2':
    # two-fold cross-validation. Trained on 50% gillispie & 50% oswald, test on the remaining; and vice versa.
    num_part1 = int(floor(fea_part1_4d.shape[0]*0.5))
    num_part2 = int(floor(fea_part2_4d.shape[0]*0.5))
    fea_fold1_4d = np.concatenate((fea_part1_4d[:num_part1, :, :, :], fea_part2_4d[:num_part2, :, :, :]))
    label_fold1 = np.concatenate((label_part1[:num_part1], label_part2[:num_part2]))
    fea_fold2_4d = np.concatenate((fea_part1_4d[num_part1:, :, :, :], fea_part2_4d[num_part2:, :, :, :]))
    label_fold2 = np.concatenate((label_part1[num_part1:], label_part2[num_part2:]))

    # shuffle data
    print("Shuffle the features...")
    fea_fold1_4d, label_fold1 = shuffle(fea_fold1_4d, label_fold1,
                                        random_state=0)
    fea_fold2_4d, label_fold2 = shuffle(fea_fold2_4d, label_fold2,
                                        random_state=0)

    model_type = 'resnet34_expt'
    # model_type = 'birdnet_5layers'
    # model_type = 'lenet_dropout_input_conv'
    for rr in range(5):
        proj_name = model_type+'_'+fea_type+'_os_and_gilli_run' + str(rr) + '_f1'
        print(proj_name)

        conf['log_dir'] = os.path.join(log_dir, proj_name)
        if not os.path.exists(conf['log_dir']):
            os.mkdir(conf['log_dir'])
        conf['num_filters'] = 16

        y_pred2, y_pred2_prob, best_model_path1 = one_fold_validate(
            model_type, fea_fold1_4d, label_fold1, fea_fold2_4d,
            label_fold2, conf, fold_id=1)

        y_pred1, y_pred1_prob, best_model_path2 = one_fold_validate(
            model_type, fea_fold2_4d, label_fold2, fea_fold1_4d,
            label_fold1, conf, fold_id=2)

        y_pred_tot = np.concatenate((y_pred1, y_pred2))
        label_total = label_fold1.tolist() + label_fold2.tolist()
        metrics_two_fold(label_total, y_pred_tot, conf['log_dir'],
                         'accuracy_total.txt', conf, mode='total')

