    #!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Generate training/testing data
Make spectrogram  images
Add preprocessing? Baumgartner & Lin? Gradient?
Add 1-s data, along with 3, 6, 9 s.

Created on 8/11/20
@author: atoultaro
"""
import glob
import os
from math import floor
import numpy as np
import pandas as pd
import librosa
from scipy.ndimage.filters import median_filter
from skimage.measure import block_reduce


def powerlaw(spectro_mat, nu1=1., nu2=2., gamma=1.):
    dim_f, dim_t = spectro_mat.shape

    mu_k = [powelaw_find_mu(spectro_mat[ff, :]) for ff in range(dim_f)]
    mat0 = spectro_mat ** gamma - np.array(mu_k).reshape(dim_f, 1) * np.ones(
        (1, dim_t))
    mat_a_denom = [(np.sum(mat0[:, tt] ** 2.)) ** .5 for tt in range(dim_t)]
    mat_a = mat0 / (np.ones((dim_f, 1)) * np.array(mat_a_denom).reshape(1, dim_t))

    if (np.isnan(mat_a)).sum() > 0:
        print('NaN happened!')

    mat_b_denom = [(np.sum(mat0[ff, :] ** 2.)) ** .5 for ff in range(dim_f)]
    mat_b = mat0 / (np.array(mat_b_denom).reshape(dim_f, 1) * np.ones((1, dim_t)))

    if (np.isnan(mat_b)).sum() > 0:
        print('NaN happened!')

    mat_a = mat_a * (mat_a > 0)  # set negative values into zero
    mat_b = mat_b * (mat_b > 0)

    whistle_powerlaw = (mat_a ** (2.0 * nu1)) * (mat_b ** (2.0 * nu2))

    return whistle_powerlaw


def powelaw_find_mu(time_f):
    time_f_sorted = np.sort(time_f)
    spec_half_len = int(np.floor(time_f_sorted.shape[0] * .5))
    ind_j = np.argmin(
        time_f_sorted[spec_half_len:spec_half_len * 2] - time_f_sorted[0:spec_half_len])
    mu = np.mean(time_f_sorted[ind_j:ind_j + spec_half_len])

    return mu


def nopulse_separation(spectro_mat, harm_dim=(15, 1), per_dim=(1, 15)):
    harmonic_filter = np.asarray(harm_dim, dtype=int)
    percussion_filter = np.asarray(per_dim, dtype=int)
    harmonic_slice = median_filter(spectro_mat, harmonic_filter)
    percussion_slice = median_filter(spectro_mat, percussion_filter)
    # harmonic_mask = harmonic_slice > percussion_slice  # binary
    p_mask = 2.0
    harmonic_slice_ = harmonic_slice**p_mask
    percussion_slice_ = percussion_slice**p_mask
    slice_sum = harmonic_slice_ + percussion_slice_

    bin_mask = 0.5*np.ones(spectro_mat.shape)
    for ii in range(spectro_mat.shape[0]):
        for jj in range(spectro_mat.shape[1]):
            if slice_sum[ii, jj] != 0.:
                bin_mask[ii, jj] = percussion_slice_[ii, jj] / slice_sum[ii, jj]

    # if slice_sum.sum() != 0:
    #     bin_mask = percussion_slice_ / slice_sum
    # else:
    #     bin_mask = np.zeros(slice_sum.shape)

    # spectro_mat_nopulse = spectro_mat * (harmonic_slice_ / slice_sum)
    spectro_mat_nopulse = spectro_mat * bin_mask
    return spectro_mat_nopulse


def unit_vector(whistle_freq):
    fea_sum = np.abs(whistle_freq).sum()
    if fea_sum > 0.0:
        whistle_freq = whistle_freq / fea_sum
    else:
        whistle_freq = np.zeros((whistle_freq.shape[0], whistle_freq.shape[1]))

    return whistle_freq


species_list = ['BD', 'LCD', 'SCD', 'STR', 'SPT', 'SPIN', 'PLT', 'RT', 'FKW']  # nine species
species_id = {'BD': 0, 'LCD': 1, 'SCD': 2, 'STR': 3, 'SPT': 4, 'SPIN': 5, 'PLT': 6, 'RT': 7, 'FKW': 8, 'NO': 9}
num_species = len(species_list) + 1  # noise included = 10

fea_out_path = '/home/ys587/__Data/__whistle/__domain_adaptation/__data_feature/__temp'
sound_seltab = '/home/ys587/__Data/__whistle/__domain_adaptation/__sound_seltab'
# sound_seltab = '/home/ys587/__Data/__whistle/__domain_adaptation/__sound_seltab_small'
# spec_type = 'linear'  # mel or linear
spec_type = 'mel'

conf = dict()
conf['samplerate'] = 48000
conf['fft_size'] = 1024
conf['win_length'] = 480  # 10 msec
conf['hop_length'] = 480  # 10 msec
# conf['hop_length'] = int(0.02 * conf['samplerate'])  # 960
# Old format: linear, 1024, 960, 960
# new format: mel, 2048, 1920, 960  # window too wide? ??? weird format
# new mel format: 1024, 960, 480  # data size too large?

conf['winsize_seltab'] = 1.0  # sec
conf['hopsize_seltab'] = 0.5  # sec
# conf['winsize'] = 3.0  # sec  # sound duration for the new feature
# conf['winsize'] = 6.0
winsize_list = [3.0]
# winsize_list = [3.0, 6.0, 1.0]
# winsize_list = [3.0, 1.0]
# conf['whistleness'] = 0.25
conf['whistleness_num'] = 1.
conf['score_noise'] = 0.02  # noise needs to have at least a window with the score larger than 0.02
conf['whistleness_noise'] = 1.0  # all of windows need to be noise and noise only

data = 'oswald'

for vv in winsize_list:
    print('Processing of sounds in {:.1f} sec'.format(vv))
    conf['winsize'] = vv
    conf['win_indices'] = int((conf['winsize'] - conf['winsize_seltab']) / conf['hopsize_seltab']) + 1
    sound_duration = str(int(conf['winsize']))

    fea_spectro_list = []
    fea_pcen_list = []
    fea_powerlaw_list = []
    fea_nopulse_list = []
    # fea_unit_vec_list = []
    fea_id = 0
    fea_id_list = []
    metadata_list = []

    for ss in species_list:
        # for ss in ['STR']:
        print(ss)
        seltab_list = glob.glob(os.path.join(sound_seltab, ss + '*_pos.txt'))
        seltab_list.sort()
        for ww in seltab_list:  # for a acoustic encounter
            print('--'+ww)

            # collect positive events
            df_seltab = pd.read_csv(ww, sep='\t')
            files = df_seltab['Begin Path'].unique()

            df_seltab_neg = pd.read_csv(ww[:-7] + 'neg.txt', sep='\t')
            files = df_seltab_neg['Begin Path'].unique()

            for ff in files:  # for each sound file
                print('---'+ff)
                print('----collect positive events')
                df_file = df_seltab[df_seltab['Begin Path']==ff]
                begin_time = np.array(df_file['Begin Time (s)'])
                begin_time_indices = np.array(df_file['Begin Time (s)']/conf['hopsize_seltab']).astype(int)

                samples, _ = librosa.load(ff, sr=conf['samplerate'])
                if samples.ndim == 2:  # convert to mono if stereo
                    samples = samples[:, 0]
                    samples = samples[:, -1]
                time_ind_last = floor((samples.shape[0]*1.0/conf['samplerate']-conf['winsize_seltab'])/conf['hopsize_seltab'])+1
                time_indices = np.zeros(time_ind_last)

                for bb in begin_time_indices:
                    if bb < time_ind_last:
                        time_indices[bb] = 1

                # have 5 integers in a row => 5 sec; can have fractions of an integer
                for tt in range(time_ind_last):
                    # whistle_percent = time_indices[tt:tt+conf['win_indices']].sum()/conf['win_indices']
                    whistle_num = time_indices[tt:tt + conf['win_indices']].sum()
                    begin_sample = floor(tt*conf['hopsize_seltab']*conf['samplerate'])
                    end_sample = floor((tt*conf['hopsize_seltab']+conf['winsize'])*conf['samplerate'])

                    if (whistle_num >= conf['whistleness_num']) & (end_sample <= samples.shape[0]):
                        species_label = ss
                        print('#', end='')
                        if spec_type == 'mel':
                            # mel spectrogram
                            spectro = librosa.feature.melspectrogram(
                                y=samples[begin_sample:end_sample],
                                sr=conf['samplerate'],
                                n_fft=conf['fft_size'],
                                win_length=conf['win_length'],
                                hop_length=conf['hop_length'], power=1)
                        elif spec_type == 'linear':
                            spectro0 = np.abs(librosa.stft(
                                samples[begin_sample:end_sample],
                                # sr=conf['samplerate'],
                                n_fft=conf['fft_size'],
                                win_length=conf['win_length'],
                                hop_length=conf['hop_length']
                            ))
                            spectro = block_reduce(spectro0[:-1, :], (4, 1), np.mean)
                            # spectro = librosa.pcen(spectro)
                        else:
                            print('No supported spectrogram type.')

                        # features here!!
                        # pcen
                        whistle_pcen = librosa.pcen(spectro*(2**31))
                        # powerlaw
                        # whistle_powerlaw = powerlaw(spectro)

                        # how to store features & metadata, a dataframe, mayber?.bp
                        fea_spectro_list.append(spectro)
                        fea_pcen_list.append(whistle_pcen.astype('float32'))
                        # fea_powerlaw_list.append(whistle_powerlaw.astype('float32'))
                        fea_id_list.append(fea_id)

                        # encounter & deployment
                        ff2 = os.path.normpath(os.path.dirname(ff))
                        ff2 = ff2.split(os.sep)
                        encounter = ff2[-1].split(' ')[1]
                        deployment = ff2[-2]

                        # meta data in a series
                        metadata_list.append(pd.Series({
                            'fea_id': fea_id,
                            'dataset': data,  # oswald or gillispie
                            'species': species_label,
                            'species_id': species_id[species_label],
                            'seltab': ww,
                            'file': ff,
                            'begin_time': tt*conf['hopsize_seltab'],
                            'end_time': tt*conf['hopsize_seltab']+conf['winsize'],
                            'time_index': tt,
                            'whistle_num': whistle_num,
                            'encounter': encounter,  # filenames on gillispie; folder names on oswald
                            'deployment': deployment,  # filenames on gillispie; up folder names on oswald
                        }))
                        fea_id += 1
                print('')

                # collect negative events
                # load _neg.txt with the same file basename
                print('----.scollect negative events')
                df_file_neg = df_seltab_neg[df_seltab_neg['Begin Path']==ff]
                begin_time = np.array(df_file_neg['Begin Time (s)'])
                score_neg = np.array(df_file_neg['Score'])
                begin_time_neg_indices = np.array(df_file_neg['Begin Time (s)']/conf['hopsize_seltab']).astype(int)

                # samples, _ = librosa.load(ff, sr=conf['samplerate'])
                # if samples.ndim == 2:  # convert to mono if stereo
                #     samples = samples[:, 0]
                #     samples = samples[:, -1]
                # time_ind_last = floor((samples.shape[0]*1.0/conf['samplerate']-conf['winsize_seltab'])/conf['hopsize_seltab'])+1
                time_neg_indices = np.zeros(time_ind_last)
                score_neg_arr = np.zeros(time_ind_last)

                for bb0 in range(begin_time_neg_indices.shape[0]):
                    bb = begin_time_neg_indices[bb0]
                    if bb < time_ind_last:
                        time_neg_indices[bb] = 1
                        score_neg_arr[bb] = score_neg[bb0]

                for tt in range(time_ind_last):
                    whistle_percent = time_neg_indices[tt:tt+conf['win_indices']].sum()/conf['win_indices']
                    noise_presence = score_neg_arr[tt:tt+conf['win_indices']].mean()
                    begin_sample = floor(tt*conf['hopsize_seltab']*conf['samplerate'])
                    end_sample = floor((tt*conf['hopsize_seltab']+conf['winsize'])*conf['samplerate'])

                    if (whistle_percent == conf['whistleness_noise']) & \
                            (noise_presence >= conf['score_noise']) & \
                            (end_sample <= samples.shape[0]):
                        species_label = 'NO'
                        print('#', end='')

                        if spec_type == 'mel':
                            # mel spectrogram
                            spectro = librosa.feature.melspectrogram(
                                samples[begin_sample:end_sample],
                                sr=conf['samplerate'],
                                hop_length=conf['hop_length'], power=1)
                        elif spec_type == 'linear':
                            spectro0 = np.abs(librosa.stft(
                                samples[begin_sample:end_sample],
                                n_fft=conf['fft_size'],
                                win_length=conf['win_length'],
                                hop_length=conf['hop_length']
                            ))
                            spectro = block_reduce(spectro0[:-1, :], (4, 1), np.mean)
                        else:
                            print('No supported spectrogram type.')

                        # features here!!
                        # pcen
                        whistle_pcen = librosa.pcen(spectro*(2**31))
                        # powerlaw
                        # whistle_powerlaw = powerlaw(spectro)

                        # how to store features & metadata, a dataframe, mayber?.bp
                        fea_spectro_list.append(spectro)
                        fea_pcen_list.append(whistle_pcen.astype('float32'))
                        # fea_powerlaw_list.append(whistle_powerlaw.astype('float32'))
                        fea_id_list.append(fea_id)

                        # encounter & deployment
                        ff2 = os.path.normpath(os.path.dirname(ff))
                        ff2 = ff2.split(os.sep)
                        encounter = ff2[-1].split(' ')[1]
                        deployment = ff2[-2]

                        # meta data in a series
                        metadata_list.append(pd.Series({
                            'fea_id': fea_id,
                            'dataset': data,  # oswald or gillispie
                            'species': species_label,
                            'species_id': species_id[species_label],
                            'seltab': ww,
                            'file': ff,
                            'begin_time': tt*conf['hopsize_seltab'],
                            'end_time': tt*conf['hopsize_seltab']+conf['winsize'],
                            'time_index': tt,
                            'whistle_num': 0,
                            'encounter': encounter,  # filenames on gillispie; folder names on oswald
                            'deployment': deployment,  # filenames on gillispie; up folder names on oswald
                        }))
                        fea_id += 1
                print('')

    if len(fea_spectro_list) != 0:
        fea_spectro = np.stack(fea_spectro_list)
        del fea_spectro_list
        fea_pcen = np.stack(fea_pcen_list)
        del fea_pcen_list
        # fea_powerlaw = np.stack(fea_powerlaw_list)
        # del fea_powerlaw_list
        # fea_id = np.stack(fea_id_list)

        metadata = pd.concat(metadata_list, axis=1).T

        # save features and metadata
        print('Saving features...')
        np.save(os.path.join(fea_out_path, 'features_'+data+'_'+sound_duration+'s_spectro.npy'), fea_spectro)
        np.save(os.path.join(fea_out_path, 'features_' + data + '_' + sound_duration + 's_pcen.npy'), fea_pcen)
        # np.save(os.path.join(fea_out_path, 'features_'+data+'_'+sound_duration+'s_powerlaw.npy'), fea_powerlaw)
        metadata.to_csv(os.path.join(fea_out_path, 'labels_'+data+'_'+sound_duration+'s.csv'), index=False)
