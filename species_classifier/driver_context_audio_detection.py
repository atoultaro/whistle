#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Detecting whistles in sound streams by applying a trained classifier
Created on 4/8/20
@author: atoultaro
"""
import os
import numpy as np
from math import ceil, floor
import glob
from keras.models import load_model
import librosa
import soundfile as sf
from species_classifier.driver_context_conv_audio_4fold import contour_data

pie_num = 4

# load a trained classifier model
# root_path = '/home/ys587/__Data/__whistle/__log_dir_context/__new_results/__win_1s_overlap_p5s_contour_p5s_timereso_p02/resnet34_expt_run0_f1_p701'
# model_all_path = os.path.join(root_path, 'fold1/epoch_28_valloss_0.3546_valacc_0.9431.hdf5')
root_path = '/home/ys587/__Data/__whistle/__log_dir_context/__new_results/__win_1s_overlap_p5s_contour_p2s_timereso_p02/resnet_cifar10_expt_run1_f1'
#model_all_path = os.path.join(root_path, 'fold1/epoch_37_valloss_0.4522_valacc_0.9128.hdf5')
model_all_path = []
for pp in range(pie_num):
    model_path = os.path.join(root_path, 'fold' + str(pp + 1))
    model_name = glob.glob(model_path + '/*.hdf5')[0]
    print(model_name)
    model_all_path.append(model_name)

# find the sound files for each pie
from cape_cod_whale.preprocess import bin_extract, contour_target_retrieve
bin_dir = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/cv4'
sound_dir = '/home/ys587/__Data/__whistle/__sound_species/'

time_multi = 50
pred_score_thre = 0.99
species_name = ['bottlenose', 'common', 'spinner', 'melon-headed']
species_num = len(species_name)
conf = dict()
conf['time_reso'] = 0.02
conf['sample_rate'] = sample_rate = 192000
conf['hop_size'] = int(conf['time_reso']*conf['sample_rate'])

label_truth_list = []
whistle_presence_list = []
label_pred_list = []
prob_pred_list = []

# pp=0
for pp in range(pie_num):
    print('Cross-validation: '+str(pp+1)+' / '+str(pie_num))
    # classifier_model = load_model(model_all_path)
    print('Trained classifier model:')
    print(model_all_path[pp])
    classifier_model = load_model(model_all_path[pp])

    bin_dir_fold = dict()
    bin_dir_fold['pie' + str(pp + 1)] = os.path.join(bin_dir, 'pie' + str(pp + 1))

    contour_pie_curr, bin_wav_pair_pie_curr = bin_extract(
        bin_dir_fold['pie' + str(pp + 1)], sound_dir, species_name)
    # ground truth of whistles for a pie
    contour_target_list = contour_target_retrieve(contour_pie_curr,
                                                    bin_dir_fold['pie'+str(pp+1)],
                                                    conf['time_reso'])

    # run the trained classifier on sound stream
    # from upcall.run_detector_dsp import make_sound_stream, run_detector_1day_parallel_fft
    # sound_file = os.path.join(root_path, 'data_whale_upcall_net/dclde2013_testing/sound_3_days/NOPP6_EST_20090403')
    # sample_stream = make_sound_stream(sound_file)
    for ff in range(len(contour_target_list)):
    # for ff in range(2):  # debug
        filename = contour_target_list[ff][0]
        print('\n' + filename)
        label_contour = contour_target_list[ff][1]
        file_contour = contour_target_list[ff][2]

        contour_target_ff, start_time, end_time, freq_low, freq_high = contour_data(
            file_contour, conf['time_reso'])

        sound_path = os.path.join(sound_dir, label_contour, filename + '.wav')
        sound_info = sf.info(sound_path)
        assert(sound_info.samplerate == conf['sample_rate'])

        # ground truth
        win_hop_size = time_multi*conf['time_reso']  # 1.0
        timesteps = ceil(sound_info.duration / conf['time_reso'])
        whistle_presence0 = np.zeros((int(timesteps)))
        for cc in contour_target_ff:
            time_ind_start = int(floor(cc['Time'][0] / conf['time_reso']))
            for ii in range(cc['Time'].shape[0]-1):
                whistle_presence0[time_ind_start + ii] = 1.0
            # print('')
        whistle_presence = np.add.reduceat(whistle_presence0, range(0, whistle_presence0.shape[0], time_multi))
        whsitle_len = floor(whistle_presence0.shape[0] / time_multi)
        whistle_presence = whistle_presence[:whsitle_len]/time_multi
        whistle_presence_list.append(whistle_presence)
        label_truth = (whistle_presence>0).astype(int)
        label_truth_list.append(label_truth)

        # detect on the sound pie
        # spectrogram named whistle_freq for each file
        # sound_path = os.path.join(sound_dir, label_contour, filename + '.wav')
        samples, _ = librosa.load(sound_path, sr=conf['sample_rate'])
        whistle_freq = np.abs(librosa.pseudo_cqt(samples, sr=conf['sample_rate'],
                                                 hop_length=conf['hop_size'],
                                                 fmin=4000.0, bins_per_octave=36,
                                                 n_bins=144))

        # given cqt-spectrogram, whistle_freq, make predictions
        whistle_freq_list = []
        for ss in range(whsitle_len):
            whistle_freq_curr = whistle_freq[:, ss*time_multi:(ss+1)*time_multi]
            whistle_freq_list.append(whistle_freq_curr)
        whistle_image = np.stack(whistle_freq_list)
        whistle_image_4d = np.expand_dims(whistle_image, axis=3)
        predictions = classifier_model.predict(whistle_image_4d)
        predictions_bin = np.stack((predictions[:, -1], predictions[:, :-1].sum(axis=1)), axis=-1)
        prob_pred_list.append(predictions_bin)

        # Methods to pick up the predictive labels
        # Option 1: argmax of predictive probs
        label_pred0 = np.argmax(predictions, axis=1)
        label_pred = (label_pred0 <= 3).astype(int)
        # Option 2: probs of noises is smaller than 0.1
        # label_pred = (predictions[:, species_num] <= pred_score_thre).astype(int)

        label_pred_list.append(label_pred)

        # print('')

label_truth_all = np.concatenate(label_truth_list)
whistle_presence_all = np.concatenate(whistle_presence_list)
prob_pred_all = np.concatenate(prob_pred_list)
label_pred_all = np.concatenate(label_pred_list)

from sklearn.metrics import classification_report, confusion_matrix
print(classification_report(label_truth_all, label_pred_all, target_names=['noise', 'whistle']))
print(confusion_matrix(label_truth_all, label_pred_all))

# fn
fn_arr = ((label_truth_all == 1) & (label_pred_all == 0)).astype(int)
fn_ind = np.where(fn_arr)[0]
whistle_percent_fn = whistle_presence_all[fn_ind]
print('FN:')
print('Median: '+str(np.median(whistle_percent_fn)))
print('Mean: '+str(np.mean(whistle_percent_fn)))
# tp
tp_arr = ((label_truth_all == 1) & (label_pred_all == 1)).astype(int)
tp_ind = np.where(tp_arr)[0]
whistle_percent_tp = whistle_presence_all[tp_ind]
print('TP:')
print('Median: '+str(np.median(whistle_percent_tp)))
print('Mean: '+str(np.mean(whistle_percent_tp)))

import pandas as pd
whistleness = np.concatenate((whistle_percent_tp, whistle_percent_fn))
pred_class = ['whistle']*whistle_percent_tp.shape[0]+['noise']*whistle_percent_fn.shape[0]
data = {"whistleness(%)":whistleness, "pred_class":pred_class}
whistle_df = pd.DataFrame(data)

# boxplot: whistleness vs pred_class
import matplotlib.pyplot as plt
import seaborn as sns
sns.set(style='darkgrid')
ax = sns.boxplot(x="pred_class", y="whistleness(%)", data=whistle_df)
plt.show()

# precision-recall curve
from sklearn.metrics import precision_recall_curve
precision, recall, thres = precision_recall_curve(label_truth_all, prob_pred_all[:,1])
curve_data = {'precision': precision, 'recall': recall}
df_curve = pd.DataFrame(curve_data)
ax = sns.lineplot(x='recall', y='precision', data=df_curve)
plt.show()

# average precision score
from sklearn.metrics import average_precision_score
average_precision = average_precision_score(label_truth_all, prob_pred_all[:,1], average=None)
print('average_precision: '+str(average_precision))