#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Created on 6/11/20
@author: atoultaro
"""
import os
import numpy as np
from sklearn.metrics import confusion_matrix
# import ml_metrics
import pandas as pd
import seaborn as sn
import matplotlib.pyplot as plt
from sklearn.utils import shuffle
# %matplotlib inline


# truth result
fea_part1_out = '/home/ys587/__Data/__whistle/__whistle_all/__feature_all/feature_label_part1.npz'
fea_part2_out = '/home/ys587/__Data/__whistle/__whistle_all/__feature_all/feature_label_part2.npz'

fea_part1_loadfile = np.load(fea_part1_out)
fea_part1_4d = fea_part1_loadfile['fea_part1_4d']
label_part1 = fea_part1_loadfile['label_part1']
_, label_part1 = shuffle(fea_part1_4d, label_part1, random_state=0)

fea_part2_loadfile = np.load(fea_part2_out)
fea_part2_4d = fea_part2_loadfile['fea_part2_4d']
label_part2 = fea_part2_loadfile['label_part2']
_, label_part2 = shuffle(fea_part2_4d, label_part2, random_state=0)

# label_tot = label_part1 + label_part2
label_tot = np.concatenate((label_part1, label_part2))

# experimental result
log_dir = '/home/ys587/__Data/__whistle/__log_dir_context/contour_temp_audio/resnet34_expt_two_fold_run0_f1'
log_dir1 = os.path.join(log_dir, 'fold1')
log_dir2 = os.path.join(log_dir, 'fold2')

pred_label2 = np.loadtxt(os.path.join(log_dir1, 'pred_label.txt'), delimiter=',')
pred_label1 = np.loadtxt(os.path.join(log_dir2, 'pred_label.txt'), delimiter=',')

y_pred_tot = np.concatenate((pred_label1, pred_label2))

# metrics
# ml_metrics.mapk(label_tot.tolist(), y_pred_tot.tolist(), 1)
data = confusion_matrix(label_tot, y_pred_tot)
df_cm = pd.DataFrame(data, columns=np.unique(label_tot), index=np.unique(label_tot))
df_cm.index.name = 'True'
df_cm.columns.name = 'Predicted'
plt.figure(figsize=(30, 21))
sn.set(font_scale=1.4)  #for label size
sn.heatmap(df_cm, cmap="Blues", annot=True, annot_kws={"size": 16})  # font size
plt.show()