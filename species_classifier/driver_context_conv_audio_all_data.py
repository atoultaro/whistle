#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
train a classifier using all data (all four pies)
It will be used for whistleness detection

Created on 4/23/20
@author: atoultaro
"""
import os
import numpy as np
from math import floor, ceil

from sklearn.metrics import confusion_matrix, balanced_accuracy_score, \
    classification_report, f1_score
from sklearn.model_selection import train_test_split
import librosa
from keras import backend
from keras.losses import categorical_crossentropy
from keras.utils import to_categorical
from keras.optimizers import Adam, Adadelta

from keras.callbacks import ModelCheckpoint, TensorBoard, EarlyStopping
import cv2
from keras.models import Sequential, load_model, Model

from cape_cod_whale.preprocess import bin_extract, contour_target_retrieve, \
    data_generator

from cape_cod_whale.classifier import two_fold_cross_validate_random_forest, metrics_two_fold
from cape_cod_whale.load_feature_model import load_fea_model, find_best_model

from contextlib import redirect_stdout

from keras.callbacks import Callback
import matplotlib.patches as mpatches
import itertools

from species_classifier.build_model import *
from species_classifier.driver_context_conv_audio_4fold import \
    all_data_train_validate, prepare_data_audio


def main():
    # bin files for training & testing
    bin_dir = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/cv4'
    # bin_dir_train = os.path.join(bin_dir, 'cv2/__first_pie')
    # bin_dir_test = os.path.join(bin_dir, 'cv2/__second_pie')
    bin_dir_fold = dict()
    for pp in range(4):  # 'bin_dir_fold['pie1']'
        bin_dir_fold['pie'+str(pp+1)] = os.path.join(bin_dir, 'pie'+str(pp+1))

    sound_dir = '/home/ys587/__Data/__whistle/__sound_species/'
    species_name = ['bottlenose', 'common', 'spinner', 'melon-headed']
    species_id = {'bottlenose': 0, 'common': 1, 'spinner': 2, 'melon-headed': 3}

    conf_gen = dict()
    conf_gen['log_dir'] = "/home/ys587/__Data/__whistle/__log_dir_context/contour_temp_audio"
    # conf_gen['save_dir'] = "/home/ys587/__Data/__whistle/__log_dir_context/audio_data_store_temp"
    conf_gen['save_dir'] = '/home/ys587/__Data/__whistle/__log_dir_context/audio_data_store/__pcen_nopulse/__1s_win_p2s_hop_p4s_contour_48k_samplerate'
    # conf_gen['data_store'] = "/home/ys587/__Data/__whistle/__log_dir_context/audio_data_store/__four_class"
    conf_gen['bin_dir'] = bin_dir

    conf_gen['species_name'] = species_name
    conf_gen['species_id'] = species_id
    # conf_gen['time_reso'] = 0.01  # 10 ms
    # conf_gen['time_reso'] = 0.05  # 50 ms
    conf_gen['time_reso'] = 0.02  # 20 ms

    # cepstral coefficient
    # conf_gen['sample_rate'] = 192000
    conf_gen['sample_rate'] = 48000
    # conf_gen["num_class"] = len(species_name)
    conf_gen["num_class"] = 2-1

    # conf_gen['context_winsize'] = 5.0  # sec
    # conf_gen['context_hopsize'] = 1.0  # sec
    # conf_gen['contour_timethre'] = 50  # 1 s
    # conf_gen['context_winsize'] = 5.12  # sec for attention model

    conf_gen['context_winsize'] = 1.0  # sec
    conf_gen['context_hopsize'] = 0.1  # sec
    conf_gen['contour_timethre'] = 20  # 0.4 s; 25 for 0.5 s

    # fea_type = 'cqt'
    # fea_type = 'pcen'
    fea_type = 'pcen_nopulse'
    # fea_type = 'powerlaw'
    # fea_type = 'powerlawsym'

    conf_gen['fft_size'] = 4096
    conf_gen['hop_size'] = int(conf_gen['time_reso']*conf_gen['sample_rate'])

    # audio
    # conf_gen['freq_ind_low'] = 0
    # # conf_gen['freq_ind_low'] = 20
    # conf_gen['freq_ind_high'] = 144
    # # CQT
    # conf_gen['fmin'] = 3000.0
    # conf_gen['bins_per_octave'] = 48
    # conf_gen['n_bins'] = 144

    conf_gen['img_t'] = int(floor((conf_gen['context_winsize'] / conf_gen['time_reso'])))
    # conf_gen['img_f'] = conf_gen['freq_ind_high'] - conf_gen['freq_ind_low']
    conf_gen['img_f'] = 64
    conf_gen['input_shape'] = (conf_gen['img_f'], conf_gen['img_t'], 1)

    conf_gen['l2_regu'] = 0.01
    # conf_gen['l2_regu'] = 0.001
    # conf_gen['l2_regu'] = 0.2
    conf_gen['dropout'] = 0.1
    # conf_gen['batch_size'] = 128  # lstm_2lay
    # conf_gen['batch_size'] = 32  # resnet 18, 34
    conf_gen['batch_size'] = 64
    conf_gen['epoch'] = 200
    # conf_gen['epoch'] = 1  # debug
    # conf_gen['learning_rate'] = 1.0
    conf_gen['learning_rate'] = 0.5
    # conf_gen['learning_rate'] = 0.1
    pie_num = 4
    conf_gen['pie_num'] = pie_num

    conf_gen['confusion_callback'] = False
    conf_gen['spectro_dilation'] = False

    # conf_gen['numpy_data_output'] = False  # !!
    # conf_gen['numpy_data_use'] = not conf_gen['numpy_data_output']
    conf_gen['numpy_data_use'] = True
    conf_gen['img_data_output'] = False  # output image of spectrogram data

    # add one more class 'noise': 4 species class + 1 noise class
    conf_gen['class_noise'] = True  # add the fifth class: noise

    # Use masked spectrogram of whistle contours, instead of original spectrograms
    # conf_gen['mask_spec_contour'] = True
    conf_gen['mask_spec_contour'] = False

    for pp in range(pie_num):  # 'pie1_data.npz'
        conf_gen['save_file_pie'+str(pp+1)] = os.path.join(conf_gen['save_dir'],
                                                   'pie'+str(pp+1)+'_data.npz')
    for pp in range(pie_num):
        conf_gen['image_pie' + str(pp + 1)] = os.path.join(conf_gen['save_dir'],
                                                               'pie' + str(pp + 1))

    if not os.path.exists(conf_gen['log_dir']):
        os.mkdir(conf_gen['log_dir'])

    conf_gen['network_type'] = 'cnn'
    # conf_gen['recurrent_dropout'] = 0.1
    conf_gen['recurrent_dropout'] = 0.01
    conf_gen['dense_size'] = 128

    # Read species labels, filenames & extract time and frequency sequences
    contour_pie_list = []
    bin_wav_pair_pie_list = []
    for pp in range(pie_num):
        contour_pie_curr, bin_wav_pair_pie_curr = bin_extract(
            bin_dir_fold['pie'+str(pp+1)], sound_dir, species_name)
        contour_pie_list.append(contour_pie_curr)
        bin_wav_pair_pie_list.append(bin_wav_pair_pie_curr)

    # read contours from bin files
    contour_pie_list_alllist =  []
    for pp in range(pie_num):
        contour_pie_list_curr = contour_target_retrieve(contour_pie_list[pp],
                                                    bin_dir_fold['pie'+str(pp+1)], conf_gen['time_reso'])
        contour_pie_list_alllist.append(contour_pie_list_curr)

    # prepare training & testing data
    if conf_gen['class_noise']:
        conf_gen['species_name'].append('noise')
        conf_gen['species_id'].update({'noise': 4})
        conf_gen["num_class"] += 1
        if not conf_gen['mask_spec_contour']:
            conf_gen['data_store'] = conf_gen['save_dir']
            # expt
            # conf_gen['data_store'] = "/home/ys587/__Data/__whistle/__log_dir_context/audio_data_store/__five_class"
            # 1-s win
            # conf_gen['data_store'] = "/home/ys587/__Data/__whistle/__log_dir_context/audio_data_store/__1s_win/__five_class"
            # 1-s win, 0.5-s overlap
            # conf_gen['data_store'] = "/home/ys587/__Data/__whistle/__log_dir_context/audio_data_store/__1s_win/__five_class_win_1s_overlap_p5s_contour_p2s_timereso_p02"
            # 1-s win, 0.5-s overlap, 0.4s and up contour, 48 kHz
            # conf_gen['data_store'] = "/home/ys587/__Data/__whistle/__log_dir_context/audio_data_store/__pcen_nopulse/__1s_win_p5s_hop_p4s_contour_48k_samplerate"
            # 1-s win, 0.2-s overlap, 0.4s and up contour, 48 kHz
            # conf_gen['data_store'] = "/home/ys587/__Data/__whistle/__log_dir_context/audio_data_store/__pcen_nopulse/__1s_win_p2s_hop_p4s_contour_48k_samplerate"
            # attention
            # conf_gen['data_store'] = "/home/ys587/__Data/__whistle/__log_dir_context/audio_data_store/__five_class_attention"
        else:  # Masked-spectrogram-contour
            conf_gen['data_store'] = \
                "/home/ys587/__Data/__whistle/__log_dir_context/audio_data_store/__five_class_mask_spec_pasterization"

    # Pie 1 - 4
    whistle_image_pie_list = []
    label_pie_list = []
    # freq_high_pie_list = []
    # freq_low_pie_list = []
    for pp in range(pie_num):
        pie_curr_data_path = os.path.join(conf_gen['data_store'], 'pie'+str(pp+1)+'_data.npz')
        if os.path.exists(pie_curr_data_path) & conf_gen['numpy_data_use']:
            print('Loading pie1 data...')
            # data_temp = np.load(pie_curr_data_path)
            data_temp = np.load(pie_curr_data_path, allow_pickle=True)
            whistle_image_pie_curr = data_temp['whistle_image']
            label_pie_curr = data_temp['label'].tolist()
        else:  # extract features
            whistle_image_pie_curr, label_pie_curr, _, _ = prepare_data_audio(
                contour_pie_list_alllist[pp], sound_dir, conf_gen, conf_gen['image_pie'+str(pp+1)],
                conf_gen['save_file_pie'+str(pp+1)], fea_type=fea_type)
        whistle_image_pie_list.append(whistle_image_pie_curr)
        label_pie_list.append(label_pie_curr)

    # Change the dimensions
    if conf_gen['network_type'] is 'cnn':
        whistle_image_pie_4d_list = []
        for pp in range(pie_num):
            whistle_image_pie_curr_4d = np.expand_dims(whistle_image_pie_list[pp], axis=3)
            whistle_image_pie_4d_list.append(whistle_image_pie_curr_4d)
    elif conf_gen['network_type'] is 'rnn':
        whistle_image_pie_4d_list = []
        for pp in range(pie_num):
            whistle_image_pie_curr_4d = np.transpose(whistle_image_pie_list[pp], (0, 2, 1))
            whistle_image_pie_4d_list.append(whistle_image_pie_curr_4d)
    elif conf_gen['network_type'] is 'conv2d_lstm':
        whistle_image_pie_4d_list = []
        for pp in range(pie_num):
            whistle_image_pie_curr_0 = np.expand_dims(whistle_image_pie_list[pp],
                                                  axis=(3, 4))
            whistle_image_pie_curr_4d = np.transpose(whistle_image_pie_curr_0,
                                                 (0, 2, 1, 3, 4))
            whistle_image_pie_4d_list.append(whistle_image_pie_curr_4d)

    log_dir = '/home/ys587/__Data/__whistle/__log_dir_context/__new_results'

    learning_rate_list = []
    le_regu_list = []
    for ii_lr in [1.0, 0.1]:
        for ii_l2 in [0.1, 0.01]:
            learning_rate_list.append(ii_lr)
            le_regu_list.append(ii_l2)

    for rr in range(len(le_regu_list)):
        conf_gen['learning_rate'] = learning_rate_list[rr]
        conf_gen['l2_regu'] = le_regu_list[rr]

        model_type = 'resnet34_expt'
        proj_name = model_type + '_alldata_run' + str(rr) + '_f1'
        print(proj_name)

        conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
        if not os.path.exists(conf_gen['log_dir']):
            os.mkdir(conf_gen['log_dir'])
        conf_gen['num_filters'] = 16

        whistle_image_train_fold1_4d = np.vstack((whistle_image_pie_4d_list[0],
        whistle_image_pie_4d_list[1], whistle_image_pie_4d_list[2],
        whistle_image_pie_4d_list[3]))
        label_train_all0 = label_pie_list[0] + label_pie_list[1] + \
                            label_pie_list[2] + label_pie_list[3]
        label_train_all = [1 if (x is 0) | (x is 1) | (x is 2) | (x is 3) else 0 for x in label_train_all0]

        best_model_path = all_data_train_validate(model_type,
                                                  whistle_image_train_fold1_4d,
                                                  label_train_all,
                                                  conf_gen,
                                                  conf_gen['log_dir'])

        if False:
            model_type = 'resnet_cifar10_expt'
            proj_name = model_type + '_alldata_run' + str(rr) + '_f1'
            print(proj_name)

            conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
            if not os.path.exists(conf_gen['log_dir']):
                os.mkdir(conf_gen['log_dir'])
            conf_gen['num_filters'] = 16

            whistle_image_train_4d = np.vstack((whistle_image_pie_4d_list[0],
            whistle_image_pie_4d_list[1], whistle_image_pie_4d_list[2],
            whistle_image_pie_4d_list[3]))
            label_train_all0 = label_pie_list[0] + label_pie_list[1] + \
                                label_pie_list[2] + label_pie_list[3]
            label_train_all = [1 if (x is 0) | (x is 1) | (x is 2) | (x is 3) else 0 for x in label_train_all0]

            best_model_path = all_data_train_validate(model_type,
                                                      whistle_image_train_4d,
                                    label_train_all, conf_gen, conf_gen['log_dir'])


if __name__ == '__main__':
    main()