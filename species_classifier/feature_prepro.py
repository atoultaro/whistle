#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Created on 10/28/20
@author: atoultaro
"""

fea_in_path = '/home/ys587/__Data/__whistle/__domain_adaptation/__data_feature/__temp'
fea_out_path = fea_in_path

import numpy as np
import random
import os
import cv2
import seaborn as sns
from scipy.ndimage.filters import median_filter


def nopulse_median(spectro_mat, per_dim=(15, 1)):
    """
    Gillispie's median filter
    """
    percussion_filter = np.asarray(per_dim, dtype=int)

    spectro_mat += 1. / 32767
    # spectro_mat = 10*np.log10(spectro_mat)

    percussion_slice = median_filter(spectro_mat, percussion_filter)

    spectro_mat_nopulse = spectro_mat - percussion_slice

    return spectro_mat_nopulse


def spectro_median(fea_img):
    fea_nopulse_list = []
    for ii in range(fea_img.shape[0]):
        fea_nopulse_curr = nopulse_median(fea_img[ii, :, :])
        fea_nopulse_list.append(fea_nopulse_curr)
    fea_nopulse = np.stack(fea_nopulse_list)

    return fea_nopulse


# average subtraction: input=fea_median, output=fea_noiseless
def avg_sub(fea_input, alpha=0.2):
    bg_noise = np.zeros(fea_input.shape)
    bg_noise[:, 0] = fea_input[:, 0]

    for tt in range(1, fea_input.shape[1]):
        bg_noise[:, tt] = alpha * fea_input[:, tt] + (1.0 - alpha) * bg_noise[
                                                                     :, tt - 1]

    return fea_input - bg_noise


def spectro_nonoise(fea_img, alpha):
    fea_nopulse_list = []
    for ii in range(fea_img.shape[0]):
        fea_nopulse_curr = avg_sub(fea_img[ii, :, :], alpha)
        fea_nopulse_list.append(fea_nopulse_curr)
    fea_nopulse = np.stack(fea_nopulse_list)

    return fea_nopulse


# Gaussian smoothing
def gaussian_smoothing(spectro_mat):
    """
    Gaussian smoothing
    """
    spectro_smooth = cv2.GaussianBlur(spectro_mat, (3, 3), cv2.BORDER_DEFAULT)

    return spectro_smooth


def spectro_smooth(fea_img):
    fea_smooth_list = []
    for ii in range(fea_img.shape[0]):
        fea_smooth_curr = gaussian_smoothing(fea_img[ii, :, :])
        fea_smooth_list.append(fea_smooth_curr)
    fea_smooth = np.stack(fea_smooth_list)

    return fea_smooth

# feature_path = '/home/ys587/__Data/__whistle/__domain_adaptation/__data_feature/__mel128_3s_pcen_train_test_file_encounter/__20201009'


species_list = ['BD', 'LCD', 'SCD', 'STR', 'SPT', 'SPIN', 'PLT', 'RT', 'FKW', 'NO']
# file_type= ['_universal', '_file', '_encounter']
fea_type = ['linear', 'mel']
file_type= ['_file', '_encounter']
train_test = ['_train', '_test']

alpha = 1.0-np.exp(np.log(0.15)*.02/10.)
print('alpha = {:.3f}'.format(alpha))

for mm in fea_type:
    print('Fea type: {:s}'.format(mm))
    if mm == 'linear':
        feature_path = '/home/ys587/__Data/__whistle/__domain_adaptation/__data_feature/__linear_3s_pcen_train_test_file_encounter'
    elif mm == 'mel':
        feature_path = '/home/ys587/__Data/__whistle/__domain_adaptation/__data_feature/__mel128_3s_pcen_train_test_file_encounter'

    for ff in file_type:
        for tt in train_test:
            # loading features
            fea_file = 'features_oswald_3s_pcen'+tt+ff+'.npy'
            fea_file_out = 'features_oswald_3s_pcen' + tt + ff + '_cleansed.npy'
            print('..Reading {:s}'.format(fea_file))

            print('....Loading spectrograms')
            fea_target = np.load(os.path.join(feature_path, fea_file))

            # median filter
            print('....Applying vertical median filter to remove clicks...')
            fea_median = spectro_median(fea_target)
            del fea_target

            # average subtraction
            print('....Applying average substraction to remove background noise...')
            fea_nonoise = spectro_nonoise(fea_median, alpha)
            del fea_median

            print('....Saving the cleansed features back to the file')
            np.save(os.path.join(feature_path, fea_file_out), fea_nonoise.astype('float32'))
            del fea_nonoise
