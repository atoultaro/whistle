#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Convert data from pd_dataframe to csv files

Created on 9/18/19
@author: atoultaro
"""
import os
import pandas as pd
import numpy as np
import pickle
import random
import matplotlib.pyplot as plt

# import librosa
# from keras.utils import to_categorical

from classifier.recurrent import train_and_evaluate
# from classifier.buildmodels import build_model
from classifier.batchgenerator import PaddedBatchGenerator


from cape_cod_whale.preprocess import bin_extract, \
    fea_label_generate_no_harmonics, fea_label_generate_no_harmonics_v2, \
    save_fea, load_fea, freq_seq_label_generate, fea_label_shuffle, \
    fea_label_generate_no_harmonics_v3
from cape_cod_whale.classifier import train_validation, \
    two_fold_cross_validate_mlp, two_fold_cross_validate_time, \
    two_fold_cross_validate_time_uneven, convert_df_2_contour_list, \
    convert_df_2_win_list, convert_df_2_win_contour_list, \
    convert_df_2_win_list_half_diff

# bin files for training & testing
# data_dir = '/Users/ys587/__Data/whistle_Contours/tonals_20190210' # bin files
bin_dir_train = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__first_pie'
bin_dir_test = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__second_pie'
bin_dir_all = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__all'
sound_dir = '/home/ys587/__Data/__whistle/__sound_species/'
log_dir = "/home/ys587/__Data/__whistle/__log_dir_audio_time/__logdir_temp"

species_name = ['bottlenose', 'common', 'spinner', 'melon-headed']
species_id = {'bottlenose': 0, 'common': 1, 'spinner': 2, 'melon-headed': 3}
contour_train, bin_wav_pair_train = bin_extract(bin_dir_train, sound_dir,
                                                species_name)
contour_test, bin_wav_pair_test = bin_extract(bin_dir_test, sound_dir,
                                              species_name)

# find contours longer than a pre-defined duration and extract audio features
conf_model = dict()
# conf_model["duration_thre"] = 0.2
conf_model["duration_thre"] = 0.5
# conf_model["duration_thre"] = 1.0
percentile_list = [0, 25, 50, 75, 100]
# conf_model['time_reso'] = 0.02  # should be 0.01067 sec
conf_model['time_reso'] = 0.01
# conf_model['gap'] = 0.02
conf_model['shuffle'] = True
conf_model['win_back'] = 10

conf_model['num_class'] = len(species_name)
conf_model['batch_size'] = 256
conf_model['epochs'] = 100
# conf_model['epochs'] = 1  # for debugging
conf_model['learning_rate'] = 0.001
# conf_model['learning_rate'] = 0.01
conf_model["l2_regu"] = 1.e-2
conf_model["dropout"] = 0.2
conf_model["recurrent_dropout"] = 0.2
conf_model["class_weight"] = {0: 1.0, 1: 1.0, 2: 1.0, 3: 1.0}
conf_model["bi_mod"] = "concat"
# conf_model["bbi_mod"] = "sum"
conf_model["fea_dim"] = 64  # 64-filter Cepstrum
# conf_model["fea_dim"] = fea_list_test[0].shape[1]
# conf_model["fea_dim"] = fea_train[0].shape[1]
conf_model["type"] = 'lstm'  #'lstm', 'conv2d', 'conv2d_lstm'; will affect input data shape
conf_model["best_model"] = "NA"

# features & labels
# train_data_file = u'/home/ys587/__Data/__whistle/__log_dir_audio_time/__feature/df_train.pkl'
train_data_file = u'/home/ys587/__Data/__whistle/__log_dir_audio_time/__feature/df_train_energy.pkl'
if os.path.exists(train_data_file) is True:
    df_train = pd.read_pickle(train_data_file)
else:
    # df_train,  dur_train_max, count_long_train, count_all_train = \
    #     fea_label_generate_no_harmonics_v2(contour_train, bin_wav_pair_train,
    #                                        bin_dir_train, species_id, conf_model)
    df_train,  dur_train_max, count_long_train, count_all_train = \
        fea_label_generate_no_harmonics_v3(contour_train, bin_wav_pair_train,
                                           bin_dir_train, species_id, conf_model)

    df_train.to_pickle(train_data_file)

# test_data_file = u'/home/ys587/__Data/__whistle/__log_dir_audio_time/__feature/df_test.pkl'
test_data_file = u'/home/ys587/__Data/__whistle/__log_dir_audio_time/__feature/df_test_energy.pkl'
if os.path.exists(test_data_file) is True:
    df_test = pd.read_pickle(test_data_file)
else:
    # df_test,  dur_test_max, count_long_test, count_all_test = \
    #     fea_label_generate_no_harmonics_v2(contour_test, bin_wav_pair_test,
    #                                        bin_dir_test, species_id, conf_model)
    df_test,  dur_test_max, count_long_test, count_all_test = \
        fea_label_generate_no_harmonics_v3(contour_test, bin_wav_pair_test,
                                           bin_dir_test, species_id, conf_model)

    df_test.to_pickle(test_data_file)

# frame-based cepstral coefficient
if False:
    fea_list_train, label_train, dur_train_max = convert_df_2_win_list(df_train, win_back=1)
    fea_train = np.squeeze(np.stack(fea_list_train))
    csv_out = '/home/ys587/PycharmProjects/dash-sample-apps-master/apps/dash-tsne/data'
    np.savetxt(os.path.join(csv_out, 'whistle_fea.csv'), fea_train, fmt='%.9e', delimiter=",")
    lab_train = np.array(label_train)
    np.savetxt(os.path.join(csv_out, 'whistle_label.csv'), lab_train, fmt='%d', delimiter=",")

# sequence cepstral coefficient
# win_back = 20
win_back = 1

FEA_TYPE = 1  # cepstral coefficients
# FEA_TYPE = 2  # cepstral coefficients delta
# FEA_TYPE = 3  # cepstral coefficients delta*2

# train data only
if False:
    # fea_list_train, label_train, _ = convert_df_2_win_list(df_train, win_back=win_back)
    fea_list_train, label_train, _ = convert_df_2_win_list_half_diff(
        df_train, win_back=win_back, type=1)
    fea_train = np.stack(fea_list_train)
    if fea_train.shape[1] > 1:
        fea_train = np.reshape(fea_train, (fea_train.shape[0], fea_train.shape[1]*fea_train.shape[2]))
    else:
        fea_train = np.squeeze(np.stack(fea_list_train))
    csv_out = '/home/ys587/PycharmProjects/dash-sample-apps-master/apps/dash-tsne/data/temp'
    np.savetxt(os.path.join(csv_out, 'whistle_win'+str(win_back)+'_input.csv'), fea_train, fmt='%.9e', delimiter=",")
    lab_train = np.array(label_train)
    np.savetxt(os.path.join(csv_out, 'whistle_win'+str(win_back)+'_labels.csv'), lab_train, fmt='%d', delimiter=",")

# test data only
if False:
    fea_list_test, label_test, _ = convert_df_2_win_list_half_diff(
        df_test, win_back=win_back, type=1)
    fea_test = np.stack(fea_list_test)
    if fea_test.shape[1] > 1:
        fea_test = np.reshape(fea_test, (fea_test.shape[0], fea_test.shape[1]*fea_test.shape[2]))
    else:
        fea_test = np.squeeze(np.stack(fea_list_test))
    csv_out = '/home/ys587/PycharmProjects/dash-sample-apps-master/apps/dash-tsne/data/temp'
    np.savetxt(os.path.join(csv_out, 'whistle_win'+str(win_back)+'_input.csv'), fea_test, fmt='%.9e', delimiter=",")
    lab_test = np.array(label_test)
    np.savetxt(os.path.join(csv_out, 'whistle_win'+str(win_back)+'_labels.csv'), lab_test, fmt='%d', delimiter=",")

# train & test together
fea_list_train, label_train, _ = convert_df_2_win_list_half_diff(
    df_train, win_back=win_back, type=FEA_TYPE)
fea_train = np.stack(fea_list_train)
if fea_train.shape[1] > 1:
    fea_train = np.reshape(fea_train, (fea_train.shape[0], fea_train.shape[1]*fea_train.shape[2]))
else:
    fea_train = np.squeeze(np.stack(fea_list_train))

fea_list_test, label_test, _ = convert_df_2_win_list_half_diff(
    df_test, win_back=win_back, type=FEA_TYPE)
fea_test = np.stack(fea_list_test)
if fea_test.shape[1] > 1:
    fea_test = np.reshape(fea_test, (fea_test.shape[0], fea_test.shape[1]*fea_test.shape[2]))
else:
    fea_test = np.squeeze(np.stack(fea_list_test))

fea_total = np.concatenate((fea_train, fea_test), axis=0)
label_total = label_train + [ii+4 for ii in label_test]
csv_out = '/home/ys587/PycharmProjects/dash-sample-apps-master/apps/dash-tsne/data/temp'
if FEA_TYPE == 1:
    name_filler = ''
elif FEA_TYPE == 2:
    name_filler = '_diff'
elif FEA_TYPE ==3:
    name_filler = '_diff2'
np.savetxt(os.path.join(csv_out, 'whistle'+name_filler+'_win'+str(win_back)+'_input.csv'), fea_total, fmt='%.9e', delimiter=",")
lab_total = np.array(label_total)
np.savetxt(os.path.join(csv_out, 'whistle'+name_filler+'_win'+str(win_back)+'_labels.csv'), lab_total, fmt='%d', delimiter=",")

