#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
whislte contour classification using audio signals from the start time and end
time of of verified contours
running script for RNN experiments
To replace run_classification.py

Created on 6/3/19
@author: atoultaro
"""
import os
import numpy as np
import pickle
import random
import matplotlib.pyplot as plt

# import librosa
from keras.utils import to_categorical

from classifier.recurrent import train_and_evaluate
# from classifier.buildmodels import build_model
from classifier.batchgenerator import PaddedBatchGenerator


from cape_cod_whale.preprocess import bin_extract, fea_label_generate, \
    save_fea, load_fea
from cape_cod_whale.classifier import train_validation, \
    two_fold_cross_validate


# from cape_cod_whale.seltab import bin_to_seltab
# import sys
# import glob
# import pandas as pd


# bin files for training & testing
# data_dir = '/Users/ys587/__Data/whistle_Contours/tonals_20190210' # bin files
bin_dir_train = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__first_pie'
bin_dir_test = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__second_pie'
bin_dir_all = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__all'
sound_dir = '/home/ys587/__Data/__whistle/__sound_species/'
seltab_out_path = '/home/ys587/__Data/__whistle/__seltab'

species_name = ['bottlenose', 'common', 'spinner', 'melon-headed']
species_id = {'bottlenose': 0, 'common': 1, 'spinner': 2, 'melon-headed': 3}
contour_train, bin_wav_pair_train = bin_extract(bin_dir_train, sound_dir,
                                                species_name)
contour_test, bin_wav_pair_test = bin_extract(bin_dir_test, sound_dir,
                                              species_name)
# contour_all, bin_wav_pair = bin_extract(bin_dir_all, sound_dir, species_name)


# convert contour_train into selection tables
# bin_to_seltab(contour_all, seltab_out_path)


# record the information of time step. Do they have a single time step or
# multiples?
# timestep_file = "/home/ys587/__Data/__whistle/timestep_info.txt"
# percentile_list = [0, 25, 50, 75, 100]
# timestep_info(contour_all, timestep_file, percentile_list)


# find contours longer than a pre-defined duration and extract audio features
conf_general = dict()
conf_general["duration_thre"] = 0.40
# conf.duration_thre = 0.50
conf_general['gap'] = 0.1
percentile_list = [0, 25, 50, 75, 100]


# Extract features
feature_dir = "/home/ys587/__Data/__whistle/__feature"
if not os.path.isfile(os.path.join(feature_dir, 'train.pkl')):
    fea_list_train, label_list_train, dur_train_max, count_long_train, \
    count_all_train = fea_label_generate(contour_train, bin_wav_pair_train,
                                         conf_general["duration_thre"],
                                         percentile_list,
                                         bin_dir_train,
                                         species_id,
                                         conf_general['gap'])
    fea_list_test, label_list_test, dur_test_max, count_long_test, count_all_test \
        = fea_label_generate(contour_test, bin_wav_pair_test, conf_general["duration_thre"],
                           percentile_list, bin_dir_test, species_id)
    print("count_all_train: " + str(count_all_train))  #
    print("count_long_train: " + str(count_long_train))  #

    # save features
    pkl_path_train = os.path.join(feature_dir, 'train.pkl')
    save_fea(pkl_path_train, fea_list_train, label_list_train, dur_train_max, count_long_train, count_all_train)

    pkl_path_test = os.path.join(feature_dir, 'test.pkl')
    save_fea(pkl_path_test, fea_list_test, label_list_test, dur_test_max, count_long_test, count_all_test)
else:
    # Load features instead
    fea_list_train, label_list_train, dur_train_max, count_long_train, \
        count_all_train = load_fea(os.path.join(feature_dir, 'train.pkl'))

    fea_list_test, label_list_test, dur_test_max, count_long_test, count_all_test \
        = load_fea(os.path.join(feature_dir, 'test.pkl'))

conf_model = dict()
conf_model['batch_size'] = 128
conf_model['epochs'] = 100
conf_model['learning_rate'] = 0.005

conf_model['dur_train_max'] = dur_train_max
conf_model['dur_test_max'] = dur_test_max
conf_model["l2_regu"] = 5.e-2
conf_model["class_weight"] = {0: 1.0, 1: 1.0, 2: 1.0, 3: 1.0}
conf_model["mod"] = "sum"

# log_dir = "/home/ys587/__Data/__whistle/__log_dir/"
log_dir = "/home/ys587/__Data/__whistle/__log_dir_audio"

# num_dolphins = len(species_name)
conf_model["num_class"] = len(species_name)

# fea_dim = fea_list_train[0].shape[1]
# conf_model["fea_dim"] = fea_dim
conf_model["fea_dim"] = fea_list_train[0].shape[1]

# dur_max = int(max([dur_train_max, dur_test_max])*96000./1000.)+5


# Features & labels
train_idx = [ii for ii in range(len(label_list_train))]
random.shuffle(train_idx)
test_idx = [ii for ii in range(len(label_list_test))]
random.shuffle(test_idx)

# Use PaddedBatchGenerator to get features and labels
feature_train = [fea_list_train[tt] for tt in train_idx]
target_train = to_categorical(label_list_train)[train_idx, :]
feature_test = [fea_list_test[tt] for tt in test_idx]
target_test = to_categorical(label_list_test)[test_idx, :]


# 2-fold cross-validation for each model
# model_name = "lstm_atten_4_layer_baseline"
# model_tag = model_name + "_512_256_128_64"
# conf_model.update({"lstm1": 512, "lstm2": 256, "lstm3": 128, "lstm4": 64})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_atten_dropout_4_layer"
# model_tag = model_name + "_512_256_128_64"
# conf_model.update({"lstm1": 512, "lstm2": 256, "lstm3": 128, "lstm4": 64})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)

# model_name = "lstm_phone_3_layers"
# model_tag = model_name + "_512_128_32"
# conf_model.update({"lstm1": 512, "lstm2": 128, "lstm3": 32})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_4_layers"
# model_tag = model_name + "_512_256_128_64"
# conf_model.update({"lstm1": 512, "lstm2": 256, "lstm3": 128, "lstm4": 64})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)

# model_name = "lstm_atten_bidirect_4_layer"
# model_tag = model_name + "_512_256_128_64"
# conf_model.update({"lstm1": 512, "lstm2": 256, "lstm3": 128, "lstm4": 64})
# conf_model2 = conf_model
# conf_model2["batch_size"] = 64
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model2, log_dir, model_tag)

model_name = "lstm_phone_3_layers"
model_tag = model_name + "_128_64_32"
conf_model.update({"lstm1": 128, "lstm2": 64, "lstm3": 32})
two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
                        target_test, conf_model, log_dir, model_tag)

# model_tag = model_name + "_256_64_32"
# conf_model.update({"lstm1": 256, "lstm2": 64, "lstm3": 32})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)
#
# model_tag = model_name + "_256_128_32"
# conf_model.update({"lstm1": 256, "lstm2": 128, "lstm3": 32})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)
#
# model_tag = model_name + "_256_128_64"
# conf_model.update({"lstm1": 256, "lstm2": 128, "lstm3": 64})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)
#
# model_tag = model_name + "_512_256_64"
# conf_model.update({"lstm1": 512, "lstm2": 256, "lstm3": 64})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)
#
# model_tag = model_name + "_512_128_64"
# conf_model.update({"lstm1": 512, "lstm2": 128, "lstm3": 64})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)
#
# model_tag = model_name + "_512_128_32"
# conf_model.update({"lstm1": 512, "lstm2": 128, "lstm3": 32})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_4_layers"
# model_tag = model_name + "_256_128_64_32"
# conf_model.update({"lstm1": 256, "lstm2": 128, "lstm3": 64, "lstm4": 32})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)
#
# model_tag = model_name + "_512_256_128_64"
# conf_model.update({"lstm1": 512, "lstm2": 256, "lstm3": 128, "lstm4": 64})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)
