# -*- coding: utf-8 -*-
"""

Created on 2019-05-22
@author: atoultaro
"""
import os
import numpy as np
import pickle
import random
import matplotlib.pyplot as plt

# import librosa
from contextlib import redirect_stdout
from sklearn.metrics import confusion_matrix
from keras.utils import to_categorical

# from classifier.recurrent import train_and_evaluate
# from classifier.buildmodels import build_model
from classifier.batchgenerator import PaddedBatchGenerator
from cape_cod_whale.preprocess import bin_extract, fea_label_generate, \
    save_fea, load_fea
from cape_cod_whale.model import make_rnn_lstm, make_rnn_lstm_regular, \
    callback_generate, make_rnn_lstm_constraints, make_simple_lstm_mask, \
    make_simple_lstm_mask_regu, make_simple_lstm_mask_constra, \
    make_lstm_phone, make_lstm_phone_3_layers, make_lstm_phone_4_layers, \
    make_lstm_phone_5_layers, make_lstm_phone_bidirect_3_layers, \
    make_lstm_phone_bidirect_4_layers

# from cape_cod_whale.seltab import bin_to_seltab
# import sys
# import glob
# import pandas as pd

# data_dir = '/Users/ys587/__Data/whistle_Contours/tonals_20190210' # bin files
bin_dir_train = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__first_pie'
bin_dir_test = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__second_pie'
bin_dir_all = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__all'
sound_dir = '/home/ys587/__Data/__whistle/__sound_species/'
seltab_out_path = '/home/ys587/__Data/__whistle/__seltab'

species_name = ['bottlenose', 'common', 'spinner', 'melon-headed']
species_id = {'bottlenose': 0, 'common': 1, 'spinner': 2, 'melon-headed': 3}
contour_train, bin_wav_pair_train = bin_extract(bin_dir_train, sound_dir, species_name)
contour_test, bin_wav_pair_test = bin_extract(bin_dir_test, sound_dir, species_name)
# contour_all, bin_wav_pair = bin_extract(bin_dir_all, sound_dir, species_name)

# convert contour_train into selection tables
# bin_to_seltab(contour_all, seltab_out_path)

# record the information of time step. Do they have a single time step or
# multiples?
# timestep_file = "/home/ys587/__Data/__whistle/timestep_info.txt"
# percentile_list = [0, 25, 50, 75, 100]
# timestep_info(contour_all, timestep_file, percentile_list)

# find contours longer than a pre-defined duration and extract audio features
duration_thre = 0.40
# duration_thre = 0.50
percentile_list = [0, 25, 50, 75, 100]

# Extract features
feature_dir = "/home/ys587/__Data/__whistle/__feature"
if not os.path.isfile(os.path.join(feature_dir, 'train.pkl')):
    fea_list_train, label_list_train, dur_train_max, count_long_train, \
    count_all_train = fea_label_generate(contour_train, bin_wav_pair_train,
                                         duration_thre,
                                         percentile_list,
                                         bin_dir_train,
                                         species_id)
    fea_list_test, label_list_test, dur_test_max, count_long_test, count_all_test \
        = fea_label_generate(contour_test, bin_wav_pair_test, duration_thre,
                           percentile_list, bin_dir_test, species_id)
    print("count_all_train: " + str(count_all_train))  #
    print("count_long_train: " + str(count_long_train))  #

    # save features
    # with open(os.path.join(feature_dir, 'train.pkl'), 'wb') as f1:
    #     pickle.dump([fea_list_train, label_list_train, dur_train_max,
    #                  count_long_train, count_all_train], f1)
    pkl_path_train = os.path.join(feature_dir, 'train.pkl')
    save_fea(pkl_path_train, fea_list_train, label_list_train, dur_train_max, count_long_train, count_all_train)
    # with open(os.path.join(feature_dir, 'test.pkl'), 'wb') as f2:
    #     pickle.dump([fea_list_test, label_list_test, dur_test_max,
    #                  count_long_test, count_all_test], f2)
    pkl_path_test = os.path.join(feature_dir, 'test.pkl')
    save_fea(pkl_path_test, fea_list_test, label_list_test, dur_test_max, count_long_test, count_all_test)


# Load features instead
# feature_dir = "/home/ys587/__Data/__whistle/__feature"
fea_list_train, label_list_train, dur_train_max, count_long_train, \
    count_all_train = load_fea(os.path.join(feature_dir, 'train.pkl'))
# with open(os.path.join(feature_dir, 'train.pkl'), 'rb') as f1:
#     fea_list_train, label_list_train, dur_train_max, count_long_train, \
#     count_all_train = pickle.load(f1)
fea_list_test, label_list_test, dur_test_max, count_long_test, count_all_test \
    = load_fea(os.path.join(feature_dir, 'test.pkl'))
# with open(os.path.join(feature_dir, 'test.pkl'), 'rb') as f2:
#     fea_list_test, label_list_test, dur_test_max, count_long_test, \
#     count_all_test = pickle.load(f2)

conf = dict()
conf['batch_size'] = 128
conf['epochs'] = 200

conf['lstm_1'] = 512
conf['lstm_2'] = 256
conf['lstm_3'] = 128
conf['lstm_4'] = 64
conf['lstm_5'] = 0
conf["l2_regu"] = 5.e-2

# hyper-parameters
batch_size = conf['batch_size']
epochs = conf['epochs']
lstm_1 = conf['lstm_1']
lstm_2 = conf['lstm_2']
lstm_3 = conf['lstm_3']
lstm_4 = conf['lstm_4']
lstm_5 = conf['lstm_5']
l2_regu = conf["l2_regu"]

log_dir = "/home/ys587/__Data/__whistle/__log_dir/"


num_dolphins = len(species_name)
dur_max = int(max([dur_train_max, dur_test_max])*96000./1000.)+5

train_idx = [ii for ii in range(len(label_list_train))]
random.shuffle(train_idx)
test_idx = [ii for ii in range(len(label_list_test))]
random.shuffle(test_idx)

# Use PaddedBatchGenerator to get features and labels
traingen = \
    PaddedBatchGenerator([fea_list_train[tt] for tt in train_idx],
                         to_categorical(label_list_train)[train_idx, :],
                         batch_size=len(train_idx), pad_length=dur_max)
(x_train, y_train_onehot) = next(traingen)

testgen = \
    PaddedBatchGenerator([fea_list_test[tt] for tt in test_idx],
                         to_categorical(label_list_test)[test_idx, :],
                         batch_size=len(test_idx), pad_length=dur_max)
(x_test, y_test_onehot) = next(testgen)


callbacks_list = callback_generate(log_dir)
# model_LSTM = make_rnn_lstm([dur_max, fea_list_train[0].shape[1]], num_dolphins)
# model_LSTM = make_rnn_lstm_regular([dur_max, fea_list_train[0].shape[1]], num_dolphins, l2_regu)
# model_LSTM = make_rnn_lstm_constraints([dur_max, fea_list_train[0].shape[1]], num_dolphins)
# model_LSTM = make_simple_lstm([dur_max, fea_list_train[0].shape[1]], num_dolphins)
# model_LSTM = make_simple_lstm_mask([dur_max, fea_list_train[0].shape[1]], num_dolphins)
# model_LSTM = make_simple_lstm_mask_regu([dur_max, fea_list_train[0].shape[1]], num_dolphins, l2_regu)
# model_LSTM = make_simple_lstm_mask_constra([dur_max, fea_list_train[0].shape[1]], num_dolphins)
# model_LSTM = make_lstm_phone([dur_max, fea_list_train[0].shape[1]], num_dolphins, l2_regu)
# model_LSTM = make_lstm_phone([dur_max, fea_list_train[0].shape[1]], num_dolphins, lstm_1, lstm_2, l2_regu)

# model_LSTM = make_lstm_phone_3_layers([dur_max, fea_list_train[0].shape[1]], num_dolphins, lstm_1, lstm_2, lstm_3, l2_regu)
# model_LSTM = make_lstm_phone_4_layers([dur_max, fea_list_train[0].shape[1]], num_dolphins, lstm_1, lstm_2, lstm_3, lstm_4, l2_regu)
# model_LSTM = make_lstm_phone_5_layers([dur_max, fea_list_train[0].shape[1]], num_dolphins, lstm_1, lstm_2, lstm_3, lstm_4, lstm_5, l2_regu)

# model_LSTM = make_lstm_phone_bidirect_3_layers([dur_max, fea_list_train[0].shape[1]], num_dolphins, lstm_1, lstm_2, lstm_3, l2_regu)
model_LSTM = make_lstm_phone_bidirect_4_layers([dur_max, fea_list_train[0].shape[1]], num_dolphins, lstm_1, lstm_2, lstm_3, lstm_4, l2_regu, "sum")


# write out model
with open(os.path.join(log_dir, 'net_architecture.txt'), 'w') as f:
    with redirect_stdout(f):
        model_LSTM.summary()

model_LSTM.compile(loss='categorical_crossentropy', optimizer='Adam',
                   metrics=['acc'])
CLASS_WEIGHT = {0: 1.0,
                1: 1.0,
                2: 1.0,
                3: 1.0}
history_log = model_LSTM.fit(x_train, y_train_onehot,
                             validation_data=(x_test, y_test_onehot),
                             batch_size=batch_size,
                             epochs=epochs, verbose=1,
                             callbacks=callbacks_list,
                             class_weight=CLASS_WEIGHT,
                             )
# initial_epoch=100

# accuracy
loss, acc = model_LSTM.evaluate(x_test, y_test_onehot, batch_size=batch_size)
print('acc: %.2f' % acc)
# model_LSTM.load_weights(os.path.join(log_dir, 'epoch_09_0.6823.hdf5'))

class_prob = model_LSTM.predict(x_test)  # predict_proba the same as predict
y_pred = np.argmax(class_prob, axis=1)
y_test = np.argmax(y_test_onehot, axis=1).astype(int)
confu_mat = confusion_matrix(y_pred, y_test)
with open(os.path.join(log_dir, 'confu_matrix.txt'), 'w') as f:
    with redirect_stdout(f):
        print(confu_mat)
        print("\n\n")
        print("Batch size: "+str(batch_size))
        print("Duration thre: " + str(duration_thre))






# model_rnn.add(LSTM(64, return_sequences=True, input_shape=data_shape))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(LSTM(32, return_sequences=False))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(Dense(num_class, activation='softmax'))
#
# spec = [(LSTM, [64], {'return_sequences':True, 'input_dim': M}),
#      (Dense, [20], {'activation':'relu', 'input_dim':20}),
#      (Dense, [N], {'activation':'softmax', 'input_dim':20})
#     ]

# model_LSTM.compile(loss='categorical_crossentropy', optimizer='Adam', metrics=['acc'])
# errors, model, loss = train_and_evaluate(fea_list_train+fea_list_test,
#                                          label_list_train+label_list_test,
#                                 [ii for ii in range(len(label_list_train))],
#             [ii+len(label_list_train) for ii in range(len(label_list_test))],
#                                          model_LSTM, batch_size, epochs,
#                                          pad_length=300)


# histogram of whistle durations
if False:
    bin_space = np.linspace(0, 2, 21)
    plt.hist(np.array(duration_list), bins=bin_space)
    # plt.show()
    fig_hist_path = "/home/ys587/__Data/__whistle/duration_hist.png"
    plt.savefig(fig_hist_path, dpi=300)















# make selection tables using info from bin files
# set up pd
# selection table



# Backup

# label = []
# feature = []
# num_whistle = 0
# too_short_count = 0
# for ff in contour_all.keys():
#     print('\n'+ff)
#     this_contour = contour_all[ff]
#     sound_path = os.path.join(sound_dir, this_contour[0], ff+'.wav')
#     print("Num of whistles: "+str(len(this_contour[1])))
#     num_whistle += len(this_contour[1])
#
#     for cc in range(len(this_contour[1])):
#         label.append(species_id[this_contour[0]])
#         contour_time_freq = this_contour[1][cc]
#         feature.append(contour_time_freq['Freq'])
#         if len(contour_time_freq['Time']) <= 200:
#             # print('Too short!')
#             too_short_count += 1









# bin_1 = list(bin_wav_pair.keys())[0]
# wav_1 = bin_wav_pair[bin_1]
# tonal0 = tonal(bin_1)
# contour_list = []
# try:
#     contour_list.append(tonal0.__next__())
# except StopIteration:
#     print("End of bin file")
#
#
#     tonal_header = TonalHeader(bin_1)
#     bis = tonal_header.getDatainstream()
#     data = bis.read_record(format=time_freq_fmt, n=NumNodes)
#





# generator_all = batch_generator(file_dirs=file_dirs, chunk_dur=2,
#                                batch_size=20, adv_ms =3, len_ms = 8,
#                                freqs=(f1, f2), Only_Positive = False)


# Note:
# why are there often 383 points on a whistle contour while the time
# resolutions vary?
