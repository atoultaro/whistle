#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
whislte contour classification using audio signals from the start time and end
time of of verified contours

The same as frequency_classifier except used in audio signals instead of
whistle contours

Created on 6/27/19
@author: atoultaro
"""
import os
import pandas as pd
import numpy as np
import pickle
import random
import matplotlib.pyplot as plt

# import librosa
from keras.utils import to_categorical

from classifier.recurrent import train_and_evaluate
# from classifier.buildmodels import build_model
from classifier.batchgenerator import PaddedBatchGenerator


from cape_cod_whale.preprocess import bin_extract, \
    fea_label_generate_no_harmonics, \
    save_fea, load_fea, freq_seq_label_generate, fea_label_shuffle
from cape_cod_whale.classifier import train_validation, \
    two_fold_cross_validate, convert_df_2_contour_list

# bin files for training & testing
# data_dir = '/Users/ys587/__Data/whistle_Contours/tonals_20190210' # bin files
bin_dir_train = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__first_pie'
bin_dir_test = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__second_pie'
bin_dir_all = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__all'
sound_dir = '/home/ys587/__Data/__whistle/__sound_species/'
# seltab_out_path = '/home/ys587/__Data/__whistle/__seltab'

species_name = ['bottlenose', 'common', 'spinner', 'melon-headed']
species_id = {'bottlenose': 0, 'common': 1, 'spinner': 2, 'melon-headed': 3}
contour_train, bin_wav_pair_train = bin_extract(bin_dir_train, sound_dir,
                                                species_name)
contour_test, bin_wav_pair_test = bin_extract(bin_dir_test, sound_dir,
                                              species_name)
# contour_all, bin_wav_pair = bin_extract(bin_dir_all, sound_dir, species_name)


# find contours longer than a pre-defined duration and extract audio features
conf_gen = dict()
conf_gen["duration_thre"] = 0.5
# conf_gen["duration_thre"] = 1.0
percentile_list = [0, 25, 50, 75, 100]
conf_gen['time_reso'] = 0.02
conf_gen['gap'] = 0.05

# # conf_gen['transform'] = "zscorederiv"
# conf_gen['transform'] = "non_z"
# conf_gen['n_components'] = 64
# conf_gen['tol'] = 1e-3
# conf_gen['max_iter'] = 200
# conf_gen['covar'] = 'diag'
conf_gen['shuffle'] = False

# features & labels
train_data_file = u'/home/ys587/__Data/__whistle/__log_dir_audio/__feature/df_train.pkl'
if os.path.exists(train_data_file) is True:
    df_train = pd.read_pickle(train_data_file)
else:
    df_train,  dur_train_max, count_long_train, count_all_train = \
        fea_label_generate_no_harmonics(contour_train,
                                        bin_wav_pair_train,
                                        bin_dir_train,
                                        species_id,
                                        conf_gen)
    df_train.to_pickle(train_data_file)

test_data_file = u'/home/ys587/__Data/__whistle/__log_dir_audio/__feature/df_test.pkl'
if os.path.exists(test_data_file) is True:
    df_test = pd.read_pickle(test_data_file)
else:
    df_test,  dur_test_max, count_long_test, count_all_test = \
        fea_label_generate_no_harmonics(contour_test,
                                        bin_wav_pair_test,
                                        bin_dir_test,
                                        species_id,
                                        conf_gen)
    df_test.to_pickle(test_data_file)

# convert features from dataframe to numpy array
# frame-based features
# fea_train = df_train.iloc[:, 3:].to_numpy()
label_train0 = df_train.iloc[:, 2].astype(int).to_numpy()
# fea_test = df_test.iloc[:, 3:].to_numpy()
label_test0 = df_test.iloc[:, 2].astype(int).to_numpy()

# contour-based features
# input: df_train
# select by file
fea_list_train, label_list_train, dur_train_max = convert_df_2_contour_list(df_train)
fea_list_test, label_list_test, dur_test_max = convert_df_2_contour_list(df_test)
# find all features for a contour
# convert them into a numpy 2d array (time_step, feature_dim)

# shuffle
if conf_gen['shuffle'] is True:
    train_idx = [ii for ii in range(len(label_list_train))]
    random.shuffle(train_idx)
    test_idx = [ii for ii in range(len(label_list_test))]
    random.shuffle(test_idx)

    feature_train = [fea_list_train[tt] for tt in train_idx]
    label_train = to_categorical(label_list_train)[train_idx, :]
    feature_test = [fea_list_test[tt] for tt in test_idx]
    label_test = to_categorical(label_list_test)[test_idx, :]
else:
    feature_train = fea_list_train
    label_train = label_list_train
    feature_test = fea_list_test
    label_test = label_list_test

conf_model = dict()
conf_model['batch_size'] = 100
conf_model['epochs'] = 200
conf_model['learning_rate'] = 0.005
conf_model['dur_train_max'] = dur_train_max
conf_model['dur_test_max'] = dur_test_max
conf_model["l2_regu"] = 5.e-2
conf_model["dropout"] = 0.2
conf_model["recurrent_dropout"] = 0.2
conf_model["class_weight"] = {0: 1.0, 1: 1.0, 2: 1.0, 3: 1.0}
conf_model["mod"] = "sum"
conf_model["duration_thre"] = conf_gen["duration_thre"]
conf_model['time_reso'] = conf_gen['time_reso']


# log_dir = "/home/ys587/__Data/__whistle/__log_dir/"
log_dir = "/home/ys587/__Data/__whistle/__log_dir_audio"

# num_dolphins = len(species_name)
conf_model["num_class"] = len(species_name)
# conf_model["fea_dim"] = 1
conf_model["fea_dim"] = fea_list_test[0].shape[1]

# dur_max = int(max([dur_train_max, dur_test_max])*96000./1000.)+5
# Features & labels
# train_idx = [ii for ii in range(len(label_list_train))]
# random.shuffle(train_idx)
# test_idx = [ii for ii in range(len(label_list_test))]
# random.shuffle(test_idx)
#
# feature_train = [fea_list_train[tt] for tt in train_idx]
# target_train = to_categorical(label_list_train)[train_idx, :]
# feature_test = [fea_list_test[tt] for tt in test_idx]
# target_test = to_categorical(label_list_test)[test_idx, :]


model_name = "lstm_phone_2_layers"
model_tag = model_name + "_64_16"
conf_model.update({"lstm1": 64, "lstm2": 16, "dense_size": 20})
two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
                        label_test, conf_model, log_dir, model_tag)

# model experiments
# model_name = "lstm_phone_1_layer_3"
# model_tag = model_name + "_128"
# conf_model.update({"lstm1": 128})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, "audio_"+model_tag)

# model_name = "lstm_phone_1_layer"
# model_tag = model_name + "_128"
# conf_model.update({"lstm1": 128})
# two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
#                         label_test, conf_model, log_dir, "audio_"+model_tag)

# model_name = "lstm_phone_1_layer_2"
# model_tag = model_name + "_128"
# conf_model.update({"lstm1": 128})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)
#

#
# model_name = "lstm_phone_1_layer"
# model_tag = model_name + "_512"
# conf_model.update({"lstm1": 512})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_1_layer_2"
# model_tag = model_name + "_512"
# conf_model.update({"lstm1": 512})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)

# model_name = "lstm_phone_1_layer_3"
# model_tag = model_name + "_512"
# conf_model.update({"lstm1": 512})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_1_layer_4"
# model_tag = model_name + "_128"
# conf_model.update({"lstm1": 128})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_1_layer_5"
# model_tag = model_name + "_128"
# conf_model.update({"lstm1": 128})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_1_layer_4"
# model_tag = model_name + "_512"
# conf_model.update({"lstm1": 512})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_1_layer_5"
# model_tag = model_name + "_512"
# conf_model.update({"lstm1": 512})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)

# model_name = "lstm_phone_3_layer"
# model_tag = model_name + "_64"
# conf_model.update({"lstm1": 64})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_3_layer"
# model_tag = model_name + "_256"
# conf_model.update({"lstm1": 256})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_3_layer"
# model_tag = model_name + "_1024"
# conf_model.update({"lstm1": 1024})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_2_layers"
# model_tag = model_name + "_256_64"
# conf_model.update({"lstm1": 256, "lstm2": 64})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)

# model_name = "lstm_phone_2_layers"
# model_tag = model_name + "_64_16"
# conf_model.update({"lstm1": 64, "lstm2": 16})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_3_layers"
# model_tag = model_name + "_256_64_16"
# conf_model.update({"lstm1": 256, "lstm2": 64, "lstm3": 16})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)

# model_name = "lstm_phone_2_layers"
# model_tag = model_name + "_128_16"
# conf_model.update({"lstm1": 128, "lstm2": 16})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_2_layers"
# model_tag = model_name + "_256_32"
# conf_model.update({"lstm1": 256, "lstm2": 32})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_2_layers"
# model_tag = model_name + "_256_64"
# conf_model.update({"lstm1": 256, "lstm2": 64})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_3_layers"
# model_tag = model_name + "_64_32_16"
# conf_model.update({"lstm1": 64, "lstm2": 32, "lstm3": 16})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_3_layers"
# model_tag = model_name + "_128_64_32"
# conf_model.update({"lstm1": 128, "lstm2": 64, "lstm3": 32})
# two_fold_cross_validate(model_name, feature_train, target_train, feature_test,
#                         target_test, conf_model, log_dir, model_tag)
