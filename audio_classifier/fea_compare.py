#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Created on 9/25/19
@author: atoultaro
"""
import pandas as pd

train_cepstrum_file = u'/home/ys587/__Data/__whistle/__log_dir_audio_time/__feature/df_train.pkl'
df_train_cepstrum = pd.read_pickle(train_cepstrum_file)

train_energy_file = u'/home/ys587/__Data/__whistle/__log_dir_audio_time/__feature/df_train_energy.pkl'
df_train_energy = pd.read_pickle(train_energy_file)

df_train_cepstrum.iloc[0, 4:].plot()
df_train_energy.iloc[0, 4:].plot()