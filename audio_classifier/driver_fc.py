#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
whislte contour classification using audio signals
fully connected layers only == multi-layer perceptron (MLP)
two dense layers with relu activation and dropout
input: filter-bank cepstral coeff 64 by 1
No temporal information

Created on 9/6/19
@author: atoultaro
"""

import os
import pandas as pd
import numpy as np
import pickle
import random
import matplotlib.pyplot as plt

# import librosa
from keras.utils import to_categorical

from classifier.recurrent import train_and_evaluate
# from classifier.buildmodels import build_model
from classifier.batchgenerator import PaddedBatchGenerator


from cape_cod_whale.preprocess import bin_extract, \
    fea_label_generate_no_harmonics, \
    save_fea, load_fea, freq_seq_label_generate, fea_label_shuffle
from cape_cod_whale.classifier import train_validation, \
    two_fold_cross_validate_mlp, convert_df_2_contour_list

# bin files for training & testing
# data_dir = '/Users/ys587/__Data/whistle_Contours/tonals_20190210' # bin files
bin_dir_train = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__first_pie'
bin_dir_test = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__second_pie'
bin_dir_all = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__all'
sound_dir = '/home/ys587/__Data/__whistle/__sound_species/'
# seltab_out_path = '/home/ys587/__Data/__whistle/__seltab'

species_name = ['bottlenose', 'common', 'spinner', 'melon-headed']
species_id = {'bottlenose': 0, 'common': 1, 'spinner': 2, 'melon-headed': 3}
contour_train, bin_wav_pair_train = bin_extract(bin_dir_train, sound_dir,
                                                species_name)
contour_test, bin_wav_pair_test = bin_extract(bin_dir_test, sound_dir,
                                              species_name)
# contour_all, bin_wav_pair = bin_extract(bin_dir_all, sound_dir, species_name)


# find contours longer than a pre-defined duration and extract audio features
conf_gen = dict()
conf_gen["duration_thre"] = 0.2
# conf_gen["duration_thre"] = 1.0
percentile_list = [0, 25, 50, 75, 100]
conf_gen['time_reso'] = 0.02
conf_gen['gap'] = 0.05
conf_gen['shuffle'] = False

# features & labels
train_data_file = u'/home/ys587/__Data/__whistle/__log_dir_audio/__feature/df_train.pkl'
if os.path.exists(train_data_file) is True:
    df_train = pd.read_pickle(train_data_file)
else:
    df_train,  dur_train_max, count_long_train, count_all_train = \
        fea_label_generate_no_harmonics(contour_train,
                                        bin_wav_pair_train,
                                        bin_dir_train,
                                        species_id,
                                        conf_gen)
    df_train.to_pickle(train_data_file)

test_data_file = u'/home/ys587/__Data/__whistle/__log_dir_audio/__feature/df_test.pkl'
if os.path.exists(test_data_file) is True:
    df_test = pd.read_pickle(test_data_file)
else:
    df_test,  dur_test_max, count_long_test, count_all_test = \
        fea_label_generate_no_harmonics(contour_test,
                                        bin_wav_pair_test,
                                        bin_dir_test,
                                        species_id,
                                        conf_gen)
    df_test.to_pickle(test_data_file)

# convert features from dataframe to numpy array
# frame-based features
# feature_train = df_train.iloc[:, 3:].to_numpy()
# label_train0 = df_train.iloc[:, 2].astype(int).to_numpy()
# feature_test = df_test.iloc[:, 3:].to_numpy()
# label_test0 = df_test.iloc[:, 2].astype(int).to_numpy()
feature_train = df_train.iloc[:, 4:].to_numpy()
label_train0 = df_train.iloc[:, 3].astype(int).to_numpy()
feature_test = df_test.iloc[:, 4:].to_numpy()
label_test0 = df_test.iloc[:, 3].astype(int).to_numpy()

label_list_train = label_train0.tolist()
label_list_test = label_test0.tolist()

# fea_list_train = []
# fea_list_test = []
# for rr in range(fea_train.shape[0]):
#     fea_list_train.append(fea_train[rr, :])
# for rr in range(fea_test.shape[0]):
#     fea_list_test.append(fea_test[rr, :])
#
# # shuffle
# if conf_gen['shuffle'] is True:
#     train_idx = [ii for ii in range(len(label_list_train))]
#     random.shuffle(train_idx)
#     test_idx = [ii for ii in range(len(label_list_test))]
#     random.shuffle(test_idx)
#
#     feature_train = [fea_list_train[tt] for tt in train_idx]
#     label_train = to_categorical(label_list_train)[train_idx, :]
#     feature_test = [fea_list_test[tt] for tt in test_idx]
#     label_test = to_categorical(label_list_test)[test_idx, :]
# else:
#     feature_train = fea_list_train
#     label_train = label_list_train
#     feature_test = fea_list_test
#     label_test = label_list_test
label_train = label_list_train
label_test = label_list_test

conf_model = dict()
conf_model['batch_size'] = 128
conf_model['epochs'] = 100
conf_model['learning_rate'] = 0.001
# conf_model['learning_rate'] = 0.01
conf_model["l2_regu"] = 1.e-2
conf_model["dropout"] = 0.2
conf_model["recurrent_dropout"] = 0.2
conf_model["class_weight"] = {0: 1.0, 1: 1.0, 2: 1.0, 3: 1.0}
conf_model["mod"] = "sum"
conf_model["duration_thre"] = conf_gen["duration_thre"]
conf_model['time_reso'] = conf_gen['time_reso']


# log_dir = "/home/ys587/__Data/__whistle/__log_dir/"
log_dir = "/home/ys587/__Data/__whistle/__log_dir_audio"

# num_dolphins = len(species_name)
conf_model["num_class"] = len(species_name)
# conf_model["fea_dim"] = 1
# conf_model["fea_dim"] = fea_list_test[0].shape[1]
conf_model["fea_dim"] = feature_train.shape[1]

# model experiments
if False:
    model_name = "fully_connected"
    model_tag = model_name + "_16_lr_p01"
    conf_model.update({"dense_size": 16, "learning_rate": 0.01})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    model_name = "fully_connected"
    model_tag = model_name + "_16_lr_p005"
    conf_model.update({"dense_size": 16, "learning_rate": 0.005})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    model_name = "fully_connected"
    model_tag = model_name + "_16_lr_p001"
    conf_model.update({"dense_size": 16, "learning_rate": 0.001})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    model_name = "fully_connected"
    model_tag = model_name + "_64_lr_p01"
    conf_model.update({"dense_size": 64, "learning_rate": 0.01})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    model_name = "fully_connected"
    model_tag = model_name + "_64_lr_p005"
    conf_model.update({"dense_size": 64, "learning_rate": 0.005})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    model_name = "fully_connected"
    model_tag = model_name + "_64_lr_p001"
    conf_model.update({"dense_size": 64, "learning_rate": 0.001})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    model_name = "fully_connected_l2"
    model_tag = model_name + "_64_lr_p001"
    conf_model.update({"dense_size": 64})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    model_name = "fully_connected_leakyrelu"
    model_tag = model_name + "_64_lr_p001"
    conf_model.update({"dense_size": 64})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    model_name = "fully_connected_l2_leakyrelu"
    model_tag = model_name + "_64_lr_p001"
    conf_model.update({"dense_size": 64})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    # 64; lr=0.005
    # model_name = "fully_connected"
    # model_tag = model_name + "_64_lr_p01"
    # conf_model.update({"dense_size": 64, 'learning_rate': 0.01})
    # two_fold_cross_validate_mlp(model_name, feature_train, label_train,
    #                             feature_test, label_test, conf_model, log_dir,
    #                             model_tag)

    model_name = "fully_connected_l2"
    model_tag = model_name + "_64_lr_p005"
    conf_model.update({"dense_size": 64, 'learning_rate': 0.005})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    model_name = "fully_connected_l2"
    model_tag = model_name + "_64_lr_p01"
    conf_model.update({"dense_size": 64, 'learning_rate': 0.01})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    model_name = "fully_connected_leakyrelu"
    model_tag = model_name + "_64_lr_p005"
    conf_model.update({"dense_size": 64, 'learning_rate': 0.005})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    model_name = "fully_connected_l2_leakyrelu"
    model_tag = model_name + "_64_lr_p005"
    conf_model.update({"dense_size": 64, 'learning_rate': 0.005})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    # 128
    model_name = "fully_connected_l2"
    model_tag = model_name + "_128_lr_p001"
    conf_model.update({"dense_size": 128, 'learning_rate': 0.001})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    model_name = "fully_connected_l2"
    model_tag = model_name + "_128_lr_p005"
    conf_model.update({"dense_size": 128, 'learning_rate': 0.005})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    model_name = "fully_connected_l2"
    model_tag = model_name + "_128_lr_p01"
    conf_model.update({"dense_size": 128, 'learning_rate': 0.01})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    # 16
    model_name = "fully_connected_l2"
    model_tag = model_name + "_16_lr_p001"
    conf_model.update({"dense_size": 16, 'learning_rate': 0.001})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    model_name = "fully_connected_l2"
    model_tag = model_name + "_16_lr_p005"
    conf_model.update({"dense_size": 16, 'learning_rate': 0.005})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    model_name = "fully_connected_l2"
    model_tag = model_name + "_16_lr_p01"
    conf_model.update({"dense_size": 16, 'learning_rate': 0.01})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    # 256
    model_name = "fully_connected_l2"
    model_tag = model_name + "_256_lr_p001"
    conf_model.update({"dense_size": 128, 'learning_rate': 0.001})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    model_name = "fully_connected_l2"
    model_tag = model_name + "_256_lr_p005"
    conf_model.update({"dense_size": 128, 'learning_rate': 0.005})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    model_name = "fully_connected_l2"
    model_tag = model_name + "_256_lr_p01"
    conf_model.update({"dense_size": 128, 'learning_rate': 0.01})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    # 256
    model_name = "fully_connected_l2"
    model_tag = model_name + "_256_lr_p001"
    conf_model.update({"dense_size": 256, 'learning_rate': 0.001})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    model_name = "fully_connected_l2"
    model_tag = model_name + "_256_lr_p005"
    conf_model.update({"dense_size": 256, 'learning_rate': 0.005})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    model_name = "fully_connected_l2"
    model_tag = model_name + "_256_lr_p01"
    conf_model.update({"dense_size": 256, 'learning_rate': 0.01})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    # 64, L2, 2nd trial
    model_name = "fully_connected_l2"
    model_tag = model_name + "_64_lr_p01"
    conf_model.update({"dense_size": 64, 'learning_rate': 0.01})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    model_name = "fully_connected_leakyrelu"
    model_tag = model_name + "_64_lr_p005"
    conf_model.update({"dense_size": 64, 'learning_rate': 0.005})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    model_name = "fully_connected_l2_leakyrelu"
    model_tag = model_name + "_64_lr_p005"
    conf_model.update({"dense_size": 64, 'learning_rate': 0.005})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    # learning rate 0.0005
    model_name = "fully_connected_l2"
    model_tag = model_name + "_16_lr_p0005"
    conf_model.update({"dense_size": 16, 'epochs': 200,'learning_rate': 0.0005})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    model_name = "fully_connected_l2"
    model_tag = model_name + "_32_lr_p0005"
    conf_model.update({"dense_size": 32, 'epochs': 200,'learning_rate': 0.0005})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    model_name = "fully_connected_l2"
    model_tag = model_name + "_64_lr_p0005"
    conf_model.update({"dense_size": 64, 'epochs': 200,'learning_rate': 0.0005})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    model_name = "fully_connected_l2"
    model_tag = model_name + "_128_lr_p0005"
    conf_model.update({"dense_size": 256, 'epochs': 200,'learning_rate': 0.0005})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    model_name = "fully_connected_l2"
    model_tag = model_name + "_256_lr_p0005"
    conf_model.update({"dense_size": 256, 'epochs': 200,'learning_rate': 0.0005})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    # l2_regu = 1.e-2(original), 5.e-2, 0.1, 0.2
    model_name = "fully_connected_l2"
    model_tag = model_name + "_64_lr_p001_L2_p05"
    conf_model.update({"dense_size": 64, 'l2_regu': 0.05, 'learning_rate': 0.001})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    model_name = "fully_connected_l2"
    model_tag = model_name + "_64_lr_p005_L2_p05"
    conf_model.update({"dense_size": 64, 'l2_regu': 0.05, 'learning_rate': 0.005})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    model_name = "fully_connected_l2"
    model_tag = model_name + "_64_lr_p001_L2_p1"
    conf_model.update({"dense_size": 64, 'l2_regu': 0.1, 'learning_rate': 0.001})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    model_name = "fully_connected_l2"
    model_tag = model_name + "_64_lr_p005_L2_p1"
    conf_model.update({"dense_size": 64, 'l2_regu': 0.1, 'learning_rate': 0.005})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    model_name = "fully_connected_l2"
    model_tag = model_name + "_128_lr_p001_L2_p05"
    conf_model.update({"dense_size": 128, 'l2_regu': 0.05, 'learning_rate': 0.001})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)

    model_name = "fully_connected_l2"
    model_tag = model_name + "_128_lr_p005_L2_p05"
    conf_model.update({"dense_size": 128, 'l2_regu': 0.05, 'learning_rate': 0.005})
    two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                                feature_test, label_test, conf_model, log_dir,
                                model_tag)







model_name = "fully_connected_l2"
model_tag = model_name + "_128_lr_p001_L2_p1"
conf_model.update({"dense_size": 128, 'l2_regu': 0.1, 'learning_rate': 0.001})
two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                            feature_test, label_test, conf_model, log_dir,
                            model_tag)

model_name = "fully_connected_l2"
model_tag = model_name + "_128_lr_p005_L2_p1"
conf_model.update({"dense_size": 128, 'l2_regu': 0.1, 'learning_rate': 0.005})
two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                            feature_test, label_test, conf_model, log_dir,
                            model_tag)

model_name = "fully_connected_l2"
model_tag = model_name + "_256_lr_p001_L2_p05"
conf_model.update({"dense_size": 256, 'l2_regu': 0.05, 'learning_rate': 0.001})
two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                            feature_test, label_test, conf_model, log_dir,
                            model_tag)

model_name = "fully_connected_l2"
model_tag = model_name + "_256_lr_p005_L2_p05"
conf_model.update({"dense_size": 256, 'l2_regu': 0.05, 'learning_rate': 0.005})
two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                            feature_test, label_test, conf_model, log_dir,
                            model_tag)

model_name = "fully_connected_l2"
model_tag = model_name + "_256_lr_p001_L2_p1"
conf_model.update({"dense_size": 256, 'l2_regu': 0.1, 'learning_rate': 0.001})
two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                            feature_test, label_test, conf_model, log_dir,
                            model_tag)

model_name = "fully_connected_l2"
model_tag = model_name + "_256_lr_p005_L2_p1"
conf_model.update({"dense_size": 256, 'l2_regu': 0.1, 'learning_rate': 0.005})
two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                            feature_test, label_test, conf_model, log_dir,
                            model_tag)

# l2 = 0.2
model_name = "fully_connected_l2"
model_tag = model_name + "_64_lr_p001_L2_p2"
conf_model.update({"dense_size": 64, 'l2_regu': 0.2, 'learning_rate': 0.001})
two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                            feature_test, label_test, conf_model, log_dir,
                            model_tag)

model_name = "fully_connected_l2"
model_tag = model_name + "_64_lr_p005_L2_p2"
conf_model.update({"dense_size": 64, 'l2_regu': 0.2, 'learning_rate': 0.005})
two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                            feature_test, label_test, conf_model, log_dir,
                            model_tag)

model_name = "fully_connected_l2"
model_tag = model_name + "_128_lr_p001_L2_p2"
conf_model.update({"dense_size": 128, 'l2_regu': 0.2, 'learning_rate': 0.001})
two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                            feature_test, label_test, conf_model, log_dir,
                            model_tag)

model_name = "fully_connected_l2"
model_tag = model_name + "_128_lr_p005_L2_p2"
conf_model.update({"dense_size": 128, 'l2_regu': 0.05, 'learning_rate': 0.005})
two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                            feature_test, label_test, conf_model, log_dir,
                            model_tag)

model_name = "fully_connected_l2"
model_tag = model_name + "_256_lr_p001_L2_p2"
conf_model.update({"dense_size": 256, 'l2_regu': 0.2, 'learning_rate': 0.001})
two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                            feature_test, label_test, conf_model, log_dir,
                            model_tag)

model_name = "fully_connected_l2"
model_tag = model_name + "_256_lr_p005_L2_p2"
conf_model.update({"dense_size": 256, 'l2_regu': 0.2, 'learning_rate': 0.005})
two_fold_cross_validate_mlp(model_name, feature_train, label_train,
                            feature_test, label_test, conf_model, log_dir,
                            model_tag)

# Go deeper! 3 - 8 layers.