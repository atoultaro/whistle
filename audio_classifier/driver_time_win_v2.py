#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
4-species whistle classification
feature: filtered cepstrum & filterbank energy

Version 2: Trained on frames and validated on contours.

Fixed context window (ANN, TDNN) or unfixed (RNN, LSTM)
Increase the number of training feature sequences

Models:
ANN: T(time)xF(feature dimension) as input
TDNN: time-delay neural network
RNN


Created on 9/11/19
@author: atoultaro
"""
import os
import pandas as pd
import sys
import numpy as np
import pickle
import random
import matplotlib.pyplot as plt

# import librosa
# from keras.utils import to_categorical

from classifier.recurrent import train_and_evaluate
# from classifier.buildmodels import build_model
from classifier.batchgenerator import PaddedBatchGenerator


from cape_cod_whale.preprocess import bin_extract, \
    fea_label_generate_no_harmonics, fea_label_generate_no_harmonics_v2, \
    save_fea, load_fea, freq_seq_label_generate, fea_label_shuffle, \
    fea_label_generate_no_harmonics_v3
from cape_cod_whale.classifier import train_validation, \
    two_fold_cross_validate_mlp, two_fold_cross_validate_time, \
    two_fold_cross_validate_time_uneven, convert_df_2_contour_list, \
    convert_df_2_win_list, convert_df_2_win_contour_list

# bin files for training & testing
# data_dir = '/Users/ys587/__Data/whistle_Contours/tonals_20190210' # bin files
bin_dir_train = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__first_pie'
bin_dir_test = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__second_pie'
bin_dir_all = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__all'
sound_dir = '/home/ys587/__Data/__whistle/__sound_species/'
log_dir = "/home/ys587/__Data/__whistle/__log_dir_audio_time/__logdir_temp"

species_name = ['bottlenose', 'common', 'spinner', 'melon-headed']
species_id = {'bottlenose': 0, 'common': 1, 'spinner': 2, 'melon-headed': 3}
contour_train, bin_wav_pair_train = bin_extract(bin_dir_train, sound_dir,
                                                species_name)
contour_test, bin_wav_pair_test = bin_extract(bin_dir_test, sound_dir,
                                              species_name)

fea_type = ['cepstrum', 'energy']

for fea in fea_type:
    # find contours longer than a pre-defined duration and extract audio features
    conf_model = dict()
    # conf_model["feature"] = 'energy'  # or cepstrum
    # conf_model["feature"] = 'cepstrum'  # or energy
    conf_model["feature"] = fea

    conf_model["duration_thre"] = 0.5
    # conf_model["duration_thre"] = 1.0
    percentile_list = [0, 25, 50, 75, 100]
    # conf_model['time_reso'] = 0.02  # should be 0.01067 sec
    conf_model['time_reso'] = 0.01
    # conf_model['gap'] = 0.02
    conf_model['shuffle'] = True
    conf_model['win_back'] = 10

    conf_model['num_class'] = len(species_name)
    conf_model['batch_size'] = 256
    conf_model['epochs'] = 100
    # conf_model['epochs'] = 2  # for debugging
    conf_model['learning_rate'] = 0.001
    # conf_model['learning_rate'] = 0.01
    conf_model["l2_regu"] = 1.e-2
    conf_model["dropout"] = 0.2
    conf_model["recurrent_dropout"] = 0.2
    conf_model["class_weight"] = {0: 1.0, 1: 1.0, 2: 1.0, 3: 1.0}
    conf_model["bi_mod"] = "concat"
    # conf_model["bbi_mod"] = "sum"
    conf_model["fea_dim"] = 64  # 64-filter Cepstrum
    # conf_model["fea_dim"] = fea_list_test[0].shape[1]
    # conf_model["fea_dim"] = fea_train[0].shape[1]
    conf_model["type"] = 'lstm'  #'lstm', 'conv2d', 'conv2d_lstm'; will affect input data shape
    conf_model["best_model"] = "NA"
    conf_model['species_name'] = species_name

    # features & labels
    # train data
    if conf_model["feature"] is 'cepstrum':
        train_data_file = u'/home/ys587/__Data/__whistle/__log_dir_audio_time/__feature/df_train_cepstrum.pkl'
    elif conf_model["feature"] is 'energy':
        train_data_file = u'/home/ys587/__Data/__whistle/__log_dir_audio_time/__feature/df_train_energy.pkl'
    else:
        print("The requested feature type is not supported.")
        sys.exit()

    if os.path.exists(train_data_file) is True:
        df_train = pd.read_pickle(train_data_file)
    else:
        if conf_model["feature"] is 'cepstrum':
            df_train, dur_train_max, count_long_train, count_all_train = \
                fea_label_generate_no_harmonics_v2(
                    contour_train, bin_wav_pair_train,
                    bin_dir_train, species_id, conf_model)
        else:  # conf_model["feature"] is 'energy':
            df_train, dur_train_max, count_long_train, count_all_train = \
                fea_label_generate_no_harmonics_v3(
                    contour_train, bin_wav_pair_train,
                    bin_dir_train, species_id, conf_model)
        df_train.to_pickle(train_data_file)

    # test data
    if conf_model["feature"] is 'cepstrum':
        test_data_file = u'/home/ys587/__Data/__whistle/__log_dir_audio_time/__feature/df_test_cepstrum.pkl'
    elif conf_model["feature"] is 'energy':
        test_data_file = u'/home/ys587/__Data/__whistle/__log_dir_audio_time/__feature/df_test_energy.pkl'
    else:
        print("The requested feature type is not supported.")
        sys.exit()

    if os.path.exists(test_data_file) is True:
        df_test = pd.read_pickle(test_data_file)
    else:
        if conf_model["feature"] is 'cepstrum':
            df_test,  dur_test_max, count_long_test, count_all_test = \
                fea_label_generate_no_harmonics_v2(
                    contour_test, bin_wav_pair_test,
                    bin_dir_test, species_id, conf_model)
        else:  # conf_model["feature"] is 'energy':
            df_test,  dur_test_max, count_long_test, count_all_test = \
                fea_label_generate_no_harmonics_v3(
                    contour_test, bin_wav_pair_test,
                    bin_dir_test, species_id, conf_model)
        df_test.to_pickle(test_data_file)

    # model experiment
    conf_model['win_back'] = 10
    conf_model['type'] = 'conv2d_lstm'

    model_name = "conv2d_lstm"
    model_tag = 'fea_'+fea+'_'+model_name + "_winback_10_16_8_2lay_l2_p2_dropout_p2"
    conf_model.update(
        {'filt1': 16, 'filt2': 8, 'l2_regu': 0.2, 'learning_rate': 0.0005,
         'recurrent_dropout': 0.2, 'dropout': 0.2})
    two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
                                        conf_model, log_dir)

    model_name = "conv2d_lstm"
    model_tag = 'fea_'+fea+'_'+ model_name + "_winback_10_32_16_2lay_l2_p2_dropout_p2"
    conf_model.update(
        {'filt1': 32, 'filt2': 16, 'l2_regu': 0.2, 'learning_rate': 0.0005,
         'recurrent_dropout': 0.2, 'dropout': 0.2})
    two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
                                        conf_model, log_dir)

    model_name = "conv2d_lstm"
    model_tag = 'fea_'+fea+'_'+ model_name + "_winback_10_64_32_2lay_l2_p2_dropout_p2"
    conf_model.update(
        {'filt1': 64, 'filt2': 32, 'l2_regu': 0.2, 'learning_rate': 0.0005,
         'recurrent_dropout': 0.2, 'dropout': 0.2})
    two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
                                        conf_model, log_dir)

    model_name = "conv2d_lstm"
    model_tag = 'fea_'+fea+'_'+ model_name + "_winback_10_128_64_2lay_l2_p2_dropout_p2"
    conf_model.update(
        {'filt1': 128, 'filt2': 64, 'l2_regu': 0.2, 'learning_rate': 0.0005,
         'recurrent_dropout': 0.2, 'dropout': 0.2})
    two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
                                        conf_model, log_dir)

    #
    #
    # conf_model['type'] = 'lstm'
    # model_name = "lstm_2lay"
    # model_tag = model_name + "_winback_10_8_4_lr_p001_l2_p2_dropout_p2"
    # conf_model.update({'lstm1': 8, 'lstm2': 4, 'l2_regu': 0.2,
    #                    'learning_rate': 0.001, 'recurrent_dropout': 0.2,
    #                    'dropout': 0.2})
    # two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
    #                                     conf_model, log_dir)
    # model_name = "lstm_2lay"
    # model_tag = model_name + "_winback_10_16_8_lr_p001_l2_p2_dropout_p2"
    # conf_model.update({'lstm1': 16, 'lstm2': 8, 'l2_regu': 0.2,
    #                    'learning_rate': 0.001, 'recurrent_dropout': 0.2,
    #                    'dropout': 0.2})
    # two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
    #                                     conf_model, log_dir)
    # model_name = "lstm_2lay"
    # model_tag = model_name + "_winback_10_32_16_lr_p001_l2_p2_dropout_p1"
    # conf_model.update({'lstm1': 32, 'lstm2': 16, 'l2_regu': 0.2,
    #                    'learning_rate': 0.001, 'recurrent_dropout': 0.1,
    #                    'dropout': 0.1})
    # two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
    #                                     conf_model, log_dir)
    # model_name = "lstm_2lay"
    # model_tag = model_name + "_winback_10_32_16_lr_p001_l2_p2_dropout_p2"
    # conf_model.update({'lstm1': 32, 'lstm2': 16, 'l2_regu': 0.2,
    #                    'learning_rate': 0.001, 'recurrent_dropout': 0.2,
    #                    'dropout': 0.2})
    # two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
    #                                     conf_model, log_dir)

    # model_name = "lstm_2lay"
    # model_tag = 'fea_'+fea+'_'+ model_name + "_winback_10_32_16_lr_p001_l2_p2_dropout_p4"
    # conf_model.update({'lstm1': 32, 'lstm2': 16, 'l2_regu': 0.2,
    #                    'learning_rate': 0.001, 'recurrent_dropout': 0.4,
    #                    'dropout': 0.4})
    # two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
    #                                     conf_model, log_dir)
    #
    # model_name = "lstm_2lay"
    # model_tag = 'fea_'+fea+'_'+ model_name + "_winback_10_64_32_lr_p001_l2_p2_dropout_p2"
    # conf_model.update({'lstm1': 64, 'lstm2': 32, 'l2_regu': 0.2,
    #                    'learning_rate': 0.001, 'recurrent_dropout': 0.2,
    #                    'dropout': 0.2})
    # two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
    #                                     conf_model, log_dir)
    #
    # model_name = "lstm_2lay"
    # model_tag = 'fea_'+fea+'_'+ model_name + "_winback_10_64_32_lr_p001_l2_p2_dropout_p4"
    # conf_model.update({'lstm1': 64, 'lstm2': 32, 'l2_regu': 0.2,
    #                    'learning_rate': 0.001, 'recurrent_dropout': 0.4,
    #                    'dropout': 0.4})
    # two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
    #                                     conf_model, log_dir)













if False:
    conf_model['type'] = 'conv2d_lstm'

    model_name = "conv2d_lstm"
    model_tag = model_name + "_winback_10_8_4_2lay"
    conf_model.update({'filt1': 8, 'filt2': 4, 'l2_regu': 0.2,
                       'learning_rate': 0.0005, 'recurrent_dropout': 0.1,
                       'dropout': 0.1})
    two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
                                        conf_model, log_dir)

    model_name = "conv2d_lstm"
    model_tag = model_name + "_winback_10_16_8_2lay"
    conf_model.update({'filt1': 16, 'filt2': 8, 'l2_regu': 0.2,
                       'learning_rate': 0.0005, 'recurrent_dropout': 0.1,
                       'dropout': 0.1})
    two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
                                        conf_model, log_dir)

    conf_model["type"] = 'lstm'
    model_name = "mlp_time_l2"
    model_tag = model_name + "_winback_10_16_8"
    conf_model.update({'filt1': 16, 'filt2': 8, 'l2_regu': 0.2,
                       'learning_rate': 0.001, 'recurrent_dropout': 0.2,
                       'dropout': 0.2})
    two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
                                        conf_model, log_dir)





# model_name = "lstm_2lay"
# model_tag = model_name + "_winback_10_64_16_lr_p001_l2_p2_dropout_p4"
# conf_model.update({'lstm1': 64, 'lstm2': 16, 'l2_regu': 0.2,
#                    'learning_rate': 0.001, 'recurrent_dropout': 0.4,
#                    'dropout': 0.4})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)
# model_name = "lstm_2lay"
# model_tag = model_name + "_winback_10_64_16_lr_p001_l2_p1_dropout_p4"
# conf_model.update({'lstm1': 64, 'lstm2': 16, 'l2_regu': 0.1,
#                    'learning_rate': 0.001, 'recurrent_dropout': 0.4,
#                    'dropout': 0.4})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)
# model_name = "lstm_2lay"
# model_tag = model_name + "_winback_10_64_16_lr_p001_l2_p2_dropout_p2"
# conf_model.update({'lstm1': 64, 'lstm2': 16, 'l2_regu': 0.2,
#                    'learning_rate': 0.001, 'recurrent_dropout': 0.2,
#                    'dropout': 0.2})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)
# model_name = "lstm_2lay"
# model_tag = model_name + "_winback_10_64_16_lr_p001_l2_p1_dropout_p2"
# conf_model.update({'lstm1': 64, 'lstm2': 16, 'l2_regu': 0.1,
#                    'learning_rate': 0.001, 'recurrent_dropout': 0.2,
#                    'dropout': 0.2})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)


#
#
# conf_model['type'] = 'conv2d_lstm'
# conf_model['win_back'] = 20
# model_name = "conv2d_lstm"
# model_tag = model_name + "_winback_20_8_4_2lay"
# conf_model.update({'filt1': 8, 'filt2': 4, 'l2_regu': 0.2,
#                    'learning_rate': 0.0005, 'recurrent_dropout': 0.1,
#                    'dropout': 0.1})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
# #                                     conf_model, log_dir)
#
# model_name = "conv2d_lstm"
# model_tag = model_name + "_winback_20_16_8_2lay"
# conf_model.update({'filt1': 16, 'filt2': 8, 'l2_regu': 0.2,
#                    'learning_rate': 0.0005, 'recurrent_dropout': 0.1,
#                    'dropout': 0.1})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)
#

conf_model["type"] = 'lstm'
conf_model['win_back'] = 10

# model_name = "mlp_time_l2"
# model_tag = model_name + "_winback_10_64_64"
# conf_model.update({'filt1': 64, 'filt2': 64, 'l2_regu': 0.2,
#                    'learning_rate': 0.001, 'recurrent_dropout': 0.2,
#                    'dropout': 0.2})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)

# model_name = "mlp_time_l2"
# model_tag = model_name + "_winback_10_16_16"
# conf_model.update({'filt1': 16, 'filt2': 16, 'l2_regu': 0.2,
#                    'learning_rate': 0.001, 'recurrent_dropout': 0.2,
#                    'dropout': 0.2})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)
#
# model_name = "mlp_time_l2"
# model_tag = model_name + "_winback_10_32_32"
# conf_model.update({'filt1': 32, 'filt2': 32, 'l2_regu': 0.2,
#                    'learning_rate': 0.001, 'recurrent_dropout': 0.2,
#                    'dropout': 0.2})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)
#
# model_name = "mlp_time_l2"
# model_tag = model_name + "_winback_10_128_128"
# conf_model.update({'filt1': 128, 'filt2': 128, 'l2_regu': 0.2,
#                    'learning_rate': 0.001, 'recurrent_dropout': 0.2,
#                    'dropout': 0.2})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)
#
#
# model_name = "mlp_time_l2"
# model_tag = model_name + "_winback_10_32_16"
# conf_model.update({'filt1': 32, 'filt2': 16, 'l2_regu': 0.2,
#                    'learning_rate': 0.001, 'recurrent_dropout': 0.2,
#                    'dropout': 0.2})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)
#
# model_name = "mlp_time_l2"
# model_tag = model_name + "_winback_10_64_32"
# conf_model.update({'filt1': 64, 'filt2': 32, 'l2_regu': 0.2,
#                    'learning_rate': 0.001, 'recurrent_dropout': 0.2,
#                    'dropout': 0.2})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)






# model_name = "conv2d_lstm"
# model_tag = model_name + "_winback_10_64_32"
# conf_model.update({'filt1': 64, 'filt2': 32, 'l2_regu': 0.2,
#                    'learning_rate': 0.001, 'recurrent_dropout': 0.2,
#                    'dropout': 0.2})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)
#
# model_name = "conv2d_lstm"
# model_tag = model_name + "_winback_10_128_64"
# conf_model.update({'filt1': 128, 'filt2': 64, 'l2_regu': 0.2,
#                    'learning_rate': 0.001, 'recurrent_dropout': 0.2,
#                    'dropout': 0.2})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)
#
# conf_model['win_back'] = 40
#
# model_name = "conv2d_lstm"
# model_tag = model_name + "_winback_40_32_16"
# conf_model.update({'filt1': 32, 'filt2': 16, 'l2_regu': 0.2,
#                    'learning_rate': 0.001, 'recurrent_dropout': 0.2,
#                    'dropout': 0.2})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)

# Sept 17, 2019 starts here.
if False:
    model_name = "conv2d_lstm"
    model_tag = model_name + "_winback_40_64_32"
    conf_model.update({'filt1': 64, 'filt2': 32, 'l2_regu': 0.2,
                       'learning_rate': 0.001, 'recurrent_dropout': 0.2,
                       'dropout': 0.2})
    two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
                                        conf_model, log_dir)

    model_name = "conv2d_lstm"
    model_tag = model_name + "_winback_40_128_64"
    conf_model.update({'filt1': 128, 'filt2': 64, 'l2_regu': 0.2,
                       'learning_rate': 0.001, 'recurrent_dropout': 0.2,
                       'dropout': 0.2})
    two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
                                        conf_model, log_dir)


    conf_model['type'] = 'lstm'
    model_name = "lstm_2lay"
    model_tag = model_name + "_winback_40_128_32_lr_p001_l2_p2_dropout_p2"
    conf_model.update({'lstm1': 128, 'lstm2': 32, 'l2_regu': 0.2,
                       'learning_rate': 0.001, 'recurrent_dropout': 0.2,
                       'dropout': 0.2})
    two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
                                        conf_model, log_dir)

    model_name = "lstm_2lay"
    model_tag = model_name + "_winback_40_256_64_lr_p001_l2_p2_dropout_p2"
    conf_model.update({'lstm1': 256, 'lstm2': 64, 'l2_regu': 0.2,
                       'learning_rate': 0.001, 'recurrent_dropout': 0.2,
                       'dropout': 0.2})
    two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
                                        conf_model, log_dir)

    model_name = "lstm_2lay"
    model_tag = model_name + "_winback_40_32_8_lr_p001_l2_p2_dropout_p2"
    conf_model.update({'lstm1': 32, 'lstm2': 8, 'l2_regu': 0.2,
                       'learning_rate': 0.001, 'recurrent_dropout': 0.2,
                       'dropout': 0.2})
    two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
                                        conf_model, log_dir)

    model_name = "lstm_bidir_2lay"
    model_tag = model_name + "_winback_40_64_16_lr_p001_l2_p2_dropout_p2"
    conf_model.update({'lstm1': 64, 'lstm2': 16, 'l2_regu': 0.2,
                       'learning_rate': 0.001, 'recurrent_dropout': 0.2,
                       'dropout': 0.2})
    two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
                                        conf_model, log_dir)

    model_name = "lstm_bidir_2lay"
    model_tag = model_name + "_winback_40_128_32_lr_p001_l2_p2_dropout_p2"
    conf_model.update({'lstm1': 128, 'lstm2': 32, 'l2_regu': 0.2,
                       'learning_rate': 0.001, 'recurrent_dropout': 0.2,
                       'dropout': 0.2})
    two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
                                        conf_model, log_dir)

    model_name = "lstm_bidir_2lay"
    model_tag = model_name + "_winback_40_256_64_lr_p001_l2_p2_dropout_p2"
    conf_model.update({'lstm1': 256, 'lstm2': 64, 'l2_regu': 0.2,
                       'learning_rate': 0.001, 'recurrent_dropout': 0.2,
                       'dropout': 0.2})
    two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
                                        conf_model, log_dir)

    model_name = "lstm_bidir_2lay"
    model_tag = model_name + "_winback_40_32_8_lr_p001_l2_p2_dropout_p2"
    conf_model.update({'lstm1': 32, 'lstm2': 8, 'l2_regu': 0.2,
                       'learning_rate': 0.001, 'recurrent_dropout': 0.2,
                       'dropout': 0.2})
    two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
                                        conf_model, log_dir)


# model_name = "conv2d_time_fea"
# model_tag = model_name + "_winback_20_32_8_l2_p2"
# conf_model.update({'conv2d_1': 32, 'conv2d_2': 8, 'l2_regu': 0.2,
#                    'type': 'conv2d', 'learning_rate': 0.001, 'win_back': 20})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)
#
# model_name = "conv2d_time_fea"
# model_tag = model_name + "_winback_30_32_8_l2_p2"
# conf_model.update({'conv2d_1': 32, 'conv2d_2': 8, 'l2_regu': 0.2,
#                    'type': 'conv2d', 'learning_rate': 0.001, 'win_back': 30})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)
#
#
# conf_model['win_back'] = 20
# conf_model['type'] = 'lstm'
# model_name = "lstm_2lay"
# model_tag = model_name + "_winback_20_64_16_lr_p001_l2_p2_dropout_p4"
# conf_model.update({'lstm1': 64, 'lstm2': 16, 'l2_regu': 0.2,
#                    'learning_rate': 0.001, 'recurrent_dropout': 0.4,
#                    'dropout': 0.4})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)
# model_name = "lstm_2lay"
# model_tag = model_name + "_winback_20_64_16_lr_p001_l2_p1_dropout_p4"
# conf_model.update({'lstm1': 64, 'lstm2': 16, 'l2_regu': 0.1,
#                    'learning_rate': 0.001, 'recurrent_dropout': 0.4,
#                    'dropout': 0.4})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)
# model_name = "lstm_2lay"
# model_tag = model_name + "_winback_20_64_16_lr_p001_l2_p2_dropout_p2"
# conf_model.update({'lstm1': 64, 'lstm2': 16, 'l2_regu': 0.2,
#                    'learning_rate': 0.001, 'recurrent_dropout': 0.2,
#                    'dropout': 0.2})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)
# model_name = "lstm_2lay"
# model_tag = model_name + "_winback_20_64_16_lr_p001_l2_p1_dropout_p2"
# conf_model.update({'lstm1': 64, 'lstm2': 16, 'l2_regu': 0.1,
#                    'learning_rate': 0.001, 'recurrent_dropout': 0.2,
#                    'dropout': 0.2})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)
#
#
# conf_model['win_back'] = 30
# model_name = "lstm_2lay"
# model_tag = model_name + "_winback_30_64_16_lr_p001_l2_p2_dropout_p4"
# conf_model.update({'lstm1': 64, 'lstm2': 16, 'l2_regu': 0.2,
#                    'learning_rate': 0.001, 'recurrent_dropout': 0.4,
#                    'dropout': 0.4})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)
# model_name = "lstm_2lay"
# model_tag = model_name + "_winback_30_64_16_lr_p001_l2_p1_dropout_p4"
# conf_model.update({'lstm1': 64, 'lstm2': 16, 'l2_regu': 0.1,
#                    'learning_rate': 0.001, 'recurrent_dropout': 0.4,
#                    'dropout': 0.4})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)
# model_name = "lstm_2lay"
# model_tag = model_name + "_winback_30_64_16_lr_p001_l2_p2_dropout_p2"
# conf_model.update({'lstm1': 64, 'lstm2': 16, 'l2_regu': 0.2,
#                    'learning_rate': 0.001, 'recurrent_dropout': 0.2,
#                    'dropout': 0.2})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)
# model_name = "lstm_2lay"
# model_tag = model_name + "_winback_30_64_16_lr_p001_l2_p1_dropout_p2"
# conf_model.update({'lstm1': 64, 'lstm2': 16, 'l2_regu': 0.1,
#                    'learning_rate': 0.001, 'recurrent_dropout': 0.2,
#                    'dropout': 0.2})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)











# model_name = "lstm_2lay"
# model_tag = model_name + "_32_8_lr_p001_L2_p2_dropout_p4"
# conf_model.update({'lstm1': 32, 'lstm2': 8, 'l2_regu': 0.2,
#                    'learning_rate': 0.001, 'recurrent_dropout': 0.4,
#                    'dropout': 0.4})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)
#
# model_name = "lstm_2lay"
# model_tag = model_name + "_32_8_lr_p001_L2_p2_dropout_p4"
# conf_model.update({'lstm1': 32, 'lstm2': 8, 'l2_regu': 0.2,
#                    'learning_rate': 0.001, 'recurrent_dropout': 0.4,
#                    'dropout': 0.4})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)








# model_name = "lstm_2lay"
# model_tag = model_name + "_16_4_lr_p001_L2_p2_dropout_p4"
# conf_model.update({'lstm1': 16, 'lstm2': 4, 'l2_regu': 0.2,
#                    'learning_rate': 0.001, 'recurrent_dropout': 0.4,
#                    'dropout': 0.4})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)
#
# model_name = "lstm_3lay"
# model_tag = model_name + "_64_16_4_lr_p001_L2_p2_dropout_p4"
# conf_model.update({'lstm1': 64, 'lstm2': 16, 'lstm3': 4, 'l2_regu': 0.2,
#                    'learning_rate': 0.001, 'recurrent_dropout': 0.4,
#                    'dropout': 0.4})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)
#
# model_name = "lstm_3lay"
# model_tag = model_name + "_64_32_16_lr_p001_L2_p2_dropout_p4"
# conf_model.update({'lstm1': 64, 'lstm2': 32, 'lstm3': 16, 'l2_regu': 0.2,
#                    'learning_rate': 0.001, 'recurrent_dropout': 0.4,
#                    'dropout': 0.4})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)
#
# model_name = "lstm_3lay"
# model_tag = model_name + "_32_16_8_lr_p001_L2_p2_dropout_p4"
# conf_model.update({'lstm1': 32, 'lstm2': 16, 'lstm3': 8, 'l2_regu': 0.2,
#                    'learning_rate': 0.001, 'recurrent_dropout': 0.4,
#                    'dropout': 0.4})
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)



















# model_name = "lstm_bidir_2lay"
# model_tag = model_name + "_8_4_lr_p001_L2_p1"
# conf_model.update({'lstm1': 8, 'lstm2': 4, 'l2_regu': 0.1, 'learning_rate': 0.001})
# two_fold_cross_validate_time(model_name, fea_train, label_train,
#                                  fea_test, label_test, conf_model,
#                                  log_dir, model_tag)
#
# model_name = "lstm_2lay"
# model_tag = model_name + "_8_8_lr_p001_L2_p1"
# conf_model.update({'lstm1': 8, 'lstm2': 8, 'l2_regu': 0.1, 'learning_rate': 0.001})
# two_fold_cross_validate_time(model_name, fea_train, label_train,
#                                  fea_test, label_test, conf_model,
#                                  log_dir, model_tag)
#
# model_name = "lstm_bidir_2lay"
# model_tag = model_name + "_8_8_lr_p001_L2_p1"
# conf_model.update({'lstm1': 8, 'lstm2': 8, 'l2_regu': 0.1, 'learning_rate': 0.001})
# two_fold_cross_validate_time(model_name, fea_train, label_train,
#                                  fea_test, label_test, conf_model,
#                                  log_dir, model_tag)
#
# model_name = "lstm_2lay"
# model_tag = model_name + "_16_4_lr_p001_L2_p1"
# conf_model.update({'lstm1': 16, 'lstm2': 4, 'l2_regu': 0.1, 'learning_rate': 0.001})
# two_fold_cross_validate_time(model_name, fea_train, label_train,
#                                  fea_test, label_test, conf_model,
#                                  log_dir, model_tag)
#
# model_name = "lstm_bidir_2lay"
# model_tag = model_name + "_16_4_lr_p001_L2_p1"
# conf_model.update({'lstm1': 16, 'lstm2': 4, 'l2_regu': 0.1, 'learning_rate': 0.001})
# two_fold_cross_validate_time(model_name, fea_train, label_train,
#                                  fea_test, label_test, conf_model,
#                                  log_dir, model_tag)
#
# model_name = "lstm_2lay"
# model_tag = model_name + "_16_8_lr_p001_L2_p1"
# conf_model.update({'lstm1': 16, 'lstm2': 8, 'l2_regu': 0.1, 'learning_rate': 0.001})
# two_fold_cross_validate_time(model_name, fea_train, label_train,
#                                  fea_test, label_test, conf_model,
#                                  log_dir, model_tag)
#
# model_name = "lstm_bidir_2lay"
# model_tag = model_name + "_16_8_lr_p001_L2_p1"
# conf_model.update({'lstm1': 16, 'lstm2': 8, 'l2_regu': 0.1, 'learning_rate': 0.001})
# two_fold_cross_validate_time(model_name, fea_train, label_train,
#                                  fea_test, label_test, conf_model,
#                                  log_dir, model_tag)
#
# model_name = "lstm_2lay"
# model_tag = model_name + "_16_16_lr_p001_L2_p1"
# conf_model.update({'lstm1': 16, 'lstm2': 16, 'l2_regu': 0.1, 'learning_rate': 0.001})
# two_fold_cross_validate_time(model_name, fea_train, label_train,
#                                  fea_test, label_test, conf_model,
#                                  log_dir, model_tag)
#
# model_name = "lstm_bidir_2lay"
# model_tag = model_name + "_16_16_lr_p001_L2_p1"
# conf_model.update({'lstm1': 16, 'lstm2': 16, 'l2_regu': 0.1, 'learning_rate': 0.001})
# two_fold_cross_validate_time(model_name, fea_train, label_train,
#                                  fea_test, label_test, conf_model,
#                                  log_dir, model_tag)
#
# model_name = "lstm_2lay"
# model_tag = model_name + "_32_8_lr_p001_L2_p1"
# conf_model.update({'lstm1': 32, 'lstm2': 8, 'l2_regu': 0.1, 'learning_rate': 0.001})
# two_fold_cross_validate_time(model_name, fea_train, label_train,
#                                  fea_test, label_test, conf_model,
#                                  log_dir, model_tag)
#
# model_name = "lstm_bidir_2lay"
# model_tag = model_name + "_32_8_lr_p001_L2_p1"
# conf_model.update({'lstm1': 32, 'lstm2': 8, 'l2_regu': 0.1, 'learning_rate': 0.001})
# two_fold_cross_validate_time(model_name, fea_train, label_train,
#                                  fea_test, label_test, conf_model,
#                                  log_dir, model_tag)
#
# model_name = "lstm_2lay"
# model_tag = model_name + "_32_16_lr_p001_L2_p1"
# conf_model.update({'lstm1': 32, 'lstm2': 8, 'l2_regu': 0.1, 'learning_rate': 0.001})
# two_fold_cross_validate_time(model_name, fea_train, label_train,
#                                  fea_test, label_test, conf_model,
#                                  log_dir, model_tag)
#
# model_name = "lstm_bidir_2lay"
# model_tag = model_name + "_32_16_lr_p001_L2_p1"
# conf_model.update({'lstm1': 32, 'lstm2': 8, 'l2_regu': 0.1, 'learning_rate': 0.001})
# two_fold_cross_validate_time(model_name, fea_train, label_train,
#                                  fea_test, label_test, conf_model,
#                                  log_dir, model_tag)
#
# model_name = "lstm_2lay"
# model_tag = model_name + "_32_32_lr_p001_L2_p1"
# conf_model.update({'lstm1': 32, 'lstm2': 8, 'l2_regu': 0.1, 'learning_rate': 0.001})
# two_fold_cross_validate_time(model_name, fea_train, label_train,
#                                  fea_test, label_test, conf_model,
#                                  log_dir, model_tag)
#
# model_name = "lstm_bidir_2lay"
# model_tag = model_name + "_32_32_lr_p001_L2_p1"
# conf_model.update({'lstm1': 32, 'lstm2': 8, 'l2_regu': 0.1, 'learning_rate': 0.001})
# two_fold_cross_validate_time(model_name, fea_train, label_train,
#                                  fea_test, label_test, conf_model,
#                                  log_dir, model_tag)

# change optimizer from Adam to AdamGrad, SGD+momentum


# fea_list_train0, label_train, dur_train_max = convert_df_2_win_list(
#     df_train, conf_model['win_back'])
# # return a list, each element of which is features for each contour
# fea_list_test, label_test, dur_test_max = convert_df_2_win_contour_list(
#     df_test, conf_model['win_back'])
#
# # shuffle: only training data
# if conf_model['shuffle'] is True:
#     train_idx = [ii for ii in range(len(label_train))]
#     random.shuffle(train_idx)
#     # test_idx = [ii for ii in range(len(label_test))]
#     # random.shuffle(test_idx)
#
#     fea_list_train = [fea_list_train0[tt] for tt in train_idx]
#     label_train = [label_train[tt] for tt in train_idx]
#     # fea_list_test = [fea_list_test0[tt] for tt in test_idx]
#     # label_test = [label_test[tt] for tt in train_idx]
#
#     del fea_list_train0
# else:
#     fea_list_train = fea_list_train0
#     # fea_list_test = fea_list_test0
#     del fea_list_train0
#
# fea_train = np.stack(fea_list_train, axis=0)
# fea_test = np.stack(fea_list_test, axis=0)




# fight against overfitting
# model_name = "lstm_2lay"
# model_tag = model_name + "_8_4_lr_p001_L2_p1"
# conf_model.update({'lstm1': 8, 'lstm2': 4, 'l2_regu': 0.1, 'learning_rate': 0.001})
# # two_fold_cross_validate_time(model_name, fea_train, label_train,
# #                                  fea_test, label_test, conf_model,
# #                                  log_dir, model_tag)
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)

# model_name = "lstm_2lay"
# model_tag = model_name + "_64_16_lr_p001_L2_p1"
# conf_model.update({'lstm1': 64, 'lstm2': 16, 'l2_regu': 0.1, 'learning_rate': 0.001})
# # two_fold_cross_validate_time(model_name, fea_train, label_train,
# #                                  fea_test, label_test, conf_model,
# #                                  log_dir, model_tag)
# two_fold_cross_validate_time_uneven(model_name, model_tag, df_train, df_test,
#                                     conf_model, log_dir)
