#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
4-species whistle classification
feature: filtered cepstrum

Version 1: Trained on frames and validated on frames

Fixed context window (ANN, TDNN) or unfixed (RNN, LSTM)
Increase the number of training feature sequences

Models:
ANN: T(time)xF(feature dimension) as input
TDNN: time-delay neural network
RNN


Created on 9/9/19
@author: atoultaro
"""
import os
import pandas as pd
import numpy as np
import pickle
import random
import matplotlib.pyplot as plt

# import librosa
# from keras.utils import to_categorical

from classifier.recurrent import train_and_evaluate
# from classifier.buildmodels import build_model
from classifier.batchgenerator import PaddedBatchGenerator


from cape_cod_whale.preprocess import bin_extract, \
    fea_label_generate_no_harmonics, fea_label_generate_no_harmonics_v2, \
    save_fea, load_fea, freq_seq_label_generate, fea_label_shuffle
from cape_cod_whale.classifier import train_validation, \
    two_fold_cross_validate_mlp, two_fold_cross_validate_mlp_time, \
    convert_df_2_contour_list, \
    convert_df_2_win_contour_list

# bin files for training & testing
# data_dir = '/Users/ys587/__Data/whistle_Contours/tonals_20190210' # bin files
bin_dir_train = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__first_pie'
bin_dir_test = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__second_pie'
bin_dir_all = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__all'
sound_dir = '/home/ys587/__Data/__whistle/__sound_species/'
# seltab_out_path = '/home/ys587/__Data/__whistle/__seltab'

species_name = ['bottlenose', 'common', 'spinner', 'melon-headed']
species_id = {'bottlenose': 0, 'common': 1, 'spinner': 2, 'melon-headed': 3}
contour_train, bin_wav_pair_train = bin_extract(bin_dir_train, sound_dir,
                                                species_name)
contour_test, bin_wav_pair_test = bin_extract(bin_dir_test, sound_dir,
                                              species_name)
# contour_all, bin_wav_pair = bin_extract(bin_dir_all, sound_dir, species_name)


# find contours longer than a pre-defined duration and extract audio features
conf_gen = dict()
# conf_gen["duration_thre"] = 0.5
conf_gen["duration_thre"] = 1.0
percentile_list = [0, 25, 50, 75, 100]
# conf_gen['time_reso'] = 0.02  # should be 0.01067 sec
conf_gen['time_reso'] = 0.01
conf_gen['gap'] = 0.05
conf_gen['shuffle'] = False
conf_gen['win_back'] = 10

# features & labels
train_data_file = u'/home/ys587/__Data/__whistle/__log_dir_audio_time/__feature/df_train.pkl'
if os.path.exists(train_data_file) is True:
    df_train = pd.read_pickle(train_data_file)
else:
    df_train,  dur_train_max, count_long_train, count_all_train = \
        fea_label_generate_no_harmonics_v2(contour_train, bin_wav_pair_train,
                                           bin_dir_train, species_id, conf_gen)
    df_train.to_pickle(train_data_file)

test_data_file = u'/home/ys587/__Data/__whistle/__log_dir_audio_time/__feature/df_test.pkl'
if os.path.exists(test_data_file) is True:
    df_test = pd.read_pickle(test_data_file)
else:
    df_test,  dur_test_max, count_long_test, count_all_test = \
        fea_label_generate_no_harmonics_v2(contour_test, bin_wav_pair_test,
                                           bin_dir_test, species_id, conf_gen)
    df_test.to_pickle(test_data_file)

fea_list_train0, label_train, dur_train_max = convert_df_2_win_contour_list(
    df_train, conf_gen['win_back'])
fea_list_test0, label_test, dur_test_max = convert_df_2_win_contour_list(
    df_test, conf_gen['win_back'])

# shuffle
if conf_gen['shuffle'] is True:
    train_idx = [ii for ii in range(len(label_train))]
    random.shuffle(train_idx)
    test_idx = [ii for ii in range(len(label_test))]
    random.shuffle(test_idx)

    fea_list_train = [fea_list_train0[tt] for tt in train_idx]
    label_train = [label_train[tt] for tt in train_idx]
    # label_train = to_categorical(label_list_train)[train_idx, :]
    fea_list_test = [fea_list_test0[tt] for tt in test_idx]
    label_test = [label_test[tt] for tt in train_idx]

    del fea_list_train0, fea_list_test0
else:
    fea_list_train = fea_list_train0
    fea_list_test = fea_list_test0
    del fea_list_train0, fea_list_test0

fea_train = np.stack(fea_list_train, axis=0)
fea_test = np.stack(fea_list_test, axis=0)

conf_model = dict()
conf_model['num_class'] = 4
conf_model['batch_size'] = 128
conf_model['epochs'] = 100
# conf_model['epochs'] = 50
conf_model['learning_rate'] = 0.001
# conf_model['learning_rate'] = 0.01
conf_model["l2_regu"] = 1.e-2
conf_model["dropout"] = 0.2
conf_model["recurrent_dropout"] = 0.2
conf_model["class_weight"] = {0: 1.0, 1: 1.0, 2: 1.0, 3: 1.0}
conf_model["bi_mod"] = "concat"
# conf_model["bbi_mod"] = "sum"
conf_model["duration_thre"] = conf_gen["duration_thre"]
conf_model['time_reso'] = conf_gen['time_reso']
conf_model['win_back'] = conf_gen['win_back']


# log_dir = "/home/ys587/__Data/__whistle/__log_dir/"
log_dir = "/home/ys587/__Data/__whistle/__log_dir_audio_time/__logdir_temp"

# num_dolphins = len(species_name)
conf_model["num_class"] = len(species_name)
# conf_model["fea_dim"] = 1
# conf_model["fea_dim"] = fea_list_test[0].shape[1]
conf_model["fea_dim"] = fea_list_train[0].shape[1]

# model experiment
if False:
    model_name = "mlp_time_l2"
    model_tag = model_name + "_dur_p5_64_lr_p001_L2_p1"
    conf_model.update({'dense_size': 64, 'l2_regu': 0.1, 'learning_rate': 0.001})
    two_fold_cross_validate_mlp_time(model_name, fea_train, label_train,
                                     fea_test, label_test, conf_model,
                                     log_dir, model_tag)

    model_name = "mlp_time_3lay_l2"
    model_tag = model_name + "_dur_p5_64_lr_p001_L2_p1"
    conf_model.update({'dense_size': 64, 'l2_regu': 0.1, 'learning_rate': 0.001})
    two_fold_cross_validate_mlp_time(model_name, fea_train, label_train,
                                     fea_test, label_test, conf_model,
                                     log_dir, model_tag)

    model_name = "mlp_time_4lay_l2"
    model_tag = model_name + "_dur_p5_64_lr_p001_L2_p1"
    conf_model.update({'dense_size': 64, 'l2_regu': 0.1, 'learning_rate': 0.001})
    two_fold_cross_validate_mlp_time(model_name, fea_train, label_train,
                                     fea_test, label_test, conf_model,
                                     log_dir, model_tag)

    model_name = "mlp_time_5lay_l2"
    model_tag = model_name + "_dur_p5_64_lr_p001_L2_p1"
    conf_model.update({'dense_size': 64, 'l2_regu': 0.1, 'learning_rate': 0.001})
    two_fold_cross_validate_mlp_time(model_name, fea_train, label_train,
                                     fea_test, label_test, conf_model,
                                     log_dir, model_tag)

    model_name = "mlp_time_l2"
    model_tag = model_name + "_dur_p5_128_lr_p001_L2_p1"
    conf_model.update({'dense_size': 128, 'l2_regu': 0.1, 'learning_rate': 0.001})
    two_fold_cross_validate_mlp_time(model_name, fea_train, label_train,
                                     fea_test, label_test, conf_model,
                                     log_dir, model_tag)

    model_name = "mlp_time_3lay_l2"
    model_tag = model_name + "_dur_p5_128_lr_p001_L2_p1"
    conf_model.update({'dense_size': 128, 'l2_regu': 0.1, 'learning_rate': 0.001})
    two_fold_cross_validate_mlp_time(model_name, fea_train, label_train,
                                     fea_test, label_test, conf_model,
                                     log_dir, model_tag)

    model_name = "mlp_time_4lay_l2"
    model_tag = model_name + "_dur_p5_128_lr_p001_L2_p1"
    conf_model.update({'dense_size': 128, 'l2_regu': 0.1, 'learning_rate': 0.001})
    two_fold_cross_validate_mlp_time(model_name, fea_train, label_train,
                                     fea_test, label_test, conf_model,
                                     log_dir, model_tag)

    model_name = "mlp_time_5lay_l2"
    model_tag = model_name + "_dur_p5_128_lr_p001_L2_p1"
    conf_model.update({'dense_size': 128, 'l2_regu': 0.1, 'learning_rate': 0.001})
    two_fold_cross_validate_mlp_time(model_name, fea_train, label_train,
                                     fea_test, label_test, conf_model,
                                     log_dir, model_tag)
    model_name = "mlp_time_l2"
    model_tag = model_name + "_dur_p5_64_lr_p001_L2_p1"
    conf_model.update({'dense_size': 64, 'l2_regu': 0.1, 'learning_rate': 0.001})
    two_fold_cross_validate_mlp_time(model_name, fea_train, label_train,
                                     fea_test, label_test, conf_model,
                                     log_dir, model_tag)


# model_name = "lstm_2lay"
# model_tag = model_name + "_64_16_lr_p001_L2_p1"
# conf_model.update({'lstm1': 64, 'lstm2': 16, 'l2_regu': 0.1, 'learning_rate': 0.001})
# two_fold_cross_validate_mlp_time(model_name, fea_train, label_train,
#                                  fea_test, label_test, conf_model,
#                                  log_dir, model_tag)
#
# model_name = "lstm_bidir_2lay"
# model_tag = model_name + "_64_16_lr_p001_L2_p1"
# conf_model.update({'lstm1': 64, 'lstm2': 16, 'l2_regu': 0.1, 'learning_rate': 0.001})
# two_fold_cross_validate_mlp_time(model_name, fea_train, label_train,
#                                  fea_test, label_test, conf_model,
#                                  log_dir, model_tag)


# fight against overfitting
model_name = "lstm_2lay"
model_tag = model_name + "_8_4_lr_p001_L2_p1"
conf_model.update({'lstm1': 8, 'lstm2': 4, 'l2_regu': 0.1, 'learning_rate': 0.001})
two_fold_cross_validate_mlp_time(model_name, fea_train, label_train,
                                 fea_test, label_test, conf_model,
                                 log_dir, model_tag)

model_name = "lstm_bidir_2lay"
model_tag = model_name + "_8_4_lr_p001_L2_p1"
conf_model.update({'lstm1': 8, 'lstm2': 4, 'l2_regu': 0.1, 'learning_rate': 0.001})
two_fold_cross_validate_mlp_time(model_name, fea_train, label_train,
                                 fea_test, label_test, conf_model,
                                 log_dir, model_tag)

model_name = "lstm_2lay"
model_tag = model_name + "_8_8_lr_p001_L2_p1"
conf_model.update({'lstm1': 8, 'lstm2': 8, 'l2_regu': 0.1, 'learning_rate': 0.001})
two_fold_cross_validate_mlp_time(model_name, fea_train, label_train,
                                 fea_test, label_test, conf_model,
                                 log_dir, model_tag)

model_name = "lstm_bidir_2lay"
model_tag = model_name + "_8_8_lr_p001_L2_p1"
conf_model.update({'lstm1': 8, 'lstm2': 8, 'l2_regu': 0.1, 'learning_rate': 0.001})
two_fold_cross_validate_mlp_time(model_name, fea_train, label_train,
                                 fea_test, label_test, conf_model,
                                 log_dir, model_tag)

model_name = "lstm_2lay"
model_tag = model_name + "_16_4_lr_p001_L2_p1"
conf_model.update({'lstm1': 16, 'lstm2': 4, 'l2_regu': 0.1, 'learning_rate': 0.001})
two_fold_cross_validate_mlp_time(model_name, fea_train, label_train,
                                 fea_test, label_test, conf_model,
                                 log_dir, model_tag)

model_name = "lstm_bidir_2lay"
model_tag = model_name + "_16_4_lr_p001_L2_p1"
conf_model.update({'lstm1': 16, 'lstm2': 4, 'l2_regu': 0.1, 'learning_rate': 0.001})
two_fold_cross_validate_mlp_time(model_name, fea_train, label_train,
                                 fea_test, label_test, conf_model,
                                 log_dir, model_tag)

model_name = "lstm_2lay"
model_tag = model_name + "_16_8_lr_p001_L2_p1"
conf_model.update({'lstm1': 16, 'lstm2': 8, 'l2_regu': 0.1, 'learning_rate': 0.001})
two_fold_cross_validate_mlp_time(model_name, fea_train, label_train,
                                 fea_test, label_test, conf_model,
                                 log_dir, model_tag)

model_name = "lstm_bidir_2lay"
model_tag = model_name + "_16_8_lr_p001_L2_p1"
conf_model.update({'lstm1': 16, 'lstm2': 8, 'l2_regu': 0.1, 'learning_rate': 0.001})
two_fold_cross_validate_mlp_time(model_name, fea_train, label_train,
                                 fea_test, label_test, conf_model,
                                 log_dir, model_tag)

model_name = "lstm_2lay"
model_tag = model_name + "_16_16_lr_p001_L2_p1"
conf_model.update({'lstm1': 16, 'lstm2': 16, 'l2_regu': 0.1, 'learning_rate': 0.001})
two_fold_cross_validate_mlp_time(model_name, fea_train, label_train,
                                 fea_test, label_test, conf_model,
                                 log_dir, model_tag)

model_name = "lstm_bidir_2lay"
model_tag = model_name + "_16_16_lr_p001_L2_p1"
conf_model.update({'lstm1': 16, 'lstm2': 16, 'l2_regu': 0.1, 'learning_rate': 0.001})
two_fold_cross_validate_mlp_time(model_name, fea_train, label_train,
                                 fea_test, label_test, conf_model,
                                 log_dir, model_tag)

model_name = "lstm_2lay"
model_tag = model_name + "_32_8_lr_p001_L2_p1"
conf_model.update({'lstm1': 32, 'lstm2': 8, 'l2_regu': 0.1, 'learning_rate': 0.001})
two_fold_cross_validate_mlp_time(model_name, fea_train, label_train,
                                 fea_test, label_test, conf_model,
                                 log_dir, model_tag)

model_name = "lstm_bidir_2lay"
model_tag = model_name + "_32_8_lr_p001_L2_p1"
conf_model.update({'lstm1': 32, 'lstm2': 8, 'l2_regu': 0.1, 'learning_rate': 0.001})
two_fold_cross_validate_mlp_time(model_name, fea_train, label_train,
                                 fea_test, label_test, conf_model,
                                 log_dir, model_tag)

model_name = "lstm_2lay"
model_tag = model_name + "_32_16_lr_p001_L2_p1"
conf_model.update({'lstm1': 32, 'lstm2': 8, 'l2_regu': 0.1, 'learning_rate': 0.001})
two_fold_cross_validate_mlp_time(model_name, fea_train, label_train,
                                 fea_test, label_test, conf_model,
                                 log_dir, model_tag)

model_name = "lstm_bidir_2lay"
model_tag = model_name + "_32_16_lr_p001_L2_p1"
conf_model.update({'lstm1': 32, 'lstm2': 8, 'l2_regu': 0.1, 'learning_rate': 0.001})
two_fold_cross_validate_mlp_time(model_name, fea_train, label_train,
                                 fea_test, label_test, conf_model,
                                 log_dir, model_tag)

model_name = "lstm_2lay"
model_tag = model_name + "_32_32_lr_p001_L2_p1"
conf_model.update({'lstm1': 32, 'lstm2': 8, 'l2_regu': 0.1, 'learning_rate': 0.001})
two_fold_cross_validate_mlp_time(model_name, fea_train, label_train,
                                 fea_test, label_test, conf_model,
                                 log_dir, model_tag)

model_name = "lstm_bidir_2lay"
model_tag = model_name + "_32_32_lr_p001_L2_p1"
conf_model.update({'lstm1': 32, 'lstm2': 8, 'l2_regu': 0.1, 'learning_rate': 0.001})
two_fold_cross_validate_mlp_time(model_name, fea_train, label_train,
                                 fea_test, label_test, conf_model,
                                 log_dir, model_tag)

# change optimizer from Adam to AdamGrad, SGD+momentum

