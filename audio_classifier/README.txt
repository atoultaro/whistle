

4022

num_dolphins = np.array([843., 1923., 684., 572.])

to-do list:
1. write out the source code of model functions
2. For 2-fold cross-validation, find the best model and make prediction
3. Use another metrics that presents each class equally. Currently the accuracy is weighted by the number of data points
for each species
4. What's the proper procedure to find the best model?
5. upload to bitbucket
