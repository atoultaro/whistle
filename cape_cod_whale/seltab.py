"""
selection table module:
Functions to process selection tables

Author: Yu Shiu
Date: May 24, 2019
"""
import pandas as pd
import os
import numpy as np


def bin_to_seltab(contour_all, seltab_out_path):
    num_whistle_tot = 0
    # for ff in contour_all.keys():
    contour_keys = list(contour_all.keys())
    # for ff in (sorted(contour_keys))[0:5]:
    for ff in sorted(contour_keys):
        print('\n' + ff)
        this_contour = contour_all[ff]
        # sound_path = os.path.join(sound_dir0, this_contour[0], ff + '.wav')
        print("Num of whistles: " + str(len(this_contour[1])))
        num_whistle_tot += len(this_contour[1])

        begin_time = []
        end_time = []
        low_freq = []
        duration = []
        high_freq = []
        filename = []

        for cc in range(len(this_contour[1])):
            contour_time = np.array(this_contour[1][cc]['Time'])
            contour_freq = np.array(this_contour[1][cc]['Freq'])

            time_min = contour_time.min()
            begin_time.append(time_min)
            time_max = contour_time.max()
            end_time.append(time_max)
            duration.append(time_max - time_min)
            low_freq.append(contour_freq.min())
            high_freq.append(contour_freq.max())
            filename.append(ff)

        data_seltab = {"Selection": range(1, len(begin_time) + 1),
                       "View": "Spectrogram 1", "Channel": str(1),
                       "Begin Time (s)": begin_time, "End Time (s)": end_time,
                       "Low Freq (Hz)": low_freq, "High Freq (Hz)": high_freq,
                       "Duration (s)": duration}

        df_curr = pd.DataFrame(data_seltab)
        df_curr.to_csv(
            os.path.join(seltab_out_path, this_contour[0] + '_' + ff + '.txt'),
            sep='\t', mode='a', index=False,
            columns=["Selection", "View", "Channel", "Begin Time (s)",
                     "End Time (s)", "Low Freq (Hz)", "High Freq (Hz)",
                     "Duration (s)"])

    print("Total number of whistles: " + str(num_whistle_tot))

    return None


