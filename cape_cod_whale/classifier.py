#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
train & validation

Created on 6/3/19
@author: atoultaro
"""
import os
import numpy as np
from contextlib import redirect_stdout
import gc
import glob
import re
import random

from keras.optimizers import Adam, Adadelta, Adagrad
from keras import backend
from keras.models import load_model
from keras.callbacks import ModelCheckpoint, Callback, TensorBoard, \
    EarlyStopping
from keras.utils import to_categorical
# from extra_keras_metrics import mean_per_class_accuracy
import tensorflow as tf
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix, balanced_accuracy_score, \
    classification_report, f1_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA

from classifier.batchgenerator import PaddedBatchGenerator
import cape_cod_whale.model as cm
# from cape_cod_whale.load_feature_model import load_fea_model


def macro_f1(y_true, y_pred):
    return f1_score(y_true, y_pred, average='macro')


def most_freq(in_list):
    return max(set(in_list), key=in_list.count)


def two_fold_cross_validate_time_uneven(model_func_name, model_tag, df_train,
                                        df_test, conf, log_dir):
    """
    Uneven training & testing
    Trained on fixed window features
    Tested on features from a single contour
    Move the feature preparation from outside to inside this function
    df_target -->> fea_fold1, label_fold1, fea_fold2, label_fold2
    :param model_func_name:
    :param fea_fold1:
    :param label_fold1:
    :param fea_test:
    :param label_test:
    :param conf:
    :param log_dir:
    :param model_tag:
    :return:
    """
    log_dir = os.path.join(log_dir, model_tag)
    os.mkdir(log_dir)

    # 1st fold
    model_func = getattr(cm, model_func_name)
    # shape: (time_stamp, fea_dim)
    if conf['type'] is 'conv2d':
        model = model_func([conf['win_back'], conf['fea_dim'], 1], conf)
    elif conf['type'] is 'conv2d_lstm':
        model = model_func([conf['win_back'], conf['fea_dim'], 1, 1], conf)
    else:
        model = model_func([conf['win_back'], conf['fea_dim']], conf)
    log_dir1 = os.path.join(log_dir, "fold1")
    os.mkdir(log_dir1)
    y_pred0, y_test0, y_pred0_frame, y_test0_frame = train_validation_win_uneven(model, df_train,
                                                   df_test, conf, log_dir1,
                                                   model_tag)
    del model
    gc.collect()
    backend.clear_session()

    # 2nd fold: switch train and test data
    # count_species2 = label_fold2.sum(axis=0).tolist()
    # conf["class_weight"] = (max(count_species2) / np.array(count_species2)).tolist()
    # model = model_func([None, conf["fea_dim"]], conf)  # frame-based GMM & MLP
    # model = model_func([conf["fea_dim"]], conf)
    # model = model_func([conf['win_back'], conf['fea_dim']], conf)
    if conf['type'] is 'conv2d':
        model = model_func([conf['win_back'], conf['fea_dim'], 1], conf)
    elif conf['type'] is 'conv2d_lstm':
        model = model_func([conf['win_back'], conf['fea_dim'], 1, 1], conf)
    else:
        model = model_func([conf['win_back'], conf['fea_dim']], conf)

    log_dir2 = os.path.join(log_dir, "fold2")
    os.mkdir(log_dir2)
    y_pred1, y_test1, y_pred1_frame, y_test1_frame = train_validation_win_uneven(model, df_test,
                                                   df_train, conf, log_dir2,
                                                   model_tag)
    # del model
    gc.collect()
    backend.clear_session()

    y_pred = np.concatenate((y_pred0, y_pred1))
    y_test = np.concatenate((y_test0, y_test1))
    y_pred_frame = np.concatenate((y_pred0_frame, y_pred1_frame))
    y_test_frame = np.concatenate((y_test0_frame, y_test1_frame))

    # metrics
    metrics_two_fold(y_test_frame, y_pred_frame, log_dir, 'info_frame.txt', model, conf,
                     mode="total")
    metrics_two_fold(y_test, y_pred, log_dir, 'info.txt', model, conf,
                     mode="total")

    return y_pred, y_test


def two_fold_cross_validate_time(model_func_name, fea_fold1, label_fold1,
                                fea_fold2, label_fold2, conf, log_dir,
                                model_tag):
    """
    For fixed window features
    :param model_func_name:
    :param fea_fold1:
    :param label_fold1:
    :param fea_test:
    :param label_test:
    :param conf:
    :param log_dir:
    :param model_tag:
    :return:
    """
    log_dir = os.path.join(log_dir, model_tag)
    os.mkdir(log_dir)
    label_fold1 = to_categorical(label_fold1, num_classes=conf['num_class'])
    label_fold2 = to_categorical(label_fold2, num_classes=conf['num_class'])

    # 1st fold
    count_species1 = label_fold1.sum(axis=0).tolist()
    conf["class_weight"] = (max(count_species1) / np.array(count_species1)).tolist()

    model_func = getattr(cm, model_func_name)
    # model_mlp = model_func([conf["fea_dim"]], conf)
    model_mlp = model_func([conf['win_back'], conf['fea_dim']], conf)
    log_dir1 = os.path.join(log_dir, "fold1")
    os.mkdir(log_dir1)
    y_pred0, y_test0 = train_validation_win(model_mlp, fea_fold1, label_fold1,
                                            fea_fold2, label_fold2, conf,
                                            log_dir1, model_tag)
    del model_mlp
    gc.collect()
    backend.clear_session()

    # 2nd fold: switch train and test data
    count_species2 = label_fold2.sum(axis=0).tolist()
    conf["class_weight"] = (max(count_species2) / np.array(count_species2)).tolist()
    # model_mlp = model_func([None, conf["fea_dim"]], conf)
    # model_mlp = model_func([conf["fea_dim"]], conf)
    model_mlp = model_func([conf['win_back'], conf['fea_dim']], conf)
    log_dir2 = os.path.join(log_dir, "fold2")
    os.mkdir(log_dir2)
    y_pred1, y_test1 = train_validation_win(model_mlp, fea_fold2, label_fold2,
                                        fea_fold1, label_fold1, conf, log_dir2,
                                        model_tag)
    # del model_mlp
    gc.collect()
    backend.clear_session()

    y_pred = np.concatenate((y_pred0, y_pred1))
    y_test = np.concatenate((y_test0, y_test1))

    # metrics
    metrics_two_fold(y_test, y_pred, log_dir, 'info.txt', model_mlp, conf, mode="total")

    return y_pred, y_test


def two_fold_cross_validate_mlp(model_func_name, fea_fold1, label_fold1,
                                fea_fold2, label_fold2, conf, log_dir,
                                model_tag):
    '''
    for multi-layer perceptron only
    No generator needed
    :param model_func_name:
    :param fea_fold1:
    :param label_fold1:
    :param fea_test:
    :param label_test:
    :param conf:
    :param log_dir:
    :param model_tag:
    :return:
    '''
    log_dir = os.path.join(log_dir, model_tag)
    os.mkdir(log_dir)
    # label_fold1 = to_categorical(label_fold1)
    # label_fold2 = to_categorical(label_fold2)
    label_fold1 = to_categorical(label_fold1, num_classes=conf['num_class'])
    label_fold2 = to_categorical(label_fold2, num_classes=conf['num_class'])


    # 1st fold
    count_species1 = label_fold1.sum(axis=0).tolist()
    conf["class_weight"] = (max(count_species1) / np.array(count_species1)).tolist()

    model_func = getattr(cm, model_func_name)
    model_mlp = model_func([conf["fea_dim"]], conf)
    log_dir1 = os.path.join(log_dir, "fold1")
    os.mkdir(log_dir1)
    y_pred0, y_test0 = train_validation_win(model_mlp, fea_fold1, label_fold1,
                                            fea_fold2, label_fold2, conf,
                                            log_dir1, model_tag)
    del model_mlp
    gc.collect()
    backend.clear_session()

    # 2nd fold: switch train and test data
    count_species2 = label_fold2.sum(axis=0).tolist()
    conf["class_weight"] = (max(count_species2) / np.array(count_species2)).tolist()
    # model_mlp = model_func([None, conf["fea_dim"]], conf)
    model_mlp = model_func([conf["fea_dim"]], conf)
    log_dir2 = os.path.join(log_dir, "fold2")
    os.mkdir(log_dir2)
    y_pred1, y_test1 = train_validation_win(model_mlp, fea_fold2, label_fold2,
                                        fea_fold1, label_fold1, conf, log_dir2,
                                        model_tag)
    # del model_mlp
    gc.collect()
    backend.clear_session()

    y_pred = np.concatenate((y_pred0, y_pred1))
    y_test = np.concatenate((y_test0, y_test1))

    # metrics
    metrics_two_fold(y_test, y_pred, log_dir, 'info.txt', model_mlp, conf, mode="total")

    return y_pred, y_test


def two_fold_cross_validate(model_func_name, fea_train, label_train, fea_test,
                            label_test, conf, log_dir, model_tag):
    log_dir = os.path.join(log_dir, model_tag)
    if not os.path.exists(log_dir):
        os.mkdir(log_dir)
    label_train = to_categorical(label_train)
    label_test = to_categorical(label_test)

    # 1st fold
    count_species1 = label_train.sum(axis=0).tolist()
    # conf["class_weight"] = (max(count_species1) / np.array(count_species1)).tolist()
    conf["class_weight"] = ((max(count_species1) / np.array(count_species1))**2.0).tolist()
    # conf["class_weight"] = ((max(count_species1) / np.array(count_species1))*1.5).tolist()
    # conf["class_weight"] = np.ceil(max(count_species1) / np.array(count_species1)).astype(int).tolist()
    traingen = PaddedBatchGenerator(fea_train, label_train,
                                    batch_size=conf['batch_size'])
    testgen = PaddedBatchGenerator(fea_test, label_test,
                                   batch_size=label_test.shape[0]
                                   )
    model_func = getattr(cm, model_func_name)
    model_lstm = model_func([None, conf["fea_dim"]], conf)
    # model_lstm = model_func([conf["fea_dim"]], conf)
    log_dir1 = os.path.join(log_dir, "fold1")
    if not os.path.exists(log_dir1):
        os.mkdir(log_dir1)
    y_pred0, y_test0 = train_validation(model_lstm, traingen, testgen, conf,
                                            log_dir1, model_tag)

    del model_lstm
    gc.collect()
    backend.clear_session()

    # 2nd fold: switch train and test data
    count_species2 = label_test.sum(axis=0).tolist()
    # conf["class_weight"] = (max(count_species2) / np.array(count_species2)).tolist()
    conf["class_weight"] = ((max(count_species2) / np.array(count_species2))**2.0).tolist()
    # conf["class_weight"] = np.ceil(max(count_species2) / np.array(count_species2)).astype(int).tolist()
    # conf["class_weight"] = ((max(count_species2) / np.array(
    #     count_species2)) * 1.5).tolist()
    traingen2 = PaddedBatchGenerator(fea_test, label_test,
                                    batch_size=conf['batch_size'])
    testgen2 = PaddedBatchGenerator(fea_train, label_train,
                                   batch_size=label_train.shape[0]
                                   )

    model_lstm = model_func([None, conf["fea_dim"]], conf)
    log_dir2 = os.path.join(log_dir, "fold2")
    if not os.path.exists(log_dir2):
        os.mkdir(log_dir2)
    y_pred1, y_test1 = train_validation(model_lstm, traingen2, testgen2, conf,
                                        log_dir2, model_tag)

    # del model_lstm
    gc.collect()
    backend.clear_session()

    y_pred = np.concatenate((y_pred0, y_pred1))
    y_test = np.concatenate((y_test0, y_test1))

    # metrics
    metrics_two_fold(y_test, y_pred, log_dir, 'info.txt', model_lstm, conf, mode="total")

    with open(os.path.join(log_dir, 'configuration.txt'),
              'w') as f:
        with redirect_stdout(f):
            for key in list(conf.keys()):
                print(key + '->' + str(conf[key]))

    return y_pred, y_test


def two_fold_cross_validate_random_forest(feature_1, feature_2,
                                          label_1, label_2, log_dir,
                                          conf):
    if not os.path.exists(log_dir):
        os.mkdir(log_dir)

    # 1st fold
    clf = make_pipeline(StandardScaler(), PCA(n_components=
                                              conf['pca_components']),
                        RandomForestClassifier(n_estimators=conf['n_est'],
                                               n_jobs=conf['n_jobs'],
                                               class_weight='balanced_subsample',
                                               verbose=1, random_state=10))
    clf.fit(feature_1, to_categorical(label_1))
    y_pred1 = clf.predict(feature_2)
    y_pred1 = np.argmax(y_pred1, axis=1)
    metrics_two_fold(label_2, y_pred1, log_dir, 'fold1.txt', None, conf)

    # 2nd fold: switch train and test data
    clf = make_pipeline(StandardScaler(), PCA(n_components=
                                              conf['pca_components']),
                        RandomForestClassifier(n_estimators=conf['n_est'],
                                               n_jobs=conf['n_jobs'],
                                               class_weight='balanced_subsample',
                                               verbose=1, random_state=10))
    clf.fit(feature_2, to_categorical(label_2))
    y_pred2 = clf.predict(feature_1)
    y_pred2 = np.argmax(y_pred2, axis=1)
    metrics_two_fold(label_1, y_pred2, log_dir, 'fold2.txt', None, conf)

    y_pred = np.concatenate((y_pred2, y_pred1))
    y_test = np.concatenate((label_1, label_2))

    # confusion matrixt
    metrics_two_fold(y_test, y_pred, log_dir, 'accuracy.txt', None, conf)
    #
    # confu_mat = confusion_matrix(y_test, y_pred)
    # # balanced accuracy
    # balanced_accuracy = balanced_accuracy_score(y_test, y_pred)
    # # classification report
    # class_report = classification_report(y_test, y_pred)
    # # f1-score
    # f1_class_avg = f1_score(y_test, y_pred, average='macro')
    #
    # with open(os.path.join(log_dir, 'accuracy.txt'), 'w') as f:
    #     with redirect_stdout(f):
    #         print("\nConfusion matrix:\n")
    #         print(confu_mat)
    #         print("\nBalanced accuracy:")
    #         print(balanced_accuracy)
    #         print("\nClassification_report:")
    #         print(class_report)
    #         print("\nf1 score:")
    #         print(f1_class_avg)

    return y_pred, y_test


def train_validation_win_uneven(model, df_train, df_test, conf, log_dir,
                                model_tag):
    """
    fea_train, label_train, fea_test, label_test
    validation and test are the same data set
    :param model:
    :param train_generator:
    :param test_generator:
    :param conf_model:
    :param log_dir:
    :param model_tag:
    :return:
    """
    # model
    model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=conf['learning_rate']),
                       metrics=['acc'])
    # model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=conf['learning_rate']),
    #                    metrics=['acc', macro_f1])

    with open(os.path.join(log_dir, 'net_architecture.txt'), 'w') as f:
        with redirect_stdout(f):
            model.summary()

    # data
    fea_list_train, label_train, dur_train_max = convert_df_2_win_list(
        df_train, conf['win_back'])
    # fea_train = np.stack(fea_list_train, axis=0)

    # shuffle
    if conf['shuffle'] is True:
        train_idx = [ii for ii in range(len(label_train))]
        random.shuffle(train_idx)
        fea_list_train = [fea_list_train[tt] for tt in train_idx]
        label_train = [label_train[tt] for tt in train_idx]

    fea_train = np.stack(fea_list_train, axis=0)
    if conf['type'] is 'conv2d':
        fea_train = np.expand_dims(fea_train, axis=3)
    elif conf['type'] is 'conv2d_lstm':
        fea_train = np.expand_dims(fea_train, axis=3)
        fea_train = np.expand_dims(fea_train, axis=4)

    label_fold1 = to_categorical(label_train, num_classes=conf['num_class'])

    # fea_list_test, label_test, dur_test_max = convert_df_2_win_contour_list(
    #     df_test, conf['win_back'])
    fea_list_test, label_test, dur_test_max = convert_df_2_win_contour_list_half_diff(
        df_test, conf['win_back'], type=1)

    count_species1 = label_fold1.sum(axis=0).tolist()
    conf["class_weight"] = (max(count_species1) / np.array(count_species1)).tolist()

    # model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=conf['learning_rate']),
    #                    metrics=['acc'])
    callbacks_list = callback_generate(log_dir)
    # with open(os.path.join(log_dir, 'net_architecture.txt'), 'w') as f:
    #     with redirect_stdout(f):
    #         model.summary()

    # Use part of train data to be validation_data
    model.fit(x=fea_train, y=label_fold1,
              validation_split=0.2,
              epoch=conf['epoch'], verbose=1,
              callbacks=callbacks_list,
              class_weight=conf["class_weight"])

    # load the best model of this run & fold
    # fmt = "epoch_\d+_[0-1].\d+_(\d+.\d{4}).hdf5"  # val loss
    fmt = "epoch_\d+_([0-1].\d+)_\d+.\d{4}.hdf5"  # val_acc
    # best_model_path, best_accu = find_best_model(log_dir, fmt, is_max=False)
    best_model_path, best_accu = find_best_model(log_dir, fmt, is_max=True)
    model = load_model(best_model_path)
    conf["best_model"] = best_model_path

    # frame-based performance
    # fea_list_frame_test, label_frame_test, dur_test_max = \
    #     convert_df_2_win_list(df_test, conf['win_back'])
    fea_list_frame_test, label_frame_test, dur_test_max = \
        convert_df_2_win_list_half_diff(df_test, conf['win_back'])

    fea_frame_test = np.stack(fea_list_frame_test, axis=0)
    if conf['type'] is 'conv2d':
        fea_frame_test = np.expand_dims(fea_frame_test, axis=3)
    elif conf['type'] is 'conv2d_lstm':
        fea_frame_test = np.expand_dims(fea_frame_test, axis=3)
        fea_frame_test = np.expand_dims(fea_frame_test, axis=4)

    label_frame_test = to_categorical(label_frame_test, num_classes=conf['num_class'])

    class_prob_frame = model.predict(fea_frame_test)  # predict_proba the same as predict
    y_pred_frame = np.argmax(class_prob_frame, axis=1)
    y_test_frame = np.argmax(label_frame_test, axis=1).astype(int)

    metrics_two_fold(y_test_frame, y_pred_frame, log_dir, 'info_frame.txt', model, conf, mode="fold")

    # Predict the class of a given contour from its feature sequence
    print('Making predictions...')
    y_pred = []
    for fea in range(len(fea_list_test)):
        fea_test = np.stack(fea_list_test[fea], axis=0)
        if conf['type'] is 'conv2d':
            fea_test = np.expand_dims(fea_test, axis=3)
        elif conf['type'] is 'conv2d_lstm':
            fea_test = np.expand_dims(fea_test, axis=3)
            fea_test = np.expand_dims(fea_test, axis=4)

        class_prob = model.predict(fea_test)

        # majority voting
        # y_pred_contour = (np.argmax(class_prob, axis=1).astype(int)).tolist()
        # y_pred_curr = most_freq(y_pred_contour)

        # # committees of prediction
        class_prob_sum = class_prob.sum(axis=0)
        y_pred_curr = np.argmax(class_prob_sum).astype(int)

        y_pred.append(y_pred_curr)
    y_test = label_test
    metrics_two_fold(y_test, y_pred, log_dir, 'info.txt', model, conf, mode="fold")

    return y_pred, y_test, y_pred_frame, y_test_frame


def train_validation_win(model, fea_train, label_train, fea_test, label_test,
                         conf_model, log_dir, model_tag):
    """
    validation and test are the same data set
    :param model:
    :param train_generator:
    :param test_generator:
    :param conf_model:
    :param log_dir:
    :param model_tag:
    :return:
    """
    model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=conf_model['learning_rate']),
                       metrics=['acc'])
    callbacks_list = callback_generate(log_dir)

    with open(os.path.join(log_dir, 'net_architecture.txt'), 'w') as f:
        with redirect_stdout(f):
            model.summary()
    model.fit(x=fea_train, y=label_train,
              validation_data=(fea_test, label_test),
              epoch=conf_model['epoch'], verbose=1,
              callbacks=callbacks_list,
              class_weight=conf_model["class_weight"])

    # load the best model of this run & fold
    best_model_path, best_accu = find_best_model(log_dir)
    model = load_model(best_model_path)

    # accuracy
    loss, acc = model.evaluate(fea_test, label_test,
                               batch_size=conf_model['batch_size'])
    print('acc: %.2f' % acc)
    # model.load_weights(os.path.join(log_dir, 'epoch_09_0.6823.hdf5'))

    class_prob = model.predict(fea_test)  # predict_proba the same as predict
    y_pred = np.argmax(class_prob, axis=1)
    y_test = np.argmax(label_test, axis=1).astype(int)

    metrics_two_fold(y_test, y_pred, log_dir, 'info.txt', model, conf_model, mode="fold")

    return y_pred, y_test


def train_validation(model, train_generator, test_generator, conf_model,  log_dir, model_tag):

    # model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=conf_model['learning_rate']),
    #                    metrics=['acc'])
    model.compile(loss='categorical_crossentropy',
                  optimizer=Adagrad(lr=conf_model['learning_rate']),
                  metrics=['acc'])
    callbacks_list = callback_generate(log_dir)

    with open(os.path.join(log_dir, 'net_architecture.txt'), 'w') as f:
        with redirect_stdout(f):
            model.summary()

    (x_test, y_test_onehot) = next(test_generator)
    steps = train_generator.get_batches_per_epoch()
    model.fit_generator(train_generator, validation_data=(x_test, y_test_onehot),
                             steps_per_epoch=steps,
                             epochs=conf_model['epoch'], verbose=1,
                             callbacks=callbacks_list,
                             class_weight=conf_model["class_weight"])

    # load the best model of this run & fold
    # fmt = "epoch_\d+_([0-1].\d+)_\d+.\d{4}.hdf5"  # val_acc
    fmt = "epoch_\d+_[0-1].\d+_(\d+.\d{4}).hdf5"  # val loss
    best_model_path, best_accu = find_best_model(log_dir, fmt, is_max=False)
    # best_model_path, best_accu = find_best_model(log_dir, fmt, is_max=True)
    conf_model["best_model"] = best_model_path

    model = load_model(best_model_path)

    # accuracy
    loss, acc = model.evaluate(x_test, y_test_onehot,
                                    batch_size=conf_model['batch_size'])
    print('acc: %.2f' % acc)
    # model.load_weights(os.path.join(log_dir, 'epoch_09_0.6823.hdf5'))

    class_prob = model.predict(x_test)  # predict_proba the same as predict
    y_pred = np.argmax(class_prob, axis=1)
    y_test = np.argmax(y_test_onehot, axis=1).astype(int)

    metrics_two_fold(y_test, y_pred, log_dir, 'info.txt', model, conf_model, mode="fold")

    return y_pred, y_test


class AccuracyHistory(Callback):
    """
    Callback function ro report classifier accuracy on-th-fly

    Args:
        keras.callbacks.Callback: keras callback
    """
    def __init__(self):
        super().__init__()
        self.acc = []
        self.val_acc = []

    def on_train_begin(self, logs={}):
        self.acc = []
        self.val_acc = []

        return

    def on_train_end(self, logs={}):
        return

    def on_epoch_end(self, epoch, logs={}):
        self.acc.append(logs.get('acc'))
        self.val_acc.append(logs.get('val_acc'))
        # self.bal_acc.append(logs.get('mean_per_class_accuracy'))
        # self.val_bal_acc.append(logs.get('val_mean_per_class_accuracy'))

        return


def callback_generate(log_dir):
    model_name_format = 'epoch_{epoch:02d}_{val_acc:.4f}_{val_loss:.4f}.hdf5'
    check_path = os.path.join(log_dir, model_name_format)
    # checkpoint = ModelCheckpoint(check_path, monitor='val_acc', verbose=1,
    #                              save_best_only=True, mode='max')
    checkpoint = ModelCheckpoint(check_path, monitor='val_loss', verbose=1,
                                 save_best_only=True, mode='min')
    history = AccuracyHistory()
    call_early_stop = EarlyStopping(monitor='val_loss', mode='min', patience=20)
    callbacks_list = [checkpoint, history, call_early_stop, TensorBoard(log_dir=log_dir)]

    return callbacks_list


def callback_generate_multi_class(log_dir):
    model_name_format = 'epoch_{epoch:02d}_{val_bal_acc:.4f}.hdf5'
    check_path = os.path.join(log_dir, model_name_format)
    checkpoint = ModelCheckpoint(check_path, monitor='val_bal_acc', verbose=1,
                                 save_best_only=True, mode='max')
    history = AccuracyHistory()
    callbacks_list = [checkpoint, history, TensorBoard(log_dir=log_dir)]

    return callbacks_list


def find_best_model(classifier_path, fmt, is_max=False, purge=True):
    """
    Return the path to the model with the best accuracy, given the path to
    all the trained classifiers
    Args:
        classifier_path: path to all the trained classifiers
        fmt: e.g. "epoch_\d+_[0-1].\d+_(\d+.\d{4}).hdf5"
        'epoch_\d+_valloss_(\d+.\d{4})_valacc_\d+.\d{4}.hdf5'
        is_max: use max; otherwise, min
        purge: True to purge models files except the best one
    Return:
        the path of the model with the best accuracy
    """
    # list all files ending with .hdf5
    day_list = sorted(glob.glob(os.path.join(classifier_path + '/', '*.hdf5')))

    # re the last 4 digits for accuracy
    hdf5_filename = []
    hdf5_accu = np.zeros(len(day_list))
    for dd in range(len(day_list)):
        filename = os.path.basename(day_list[dd])
        hdf5_filename.append(filename)
        # m = re.search("_F1_(0.\d{4}).hdf5", filename)
        # m = re.search("_([0-1].\d{4}).hdf5", filename)
        # m = re.search("epoch_\d+_[0-1].\d+_(\d+.\d{4}).hdf5", filename)
        m = re.search(fmt, filename)
        try:
            hdf5_accu[dd] = float(m.groups()[0])
        except:
            continue

    # select the laregest one and write to the variable classifier_file
    if len(hdf5_accu) == 0:
        best_model_path = ''
        best_accu = 0
    else:
        if is_max is True:
            ind_max = np.argmax(hdf5_accu)
        else: # use min instead
            ind_max = np.argmin(hdf5_accu)
        best_model_path = day_list[int(ind_max)]
        best_accu = hdf5_accu[ind_max]
        # purge all model files except the best_model
        if purge:
            for ff in day_list:
                if ff != best_model_path:
                    os.remove(ff)
    print('Best model:'+str(best_accu))
    print(best_model_path)
    return best_model_path, best_accu


def metrics_two_fold(y_test, y_pred, log_dir, filename, conf, mode=None):
    """
    :param y_test:
    :param y_pred:
    :param log_dir:
    :param filename:
    :param model:
    :param conf:
    :param mode: "total" or "fold"
    :return:
    """
    # confusion matrix
    confu_mat = confusion_matrix(y_test, y_pred)
    # balanced accuracy
    balanced_accuracy = balanced_accuracy_score(y_test, y_pred)
    # classification report
    class_report = classification_report(y_test, y_pred, digits=3)
    # f1-score
    f1_class_avg = f1_score(y_test, y_pred, average='macro')

    with open(os.path.join(log_dir, filename), 'w') as f:
        with redirect_stdout(f):
            print(conf['species_name'])

            print("\nConfusion matrix:\n")
            print(confu_mat)
            print("\nBalanced accuracy:")
            print(balanced_accuracy)
            print("\nClassification_report:")
            print(class_report)
            print("\nf1 score:")
            print(f1_class_avg)

            if mode is "total":
                print("\nHyper-parameters:")
                # model.summary()
                print("\nBatch size: " + str(conf['batch_size']))
                print("Epoch: " + str(conf['epoch']))
                print("\nLearning rate: " + str(conf['learning_rate']))
                # print("L2_regularization: " + str(conf['l2_regu']))
                # print("Dropout: " + str(conf['dropout']))
                # print("Recurrent dropout: " + str(conf['recurrent_dropout']))
                # print("Duration threshold: " + str(conf['duration_thre']))
                # print("Time resolution: " + str(conf['time_reso']))
                # print("Bidirectional mode: " + str(conf['bi_mod']))
                # print('Window look back: '+str(conf['win_back']))
                # print("Train data shuffle: " + str(conf['shuffle']))
            elif mode is "fold":
                print("Class weight: " + str(conf['class_weight']))
                print("Best model: " + str(conf['best_model']))


def bal_acc(y_true, y_pred): # failed!
    # y_pred_arr = np.argmax(y_pred, axis=0)
    return tf.py_func(accuracy_score, (y_true, y_pred), tf.double)


# def convert_df_2_frame_list(df_target):
#
#     return fea_curr_list, label_curr_list


def convert_df_2_contour_list(df_target):
    dur_max = 0
    df_target['contour_id'] = df_target['contour_id'].astype(int)
    unique_file = list(df_target.groupby(['file']).groups.keys())
    fea_curr_list = []
    label_curr_list = []
    for dd in sorted(unique_file):
        print('Converting dataframe of the file '+dd)
        file_curr = df_target.loc[df_target['file'] == dd]
        dur_max = max(dur_max, file_curr['contour_id'].max()+1)
        unique_contour = list(file_curr.groupby(['contour_id']).groups.keys())
        for cc in sorted(unique_contour):
            contour_curr = file_curr.loc[file_curr['contour_id'] == cc]
            contour_curr = contour_curr.sort_index(axis=0)
            fea_curr = contour_curr.iloc[:, 4:].to_numpy()
            fea_curr_list.append(fea_curr)
            label_curr_list.append(contour_curr.iloc[0, 3])
    return fea_curr_list, label_curr_list, dur_max


def convert_df_2_win_list(df_target, win_back):
    '''

    :param df_target:
    :param win_back: the numbers of time instants we look back, including the current one
    :return:
    '''
    dur_max = win_back
    df_target['contour_id'] = df_target['contour_id'].astype(int)
    unique_file = list(df_target.groupby(['file']).groups.keys())
    fea_curr_list = []
    label_curr_list = []
    for dd in sorted(unique_file):
        print('Converting dataframe of the file '+dd)
        file_curr = df_target.loc[df_target['file'] == dd]
        # dur_max = max(dur_max, file_curr['contour_id'].max()+1)
        unique_contour = list(file_curr.groupby(['contour_id']).groups.keys())
        for cc in sorted(unique_contour):
            contour_curr = file_curr.loc[file_curr['contour_id'] == cc]
            # contour_curr = contour_curr.sort_index(axis=0)
            contour_curr = contour_curr.sort_values(by=['time_id'])
            fea_curr = contour_curr.iloc[:, 4:].to_numpy()
            # fea_curr_pd = contour_curr.iloc[:, 4:]
            for mm in range(fea_curr.shape[0]-win_back):
                fea_curr_win = fea_curr[mm:mm+win_back,:]
                fea_curr_list.append(fea_curr_win)
                label_curr_list.append(contour_curr.iloc[0, 3])
    return fea_curr_list, label_curr_list, dur_max


def convert_df_2_win_list_half_diff(df_target, win_back, type=1):
    '''
    convert contour dataframe to a list of features
    Added 50% overlap (aka. half) instead of 1 frame step
    Difference between frames of cepstral coefficientss(df_target, win_back=5):

    :param df_target:
    :param win_back: the numbers of time instants we look back, including the current one
    :param type: 1: original features; 2: delta; 3: 2nd order delta
    :return:
    '''
    dur_max = win_back
    if win_back == 1:
        win_back_half = 1
    else:
        win_back_half = int(win_back/2)  # 50%
    df_target['contour_id'] = df_target['contour_id'].astype(int)
    unique_file = list(df_target.groupby(['file']).groups.keys())
    fea_curr_list = []
    label_curr_list = []
    for dd in sorted(unique_file):
        print('Converting dataframe of the file '+dd)
        file_curr = df_target.loc[df_target['file'] == dd]
        # dur_max = max(dur_max, file_curr['contour_id'].max()+1)
        unique_contour = list(file_curr.groupby(['contour_id']).groups.keys())
        for cc in sorted(unique_contour):
            contour_curr = file_curr.loc[file_curr['contour_id'] == cc]
            # contour_curr = contour_curr.sort_index(axis=0)
            contour_curr = contour_curr.sort_values(by=['time_id'])
            fea_curr = contour_curr.iloc[:, 4:].to_numpy()
            # fea_curr_pd = contour_curr.iloc[:, 4:]
            # for mm in range(fea_curr.shape[0]-win_back):
            for mm in range(0, fea_curr.shape[0] - win_back, win_back_half):
                fea_curr_win = fea_curr[mm:mm+win_back, :]
                if type == 1:
                    fea_curr_list.append(fea_curr_win)
                elif type == 2:  # cepstral delta
                    # diff of cepstral coefficients
                    fea_curr_list.append(np.diff(fea_curr_win, axis=0))
                elif type == 3:  # ceptral delta*2
                    fea_curr_list.append(np.diff(fea_curr_win, n=2, axis=0))

                label_curr_list.append(contour_curr.iloc[0, 3])
    return fea_curr_list, label_curr_list, dur_max


def convert_df_2_win_contour_list(df_target, win_back):
    '''

    :param df_target:
    :param win_back: the numbers of time instants we look back, including the current one
    :return: a list, each element of which is an array (num_samples, num_features)
    '''
    dur_max = win_back
    df_target['contour_id'] = df_target['contour_id'].astype(int)
    unique_file = list(df_target.groupby(['file']).groups.keys())
    fea_curr_list = []
    label_curr_list = []
    for dd in sorted(unique_file):
        print('Converting dataframe of the file '+dd)
        file_curr = df_target.loc[df_target['file'] == dd]
        # dur_max = max(dur_max, file_curr['contour_id'].max()+1)
        unique_contour = list(file_curr.groupby(['contour_id']).groups.keys())
        for cc in sorted(unique_contour):
            contour_curr = file_curr.loc[file_curr['contour_id'] == cc]
            # contour_curr = contour_curr.sort_index(axis=0)
            contour_curr = contour_curr.sort_values(by=['time_id'])
            fea_curr = contour_curr.iloc[:, 4:].to_numpy()
            # fea_curr_pd = contour_curr.iloc[:, 4:]
            fea_per_contour_list = []
            for mm in range(fea_curr.shape[0]-win_back):
                fea_curr_win = fea_curr[mm:mm+win_back,:]
                fea_per_contour_list.append(fea_curr_win)
            label_curr_list.append(contour_curr.iloc[0, 3])
            fea_curr_list.append(fea_per_contour_list)
    return fea_curr_list, label_curr_list, dur_max


def convert_df_2_win_contour_list_half_diff(df_target, win_back, type=1):
    '''

    :param df_target:
    :param win_back: the numbers of time instants we look back, including the current one
    :return: a list, each element of which is an array (num_samples, num_features)
    '''
    dur_max = win_back
    if win_back == 1:
        win_back_half = 1
    else:
        win_back_half = int(win_back/2)  # 50%

    df_target['contour_id'] = df_target['contour_id'].astype(int)
    unique_file = list(df_target.groupby(['file']).groups.keys())
    fea_curr_list = []
    label_curr_list = []
    for dd in sorted(unique_file):
        print('Converting dataframe of the file '+dd)
        file_curr = df_target.loc[df_target['file'] == dd]
        # dur_max = max(dur_max, file_curr['contour_id'].max()+1)
        unique_contour = list(file_curr.groupby(['contour_id']).groups.keys())
        for cc in sorted(unique_contour):
            contour_curr = file_curr.loc[file_curr['contour_id'] == cc]
            # contour_curr = contour_curr.sort_index(axis=0)
            contour_curr = contour_curr.sort_values(by=['time_id'])
            fea_curr = contour_curr.iloc[:, 4:].to_numpy()
            # fea_curr_pd = contour_curr.iloc[:, 4:]
            fea_per_contour_list = []
            for mm in range(0, fea_curr.shape[0] - win_back, win_back_half):
                fea_curr_win = fea_curr[mm:mm+win_back, :]
                if type == 1:
                    fea_per_contour_list.append(fea_curr_win)
                elif type == 2:  # cepstral delta
                    # diff of cepstral coefficients
                    fea_per_contour_list.append(np.diff(fea_curr_win, axis=0))
                elif type == 3:  # ceptral delta*2
                    fea_per_contour_list.append(np.diff(fea_curr_win, n=2, axis=0))
            # for mm in range(fea_curr.shape[0]-win_back):
            #     fea_curr_win = fea_curr[mm:mm+win_back,:]
            #     fea_per_contour_list.append(fea_curr_win)
            label_curr_list.append(contour_curr.iloc[0, 3])
            fea_curr_list.append(fea_per_contour_list)
    return fea_curr_list, label_curr_list, dur_max


# def calculate_map(y_true,y_pred):
#
#     num_classes = y_true.shape[1]
#     average_precisions = []
#     relevant = backend.sum(K.round(K.clip(y_true, 0, 1)))
#     tp_whole = backend.round(K.clip(y_true * y_pred, 0, 1))
#     for index in range(num_classes):
#         temp = backend.sum(tp_whole[:,:index+1],axis=1)
#         average_precisions.append(temp * (1/(index + 1)))
#     AP = Add()(average_precisions) / relevant
#     mAP = backend.mean(AP,axis=0)
#
#     return mAP
