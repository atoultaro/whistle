#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Exploring how to load part of the model doing feature extraction from a
trained model
Created on 10/9/19
@author: atoultaro
"""
import os
from keras.models import load_model, Model
from cape_cod_whale.classifier import find_best_model


def load_fea_model(log_fea_dir, layer_target='flatten_1'):
    fmt = "epoch_\d+_([0-1].\d+)_\d+.\d{4}.hdf5"  # val_acc
    best_model_path, best_accu = find_best_model(log_fea_dir, fmt, is_max=True)
    model = load_model(best_model_path)

    # model of feature extraction: feature dimension 894
    model_fea = Model(model.input, model.get_layer(layer_target).output)
    # model_fea = Model(model.input, model.get_layer('flatten_1').output)
    # model_fea = Model(model.input, model.layers[4].output)
    # model_fea = Model(model.input, model.layers[-3].output)

    return model_fea


# log_dir = '/home/ys587/__Data/__whistle/__log_dir_audio_time/__logdir_temp/fea_cepstrum_conv2d_lstm_winback_10_128_64_2lay_l2_p2_dropout_p2_f1_p80'
# log_dir_fold1 = os.path.join(log_dir, 'fold1')
# # log_dir_fold2 = os.path.join(log_dir, 'fold2')
# #
# # fmt = "epoch_\d+_([0-1].\d+)_\d+.\d{4}.hdf5"  # val_acc
# #
# # fold 1
# mode_fea1 = load_fea_model(log_dir_fold1)
# best_model_path, best_accu = find_best_model(log_dir_fold1, fmt, is_max=True)
# model = load_model(best_model_path)
#
# # model of feature extraction: feature dimension 894
# model_fea = Model(model.input, model.get_layer('flatten_1').output)
# # model_fea = Model(model.input, model.layers[4].output)

