from keras.layers import Dense, LSTM, Dropout, Masking, BatchNormalization, \
    Bidirectional, TimeDistributed, Flatten, ConvLSTM2D, RepeatVector, \
    LeakyReLU, GlobalAveragePooling1D, GlobalMaxPooling1D, Conv1D, Input, \
    Reshape, Conv2D, MaxPooling1D, MaxPooling2D

from keras.models import Sequential, Model
# from keras.layers.convolutional import Conv1D

from keras import regularizers
from keras import constraints
from keras_self_attention import SeqSelfAttention

import numpy as nps


# to-do:
# more experiments on conv1d's step
# Add dropout to conv
# Apply conv1d along channels
# longer sequence, say, 0.2 s or 0.4 s


def conv2d_lstm(data_shape, conf):
    l2_regu = conf["l2_regu"]
    dropout_rate = conf["dropout"]
    recurr_dropout_rate = conf["recurrent_dropout"]

    model = Sequential()
    model.add(ConvLSTM2D(filters=conf['filt1'], kernel_size=(4, 1),
                         strides=(2, 1),
                         input_shape=data_shape, padding='valid',
                         kernel_regularizer=regularizers.l2(l2_regu),
                         recurrent_regularizer=regularizers.l2(l2_regu),
                         dropout=dropout_rate,
                         recurrent_dropout=recurr_dropout_rate,
                         return_sequences=True))
    model.add(BatchNormalization())
    model.add(ConvLSTM2D(filters=conf['filt2'], kernel_size=(4, 1),
                         strides=(2, 1),
                         padding='valid',
                         kernel_regularizer=regularizers.l2(l2_regu),
                         recurrent_regularizer=regularizers.l2(l2_regu),
                         dropout=dropout_rate,
                         recurrent_dropout=recurr_dropout_rate,
                         return_sequences=False))
    model.add(BatchNormalization())

    model.add(Flatten())
    model.add(Dense(1000))
    model.add(Dense(conf["num_class"], activation='softmax'))

    return model


def conv2d_time_fea(data_shape, conf):
    model = Sequential()
    model.add(Conv2D(conf['conv2d_1'], kernel_size=(2, 4), strides=(2, 2),
                     activation='relu', padding='same', input_shape=data_shape))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(1, 1), padding='same'))
    model.add(Conv2D(conf['conv2d_2'], kernel_size=(2, 4), strides=(2, 2),
                     padding='same', activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(1, 1), padding='same'))
    model.add(Flatten())
    model.add(Dense(20, activation='relu'))
    model.add(Dense(conf["num_class"], activation='softmax'))

    return model


def conv1d_fea_conv1d_time(data_shape, conf):  # data_shape: (batch_size, time_stamp, fea_dim, 1)
    x = Input(shape=data_shape, name='input')
    y = Conv1D(conf['conv1d_1'], kernel_size=2, strides=2, padding='same',
               activation='relu')(x)
    y = MaxPooling1D(pool_size=2, strides=2, padding='same')(y)
    # y = Flatten()(y)
    # # y = Reshape((-1, (int(data_shape[0]/2)-2)*conf['conv1d_1']))(y)
    # y = Reshape((-1, 1))(y)
    y = Conv1D(conf['conv1d_2'], kernel_size=2, strides=2, padding='same',
               activation='relu')(y)
    y = MaxPooling1D(pool_size=2, strides=2, padding='same')(y)
    y = Flatten()(y)
    y = Dense(20, activation='relu')(y)
    y = Dense(conf["num_class"], activation='softmax')(y)
    model = Model(x, y)

    return model


def conv1d_2lay(data_shape, conf):  # weird...
    model = Sequential()
    model.add(Conv1D(conf['conv1d_1'], kernel_size=2, strides=1,
                     activation='relu', input_shape=data_shape))
    model.add(MaxPooling1D(pool_size=2, strides=2))
    model.add(Conv1D(conf['conv1d_2'], kernel_size=2, strides=1,
                     activation='relu'))
    model.add(MaxPooling1D(pool_size=2, strides=2))
    model.add(Flatten())
    model.add(Dense(20, activation='relu'))
    model.add(Dense(conf["num_class"], activation='softmax'))

    return model


def conv_lstm_1d(data_shape, conf):  # weird...
    model = Sequential()
    model.add(Conv1D(filters=conf['conv1d_1'], kernel_size=4, strides=1, activation='relu',
                     input_shape=data_shape))
    model.add(MaxPooling1D(pool_size=4))
    model.add(LSTM(conf['lstm1']))
    model.add(Dense(100, activation='relu'))
    model.add(Dense(conf["num_class"], activation='softmax'))

    return model


def lstm_bidir_2lay(data_shape, conf):
    """
    LSTM bidirectional
    :param data_shape:
    :param conf:
    :return:
    """
    l2_regu = conf["l2_regu"]
    dropout_rate = conf["dropout"]
    recurr_dropout_rate = conf["recurrent_dropout"]

    model = Sequential()
    model.add(Bidirectional(LSTM(conf["lstm1"],
                                 return_sequences=True,
                                 kernel_regularizer=regularizers.l2(l2_regu),
                                 recurrent_regularizer=regularizers.l2(l2_regu),
                                 dropout=dropout_rate,
                                 recurrent_dropout=recurr_dropout_rate),
                            # input_shape=(data_shape[0], data_shape[1]),
                            input_shape=data_shape,
                            merge_mode=conf["bi_mod"]))
    model.add(BatchNormalization(center=False, scale=False))

    model.add(Bidirectional(LSTM(conf["lstm2"],
                                 return_sequences=False,
                                 kernel_regularizer=regularizers.l2(l2_regu),
                                 recurrent_regularizer=regularizers.l2(l2_regu),
                                 dropout=dropout_rate,
                                 recurrent_dropout=recurr_dropout_rate),
                            merge_mode=conf["bi_mod"]))
    model.add(BatchNormalization(center=False, scale=False))

    model.add(Dense(conf["num_class"], activation='softmax'))

    return model


def lstm_2lay(data_shape, conf):
    """
    LSTM
    :param data_shape:
    :param conf:
    :return:
    """
    l2_regu = conf["l2_regu"]
    dropout_rate = conf["dropout"]
    recurr_dropout_rate = conf["recurrent_dropout"]

    model_rnn = Sequential()
    model_rnn.add(LSTM(conf["lstm1"],
                       input_shape=data_shape,
                       return_sequences=True,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       dropout=dropout_rate,
                       recurrent_dropout=recurr_dropout_rate
                       ))
    model_rnn.add(BatchNormalization(center=False, scale=False))

    model_rnn.add(LSTM(conf["lstm2"],
                       return_sequences=False,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       dropout=dropout_rate,
                       recurrent_dropout=recurr_dropout_rate))
    model_rnn.add(BatchNormalization(center=False, scale=False))
    model_rnn.add(Dense(conf["num_class"], activation='softmax'))
    return model_rnn


def lstm_3lay(data_shape, conf):
    """
    LSTM
    :param data_shape:
    :param conf:
    :return:
    """
    l2_regu = conf["l2_regu"]
    dropout_rate = conf["dropout"]
    recurr_dropout_rate = conf["recurrent_dropout"]

    model_rnn = Sequential()
    model_rnn.add(LSTM(conf["lstm1"],
                       input_shape=data_shape,
                       return_sequences=True,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       dropout=dropout_rate,
                       recurrent_dropout=recurr_dropout_rate
                       ))
    model_rnn.add(BatchNormalization(center=False, scale=False))

    model_rnn.add(LSTM(conf["lstm2"],
                       return_sequences=True,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       dropout=dropout_rate,
                       recurrent_dropout=recurr_dropout_rate))
    model_rnn.add(BatchNormalization(center=False, scale=False))
    model_rnn.add(LSTM(conf["lstm3"],
                       return_sequences=False,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       dropout=dropout_rate,
                       recurrent_dropout=recurr_dropout_rate))
    model_rnn.add(BatchNormalization(center=False, scale=False))

    model_rnn.add(Dense(conf["num_class"], activation='softmax'))
    return model_rnn


def tdnn(data_shape, conf):
    dense_size = conf["dense_size"]
    dropout_rate = conf["dropout"]
    l2_regu = conf["l2_regu"]

    model = Sequential()


    return model


def mlp_time_l2(data_shape, conf):
    """
    2-layer mlp
    :param data_shape:
    :param conf:
    :return:
    """
    # dense_size = conf["dense_size"]
    dropout_rate = conf["dropout"]
    l2_regu = conf["l2_regu"]

    model = Sequential()
    model.add(Dense(conf['filt1'], activation='relu',
                    kernel_regularizer=regularizers.l2(l2_regu),
                    input_shape=data_shape))
    model.add(Dropout(dropout_rate))
    model.add(Dense(conf['filt2'], activation='relu',
                    kernel_regularizer=regularizers.l2(l2_regu)))
    model.add(Dropout(dropout_rate))
    # model.add(Dense(1))
    model.add(GlobalAveragePooling1D())
    # model.add(GlobalMaxPooling1D())
    # model.add(Flatten())
    model.add(Dense(conf["num_class"], activation='softmax'))

    return model


def mlp_time_3lay_l2(data_shape, conf):
    dense_size = conf["dense_size"]
    dropout_rate = conf["dropout"]
    l2_regu = conf["l2_regu"]

    model = Sequential()
    model.add(Dense(dense_size, activation='relu',
                    kernel_regularizer=regularizers.l2(l2_regu),
                    input_shape=data_shape))
    model.add(Dropout(dropout_rate))
    model.add(Dense(dense_size, activation='relu',
                    kernel_regularizer=regularizers.l2(l2_regu)))
    model.add(Dropout(dropout_rate))
    model.add(Dense(dense_size, activation='relu',
                    kernel_regularizer=regularizers.l2(l2_regu),
                    input_shape=data_shape))
    model.add(Dropout(dropout_rate))
    model.add(GlobalAveragePooling1D())
    # model.add(Flatten())
    model.add(Dense(conf["num_class"], activation='softmax'))

    return model


def mlp_time_4lay_l2(data_shape, conf):
    dense_size = conf["dense_size"]
    dropout_rate = conf["dropout"]
    l2_regu = conf["l2_regu"]

    model = Sequential()
    model.add(Dense(dense_size, activation='relu',
                    kernel_regularizer=regularizers.l2(l2_regu),
                    input_shape=data_shape))
    model.add(Dropout(dropout_rate))
    model.add(Dense(dense_size, activation='relu',
                    kernel_regularizer=regularizers.l2(l2_regu)))
    model.add(Dropout(dropout_rate))
    model.add(Dense(dense_size, activation='relu',
                    kernel_regularizer=regularizers.l2(l2_regu),
                    input_shape=data_shape))
    model.add(Dropout(dropout_rate))
    model.add(Dense(dense_size, activation='relu',
                    kernel_regularizer=regularizers.l2(l2_regu),
                    input_shape=data_shape))
    model.add(Dropout(dropout_rate))
    model.add(GlobalAveragePooling1D())
    # model.add(Flatten())
    model.add(Dense(conf["num_class"], activation='softmax'))

    return model


def mlp_time_5lay_l2(data_shape, conf):
    dense_size = conf["dense_size"]
    dropout_rate = conf["dropout"]
    l2_regu = conf["l2_regu"]

    model = Sequential()
    model.add(Dense(dense_size, activation='relu',
                    kernel_regularizer=regularizers.l2(l2_regu),
                    input_shape=data_shape))
    model.add(Dropout(dropout_rate))
    model.add(Dense(dense_size, activation='relu',
                    kernel_regularizer=regularizers.l2(l2_regu)))
    model.add(Dropout(dropout_rate))
    model.add(Dense(dense_size, activation='relu',
                    kernel_regularizer=regularizers.l2(l2_regu),
                    input_shape=data_shape))
    model.add(Dropout(dropout_rate))
    model.add(Dense(dense_size, activation='relu',
                    kernel_regularizer=regularizers.l2(l2_regu),
                    input_shape=data_shape))
    model.add(Dropout(dropout_rate))
    model.add(Dense(dense_size, activation='relu',
                    kernel_regularizer=regularizers.l2(l2_regu),
                    input_shape=data_shape))
    model.add(Dropout(dropout_rate))
    model.add(GlobalAveragePooling1D())
    # model.add(Flatten())
    model.add(Dense(conf["num_class"], activation='softmax'))

    return model


def fully_connected(data_shape, conf):
    dense_size = conf["dense_size"]
    dropout_rate = conf["dropout"]

    model = Sequential()
    model.add(Dense(dense_size, activation='relu', input_shape=data_shape))
    model.add(Dropout(dropout_rate))
    model.add(Dense(dense_size, activation='relu'))
    model.add(Dropout(dropout_rate))
    model.add(Dense(conf["num_class"], activation='softmax'))

    return model


def fully_connected_l2(data_shape, conf):
    # dense_size = conf["dense_size"]
    dropout_rate = conf["dropout"]
    l2_regu = conf["l2_regu"]

    model = Sequential()
    model.add(Dense(conf['filt1'], activation='relu',
                    kernel_regularizer=regularizers.l2(l2_regu),
                    input_shape=data_shape))
    model.add(Dropout(dropout_rate))
    model.add(Dense(conf['filt2'], activation='relu',
                    kernel_regularizer=regularizers.l2(l2_regu)))
    model.add(Dropout(dropout_rate))
    model.add(Dense(conf["num_class"], activation='softmax'))

    return model


def fully_connected_leakyrelu(data_shape, conf):
    dense_size = conf["dense_size"]
    dropout_rate = conf["dropout"]
    l2_regu = conf["l2_regu"]

    model = Sequential()
    model.add(Dense(dense_size, input_shape=data_shape))
    model.add(LeakyReLU(alpha=0.05))
    model.add(Dropout(dropout_rate))
    model.add(Dense(dense_size))
    model.add(LeakyReLU(alpha=0.05))
    model.add(Dropout(dropout_rate))
    model.add(Dense(conf["num_class"], activation='softmax'))

    return model


def fully_connected_l2_leakyrelu(data_shape, conf):
    dense_size = conf["dense_size"]
    dropout_rate = conf["dropout"]
    l2_regu = conf["l2_regu"]

    model = Sequential()
    model.add(Dense(dense_size, input_shape=data_shape))
    model.add(LeakyReLU(alpha=0.05))
    model.add(Dropout(dropout_rate))
    model.add(Dense(dense_size))
    model.add(LeakyReLU(alpha=0.05))
    model.add(Dropout(dropout_rate))
    model.add(Dense(conf["num_class"], activation='softmax'))

    return model


def lstm_phone_2_layers_expt(data_shape, conf):
    l2_regu = conf["l2_regu"]
    dense_size = conf["dense_size"]
    dropout_rate = conf["dropout"]
    # recurr_dropout_rate = conf["recurrent_dropout"]

    model_rnn = Sequential()
    model_rnn.add(Masking(mask_value=0.0, input_shape=data_shape))

    model_rnn.add(LSTM(conf["lstm1"],
                       return_sequences=True,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu)  #,
                       # dropout=dropout_rate  # ,
                       # recurrent_dropout=recurr_dropout_rate
                       ))
    model_rnn.add(Dropout(dropout_rate))
    model_rnn.add(BatchNormalization())

    model_rnn.add(LSTM(conf["lstm2"],
                       return_sequences=False,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu)  #,
                       # dropout=dropout_rate  # ,
                       # recurrent_dropout=recurr_dropout_rate
                       ))
    model_rnn.add(Dropout(dropout_rate))
    model_rnn.add(BatchNormalization())

    model_rnn.add(Dense(dense_size))
    model_rnn.add(Dropout(dropout_rate))
    model_rnn.add(Dense(dense_size))
    model_rnn.add(Dropout(dropout_rate))
    model_rnn.add(Dense(conf["num_class"], activation='softmax'))
    return model_rnn


def lstm_phone_2_layers(data_shape, conf):
    l2_regu = conf["l2_regu"]
    dropout_rate = conf["dropout"]
    recurr_dropout_rate = conf["recurrent_dropout"]
    dense_size = conf["dense_size"]

    model_rnn = Sequential()
    model_rnn.add(Masking(mask_value=0.0, input_shape=data_shape))
    # model_rnn.add(BatchNormalization())

    model_rnn.add(LSTM(conf["lstm1"],
                       return_sequences=True,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       dropout=dropout_rate  # ,
                       # recurrent_dropout=recurr_dropout_rate
                       ))
    model_rnn.add(BatchNormalization())

    model_rnn.add(LSTM(conf["lstm2"],
                       return_sequences=False,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       dropout=dropout_rate  # ,
                       # recurrent_dropout=recurr_dropout_rate
                       ))
    model_rnn.add(BatchNormalization())

    model_rnn.add(Dense(dense_size))
    model_rnn.add(Dropout(dropout_rate))

    model_rnn.add(Dense(conf["num_class"], activation='softmax'))
    return model_rnn


def lstm_atten_4_layer_baseline(data_shape, conf):
    l2_regu = conf["l2_regu"]
    dropout_rate = conf["dropout"]
    recurr_dropout_rate = conf["recurrent_dropout"]

    model_rnn = Sequential()
    model_rnn.add(Masking(mask_value=0.0, input_shape=data_shape))
    model_rnn.add(BatchNormalization())

    model_rnn.add(LSTM(conf["lstm1"],
                       return_sequences=True,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       dropout=dropout_rate,
                       recurrent_dropout=recurr_dropout_rate
                       ))
    model_rnn.add(SeqSelfAttention())
    model_rnn.add(BatchNormalization())

    model_rnn.add(LSTM(conf["lstm2"],
                       return_sequences=True,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       dropout=dropout_rate,
                       recurrent_dropout=recurr_dropout_rate
                       ))
    model_rnn.add(SeqSelfAttention())
    model_rnn.add(BatchNormalization())

    model_rnn.add(LSTM(conf["lstm3"],
                       return_sequences=True,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       dropout=dropout_rate,
                       recurrent_dropout=recurr_dropout_rate
                       ))
    model_rnn.add(SeqSelfAttention())
    model_rnn.add(BatchNormalization())

    model_rnn.add(LSTM(conf["lstm4"], return_sequences=False,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       dropout=dropout_rate, recurrent_dropout=recurr_dropout_rate))
    model_rnn.add(BatchNormalization())

    model_rnn.add(Dense(conf["num_class"], activation='softmax'))
    return model_rnn


def lstm_atten_3_layer_baseline(data_shape, conf):
    l2_regu = conf["l2_regu"]
    dropout_rate = conf["dropout"]
    recurr_dropout_rate = conf["recurrent_dropout"]

    model_rnn = Sequential()
    model_rnn.add(Masking(mask_value=0.0, input_shape=data_shape))
    model_rnn.add(BatchNormalization())

    model_rnn.add(LSTM(conf["lstm1"],
                       return_sequences=True,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       dropout=dropout_rate,
                       recurrent_dropout=recurr_dropout_rate
                       ))
    model_rnn.add(SeqSelfAttention())
    model_rnn.add(BatchNormalization())

    model_rnn.add(LSTM(conf["lstm2"],
                       return_sequences=True,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       dropout=dropout_rate,
                       recurrent_dropout=recurr_dropout_rate
                       ))
    model_rnn.add(SeqSelfAttention())
    model_rnn.add(BatchNormalization())

    model_rnn.add(LSTM(conf["lstm3"], return_sequences=False,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       dropout=dropout_rate,
                       recurrent_dropout=recurr_dropout_rate
                       ))
    model_rnn.add(BatchNormalization())

    model_rnn.add(Dense(conf["num_class"], activation='softmax'))
    return model_rnn


def lstm_atten_2_layer_baseline(data_shape, conf):
    l2_regu = conf["l2_regu"]
    dropout_rate = conf["dropout"]
    recurr_dropout_rate = conf["recurrent_dropout"]

    model_rnn = Sequential()
    model_rnn.add(Masking(mask_value=0.0, input_shape=data_shape))
    model_rnn.add(BatchNormalization())

    model_rnn.add(LSTM(conf["lstm1"],
                       return_sequences=True,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       dropout=dropout_rate,
                       recurrent_dropout=recurr_dropout_rate
                       ))
    model_rnn.add(SeqSelfAttention())
    model_rnn.add(BatchNormalization())

    model_rnn.add(LSTM(conf["lstm2"], return_sequences=False,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       dropout=dropout_rate, recurrent_dropout=recurr_dropout_rate))
    model_rnn.add(BatchNormalization())

    model_rnn.add(Dense(conf["num_class"], activation='softmax'))
    return model_rnn


def lstm_atten_dropout_4_layer(data_shape, conf):
    l2_regu = conf["l2_regu"]
    dropout_rate = conf["dropout"]
    recurr_dropout_rate = conf["recurrent_dropout"]

    model_rnn = Sequential()
    model_rnn.add(Masking(mask_value=0.0, input_shape=data_shape))
    model_rnn.add(BatchNormalization())

    model_rnn.add(LSTM(conf["lstm1"],
                       return_sequences=True,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       dropout=dropout_rate,
                       recurrent_dropout=recurr_dropout_rate
                       ))
    model_rnn.add(SeqSelfAttention())
    model_rnn.add(Dropout(dropout_rate))
    model_rnn.add(BatchNormalization())

    model_rnn.add(LSTM(conf["lstm2"],
                       return_sequences=True,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       dropout=dropout_rate,
                       recurrent_dropout=recurr_dropout_rate
                       ))
    model_rnn.add(SeqSelfAttention())
    model_rnn.add(Dropout(dropout_rate))
    model_rnn.add(BatchNormalization())

    model_rnn.add(LSTM(conf["lstm3"],
                       return_sequences=True,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       dropout=dropout_rate,
                       recurrent_dropout=recurr_dropout_rate
                       ))
    model_rnn.add(SeqSelfAttention())
    model_rnn.add(Dropout(dropout_rate))
    model_rnn.add(BatchNormalization())

    model_rnn.add(LSTM(conf["lstm4"], return_sequences=False,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       dropout=dropout_rate,
                       recurrent_dropout=recurr_dropout_rate))
    model_rnn.add(BatchNormalization())

    model_rnn.add(Dense(conf["num_class"], activation='softmax'))
    return model_rnn


# bidirectional LSTM
def lstm_atten_bidirect_4_layer(data_shape, conf):
    l2_regu = conf["l2_regu"]
    dropout_rate = conf["dropout"]
    recurr_dropout_rate = conf["recurrent_dropout"]

    model_rnn = Sequential()
    model_rnn.add(Masking(mask_value=0.0, input_shape=data_shape))
    model_rnn.add(BatchNormalization())

    model_rnn.add(Bidirectional(LSTM(
        conf["lstm1"],
        return_sequences=True,
        kernel_regularizer=regularizers.l2(l2_regu),
        recurrent_regularizer=regularizers.l2(l2_regu),
        dropout=dropout_rate,
        recurrent_dropout=recurr_dropout_rate), merge_mode=conf["mod"]))  # problem on merge_mode
    model_rnn.add(SeqSelfAttention())
    model_rnn.add(BatchNormalization())

    model_rnn.add(Bidirectional(LSTM(
        conf["lstm2"],
        return_sequences=True,
        kernel_regularizer=regularizers.l2(l2_regu),
        recurrent_regularizer=regularizers.l2(l2_regu),
        dropout=dropout_rate,
        recurrent_dropout=recurr_dropout_rate), merge_mode=conf["mod"]))
    model_rnn.add(SeqSelfAttention())
    model_rnn.add(BatchNormalization())

    model_rnn.add(Bidirectional(LSTM(
        conf["lstm3"],
        return_sequences=True,
        kernel_regularizer=regularizers.l2(l2_regu),
        recurrent_regularizer=regularizers.l2(l2_regu),
        dropout=dropout_rate,
        recurrent_dropout=recurr_dropout_rate), merge_mode=conf["mod"]))
    model_rnn.add(SeqSelfAttention())
    model_rnn.add(BatchNormalization())

    model_rnn.add(Bidirectional(LSTM(
        conf["lstm4"],
        return_sequences=False,
        kernel_regularizer=regularizers.l2(l2_regu),
        recurrent_regularizer=regularizers.l2(l2_regu),
        dropout=dropout_rate,
        recurrent_dropout=recurr_dropout_rate), merge_mode=conf["mod"]))
    model_rnn.add(BatchNormalization())

    model_rnn.add(Dense(conf["num_class"], activation='softmax'))
    return model_rnn


def lstm_phone_1_layer(data_shape, conf):
    l2_regu = conf["l2_regu"]
    dropout_rate = conf["dropout"]
    recurr_dropout_rate = conf["recurrent_dropout"]

    model_rnn = Sequential()
    model_rnn.add(Masking(mask_value=0.0, input_shape=data_shape))
    model_rnn.add(BatchNormalization())

    model_rnn.add(LSTM(conf["lstm1"],
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       dropout=dropout_rate,
                       recurrent_dropout=recurr_dropout_rate
                       ))
    model_rnn.add(BatchNormalization())

    model_rnn.add(Dense(conf["num_class"], activation='softmax'))
    return model_rnn


def lstm_phone_1_layer_2(data_shape, conf):
    # no batch normalization after masking
    l2_regu = conf["l2_regu"]
    dropout_rate = conf["dropout"]
    recurr_dropout_rate = conf["recurrent_dropout"]

    model_rnn = Sequential()
    model_rnn.add(Masking(mask_value=0.0, input_shape=data_shape))

    model_rnn.add(
        LSTM(conf["lstm1"], kernel_regularizer=regularizers.l2(l2_regu),
             recurrent_regularizer=regularizers.l2(l2_regu), dropout=dropout_rate,
             recurrent_dropout=recurr_dropout_rate))
    model_rnn.add(BatchNormalization())

    model_rnn.add(Dense(conf["num_class"], activation='softmax'))
    return model_rnn


def lstm_phone_1_layer_3(data_shape, conf):
    # no dropout & recurrent dropout
    l2_regu = conf["l2_regu"]
    dropout_rate = conf["dropout"]
    recurr_dropout_rate = conf["recurrent_dropout"]

    model_rnn = Sequential()
    model_rnn.add(Masking(mask_value=0.0, input_shape=data_shape))
    model_rnn.add(BatchNormalization())

    model_rnn.add(
        LSTM(conf["lstm1"], kernel_regularizer=regularizers.l2(l2_regu),
             recurrent_regularizer=regularizers.l2(l2_regu)))
    model_rnn.add(BatchNormalization())

    model_rnn.add(Dense(conf["num_class"], activation='softmax'))
    return model_rnn


def lstm_phone_1_layer_4(data_shape, conf):
    # no recurrent dropout
    l2_regu = conf["l2_regu"]
    dropout_rate = conf["dropout"]
    recurr_dropout_rate = conf["recurrent_dropout"]

    model_rnn = Sequential()
    model_rnn.add(Masking(mask_value=0.0, input_shape=data_shape))
    model_rnn.add(BatchNormalization())

    model_rnn.add(LSTM(conf["lstm1"],
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       dropout=dropout_rate
                       ))
    model_rnn.add(BatchNormalization())

    model_rnn.add(Dense(conf["num_class"], activation='softmax'))
    return model_rnn


def lstm_phone_1_layer_5(data_shape, conf):
    # no dropout
    l2_regu = conf["l2_regu"]
    dropout_rate = conf["dropout"]
    recurr_dropout_rate = conf["recurrent_dropout"]

    model_rnn = Sequential()
    model_rnn.add(Masking(mask_value=0.0, input_shape=data_shape))
    model_rnn.add(BatchNormalization())

    model_rnn.add(LSTM(conf["lstm1"],
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       recurrent_dropout=recurr_dropout_rate
                       ))
    model_rnn.add(BatchNormalization())

    model_rnn.add(Dense(conf["num_class"], activation='softmax'))
    return model_rnn


def lstm_phone_3_layers(data_shape, conf):
    l2_regu = conf["l2_regu"]
    dropout_rate = conf["dropout"]
    recurr_dropout_rate = conf["recurrent_dropout"]

    model_rnn = Sequential()
    model_rnn.add(Masking(mask_value=0.0, input_shape=data_shape))
    model_rnn.add(BatchNormalization())

    model_rnn.add(LSTM(conf["lstm1"],
                       return_sequences=True,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       dropout=dropout_rate,
                       recurrent_dropout=recurr_dropout_rate
                       ))
    model_rnn.add(BatchNormalization())

    model_rnn.add(LSTM(conf["lstm2"],
                       return_sequences=True,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       dropout=dropout_rate,
                       recurrent_dropout=recurr_dropout_rate
                       ))
    model_rnn.add(BatchNormalization())

    model_rnn.add(LSTM(conf["lstm3"],
                       return_sequences=False,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       dropout=dropout_rate,
                       recurrent_dropout=recurr_dropout_rate
                       ))
    model_rnn.add(BatchNormalization())

    model_rnn.add(Dense(conf["num_class"], activation='softmax'))
    return model_rnn


def lstm_phone_4_layers(data_shape, conf):
    l2_regu = conf["l2_regu"]
    dropout_rate = conf["dropout"]
    recurr_dropout_rate = conf["recurrent_dropout"]

    model_rnn = Sequential()
    model_rnn.add(Masking(mask_value=0.0, input_shape=data_shape))
    model_rnn.add(BatchNormalization())

    model_rnn.add(LSTM(conf["lstm1"],
                       return_sequences=True,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       dropout=dropout_rate,
                       recurrent_dropout=recurr_dropout_rate
                       ))
    model_rnn.add(BatchNormalization())

    model_rnn.add(LSTM(conf["lstm2"],
                       return_sequences=True,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       dropout=dropout_rate,
                       recurrent_dropout=recurr_dropout_rate
                       ))
    model_rnn.add(BatchNormalization())

    model_rnn.add(LSTM(conf["lstm3"],
                       return_sequences=True,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       dropout=dropout_rate,
                       recurrent_dropout=recurr_dropout_rate
                       ))
    model_rnn.add(BatchNormalization())

    model_rnn.add(LSTM(conf["lstm4"],
                       return_sequences=False,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu)))
    model_rnn.add(BatchNormalization())

    model_rnn.add(Dense(conf["num_class"], activation='softmax'))
    return model_rnn


def lstm_phone_bidirect_1_layer(data_shape, conf):
    l2_regu = conf["l2_regu"]
    dropout_rate = conf["dropout"]
    recurr_dropout_rate = conf["recurrent_dropout"]

    model_rnn = Sequential()
    model_rnn.add(Masking(mask_value=0.0, input_shape=data_shape))
    model_rnn.add(BatchNormalization())

    model_rnn.add(Bidirectional(LSTM(
        conf["lstm1"],
        return_sequences=False,
        kernel_regularizer=regularizers.l2(l2_regu),
        recurrent_regularizer=regularizers.l2(l2_regu))))
    model_rnn.add(Dropout(dropout_rate))
    model_rnn.add(BatchNormalization())

    model_rnn.add(Dense(conf["num_class"], activation='softmax'))
    return model_rnn


def lstm_phone_bidirect_2_layers(data_shape, conf):
    l2_regu = conf["l2_regu"]
    dropout_rate = conf["dropout"]
    recurr_dropout_rate = conf["recurrent_dropout"]

    model_rnn = Sequential()
    model_rnn.add(Masking(mask_value=0.0, input_shape=data_shape))
    model_rnn.add(BatchNormalization())

    model_rnn.add(Bidirectional(LSTM(
        conf["lstm1"],
        return_sequences=True,
        kernel_regularizer=regularizers.l2(l2_regu),
        recurrent_regularizer=regularizers.l2(l2_regu))))
    model_rnn.add(Dropout(dropout_rate))
    model_rnn.add(BatchNormalization())

    model_rnn.add(Bidirectional(LSTM(
        conf["lstm2"],
        return_sequences=False,
        kernel_regularizer=regularizers.l2(l2_regu),
        recurrent_regularizer=regularizers.l2(l2_regu))))
    model_rnn.add(Dropout(dropout_rate))
    model_rnn.add(BatchNormalization())

    model_rnn.add(Dense(conf["num_class"], activation='softmax'))
    return model_rnn


def cnn_lstm(data_shape, conf):
    l2_regu = conf["l2_regu"]
    dropout_rate = conf["dropout"]
    recurr_dropout_rate = conf["recurrent_dropout"]

    model = Sequential()
    model.add(TimeDistributed(Conv1D(filters=conf["lstm1"], kernel_size=1),
                              input_shape=data_shape))
    model.add(TimeDistributed(MaxPooling1D(pool_size=2)))
    model.add(TimeDistributed(Flatten()))
    model.add(LSTM(conf["lstm1"],
                   kernel_regularizer=regularizers.l2(l2_regu),
                   recurrent_regularizer=regularizers.l2(l2_regu)
                   )
              )
    model.add(Dense(conf["num_class"], activation='softmax'))

    return model


def conv_lstm(data_shape, conf):
    model = Sequential()
    model.add(ConvLSTM2D(filters=conf["lstm1"], kernel_size=(1, 2),
                         input_shape=data_shape))
    model.add(Flatten())
    model.add(Dense(1))

    return model


# def lstm_phone_bidirect_3_layers(data_shape, conf):
#     l2_regu = conf["l2_regu"]
#
#     model_rnn = Sequential()
#     model_rnn.add(Masking(mask_value=0.0, input_shape=data_shape))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(Bidirectional(LSTM(
#         conf["lstm1"],
#         return_sequences=True,
#         kernel_regularizer=regularizers.l2(l2_regu),
#         recurrent_regularizer=regularizers.l2(l2_regu))))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(Bidirectional(LSTM(
#         conf["lstm2"],
#         return_sequences=True,
#         kernel_regularizer=regularizers.l2(l2_regu),
#         recurrent_regularizer=regularizers.l2(l2_regu))))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(Bidirectional(LSTM(
#         conf["lstm3"],
#         return_sequences=False,
#         kernel_regularizer=regularizers.l2(l2_regu),
#         recurrent_regularizer=regularizers.l2(l2_regu))))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(Dense(conf["num_class"], activation='softmax'))
#     return model_rnn

# def lstm_phone_bidirect_atten_2_layers(data_shape, conf):
#     l2_regu = conf["l2_regu"]
#
#     model_rnn = Sequential()
#     model_rnn.add(Masking(mask_value=0.0, input_shape=data_shape))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(Bidirectional(LSTM(
#         conf["lstm1"],
#         return_sequences=True,
#         kernel_regularizer=regularizers.l2(l2_regu),
#         recurrent_regularizer=regularizers.l2(l2_regu))))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(SeqSelfAttention())
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(Bidirectional(LSTM(
#         conf["lstm2"],
#         return_sequences=False,
#         kernel_regularizer=regularizers.l2(l2_regu),
#         recurrent_regularizer=regularizers.l2(l2_regu))))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(Dense(conf["num_class"], activation='softmax'))
#
#     return model_rnn

# def lstm_phone_atten_3_layers(data_shape, conf):
#     l2_regu = conf["l2_regu"]
#
#     model_rnn = Sequential()
#     model_rnn.add(Masking(mask_value=0.0, input_shape=data_shape))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(LSTM(conf["lstm1"],
#                        return_sequences=True,
#                        kernel_regularizer=regularizers.l2(l2_regu),
#                        recurrent_regularizer=regularizers.l2(l2_regu)))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(SeqSelfAttention())
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(LSTM(conf["lstm2"],
#                        return_sequences=True,
#                        kernel_regularizer=regularizers.l2(l2_regu),
#                        recurrent_regularizer=regularizers.l2(l2_regu)))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(SeqSelfAttention())
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(LSTM(conf["lstm3"],
#                        return_sequences=False,
#                        kernel_regularizer=regularizers.l2(l2_regu),
#                        recurrent_regularizer=regularizers.l2(l2_regu)))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(Dense(conf["num_class"], activation='softmax'))
#     return model_rnn
#
#
# def lstm_phone_atten_4_layers(data_shape, conf):
#     l2_regu = conf["l2_regu"]
#
#     model_rnn = Sequential()
#     model_rnn.add(Masking(mask_value=0.0, input_shape=data_shape))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(LSTM(conf["lstm1"],
#                        return_sequences=True,
#                        kernel_regularizer=regularizers.l2(l2_regu),
#                        recurrent_regularizer=regularizers.l2(l2_regu)))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(SeqSelfAttention())
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(LSTM(conf["lstm2"],
#                        return_sequences=True,
#                        kernel_regularizer=regularizers.l2(l2_regu),
#                        recurrent_regularizer=regularizers.l2(l2_regu)))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(SeqSelfAttention())
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(LSTM(conf["lstm3"],
#                        return_sequences=True,
#                        kernel_regularizer=regularizers.l2(l2_regu),
#                        recurrent_regularizer=regularizers.l2(l2_regu)))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(SeqSelfAttention())
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(LSTM(conf["lstm4"],
#                        return_sequences=False,
#                        kernel_regularizer=regularizers.l2(l2_regu),
#                        recurrent_regularizer=regularizers.l2(l2_regu)))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(Dense(conf["num_class"], activation='softmax'))
#     return model_rnn
#
#
# def lstm_phone_bidirect_atten_3_layers(data_shape, conf):
#     l2_regu = conf["l2_regu"]
#
#     model_rnn = Sequential()
#     model_rnn.add(Masking(mask_value=0.0, input_shape=data_shape))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(Bidirectional(LSTM(
#         conf["lstm1"],
#         return_sequences=True,
#         kernel_regularizer=regularizers.l2(l2_regu),
#         recurrent_regularizer=regularizers.l2(l2_regu))))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(SeqSelfAttention())
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(Bidirectional(LSTM(
#         conf["lstm2"],
#         return_sequences=True,
#         kernel_regularizer=regularizers.l2(l2_regu),
#         recurrent_regularizer=regularizers.l2(l2_regu))))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(SeqSelfAttention())
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(Bidirectional(LSTM(
#         conf["lstm3"],
#         return_sequences=False,
#         kernel_regularizer=regularizers.l2(l2_regu),
#         recurrent_regularizer=regularizers.l2(l2_regu))))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(Dense(conf["num_class"], activation='softmax'))
#
#     return model_rnn
#
#
# def lstm_phone_bidirect_atten_2_layers(data_shape, conf):
#     l2_regu = conf["l2_regu"]
#
#     model_rnn = Sequential()
#     model_rnn.add(Masking(mask_value=0.0, input_shape=data_shape))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(Bidirectional(LSTM(
#         conf["lstm1"],
#         return_sequences=True,
#         kernel_regularizer=regularizers.l2(l2_regu),
#         recurrent_regularizer=regularizers.l2(l2_regu))))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(SeqSelfAttention())
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(Bidirectional(LSTM(
#         conf["lstm2"],
#         return_sequences=False,
#         kernel_regularizer=regularizers.l2(l2_regu),
#         recurrent_regularizer=regularizers.l2(l2_regu))))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(Dense(conf["num_class"], activation='softmax'))
#
#     return model_rnn
#
#
# # bidirectional LSTM
# def lstm_phone_bidirect_4_layers(data_shape, conf):
#     l2_regu = conf["l2_regu"]
#
#     model_rnn = Sequential()
#     model_rnn.add(Masking(mask_value=0.0, input_shape=data_shape))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(Bidirectional(LSTM(
#         conf["lstm1"],
#         return_sequences=True,
#         kernel_regularizer=regularizers.l2(l2_regu),
#         recurrent_regularizer=regularizers.l2(l2_regu)), merge_mode=conf["mod"]))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(Bidirectional(LSTM(
#         conf["lstm2"],
#         return_sequences=True,
#         kernel_regularizer=regularizers.l2(l2_regu),
#         recurrent_regularizer=regularizers.l2(l2_regu)), merge_mode=conf["mod"]))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(Bidirectional(LSTM(
#         conf["lstm3"],
#         return_sequences=True,
#         kernel_regularizer=regularizers.l2(l2_regu),
#         recurrent_regularizer=regularizers.l2(l2_regu)), merge_mode=conf["mod"]))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(Bidirectional(LSTM(
#         conf["lstm4"],
#         return_sequences=False,
#         kernel_regularizer=regularizers.l2(l2_regu),
#         recurrent_regularizer=regularizers.l2(l2_regu)), merge_mode=conf["mod"]))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(Dense(conf["num_class"], activation='softmax'))
#     return model_rnn
#
#
# def lstm_phone_bidirect_3_layers(data_shape, conf):
#     l2_regu = conf["l2_regu"]
#
#     model_rnn = Sequential()
#     model_rnn.add(Masking(mask_value=0.0, input_shape=data_shape))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(Bidirectional(LSTM(
#         conf["lstm1"],
#         return_sequences=True,
#         kernel_regularizer=regularizers.l2(l2_regu),
#         recurrent_regularizer=regularizers.l2(l2_regu))))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(Bidirectional(LSTM(
#         conf["lstm2"],
#         return_sequences=True,
#         kernel_regularizer=regularizers.l2(l2_regu),
#         recurrent_regularizer=regularizers.l2(l2_regu))))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(Bidirectional(LSTM(
#         conf["lstm3"],
#         return_sequences=False,
#         kernel_regularizer=regularizers.l2(l2_regu),
#         recurrent_regularizer=regularizers.l2(l2_regu))))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(Dense(conf["num_class"], activation='softmax'))
#     return model_rnn
#
#
# def lstm_phone_bn(data_shape, conf):
#     l2_regu = conf["l2_regu"]
#
#     model_rnn = Sequential()
#     model_rnn.add(Masking(mask_value=0.0, input_shape=data_shape))
#     model_rnn.add(BatchNormalization()) # one more bn layer after input
#     model_rnn.add(LSTM(64, return_sequences=True,
#                         kernel_regularizer=regularizers.l2(l2_regu),
#                         recurrent_regularizer=regularizers.l2(l2_regu)))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(BatchNormalization())
#     model_rnn.add(LSTM(64, return_sequences=False,
#                         kernel_regularizer=regularizers.l2(l2_regu),
#                         recurrent_regularizer=regularizers.l2(l2_regu)))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(Dense(conf["num_class"], activation='softmax'))
#     return model_rnn
#
#
# # I forgot putting the mask until now!
# def simple_lstm_mask(data_shape, conf):
#     l2_regu = conf["l2_regu"]
#
#     model_rnn = Sequential()
#     model_rnn.add(Masking(mask_value=0.0, input_shape=data_shape))
#     model_rnn.add(LSTM(64))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(BatchNormalization())
#     model_rnn.add(Dense(conf["num_class"], activation='softmax'))
#     return model_rnn
#
#
# def simple_lstm_mask_constra(data_shape, conf):
#     l2_regu = conf["l2_regu"]
#
#     model_rnn = Sequential()
#     model_rnn.add(Masking(mask_value=0.0, input_shape=data_shape))
#     model_rnn.add(LSTM(64, return_sequences=False,
#                        kernel_constraint=constraints.max_norm(3.0),
#                        recurrent_constraint=constraints.max_norm(3.0)))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(BatchNormalization())
#     model_rnn.add(Dense(conf["num_class"], activation='softmax'))
#     return model_rnn
#
#
# # Mimicing the models from Marie's phoenem classification
# def lstm_phone(data_shape, conf):
#     l2_regu = conf["l2_regu"]
#
#     model_rnn = Sequential()
#     model_rnn.add(Masking(mask_value=0.0, input_shape=data_shape))
#     model_rnn.add(LSTM(conf["lstm1"],
#                        return_sequences=True,
#                        kernel_regularizer=regularizers.l2(l2_regu),
#                        recurrent_regularizer=regularizers.l2(l2_regu)))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(BatchNormalization())
#     model_rnn.add(LSTM(conf["lstm2"],
#                        return_sequences=False,
#                        kernel_regularizer=regularizers.l2(l2_regu),
#                        recurrent_regularizer=regularizers.l2(l2_regu)))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(Dense(conf["num_class"], activation='softmax'))
#     return model_rnn
#
#
# # finalized vanilla LSTM models w/ masking
# def lstm_phone_5_layers(data_shape, conf):
#     l2_regu = conf["l2_regu"]
#
#     model_rnn = Sequential()
#     model_rnn.add(Masking(mask_value=0.0, input_shape=data_shape))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(LSTM(conf["lstm1"],
#                        return_sequences=True,
#                        kernel_regularizer=regularizers.l2(l2_regu),
#                        recurrent_regularizer=regularizers.l2(l2_regu)))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(LSTM(conf["lstm2"],
#                        return_sequences=True,
#                        kernel_regularizer=regularizers.l2(l2_regu),
#                        recurrent_regularizer=regularizers.l2(l2_regu)))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(LSTM(conf["lstm3"],
#                        return_sequences=True,
#                        kernel_regularizer=regularizers.l2(l2_regu),
#                        recurrent_regularizer=regularizers.l2(l2_regu)))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(LSTM(conf["lstm4"],
#                        return_sequences=True,
#                        kernel_regularizer=regularizers.l2(l2_regu),
#                        recurrent_regularizer=regularizers.l2(l2_regu)))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(LSTM(conf["lstm5"],
#                        return_sequences=False,
#                        kernel_regularizer=regularizers.l2(l2_regu),
#                        recurrent_regularizer=regularizers.l2(l2_regu)))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(BatchNormalization())
#
#     model_rnn.add(Dense(conf["num_class"], activation='softmax'))
#     return model_rnn
#
#
# def rnn_lstm_constraints(data_shape, conf):
#     l2_regu = conf["l2_regu"]
#
#     model_rnn = Sequential()
#     # shape: time steps, features
#     model_rnn.add(LSTM(64, return_sequences=True,
#                         kernel_constraint=constraints.max_norm(3.0),
#                         recurrent_constraint=constraints.max_norm(3.0),
#                         input_shape=data_shape))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(LSTM(32, kernel_constraint=constraints.max_norm(3.0),
#                         recurrent_constraint=constraints.max_norm(3.0)))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(Dense(conf["num_class"], activation='softmax'))
#
#     return model_rnn
#
#
# def rnn_lstm(data_shape, conf):
#     model_rnn = Sequential()
#     # shape: time steps, features
#     model_rnn.add(LSTM(conf["lstm1"], return_sequences=True, input_shape=data_shape))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(LSTM(conf["lstm2"], return_sequences=False))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(Dense(conf["num_class"], activation='softmax'))
#     print(model_rnn.summary())
#
#     return model_rnn
#
#
# def rnn_lstm_regular(data_shape, conf):
#     l2_regu = conf["l2_regu"]
#     model_rnn = Sequential()
#     model_rnn.add(LSTM(conf["lstm1"], return_sequences=True,
#                         kernel_regularizer=regularizers.l2(l2_regu),
#                         recurrent_regularizer=regularizers.l2(l2_regu),
#                         input_shape=data_shape))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(LSTM(conf["lstm2"], kernel_regularizer=regularizers.l2(l2_regu),
#                         recurrent_regularizer=regularizers.l2(l2_regu)))
#     model_rnn.add(Dropout(0.5))
#     model_rnn.add(Dense(conf["num_class"], activation='softmax'))
#     print(model_rnn.summary())
#
#     return model_rnn
