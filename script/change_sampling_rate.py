#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Created on 4/15/20
@author: atoultaro
"""
import os

sound_dir_samplerate_orig = '/home/ys587/__Data/__NOPP6/__samplerate_96k'
sound_dir_samplerate_new = '/home/ys587/__Data/__NOPP6/__samplerate_2k_wavfix'
day_folder = os.listdir(sound_dir_samplerate_orig)

day_folder.sort()
for ss in day_folder:
    ss_fullpath = os.path.join(sound_dir_samplerate_orig, ss)
    print(ss_fullpath)

    ss_fullpath_out = os.path.join(sound_dir_samplerate_new, ss)
    if not os.path.exists(ss_fullpath_out):
        os.mkdir(ss_fullpath_out)

    input_file_list = os.listdir(ss_fullpath)
    # print(encounter_file_list)

    # for ff in encounter_file_list[0]:
    input_file_list.sort()
    for ff in input_file_list:
        print(ff)
        file_target = os.path.join(ss_fullpath, ff)
        ff2 = os.path.splitext(ff)[0]+'.wav'
        print(ff2)
        file_target_out = os.path.join(ss_fullpath_out, ff2)
        print(file_target)
        print(file_target_out)

        sox_command = 'sox "'+file_target+'" -t wavpcm "'+file_target_out+'" rate 2000'
        print(sox_command)
        os.system(sox_command)


