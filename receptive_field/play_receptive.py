#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Created on 2/7/20
@author: atoultaro
"""

import receptive_field as rf
import tensorflow as tf

from species_classifier.driver_context_conv_audio_4fold import resnet_cifar10_expt

# Construct graph.
g = tf.Graph()
with g.as_default():
  images = tf.placeholder(tf.float32, shape=(1, None, None, 3), name='input_image')
  resnet_cifar10_expt(images)

# Compute receptive field parameters.
rf_x, rf_y, eff_stride_x, eff_stride_y, eff_pad_x, eff_pad_y = \
  rf.compute_receptive_field_from_graph_def( g.as_graph_def(), 'input_image', 'my_output_endpoint')