'''
Created on Dec 2, 2017

@author: mroch
'''

from sklearn.model_selection import StratifiedKFold
import numpy as np

# Local imports
from dsp.utils import Timer
from .buildmodels import build_model

class CrossValidator:

    debug = False
    
    # If not None, N-fold cross validation will abort after processing N folds
    abort_after_N_folds = 1
    
    def __init__(self, groups, model_spec, model_train_eval,
                 n_folds=10, batch_size=100, epochs=100,
                 saveroot=None, classnames=None):
        """CrossValidator
        Perfor N-fold cross validation
        :param groups:  namedtuple specifying:
            # classes - Dictionary of class names and number to which they are assigned
            # ids - List of group numbers
            # group_examples - List where each item is a list of examples in the
            #    group.
            # group_labels - Category of each group
                    into examples
        :param model_spec:  Keras model specification as list
        :model_train_eval: function to train/evaluate one fold
        Must conform to an interface that expects the following
            arguments:
                examples - list or tensor of examples
                labels - categories corresponding to examples
                train_idx - indices of examples, labels to be used
                    for training
                test_idx - indices of examples, labels to be used to
                    evaluate the model
                model - keras network to be used
                batch_size - # examples to process per batch
                epochs - Number of passes through training data
                name - test name
                saveroot - Location to save data
        :param n_folds: Number of cross-validation folds
        :param batch_size: Number of examples per batch
        :param epochs: Number of passes through data for training
        :param saveroot:  Root filename for saving information related to this
             experiment
        :param classnames:  Human readable list of names corresponding
             to the human class list.
        """
        
        # Create a plan for k-fold testing with shuffling of examples
        # http://scikit-learn.org/stable/modules/generated/sklearn.model_selection.StratifiedKFold.html    #
        kfold = StratifiedKFold(n_folds, shuffle=True)
        self.saveroot = saveroot

        foldidx = 0
        errors  = np.zeros([n_folds, 1])
        models = []
        losses = []
        timer = Timer()

        # Compute start and end indices for each group
        starts_ends = np.concatenate(
            (np.zeros((1,), dtype="int"),
             np.cumsum([len(n) for n in groups.examples], dtype="int")),
            axis=0)

        # Merge all examples
        examples = []
        for exampgrp in groups.examples:
            examples.extend(exampgrp)
        # Generate per example labels
        labels = np.zeros(int(starts_ends[-1]), dtype="int")
        for idx in range(len(groups.ids)):
            labels[starts_ends[idx]:starts_ends[idx+1]] = groups.labels[idx]


        for (train_grp, test_grp) in kfold.split(groups.ids, groups.labels):
            # Convert groups to indices
            train_idx = []
            test_idx = []
            for grp in train_grp:
                train_idx.extend(
                    [exidx
                     for exidx in range(starts_ends[grp], starts_ends[grp+1])])
            for grp in test_grp:
                test_idx.extend(
                    [exidx
                     for exidx in range(starts_ends[grp], starts_ends[grp+1])])

            # Construct examples
            name="f{}".format(foldidx)
            model = build_model(model_spec, name=name)

            (errors[foldidx], model, loss) = \
                model_train_eval(
                    examples, labels, train_idx, test_idx, model,
                    batch_size, epochs,
                    logroot=self.saveroot, name=name, classnames=classnames)
            models.append(model)
            losses.append(loss)
            print(
                "Fold {} error {}, cumulative cross-validation time {}".format(
                    foldidx, errors[foldidx], timer.elapsed()))
            foldidx = foldidx + 1

            # Useful for debugging an architecture
            if self.abort_after_N_folds is not None:
                if foldidx >= self.abort_after_N_folds:
                    break                     
                    
        # Show architecture of last model (all are the same)    
        print("Model summary\n{}".format(model.summary()))
        
        print("Fold errors:  {}".format(errors))
        print("Mean error {} +- {}".format(np.mean(errors), np.std(errors)))
        
        print("Experiment time: {}".format(timer.elapsed()))
        
        self.errors = errors
        self.models = models
        self.losses = losses


    @classmethod
    def extend_list_by_range(cls, l, lower, upper):
        """extend_list_by_range(l, lower, upper)
        Extend list l with [lower, lower+1, lower+2, ..., upper-2, upper-1]
        """
        l.extend([r for r in range(lower, upper)])

    def get_models(self):
        "get_models() - Return list of models created by cross validation"
        return self.models
    
    def get_errors(self):
        "get_errors - Return list of error rates from each fold"
        return self.errors
    
    def get_losses(self):
        "get_losses - Return list of loss histories associated with each model"
        return self.losses
                  
