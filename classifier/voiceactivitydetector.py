'''
Created on Nov 29, 2017

@author: mroch
'''

from sklearn.mixture import GaussianMixture
import numpy as np

from dsp.audioframes import AudioFrames
from dsp.multifileaudioframes import MultiFileAudioFrames
from dsp.rmsstream import RMSStream
 
class UnsupervisedVAD:
    """
    UnsupervisedVAD
    Unsupervised voice activity detector
    
    Uses a GMM to learn a two class distribution of RMS energy vectors
    
    # Arguments
    files - List of audio files from which to learn
    adv_ms - frame advance (milliseconds)
    len_ms - frame length (milliseconds)
    """
    Nclasses = 2    # two classes, speech/noise
        
    def __init__(self, files, adv_ms=10, len_ms=20):

        # Save framing parameters for later
        self.adv_ms = adv_ms
        self.len_ms = len_ms
        
        # Collect all RMS frames
        self.rms = []
        a = MultiFileAudioFrames(files, adv_ms, len_ms)
        rms = [energy for energy in RMSStream(a)]
        
        # Assemble into format the GMM can use and train the GMM
        
        train = np.array(rms)
        train = train.reshape([-1, 1])
    
        self.gmm = GaussianMixture(self.Nclasses)
        self.gmm.fit(train)

        # Determine which mixture was which  
        self.mixture_labels = dict()      
        self.mixture_labels["noise"] = np.argmin(self.gmm.means_)
        self.mixture_labels["speech"] = np.argmax(self.gmm.means_)
        
    def classify(self, file):
        """classify - Extract RMS from file using the same
        framing parameters as the constructor and return vector
        of booleans where True indicates that speech occurred
        """
        
        a = AudioFrames(file, self.adv_ms, self.len_ms)
        rms = np.array([r for r in RMSStream(a)])
        rms = rms.reshape([-1, 1])
        
        # Get mixture membership predictions
        decisions = self.gmm.predict(rms)
        # Convert to speech predicate vector
        speech = decisions == self.mixture_labels["speech"]
        
        return speech
        
            
    