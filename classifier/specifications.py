
import inspect

# Add-on modules
import keras.layers
from keras.layers import Input, Dense, Dropout, LSTM, Masking, \
    TimeDistributed, Concatenate, Flatten
from keras import Model
from keras.layers.normalization import BatchNormalization

from keras import regularizers

def build_model(name, parameters):
    """build_models(name, parameters)
    Given a model name that is in this modules generators dictionary,
    create a model specification to train.  It is expected that the named
    arguments in each generator pattern are are present in the parameter
    dictionary and will be used to populate the generator call.

    All generator patterns are expected to have arguments input_nodes and
    output_nodes.
    """

    try:
        gen = generators[name]
    except KeyError:
        raise ValueError("No such model architecture.  Valid names:%s"%(
            " ".join(generators.keys())))

    # Find out the parameter signature expected by the architecture
    sig = inspect.signature(gen)
    variables = sig.parameters.keys()

    # Build up the argument list
    missing = []
    args =[]
    for v in variables:
        try:
            args.append(parameters[v])
        except KeyError:
            missing.append(v)
    if len(missing) > 0:
        raise ValueError("Model %s ")

    model_spec = gen(*args)
    return model_spec

def lstmfuse(time_input_dim, feat_vec_dim, output_nodes,
             lstm_width=20, ff_width=20,
             dropout=0.0, l2=0.01, ):
    """
    lstmfuse - Dual input model.  Takes a combination of time series data
    that are processed wth sequence-to-value models and fused with a static
    feature vector to make a final decision
    :param time_input_dim:  Dimension of time varying features
    :param feat_vec_dim:  Dimension of static feature vector
    :param output_nodes:  Number of output classes
    :param lstm_width:  Width of LSTM network
    :param ff_width: Width for feed-forward fusion network
    :param dropout: dropout rate used throughout the network
    :param l2:  L2 penalty
    :return: Keras model
    """
    # Time varying features
    vis_timeseries = Input([None, time_input_dim], name="vis_timeseries")
    mask = Masking(mask_value=0)(vis_timeseries)

    # Masking layer feeds into the forward and backwards layers
    fseq = LSTM(lstm_width, return_sequences=True,
                    kernel_regularizer=regularizers.l2(l2),
                    recurrent_regularizer=regularizers.l2(l2), name="rnn-forward")
    fseqout = fseq(mask)
    bseq = LSTM(lstm_width, return_sequences=True,
                    kernel_regularizer=regularizers.l2(l2),
                    recurrent_regularizer=regularizers.l2(l2),
                    go_backwards=True,
                    name="rnn-backwards")
    bseqout = bseq(mask)

    # Concatenate forward and backwards layers
    fbmerge = Concatenate(axis=-1, name="forward-backward")([fseqout, bseqout])

    # Feed the forward-backward sequence into a many to one sequence
    fbseq = LSTM(lstm_width, return_sequences=False,
                 kernel_regularizer=regularizers.l2(l2),
                 recurrent_regularizer=regularizers.l2(l2), name="rnn-bidir")
    fbseqout = fbseq(fbmerge)


    # statistic features
    vis_stat = Input([feat_vec_dim], name="vis_stat")

    # Concatenate the forward-backward seqeuence with the static
    # statistical features
    fused_input = Concatenate(name="fused_features")([vis_stat,
                                                      Flatten(fbseqout)])

    # feed forward section of network
    ff1 = Dense(ff_width,
               kernel_regularizer=regularizers.l2(l2))(fused_input)
    ff2 = Dense(ff_width,
               kernel_regularizer=regulairzers.l2(l2))(ff1)

    classify = Dense(output_nodes, activation='softmax',
                     kernel_regularizer=regularizers.l2(l2))(ff2)

    model= Model([vis_timeseries, vis_stat], classify)
    return model





generators = {

    #  long-short time memory network with masking, dropout, and batchnorm
    "LSTM" : lambda input_nodes, layer_width, dropout, l2, output_nodes : [
         (Masking, [], {"mask_value":0.,
                       "input_shape":[None, input_nodes]}),

         (LSTM, [layer_width], {
             "return_sequences":True,
             "kernel_regularizer":regularizers.l2(l2),
             "recurrent_regularizer":regularizers.l2(l2),
             }),
         (Dropout, [dropout], {}),
         (BatchNormalization, [], {}),
         (LSTM, [layer_width], {
             "return_sequences":False,
             "kernel_regularizer":regularizers.l2(l2),
             "recurrent_regularizer":regularizers.l2(l2),
             }),
         (Dropout, [dropout], {}),
         (BatchNormalization, [], {}),
         (Dense, [layer_width], {'kernel_regularizer':regularizers.l2(l2)}),
         (Dropout, [dropout], {}),
         (Dense, [layer_width], {'kernel_regularizer':regularizers.l2(l2)}),
         (Dropout, [dropout], {}),
         (Dense, [output_nodes], {'activation':'softmax',
                             'kernel_regularizer':regularizers.l2(l2)}),
    ],
    "harrymodel" : lstmfuse


}