'''
Created on Sep 30, 2017

@author: mroch
'''

import math
import time
import os

import numpy as np

from keras.callbacks import TensorBoard
from keras.utils import np_utils
from keras import metrics

import keras.backend as K

from sklearn.utils import class_weight

from dsp_whistle.utils import Timer

from .batchgenerator import PaddedBatchGenerator
from .histories import ErrorHistory, LossHistory

# local modules
from .confusion import ConfusionTensorBoard, plot_confusion




def train_and_evaluate(examples, labels, train_idx, test_idx, 
                              model, batch_size=100, epochs=100, 
                              logroot=None, name="model",
                              classnames=None):
    """train_and_evaluate__model(examples, labels, train_idx, test_idx,
            model_spec, batch_size, epochs)
            
    Given:
        examples - List of examples in column major order
            (# of rows is feature dim)
        labels - list of corresponding labels
        train_idx - list of indices of examples and labels to be learned
        test_idx - list of indices of examples and labels of which
            the system should be tested.
        model_spec - Model specification, see feed_forward_model
            for details and example
    Optional arguments
        batch_size - size of minibatch
        epochs - # of epochs to compute
        logroot - Where to save data
        name - model name
        classnames - Human readable list of class names for reporting error
    Returns error rate, model, and loss history over training
    """

    # Convert labels to a one-hot vector
    # https://keras.io/utils/#to_categorical
    onehotlabels = np_utils.to_categorical(labels)

    train_classes = np.unique(labels[train_idx])
    train_classesN = train_classes.shape[0]
    # Compute class weights for inbalanced data
    lossweight = class_weight.compute_class_weight('balanced',
                np.arange(train_classesN), labels[train_idx])

    # If caller did not provide human readable names, generate numeric labels
    # for each class
    if classnames is None:
        classnames = ["%d"%(idx) for idx in range(train_classesN)]

    # Get dimension of model
    dim = examples[0].shape[1]

    error = ErrorHistory()
    loss = LossHistory()
    
    model.compile(optimizer = "Adam",
                  #loss = seq_loss,  
                  loss = "categorical_crossentropy",
                  metrics = [metrics.categorical_accuracy])
    
    model.summary()  # display
    
    examplesN = len(train_idx)  # Number training examples
    # Approx # of times fit generator must be called to complete an epoch
    steps = int(math.ceil(examplesN / batch_size))  

    
    # for debugging
    log_dir = "{}/{}_{}".format(logroot, name, time.strftime('%d%b-%H%M'))
    model_name = os.path.join(log_dir, "model.hd5")
    tensorboard = TensorBoard(
        log_dir=log_dir,
        histogram_freq=1,
        write_graph=True,
        write_grads=True
        )

    # TensorBoard confusion matrices
    confusion = ConfusionTensorBoard(log_dir, classnames,
                                     K.get_session())
    confusion.add_callbacks(model)  # fetch labels/outputs

    
    # Evaluation data
    # Reformat examples into zero-padded numpy array
    # Let PaddedBatchGenerator do the work, generating
    # one single "batch."
    # testgen = PaddedBatchGenerator(examples[test_idx],
    #                               onehotlabels[test_idx],
    #                               batch_size=len(test_idx))
    testgen = PaddedBatchGenerator([examples[tt] for tt in test_idx],
                                   onehotlabels[test_idx, :],
                                   batch_size=len(test_idx))
    (testexamples, testlabels) = next(testgen)
    # testexamples = np.squeeze(testexamples0) # [num_samples, num_dim, 1], remove the 1

    pad_batches_individually = False

    if pad_batches_individually:
        # Generator function to produce standardized length training
        # sequences for each batch
        # generator = PaddedBatchGenerator(examples[train_idx],
        #                                 onehotlabels[train_idx],
        #                                 batch_size=batch_size)
        generator = PaddedBatchGenerator([examples[tt] for tt in train_idx],
                                         onehotlabels[train_idx,:],
                                         batch_size=batch_size)

        model.fit_generator(generator, steps_per_epoch=steps,
                            epochs=epochs,
                            callbacks=[loss, error, tensorboard, confusion],
                            class_weight=lossweight,
                            validation_data=(testexamples, testlabels))
    else:
        generator = PaddedBatchGenerator([examples[tt] for tt in train_idx],
                                         onehotlabels[train_idx],
                                         batch_size=len(train_idx))
        (trainexamples, trainlabels) = next(generator)
        model.fit(trainexamples, trainlabels,
                  epochs=epochs,
                  callbacks=[loss, error, tensorboard, confusion],
                  class_weight=lossweight,
                  batch_size=batch_size,
                  validation_data=(testexamples, testlabels))
        # Noticed that providing tensorboard in the callback list
        # callbacks=[loss, tensorboard]
        # causes an error on the second model.  This appears to be
        # related to something in the previous model no longer existing.
        # It is unclear why this is not happening 
        

    model.save(model_name)   # write out the model

    print("Training loss %s"%(["%f"%(loss) for loss in loss.losses]))
    
    result = model.evaluate(testexamples, testlabels,
                            verbose=False)

    return (1 - result[1], model, loss) 
