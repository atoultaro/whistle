#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Created on 3/6/20
@author: atoultaro
"""
import os
import pandas as pd
import numpy as np

import librosa
import soundfile as sf
import sox

# label_file = '/home/ys587/__Data/__whistle/__whistle_mann/label_short.txt'
label_file = '/home/ys587/__Data/__whistle/__whistle_mann/label_orig.txt'
sound_out_dir = '/home/ys587/__Data/__whistle/__whistle_mann/sound_48k'
target_samplerate = 48000

label_df = pd.read_csv(label_file)
lab = np.zeros((label_df.shape[0]))
for ii, row in label_df.iterrows():
    print(str(row.score) + ' ' + str(row.label))
    # samples0, _ = librosa.load(row.path, sr=target_samplerate, offset=0.0,
    #                            duration=1.0)

    if row.label == 1:  # save to wav file of 48k samplerate if a while
        sound_file_out = os.path.join(sound_out_dir, os.path.basename(row.path))
        # sf.write(sound_file_out, samples0, target_samplerate)
        tfm = sox.Transformer()
        tfm.convert(samplerate=target_samplerate)
        tfm.build(row.path, sound_file_out)

    elif row.label == 0:  # augmentation!
        filename, ext = os.path.splitext(os.path.basename(row.path))
        sound_count = 0

        # Add echo
        tfm0 = sox.Transformer()
        tfm0.convert(samplerate=target_samplerate)
        tfm0.echo(1.0, 1.0, 2, [5, 50], [0.2, 0.2])
        tfm0.trim(0.0, 1.0)  # have one second for Mann's whistles
        sound_file_out = os.path.join(sound_out_dir, filename+'_'+str(sound_count)+'.wav')
        tfm0.build(row.path, sound_file_out)
        sound_count += 1

        # Add pitch
        tfm1 = sox.Transformer()
        tfm1.convert(samplerate=target_samplerate)
        tfm1.pitch(10)
        sound_file_out = os.path.join(sound_out_dir, filename+'_'+str(sound_count)+'.wav')
        tfm1.build(row.path, sound_file_out)
        sound_count += 1

        # Add echo
        tfm2 = sox.Transformer()
        tfm2.deemph()
        tfm2.convert(samplerate=target_samplerate)
        sound_file_out = os.path.join(sound_out_dir, filename+'_'+str(sound_count)+'.wav')
        tfm2.build(row.path, sound_file_out)
        sound_count += 1

    else:
        print('This label has a value unexpected: '+str(row.label))
        continue


