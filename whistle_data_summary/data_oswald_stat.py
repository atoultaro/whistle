#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Created on 3/2/20
@author: atoultaro
"""
import os
import re
import glob

import numpy as np
import soundfile as sf
import pandas as pd

data_dir = '/home/ys587/__Data/__whistle/__whistle_oswald'
# deploy_name = os.listdir(data_dir)
# deploy_name = ['HICEAS2002', 'PICEAS2005', 'STAR2000', 'STAR2003', 'STAR2006']
deploy_name = ['HICEAS2002', 'PICEAS2005', 'STAR2003', 'STAR2006']
# deploy_name = ['STAR2003']
species_name = ['spotted', 'spinner', 'striped', 'bottlenose', 'roughtoothed',
                'longbeaked_common', 'shortbeaked_common', 'pilot',
                'false_killer']

species_file = dict()  # deploy_name to sound_file_list

meta_cruise = []
meta_species = []
meta_encounter_name = []
meta_encounter_path = []
meta_duration = []
meta_samplerate = []
meta_samplerate_count = []

for dd in deploy_name:
    deploy_dir = os.path.join(data_dir, dd)
    print(deploy_dir)

    sound_folder_list = os.listdir(deploy_dir)
    for ss in sound_folder_list:
        try:
            print(ss)
            meta_cruise.append(dd)

            m = re.search('^([a-z_]+)\s((s|a)\d{2,4})$', ss)
            species_curr = m.groups()[0]
            meta_species.append(species_curr)
            print(species_curr)
            encounter_curr = m.groups()[1]
            meta_encounter_name.append(encounter_curr)
            print(encounter_curr)
        except:
            print('ERROR: ')
            print(ss)
            continue

        encounter_path = os.path.join(deploy_dir, ss)
        meta_encounter_path.append(encounter_path)
        # file_list = os.listdir(encounter_path)
        file_list = glob.glob(encounter_path+'/*.wav')
        # in each encounter,
        meta_file_samplerate = []
        meta_file_duration = []
        for ff in file_list:
            print(ff)
            # sound_meta = sf.info(os.path.join(encounter_path, ff))
            sound_meta = sf.info(ff)
            meta_file_samplerate.append(sound_meta.samplerate)
            meta_file_duration.append(sound_meta.duration)
        meta_duration.append(np.array(meta_file_duration).sum())
        meta_samplerate.append(max(set(meta_file_samplerate), key=meta_file_samplerate.count))
        meta_samplerate_count.append(len(set(meta_file_samplerate)))
    print()

    # dataframe for whistle information; one row for each acoustic encounter
    df_whistle = pd.DataFrame(list(zip(meta_encounter_name,
                          meta_species,
                          meta_duration,
                          meta_samplerate,
                          meta_encounter_path,
                          meta_samplerate_count)),
                 columns=['encounter', 'species', 'duration', 'samplerate',
                          'path', 'samplerate_count'])

    # encounter for each species
    species_encounter = dict()
    for ss in species_name:
        df_species = df_whistle.loc[df_whistle['species'] == ss]
        species_encounter.update({ss: df_species['encounter'].shape[0]})

    # duration for each species
    species_duration = dict()
    for ss in species_name:
        df_species = df_whistle.loc[df_whistle['species'] == ss]
        species_duration.update({ss: df_species['duration'].sum()})




