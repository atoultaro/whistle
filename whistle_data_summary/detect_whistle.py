#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Created on 3/5/20
@author: atoultaro
"""
import os
import numpy as np
import pandas as pd

from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.metrics import confusion_matrix

from keras.models import Model
from keras.layers import Flatten, Dense, Input, Conv2D, MaxPooling2D, GlobalAveragePooling2D, GlobalMaxPooling2D
from keras.engine.topology import get_source_inputs
from keras import backend as K

import librosa
from sklearn import svm

# weight path
model_path = '/home/ys587/PycharmProjects/python36_tensorflow12/whistle_data_summary/'
WEIGHTS_PATH = os.path.join(model_path, 'vggish_audioset_weights_without_fc2.h5')
WEIGHTS_PATH_TOP = os.path.join(model_path, 'vggish_audioset_weights.h5')

EMBEDDING_SIZE = 128
NUM_FRAMES = 96  # instead of 496
NUM_BANDS = 64


def VGGish(load_weights=True, weights='audioset',
           input_tensor=None, input_shape=None,
           out_dim=None, include_top=True, pooling='avg'):
    '''
    An implementation of the VGGish architecture.
    :param load_weights: if load weights
    :param weights: loads weights pre-trained on a preliminary version of YouTube-8M.
    :param input_tensor: input_layer
    :param input_shape: input data shape
    :param out_dim: output dimension
    :param include_top:whether to include the 3 fully-connected layers at the top of the network.
    :param pooling: pooling type over the non-top network, 'avg' or 'max'

    :return: A Keras model instance.
    '''

    if weights not in {'audioset', None}:
        raise ValueError('The `weights` argument should be either '
                         '`None` (random initialization) or `audioset` '
                         '(pre-training on audioset).')

    if out_dim is None:
        out_dim = EMBEDDING_SIZE

    # input shape
    if input_shape is None:
        input_shape = (NUM_FRAMES, NUM_BANDS, 1)

    if input_tensor is None:
        aud_input = Input(shape=input_shape, name='input_1')
    else:
        if not K.is_keras_tensor(input_tensor):
            aud_input = Input(tensor=input_tensor, shape=input_shape,
                              name='input_1')
        else:
            aud_input = input_tensor

    # Block 1
    x = Conv2D(64, (3, 3), strides=(1, 1), activation='relu', padding='same',
               name='conv1')(aud_input)
    x = MaxPooling2D((2, 2), strides=(2, 2), padding='same', name='pool1')(x)

    # Block 2
    x = Conv2D(128, (3, 3), strides=(1, 1), activation='relu', padding='same',
               name='conv2')(x)
    x = MaxPooling2D((2, 2), strides=(2, 2), padding='same', name='pool2')(x)

    # Block 3
    x = Conv2D(256, (3, 3), strides=(1, 1), activation='relu', padding='same',
               name='conv3/conv3_1')(x)
    x = Conv2D(256, (3, 3), strides=(1, 1), activation='relu', padding='same',
               name='conv3/conv3_2')(x)
    x = MaxPooling2D((2, 2), strides=(2, 2), padding='same', name='pool3')(x)

    # Block 4
    x = Conv2D(512, (3, 3), strides=(1, 1), activation='relu', padding='same',
               name='conv4/conv4_1')(x)
    x = Conv2D(512, (3, 3), strides=(1, 1), activation='relu', padding='same',
               name='conv4/conv4_2')(x)
    x = MaxPooling2D((2, 2), strides=(2, 2), padding='same', name='pool4')(x)

    if include_top:
        # FC block
        x = Flatten(name='flatten_')(x)
        x = Dense(4096, activation='relu', name='vggish_fc1/fc1_1')(x)
        x = Dense(4096, activation='relu', name='vggish_fc1/fc1_2')(x)
        x = Dense(out_dim, activation='relu', name='vggish_fc2')(x)
    else:
        if pooling == 'avg':
            x = GlobalAveragePooling2D()(x)
        elif pooling == 'max':
            x = GlobalMaxPooling2D()(x)

    if input_tensor is not None:
        inputs = get_source_inputs(input_tensor)
    else:
        inputs = aud_input
    # Create model.
    model = Model(inputs, x, name='VGGish')

    # load weights
    if load_weights:
        if weights == 'audioset':
            if include_top:
                model.load_weights(WEIGHTS_PATH_TOP)
            else:
                model.load_weights(WEIGHTS_PATH)
        else:
            print("failed to load weights")

    return model


def loading_data(label_df, sound_extractor, sample_rate=48000, time_reso=0.01):

    hop_size = int(time_reso*sample_rate)
    data = np.zeros((label_df.shape[0], NUM_FRAMES, NUM_BANDS, 1))
    lab = np.zeros((label_df.shape[0]))
    for ii, row in label_df.iterrows():
        print(str(row.score)+' '+str(row.label))
        samples0, _ = librosa.load(row.path, sr=sample_rate, offset=0.0,
                                   duration=1.0)
        whistle_freq0 = np.abs(
            librosa.pseudo_cqt(samples0, sr=sample_rate, hop_length=hop_size,
                               fmin=3000.0, bins_per_octave=24, n_bins=64))
        # print(whistle_freq0.shape)
        whistle_freq = whistle_freq0[:, :NUM_FRAMES]
        whistle_freq = np.expand_dims(whistle_freq.T, 2)
        data[ii, :, :, :] = whistle_freq
        lab[ii] = row.label

    fea = sound_extractor.predict(data)

    return fea, lab
    # lines = linecache.getlines(files)
    # sample_num = len(lines)
    # seg_num = 60
    # seg_len = 5  # 5s
    # data = np.zeros((seg_num * sample_num, 496, 64, 1))
    # label = np.zeros((seg_num * sample_num,))

    # for i in range(len(lines)):
    #     sound_file = '/mount/hudi/moe/sound_dataset/dcase/' + lines[i][:-7]
    #     sr, wav_data = wavfile.read(sound_file)
    #
    #     length = sr * seg_len           # 5s segment
    #     range_high = len(wav_data) - length
    #     seed(1)  # for consistency and replication
    #     random_start = randint(range_high, size=seg_num)
    #
    #     for j in range(seg_num):
    #         cur_wav = wav_data[random_start[j]:random_start[j] + length]
    #         cur_wav = cur_wav / 32768.0
    #         cur_spectro = preprocess_sound(cur_wav, sr)
    #         cur_spectro = np.expand_dims(cur_spectro, 3)
    #         data[i * seg_num + j, :, :, :] = cur_spectro
    #         label[i * seg_num + j] = lines[i][-2]

    # data = sound_extractor.predict(data)
    #


if __name__ == '__main__':
    sound_model = VGGish(include_top=True, load_weights=True)
    print(sound_model.summary())
    # x = sound_model.get_layer(name="conv4/conv4_2").output
    # output_layer = GlobalAveragePooling2D()(x)
    # sound_extractor = Model(input=sound_model.input, output=output_layer)
    sound_extractor = sound_model
    del sound_model

    label_file = '/home/ys587/__Data/__whistle/__whistle_mann/whistle_mann_label.txt'
    # label_file = '/home/ys587/__Data/__whistle/__whistle_mann/label_short.txt'
    label_df = pd.read_csv(label_file)
    # label_df_train = label_df.iloc[:400]
    # label_df_test = label_df.iloc[400:]

    feature, label = loading_data(label_df, sound_extractor)
    # X_train, X_test, y_train, y_test = \
    #     train_test_split(feature, label, test_size=0.33, random_state=42)

    feature_SS = StandardScaler().fit_transform(feature)
    # feature_SS = MinMaxScaler().fit_transform(feature)

    pca = PCA(n_components=10)
    feature_pca = pca.fit_transform(feature_SS)
    X_train, X_test, y_train, y_test = \
        train_test_split(feature_pca, label, test_size=0.33, random_state=42)

    clf = svm.LinearSVC(class_weight='balanced')
    clf.fit(X_train, y_train.ravel())
    y_pred = clf.predict(X_test)
    score = clf.score(X_test, y_test)
    conf_mat = confusion_matrix(y_test, y_pred)

    print(score)
    print(conf_mat)
    # p_vals_0 = clf.decision_function(X_train)
    # p_vals = clf.decision_function(X_test)


