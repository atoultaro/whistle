#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Created on 3/4/20
@author: atoultaro
"""
import os
import glob
import soundfile as sf
import numpy as np

data_dir = '/home/ys587/__Data/__whistle/__whistle_gillespie'
freq_reso_dir = ['48kHz', '96kHz', '192kHz']
freq_192k_dir = ['France', 'Spain']

# 48-kHz folders
folder_48khz = os.path.join(data_dir, freq_reso_dir[0])
species_folder = os.listdir(folder_48khz)
species_duration_48kHz = dict()
for ss in species_folder:
    print(ss)
    file_list = glob.glob(os.path.join(folder_48khz, ss)+'/*.wav')
    file_duration = []
    for ff in file_list:
        print(ff)
        sound_meta = sf.info(ff)
        file_duration.append(sound_meta.duration)
    species_dur_curr = np.array(file_duration).sum()
    species_duration_48kHz.update({ss: species_dur_curr})

# 96-kHz folders
folder_96kHz = os.path.join(data_dir, freq_reso_dir[1])
species_folder = os.listdir(folder_96kHz)
species_duration_96kHz = dict()
for ss in species_folder:
    print(ss)
    file_list = glob.glob(os.path.join(folder_96kHz, ss)+'/*.wav')
    file_duration = []
    for ff in file_list:
        print(ff)
        sound_meta = sf.info(ff)
        file_duration.append(sound_meta.duration)
    species_dur_curr = np.array(file_duration).sum()
    species_duration_96kHz.update({ss: species_dur_curr})

# 192 kHz folders France
folder_192kHz_France = os.path.join(data_dir, freq_reso_dir[2], 'France')
species_folder = os.listdir(folder_192kHz_France)
species_duration_192kHz_France = dict()
for ss in species_folder:
    print(ss)
    file_list = glob.glob(os.path.join(folder_192kHz_France, ss)+'/*.wav')
    file_duration = []
    for ff in file_list:
        print(ff)
        sound_meta = sf.info(ff)
        file_duration.append(sound_meta.duration)
    species_dur_curr = np.array(file_duration).sum()
    species_duration_192kHz_France.update({ss: species_dur_curr})

# 192 kHz folders Spain
folder_192kHz_Spain = os.path.join(data_dir, freq_reso_dir[2], 'Spain')
species_folder = os.listdir(folder_192kHz_Spain)
species_duration_192kHz_Spain = dict()
for ss in species_folder:
    print(ss)
    file_list = glob.glob(os.path.join(folder_192kHz_Spain, ss)+'/*.wav')
    file_duration = []
    for ff in file_list:
        print(ff)
        sound_meta = sf.info(ff)
        file_duration.append(sound_meta.duration)
    species_dur_curr = np.array(file_duration).sum()
    species_duration_192kHz_Spain.update({ss: species_dur_curr})
