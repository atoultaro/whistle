#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Created on 3/5/20
@author: atoultaro
"""
import glob
import os
import re
import pandas as pd
from matplotlib import pyplot as plt

# data_dir = '/home/ys587/__Data/__whistle/__whistle_mann/sound'
data_dir = '/home/ys587/__Data/__whistle/__whistle_mann/sound_48k'
csv_out = '/home/ys587/__Data/__whistle/__whistle_mann/whistle_mann_label.txt'

file_list = glob.glob(data_dir+'/*.wav')
file_list.sort()

# id = []
score = []
sound_path = []
for ff in file_list:
    ff_base = os.path.basename(ff)
    print(ff_base)
    m = re.search('~(\d{1,4})~(\d.\d{2,5}).*.wav$', ff_base)
    # id.append(m.groups()[0])
    score.append(m.groups()[1])
    sound_path.append(ff)

df_whistle = pd.DataFrame(list(zip(score, sound_path)),
                          columns=['score', 'path'])

# df_whistle.id = df_whistle.id.astype('int64')
df_whistle.score = df_whistle.score.astype(float)
df_whistle['label'] = (df_whistle.score > 0.5)
df_whistle.label = df_whistle.label.astype(int)

ax = df_whistle.score.plot.hist(bins=20)
plt.show()

df_whistle.to_csv(csv_out, index=False)
