#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 12 15:37:31 2019

@author: ys587
"""

import os
import sys
import pickle
import numpy as np
from sklearn.model_selection import train_test_split

ROOT_DIR = r'/Users/ys587/__YuPlayground/upcall-net'
sys.path.append(ROOT_DIR)
# TONAL_DIR = os.path.abspath("./tonalclassifier/src/classifier")
# sys.path.append(TONAL_DIR)
# DSP_DIR = os.path.abspath("./tonalclassifier/src")
# sys.path.append(DSP_DIR)


# pickle_file = r'/Users/ys587/__DeepContext/__Data/Whistle_Contours.dat'
# pickle_file = r'/Users/ys587/__DeepContext/__Data/Whistle_Contours_V2.dat'
pickle_file = r'/home/ys587/tonal_classifier/whistle_data/Whistle_Contours_v2.dat'
#data = [label_data, label_num, contour_data]


with open(pickle_file, "rb") as f:
    label_data, label_num, contour_data0 = pickle.load(f)

from classifier.recurrent import train_and_evaluate
from classifier.batchgenerator import PaddedBatchGenerator
from keras.layers import Dense, Input, TimeDistributed, SimpleRNN, LSTM, GRU, Masking, Dropout
from keras.models import Sequential
from keras.utils import to_categorical

label_onehot = to_categorical(label_num)

# for ss in range(len(contour_data0)):
if len(contour_data0[0].shape) == 1:
    contour_data = [np.reshape(w, (-1, 1)) for w in contour_data0]

outputN = len(set(label_num))

b = PaddedBatchGenerator(contour_data, label_onehot, batch_size=len(contour_data))
[contour_data_input, label_input] = b.__next__()
print(contour_data_input)
print(label_input)
# print("Above is batch {} epoch {}".format(idx, b.epoch))# a single batch

dim_recurrent = 1

if False:
    models_rnn = [
        [(Masking, [], {"mask_value":0.,
                       "input_shape":[None, dim_recurrent]}),
         (LSTM, [50], {
             "return_sequences":True,
             "kernel_regularizer":regularizers.l2(.01),
             "recurrent_regularizer":regularizers.l2(.01)
             }),
         (Dropout, [0.5], {}),
         (Dense, [outputN], {'activation': 'softmax',
                             'kernel_regularizer': regularizers.l2(0.01)})
         ]
    ]

if False:
    models_rnn = [
        [(Masking, [], {"mask_value":0.,
                       "input_shape":[None, dim_recurrent]}),
         (LSTM, [50], {
             "return_sequences":True,
             "kernel_regularizer":regularizers.l2(.01),
             "recurrent_regularizer":regularizers.l2(.01)
             }),
         (Dropout, [0.5], {}),
         (LSTM, [50], {
             "return_sequences":False,
             "kernel_regularizer":regularizers.l2(.01),
             "recurrent_regularizer":regularizers.l2(.01)
             }),
         (Dropout, [0.5], {}),
         (Dense, [outputN], {'activation':'softmax',
                             'kernel_regularizer':regularizers.l2(0.01)})
         ]
    ]

if False:
    models_rnn = Sequential()
    models_rnn.add(LSTM(64, return_sequences=True,
                   kernel_regularizer=regularizers.l2(0.01),
                   recurrent_regularizer=regularizers.l2(0.01),
                   input_shape=(1385, 1))) # 749, 1146, 1098 or 1385?
    models_rnn.add(Dropout(0.5))
    models_rnn.add(LSTM(16,
                   kernel_regularizer=regularizers.l2(0.01),
                   recurrent_regularizer=regularizers.l2(0.01)))
    models_rnn.add(Dropout(0.5))
    models_rnn.add(Dense(outputN, activation='softmax'))
    print(models_rnn.summary())

models_rnn = Sequential()
models_rnn.add(LSTM(64, return_sequences=True,
               input_shape=(1385, 1)))
models_rnn.add(Dropout(0.5))
models_rnn.add(LSTM(32))
models_rnn.add(Dropout(0.5))
models_rnn.add(Dense(outputN, activation='softmax'))
print(models_rnn.summary())

if True:
    train_eval = train_and_evaluate
    batch_size = 256
    epochs = 10

    debug = False
    # if debug:
    #    models = [models[-1]]  # Only test the last model in the list

    results = []
    # contour_data = np.asarray(contour_data) # WRONG!
    # results.append(CrossValidator(contour_data_input, label_num, models_rnn,
    #                              train_eval,
    #                              batch_size=batch_size,
    #                              epochs=epochs))
    indices = np.arange(len(label_num))
    x_train, x_valida, y_train, y_valida, train_idx, test_idx = train_test_split(
        contour_data_input, label_num, indices, test_size=0.33, random_state=42
    )

    error_rate, model, loss = train_and_evaluate(contour_data_input, label_num, train_idx, test_idx,
                                                 models_rnn, batch_size=batch_size, epochs=epochs)

if False:
    for architecture in models:
        model = build_model(architecture)
        results.append(CrossValidator(contour_data, label_num, model,
                        train_eval, batch_size=batch_size, epochs=epochs))

# Quaranteened

#input_shape = []
#
#model = Sequential()
#model.add(LSTM(units=128, dropout=0.05, recurrent_dropout=0.35, return_sequences=True, input_shape=input_shape))
#model.add(LSTM(units=32, dropout=0.05, recurrent_dropout=0.35, return_sequences=False))
#model.add(Dense(units=4, activation='softmax'))

#lengths = []
#for cc in contour_data:
#    lengths.append(cc.shape[1])
#f = []
#idx = 0
#for idx in range(len(lengths)):
#    f.append(np.zeros([lengths[idx], 3]) + (10 + label_num[idx]))
    
# zero-padding to sequence train data
#max_len = 0 # 1,385 x 0.002 sec = 2.77 sec
#for cc in contour_data:
#    seq_len = cc.shape[1]
#    max_len = max(max_len, seq_len)
#
#mid_point = int(np.floor(max_len/2)+1)
#seq_fea = np.zeros((len(contour_data), max_len))
#for ii in range(len(contour_data)):
#    cc = contour_data[ii][1,:]
#    half_len_cc = float(cc.shape[0])/2.
#    seq_fea[ii, mid_point-half_len_cc:mid_point-half_len_cc+len(cc)] = cc
#    
