import matplotlib.pyplot as plt
import numpy as np

from skimage.data import astronaut
from skimage.color import rgb2gray
from skimage.filters import sobel
from skimage.segmentation import felzenszwalb, slic, quickshift, watershed
from skimage.segmentation import mark_boundaries
from skimage.util import img_as_float
from skimage import io

import os
import librosa
import librosa.display as display
import numpy as np

import matplotlib
matplotlib.use('TkAgg')


def power_law_calc(spectro_mat, nu1, nu2, gamma):
    dim_f, dim_t = spectro_mat.shape
    mu_k = [power_law_find_mu(spectro_mat[ff, :]) for ff in range(dim_f)]

    mat0 = spectro_mat ** gamma - np.array(mu_k).reshape(dim_f, 1) * np.ones(
        (1, dim_t))
    mat_a_denom = [(np.sum(mat0[:, tt] ** 2.)) ** .5 for tt in range(dim_t)]
    mat_a = mat0 / (np.ones((dim_f, 1)) * np.array(mat_a_denom).reshape(1, dim_t))
    mat_b_denom = [(np.sum(mat0[ff, :] ** 2.)) ** .5 for ff in range(dim_f)]
    mat_b = mat0 / (np.array(mat_b_denom).reshape(dim_f, 1) * np.ones((1, dim_t)))

    mat_a = mat_a * (mat_a > 0)  # set negative values into zero
    mat_b = mat_b * (mat_b > 0)
    power_law_mat = (mat_a**nu1)*(mat_b**nu2)
    # power_law_mat = (mat_a ** (2.0 * nu1)) * (mat_b ** (2.0 * nu2))
    # PowerLawTFunc = np.sum((mat_a**nu1)*(mat_b**nu2), axis=0)

    return power_law_mat


def power_law_find_mu(spectro_target):
    spec_sorted = np.sort(spectro_target)
    spec_half_len = int(np.floor(spec_sorted.shape[0]*.5))
    ind_j = np.argmin(spec_sorted[spec_half_len:spec_half_len*2] - spec_sorted[0:spec_half_len])
    mu = np.mean(spec_sorted[ind_j:ind_j+spec_half_len])

    return mu


N_FFT = 4096
# OFFSET = 334
OFFSET = 20.0
NU1 = 1.0
NU2 = 2.0
GAMMA = 1.0

# sound_file = '/Users/ys587/__Data/Humpback/Total2007-2008.aif'
sound_file = r'/home/ys587/__Data/__whistle/5th_DCL_data_bottlenose/palmyra092007FS192-071011-193413.wav'  # bottlenose

if False:
    # cqt spectrogram & watershed superpixels
    offset_curr = OFFSET

    while offset_curr <= 10000:
        print('offset: '+str(offset_curr))
        samples, samplerate = librosa.load(sound_file, sr=96000, mono=True,
                                           offset=offset_curr,
                                           duration=10.0)

        fig, ax = plt.subplots(4, 1, figsize=(20, 10))

        spectro_cqt0 = np.abs(librosa.cqt(samples, sr=samplerate, hop_length=512,
                                         n_bins=12 * 9, bins_per_octave=12))

        spectro_cqt0 = np.flipud(spectro_cqt0)
        spectro_cqt = power_law_calc(spectro_cqt0, NU1, NU2, GAMMA)
        spectro_cqt_db = librosa.amplitude_to_db(spectro_cqt, ref=np.max)
        # display.specshow(spectro_cqt_db, y_axis='cqt_hz', x_axis='time', ax=ax[0],
        #                 bins_per_octave=12, hop_length=512, sr=samplerate)

        img = (spectro_cqt - spectro_cqt.min()) / (spectro_cqt.max())
        # img = (spectro_cqt_db - spectro_cqt_db.min()) / (-spectro_cqt_db.min())

        # original spectrogram
        # ax[0].imshow(img_ud)
        ax[0].imshow(img)
        gradient = sobel(rgb2gray(img))
        ax[1].imshow(gradient) # graident

        # watershed #1
        segments_watershed = watershed(gradient, markers=100)
        ax[2].imshow(mark_boundaries(img, segments_watershed))
        # ax[2].set_title("Felzenszwalbs's method")

        # watershed #2
        segments_watershed_2 = watershed(gradient, markers=100, compactness=0.1)
        ax[3].imshow(mark_boundaries(img, segments_watershed_2))
        # ax[3].set_title("Felzenszwalbs's method")

        for a in ax.ravel():
            a.set_axis_off()

        plt.tight_layout()
        # plt.show()

        offset_curr += 10.

if False:
    # cqt spectrogram & felzenszwalb superpixels
    offset_curr = OFFSET
    while offset_curr <= 10000:
        print('offset: ' + str(offset_curr))
        # sound_file = '/Users/ys587/__Data/Humpback/Total2007-2008.aif'
        #samples, samplerate = librosa.load(sound_file, sr=None, mono=True,
        #                                   offset=offset_curr,
        #                                   duration=10)
        samples, samplerate = librosa.load(sound_file, sr=96000, mono=True,
                                           offset=offset_curr,
                                           duration=10.0)

        fig, ax = plt.subplots(4, 1, figsize=(20, 10))

        spectro_cqt0 = np.abs(librosa.cqt(samples, sr=samplerate, hop_length=512,
                                         n_bins=12 * 9, bins_per_octave=12))

        spectro_cqt0 = np.flipud(spectro_cqt0)
        spectro_cqt = power_law_calc(spectro_cqt0, NU1, NU2, GAMMA)
        spectro_cqt_db = librosa.amplitude_to_db(spectro_cqt, ref=np.max)
        # display.specshow(spectro_cqt_db, y_axis='cqt_hz', x_axis='time', ax=ax[0],
        #                 bins_per_octave=12, hop_length=512, sr=samplerate)

        img = (spectro_cqt - spectro_cqt.min()) / (spectro_cqt.max())
        # img = (spectro_cqt_db - spectro_cqt_db.min()) / (-spectro_cqt_db.min())

        # original spectrogram
        # ax[0].imshow(img_ud)
        ax[0].imshow(spectro_cqt0)
        ax[1].imshow(spectro_cqt)

        # felzenszwalb #1
        segments_fz = felzenszwalb(img, scale=100, sigma=1.0, min_size=20)
        ax[2].imshow(mark_boundaries(img, segments_fz))
        # ax[2].set_title("Felzenszwalbs's method")

        # felzenszwalb #2
        segments_fz_2 = felzenszwalb(img, scale=100, sigma=0.25, min_size=20)
        ax[3].imshow(mark_boundaries(img, segments_fz_2))
        # ax[3].set_title("Felzenszwalbs's method")

        for a in ax.ravel():
            a.set_axis_off()

        plt.tight_layout()
        # plt.show()

        offset_curr += 10.


if True:
    # demo of spectrogram, log-scale spectrogram & CQT spectrogram
    offset_curr = OFFSET
    while offset_curr <= 10000:
        print('offset: ' + str(offset_curr) )
        # sound_file = '/Users/ys587/__Data/Humpback/Total2007-2008.aif'
        samples, samplerate = librosa.load(sound_file, sr=96000, mono=True,
                                           offset=offset_curr,
                                           duration=10)

        fig, ax = plt.subplots(3, 1, figsize=(20, 10))
        spectro0 = np.abs(librosa.stft(samples, n_fft=N_FFT, hop_length=int(N_FFT/2))).astype('float64')

        # spectrogram original
        display.specshow(librosa.amplitude_to_db(spectro0, ref=np.max), y_axis='linear', x_axis='time', ax=ax[0],
                         sr=samplerate, hop_length=int(N_FFT/2))

        # spectrogram log-freq
        display.specshow(librosa.amplitude_to_db(spectro0, ref=np.max), y_axis='log', x_axis='time', ax=ax[1],
                         sr=samplerate, hop_length=int(N_FFT/2))

        # cqt
        spectro_cqt = np.abs(librosa.cqt(samples, sr=samplerate, hop_length=512,
                                         n_bins=12*3*4, bins_per_octave=12*4, fmin=6000))
        spectro_cqt_db = librosa.amplitude_to_db(spectro_cqt, ref=np.max)
        display.specshow(spectro_cqt_db, y_axis='cqt_hz', x_axis='time', ax=ax[2],
                         bins_per_octave=12*4, hop_length=512, sr=samplerate, fmin=6000)
        plt.tight_layout()
        # plt.show()

        offset_curr += 10.


if False:
    # demo of four types of superpixels
    spec_min = spectro_cqt_db.min()
    # img = (spectro_cqt_db-spec_min)/(-spec_min)
    img = (spectro_cqt-spectro_cqt.min())/(spectro_cqt.max())
    segments_fz = felzenszwalb(img, scale=50, sigma=0.5, min_size=50)
    segments_slic = slic(img, n_segments=200, compactness=0.1, sigma=0.05)
    segments_quick = quickshift(np.stack((img, img, img), axis=2), kernel_size=3, max_dist=3, ratio=0.2)
    # segments_quick = felzenszwalb(img, scale=100, sigma=0.5, min_size=50)
    gradient = sobel(rgb2gray(img))
    segments_watershed = watershed(gradient, markers=200, compactness=0.001)

    print(
        "Felzenszwalb number of segments: {}".format(len(np.unique(segments_fz))))
    print('SLIC number of segments: {}'.format(len(np.unique(segments_slic))))
    print(
        'Quickshift number of segments: {}'.format(len(np.unique(segments_quick))))

    fig, ax = plt.subplots(4, 1, figsize=(20, 10), sharex=True, sharey=True)

    ax[0].imshow(mark_boundaries(img, segments_fz))
    ax[0].set_title("Felzenszwalbs's method")
    ax[1].imshow(mark_boundaries(img, segments_slic))
    ax[1].set_title('SLIC')
    ax[2].imshow(mark_boundaries(img, segments_quick))
    ax[2].set_title('Quickshift')
    ax[3].imshow(mark_boundaries(img, segments_watershed))
    ax[3].set_title('Compact watershed')
    for a in ax.ravel():
        a.set_axis_off()

    plt.tight_layout()
    plt.show()



if False:
    plt.title('Power spectrogram')
    # plt.colorbar(format='%+2.0f dB')
    # fig, ax = plt.subplots(1, 1, figsize=(10, 10))
    # ax.imshow(spectro)

    # spectrogram partial ==> failed in librosa
    spectro_part = spectro0[:int(N_FFT/4)+1, :]
    display.specshow(librosa.amplitude_to_db(spectro_part, ref=np.max),
                     y_axis='linear', x_axis='time', ax=ax[1],
                     hop_length=int(N_FFT*0.25), sr=samplerate)



if False:
    # img = img_as_float(astronaut()[::2, ::2])
    img_path = r'/Users/ys587/__Project/__faceswap/__Image/__KailiFaceSmall/UNADJUSTEDNONRAW_thumb_3ac.jpg'
    img = img_as_float(io.imread(img_path)[::2, ::2])
    # img_path = r'/Users/ys587/__YuPlayground/superpixel/__data/whistle_spectrogram.jpeg'
    # img_path = r'/Users/ys587/__YuPlayground/superpixel/__data/humpback.jpg'
    # img = img_as_float(io.imread(img_path))

    segments_fz = felzenszwalb(img, scale=100, sigma=0.5, min_size=50)
    segments_slic = slic(img, n_segments=250, compactness=10, sigma=1)
    segments_quick = quickshift(img, kernel_size=3, max_dist=6, ratio=0.5)
    gradient = sobel(rgb2gray(img))
    segments_watershed = watershed(gradient, markers=250, compactness=0.001)

    print("Felzenszwalb number of segments: {}".format(len(np.unique(segments_fz))))
    print('SLIC number of segments: {}'.format(len(np.unique(segments_slic))))
    print('Quickshift number of segments: {}'.format(len(np.unique(segments_quick))))

    fig, ax = plt.subplots(2, 2, figsize=(10, 10), sharex=True, sharey=True)

    ax[0, 0].imshow(mark_boundaries(img, segments_fz))
    ax[0, 0].set_title("Felzenszwalbs's method")
    ax[0, 1].imshow(mark_boundaries(img, segments_slic))
    ax[0, 1].set_title('SLIC')
    ax[1, 0].imshow(mark_boundaries(img, segments_quick))
    ax[1, 0].set_title('Quickshift')
    ax[1, 1].imshow(mark_boundaries(img, segments_watershed))
    ax[1, 1].set_title('Compact watershed')

    for a in ax.ravel():
        a.set_axis_off()

    plt.tight_layout()
    plt.show()

