# -*- coding: utf-8 -*-
"""

Created on 2019-05-11
@author: atoultaro
"""
from skimage.data import astronaut
import cv2
import numpy as np
import matplotlib.pyplot as plt
from skimage.util import img_as_float

# Read image
im = astronaut()[::2, ::2, 0]
# img_path = r'/Users/ys587/__Project/__faceswap/__Image/__KailiFaceSmall/UNADJUSTEDNONRAW_thumb_3ac.jpg'
# im = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)

# Set up the detector with default parameters.
detector = cv2.SimpleBlobDetector_create()

# Detect blobs.
keypoints = detector.detect(im)

# Draw detected blobs as red circles.
# cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS ensures the size of the circle corresponds to the size of blob
im_with_keypoints = cv2.drawKeypoints(im, keypoints, np.array([]), (0, 0, 255),
                                      cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

# Show keypoints
# cv2.imshow("Keypoints", im_with_keypoints)

fig, ax = plt.subplots(1, 1, figsize=(10, 5))
ax.imshow(im)
plt.show()

