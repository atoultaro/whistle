
# Project title
Whistle classification based on frequency sequences

## Get started
frequency_classifier/driver.py

## Required packages:
```
classifier
dsp
peak_picker
cape_cod_whale: RNN re-usable functions are stored here
```
