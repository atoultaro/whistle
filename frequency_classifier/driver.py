#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
whislte contour classification using freq contours from the start time and end
time of of verified contours
running script for RNN experiments
To replace run_classification.py

Created on 6/3/19
@author: atoultaro
"""
import os
import numpy as np
import pickle
import random
import matplotlib.pyplot as plt

# import librosa
from keras.utils import to_categorical

from classifier.recurrent import train_and_evaluate
# from classifier.buildmodels import build_model
from classifier.batchgenerator import PaddedBatchGenerator

from cape_cod_whale.preprocess import bin_extract, fea_label_generate, \
    save_fea, load_fea, freq_seq_label_generate, fea_label_shuffle, \
    freq_seq_label_generate_no_harmonics
from cape_cod_whale.classifier import train_validation, \
    two_fold_cross_validate

# bin files for training & testing
# data_dir = '/Users/ys587/__Data/whistle_Contours/tonals_20190210' # bin files
bin_dir_train = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__first_pie'
bin_dir_test = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__second_pie'
bin_dir_all = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__all'
sound_dir = '/home/ys587/__Data/__whistle/__sound_species/'
# seltab_out_path = '/home/ys587/__Data/__whistle/__seltab'

species_name = ['bottlenose', 'common', 'spinner', 'melon-headed']
species_id = {'bottlenose': 0, 'common': 1, 'spinner': 2, 'melon-headed': 3}
contour_train, bin_wav_pair_train = bin_extract(bin_dir_train, sound_dir,
                                                species_name)
contour_test, bin_wav_pair_test = bin_extract(bin_dir_test, sound_dir,
                                              species_name)
# contour_all, bin_wav_pair = bin_extract(bin_dir_all, sound_dir, species_name)


# find contours longer than a pre-defined duration and extract audio features
conf_general = dict()
# conf_general["duration_thre"] = 0.10
conf_general["duration_thre"] = 0.50
percentile_list = [0, 25, 50, 75, 100]
conf_general['time_reso'] = 0.02
# conf_general['transform'] = "zscorederiv"
conf_general['transform'] = "non_z"

# features & labels
fea_list_train, label_list_train, dur_train_max, count_long_train, \
count_all_train = freq_seq_label_generate_no_harmonics(contour_train,
                                                       conf_general,
                                                       species_id)

fea_list_test, label_list_test, dur_test_max, count_long_test, count_all_test \
    = freq_seq_label_generate_no_harmonics(contour_test, conf_general, species_id)

conf_model = dict()
conf_model['batch_size'] = 100
conf_model['epochs'] = 1 # debug
# conf_model['epochs'] = 200
conf_model['learning_rate'] = 0.001
conf_model['dur_train_max'] = dur_train_max
conf_model['dur_test_max'] = dur_test_max
# conf_model["l2_regu"] = 5.e-2
conf_model["l2_regu"] = 0.0
conf_model["dropout"] = 0.2
conf_model["recurrent_dropout"] = 0.2
conf_model["class_weight"] = {0: 1.0, 1: 1.0, 2: 1.0, 3: 1.0} # default
conf_model["mod"] = "sum"
conf_model["duration_thre"] = conf_general["duration_thre"]
conf_model['time_reso'] = conf_general['time_reso']

# log_dir = "/home/ys587/__Data/__whistle/__log_dir/"
log_dir = "/home/ys587/__Data/__whistle/__log_dir_freq_contour"

# num_dolphins = len(species_name)
conf_model["num_class"] = len(species_name)
# conf_model["fea_dim"] = 1  # default
conf_model["fea_dim"] = fea_list_train[0].shape[1]

# Features & labels
# shuffle
# if conf_gen['shuffle'] is True:
#     feature_train, label_train = fea_label_shuffle(fea_train.tolist(), label_train0)
#     feature_test, label_test = fea_label_shuffle(fea_test.tolist(), label_test0)
# else:
#     feature_train = fea_train
#     label_train = label_train0
#     feature_test = fea_test
#     label_test = label_test0


feature_train, label_train = fea_label_shuffle(fea_list_train, label_list_train)
feature_test, label_test = fea_label_shuffle(fea_list_test, label_list_test)

# # normalized by mean
# for ii in range(len(feature_train)):
#     feature_curr = feature_train[ii]
#     try:
#         feature_train[ii] = feature_curr/feature_curr.mean()
#     except:
#         print("Training: "+str(ii))
#         print(feature_curr.mean())
# for ii in range(len(feature_test)):
#     feature_curr = feature_test[ii]
#     try:
#         feature_test[ii] = feature_curr/feature_curr.mean()
#     except:
#         print("Testing: "+str(ii))
#         print(feature_curr.mean())

# model experiments
model_name = "lstm_phone_1_layer_4"
model_tag = model_name + "_16"
conf_model.update({"lstm1": 16})
two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
                        label_test, conf_model, log_dir, model_tag)

# model_name = "lstm_phone_2_layers"
# model_tag = model_name + "_64_16"
# conf_model.update({"lstm1": 64, "lstm2": 16, "dense_size": 20})
# two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
#                         label_test, conf_model, log_dir, model_tag)

# model_name = "lstm_phone_2_layers_expt"
# model_tag = model_name + "_20_20"
# conf_model.update({"lstm1": 20, "lstm2": 20, "dense_size": 20})
# two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
#                         label_test, conf_model, log_dir, model_tag)

# model_name = "lstm_phone_2_layers"
# model_tag = model_name + "_20_20"
# conf_model.update({"lstm1": 20, "lstm2": 20, "dense_size": 20})
# two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
#                         label_test, conf_model, log_dir, model_tag)

if False:
    model_name = "lstm_phone_bidirect_1_layer"
    model_tag = model_name + "_16"
    conf_model.update({"lstm1": 16})
    two_fold_cross_validate(model_name, feature_train, label_train,
                            feature_test, label_test, conf_model, log_dir,
                            model_tag)

    model_name = "lstm_phone_bidirect_2_layers"
    model_tag = model_name + "_64_16"
    conf_model.update({"lstm1": 64, "lstm2": 16})
    two_fold_cross_validate(model_name, feature_train, label_train,
                            feature_test, label_test, conf_model, log_dir,
                            model_tag)

    model_name = "lstm_phone_1_layer_4"
    model_tag = model_name + "_128"
    conf_model.update({"lstm1": 128})
    two_fold_cross_validate(model_name, feature_train, label_train,
                            feature_test, label_test, conf_model, log_dir,
                            model_tag)

    model_name = "lstm_phone_2_layers"
    model_tag = model_name + "_128_32"
    conf_model.update({"lstm1": 128, "lstm2": 32})
    two_fold_cross_validate(model_name, feature_train, label_train,
                            feature_test, label_test, conf_model, log_dir,
                            model_tag)


# model_name = "lstm_phone_1_layer_3"
# model_tag = model_name + "_8"
# conf_model.update({"lstm1": 8})
# two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
#                         label_test, conf_model, log_dir, model_tag)

# model_name = "lstm_phone_1_layer_3"
# model_tag = model_name + "_32"
# conf_model.update({"lstm1": 32})
# two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
#                         label_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_1_layer_3"
# model_tag = model_name + "_64"
# conf_model.update({"lstm1": 64})
# two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
#                         label_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_1_layer"
# model_tag = model_name + "_128"
# conf_model.update({"lstm1": 128})
# two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
#                         label_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_1_layer_2"
# model_tag = model_name + "_128"
# conf_model.update({"lstm1": 128})
# two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
#                         label_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_1_layer_3"
# model_tag = model_name + "_128"
# conf_model.update({"lstm1": 128})
# two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
#                         label_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_1_layer"
# model_tag = model_name + "_512"
# conf_model.update({"lstm1": 512})
# two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
#                         label_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_1_layer_2"
# model_tag = model_name + "_512"
# conf_model.update({"lstm1": 512})
# two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
#                         label_test, conf_model, log_dir, model_tag)

# model_name = "lstm_phone_1_layer_3"
# model_tag = model_name + "_512"
# conf_model.update({"lstm1": 512})
# two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
#                         label_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_1_layer_4"
# model_tag = model_name + "_128"
# conf_model.update({"lstm1": 128})
# two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
#                         label_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_1_layer_5"
# model_tag = model_name + "_128"
# conf_model.update({"lstm1": 128})
# two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
#                         label_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_1_layer_4"
# model_tag = model_name + "_512"
# conf_model.update({"lstm1": 512})
# two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
#                         label_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_1_layer_5"
# model_tag = model_name + "_512"
# conf_model.update({"lstm1": 512})
# two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
#                         label_test, conf_model, log_dir, model_tag)

# model_name = "lstm_phone_3_layer"
# model_tag = model_name + "_64"
# conf_model.update({"lstm1": 64})
# two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
#                         label_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_3_layer"
# model_tag = model_name + "_256"
# conf_model.update({"lstm1": 256})
# two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
#                         label_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_3_layer"
# model_tag = model_name + "_1024"
# conf_model.update({"lstm1": 1024})
# two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
#                         label_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_2_layers"
# model_tag = model_name + "_256_64"
# conf_model.update({"lstm1": 256, "lstm2": 64})
# two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
#                         label_test, conf_model, log_dir, model_tag)


# model_name = "lstm_phone_3_layers"
# model_tag = model_name + "_256_64_16"
# conf_model.update({"lstm1": 256, "lstm2": 64, "lstm3": 16})
# two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
#                         label_test, conf_model, log_dir, model_tag)

# model_name = "lstm_phone_2_layers"
# model_tag = model_name + "_128_16"
# conf_model.update({"lstm1": 128, "lstm2": 16})
# two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
#                         label_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_2_layers"
# model_tag = model_name + "_256_32"
# conf_model.update({"lstm1": 256, "lstm2": 32})
# two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
#                         label_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_2_layers"
# model_tag = model_name + "_256_64"
# conf_model.update({"lstm1": 256, "lstm2": 64})
# two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
#                         label_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_3_layers"
# model_tag = model_name + "_64_32_16"
# conf_model.update({"lstm1": 64, "lstm2": 32, "lstm3": 16})
# two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
#                         label_test, conf_model, log_dir, model_tag)
#
# model_name = "lstm_phone_3_layers"
# model_tag = model_name + "_128_64_32"
# conf_model.update({"lstm1": 128, "lstm2": 64, "lstm3": 32})
# two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
#                         label_test, conf_model, log_dir, model_tag)

# image/spectrogram only models
# model_name = "cnn_lstm"
# model_tag = model_name + "_16"
# conf_model.update({"lstm1": 16})
# two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
#                         label_test, conf_model, log_dir, model_tag)
#
# model_name = "conv_lstm"
# model_tag = model_name + "_16"
# conf_model.update({"lstm1": 16})
# two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
#                         label_test, conf_model, log_dir, model_tag)

