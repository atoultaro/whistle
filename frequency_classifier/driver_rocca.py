#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Contour-based whistle classification using ROCCA and features derived cepstrual
coefficients-based pretrained LSTM

50 features from Oswald 2013
Random forest classifier

Created on 10/2/19
@author: atoultaro
"""

import os
import numpy as np
import pickle
import random
import matplotlib.pyplot as plt

from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix, balanced_accuracy_score, \
    classification_report, f1_score
# import librosa
from keras.utils import to_categorical
from classifier.recurrent import train_and_evaluate
# from classifier.buildmodels import build_model
from classifier.batchgenerator import PaddedBatchGenerator

from cape_cod_whale.preprocess import bin_extract, fea_label_generate, \
    save_fea, load_fea, freq_seq_label_generate, fea_label_shuffle, \
    freq_seq_label_generate_no_harmonics, fea_freq_cep_label_generate_no_harmonics
from cape_cod_whale.classifier import two_fold_cross_validate_random_forest
from cape_cod_whale.load_feature_model import load_fea_model

# bin files for training & testing
# data_dir = '/Users/ys587/__Data/whistle_Contours/tonals_20190210' # bin files
bin_dir = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files'
bin_dir_train = os.path.join(bin_dir, '__first_pie')
bin_dir_test = os.path.join(bin_dir, '__second_pie')
sound_dir = '/home/ys587/__Data/__whistle/__sound_species/'
species_name = ['bottlenose', 'common', 'spinner', 'melon-headed']
species_id = {'bottlenose': 0, 'common': 1, 'spinner': 2, 'melon-headed': 3}
contour_train, bin_wav_pair_train = bin_extract(bin_dir_train, sound_dir,
                                                species_name)
contour_test, bin_wav_pair_test = bin_extract(bin_dir_test, sound_dir,
                                              species_name)

# find contours longer than a pre-defined duration and extract audio features
conf_gen = dict()
conf_gen['species_name'] = species_name
conf_gen['species_id'] = species_id
# conf_gen["duration_thre"] = 0.10
# conf_gen["duration_thre"] = 0.50
conf_gen["duration_thre"] = 0.30
# conf_gen["duration_thre"] = 0.80
conf_gen['time_reso'] = 0.01  # 10 ms

conf_gen['transform'] = "rocca"  # rocca, cep or rocca_cep
# conf_gen['transform'] = "rocca_cep"
# conf_gen['transform'] = "cep"

# cepstral coefficient
conf_gen['sample_rate'] = 192000
conf_gen['input_size'] = 4096  # input size for deep LSTM features
# 21.33 msec given sampling rate 192,000 Hz
conf_gen['filt_num'] = 64  # number of filterbanks in cepstral coefficients
conf_gen['low_freq'] = 5000.
conf_gen['high_freq'] = 23500.
conf_gen["num_class"] = len(species_name)

# random forest
# conf_gen['n_est'] = 200
conf_gen['n_est'] = 1000
#
conf_gen['bin_dir'] = bin_dir


# log directory
# log_dir = "/home/ys587/__Data/__whistle/__log_dir/"
log_dir = "/home/ys587/__Data/__whistle/__log_dir_rocca_cep"

# directory for the trained cepstral model
# conf_gen['transform'] = "rocca_cep"  # feature rocca, cepstrum or both is used.
# model_dir = u'/home/ys587/__Data/__whistle/__log_dir_audio_time/__logdir_temp/fea_cepstrum_conv2d_lstm_winback_10_128_64_2lay_l2_p2_dropout_p2_f1_p80'
# model_dir = u'/home/ys587/__Data/__whistle/__log_dir_audio_time/__logdir_temp/fea_cepstrum_conv2d_lstm_winback_10_128_64_2lay_l2_p2_dropout_p2'
model_dir = u'/home/ys587/__Data/__whistle/__log_dir_audio_time/__logdir_temp/fea_cepstrum_conv2d_lstm_winback_10_64_32_2lay_l2_p2_dropout_p2'
model_dir_1 = os.path.join(model_dir, 'fold1')
model_dir_2 = os.path.join(model_dir, 'fold1')

# features & labels
# fea_file = os.path.join(bin_dir, 'fea_rocca_cep_label_dur_2p0.pkl')
fea_file = os.path.join(bin_dir, 'fea_rocca_cep_label_dur_p3.pkl')
if os.path.exists(fea_file):
    with open(fea_file,  'rb') as f:
        fea_rocca_train, fea_cep_train, label_list_train, fea_rocca_test, \
            fea_cep_test, label_list_test = pickle.load(f)
else:
    fea_rocca_train, fea_cep_train, label_list_train, _, _, _, _ = \
        fea_freq_cep_label_generate_no_harmonics(contour_train,
                                                 bin_wav_pair_train,
                                                 bin_dir_train,
                                                 model_dir_1,
                                                 conf_gen,
                                                 species_id)

    fea_rocca_test, fea_cep_test, label_list_test, _, _, _, _ = \
        fea_freq_cep_label_generate_no_harmonics(contour_test,
                                                 bin_wav_pair_test,
                                                 bin_dir_test,
                                                 model_dir_2,
                                                 conf_gen,
                                                 species_id)

    with open(fea_file, 'wb') as f:
        pickle.dump([fea_rocca_train, fea_cep_train, label_list_train,
                     fea_rocca_test, fea_cep_test, label_list_test], f)

# reduce cep feature dimensions
# fea_cep_train_0 = fea_cep_train.reshape((fea_cep_train.shape[0], 14, 32))
# fea_cep_train = np.hstack((fea_cep_train_0.mean(axis=1), fea_cep_train_0.std(axis=1)))
# fea_cep_train = fea_cep_train_0.mean(axis=1)
# fea_cep_train = np.squeeze(fea_cep_train_0[:, 14-1, :])

# fea_cep_test_0 = fea_cep_test.reshape((fea_cep_test.shape[0], 14, 32))
# fea_cep_test = np.hstack((fea_cep_test_0.mean(axis=1), fea_cep_test_0.std(axis=1)))
# fea_cep_test = fea_cep_test_0.mean(axis=1)
# fea_cep_test = np.squeeze(fea_cep_test_0[:, 14-1, :])

if conf_gen['transform'] is 'rocca':
    fea_train = fea_rocca_train
elif conf_gen['transform'] is 'cep':
    fea_train = fea_cep_train
elif conf_gen['transform'] is 'rocca_cep':
    fea_train = np.hstack((fea_rocca_train, fea_cep_train))
else:
    fea_train = []
    print('No feature is selected.')
    exit()

if conf_gen['transform'] is 'rocca':
    fea_test = fea_rocca_test
elif conf_gen['transform'] is 'cep':
    fea_test = fea_cep_test
elif conf_gen['transform'] is 'rocca_cep':
    fea_test = np.hstack((fea_rocca_test, fea_cep_test))
else:
    fea_test = []
    print('No feature is selected.')
    exit()

feature_train, label_train = fea_label_shuffle(fea_train, label_list_train)
feature_test, label_test = fea_label_shuffle(fea_test, label_list_test)

y_pred, y_test = two_fold_cross_validate_random_forest(feature_train,
                                                       feature_test,
                                                       label_train,
                                                       label_test,
                                                       log_dir,
                                                       conf_gen)

# feature normalization
# eps = np.finfo(float).eps
# feature_train2 = (feature_train - feature_train.mean(axis=0))/(feature_train.std(axis=0)+eps)
# feature_test2 = (feature_test - feature_test.mean(axis=0))/(feature_test.std(axis=0)+eps)
# y_pred2, y_test2 = two_fold_cross_validate_random_forest(feature_train2,
#                                                        feature_test2,
#                                                        label_train,
#                                                        label_test,
#                                                        log_dir,
#                                                        conf_gen)


# output features & labels to t-SNE for display
tsne_dir = u'/home/ys587/PycharmProjects/dash-sample-apps-master/apps/dash-tsne/data/fea_cepstrum'
# np.savetxt(os.path.join(tsne_dir, 'fea_cep_input.csv'), fea_cep_train, delimiter=',')
# np.savetxt(os.path.join(tsne_dir, 'fea_cep_labels.csv'), label_list_train, fmt='%d', delimiter=',')

# Combine train & text
fea_cep_tot = np.vstack((fea_cep_train, fea_cep_test))
label_list_tot = label_list_train + [ss+4 for ss in label_list_test]
np.savetxt(os.path.join(tsne_dir, 'fea_cep_total_input.csv'), fea_cep_tot, delimiter=',')
np.savetxt(os.path.join(tsne_dir, 'fea_cep_total_labels.csv'), label_list_tot, fmt='%d', delimiter=',')
