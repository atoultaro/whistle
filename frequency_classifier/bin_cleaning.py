#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Remove the contours from harmonics and keep only the one for fundamental frequencies

Created on 7/16/19
@author: atoultaro
"""

import os
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

import matplotlib
matplotlib.use('TkAgg')

# import librosa
import soundfile as sf

# from classifier.buildmodels import build_model
from cape_cod_whale.preprocess import bin_extract, fea_label_generate

# bin files for training & testing
# data_dir = '/Users/ys587/__Data/whistle_Contours/tonals_20190210' # bin files
bin_dir_train = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__first_pie'
bin_dir_test = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__second_pie'
bin_dir_all = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__all'
sound_dir = '/home/ys587/__Data/__whistle/__sound_species/'
# seltab_out_path = '/home/ys587/__Data/__whistle/__seltab'
fig_out = r'/home/ys587/__Data/__whistle/__temp'

species_name = ['bottlenose', 'common', 'spinner', 'melon-headed']
species_id = {'bottlenose': 0, 'common': 1, 'spinner': 2, 'melon-headed': 3}
contour_train, bin_wav_pair_train = bin_extract(bin_dir_train, sound_dir,
                                                species_name)
# contour_test, bin_wav_pair_test = bin_extract(bin_dir_test, sound_dir,
#                                               species_name)
# contour_all, bin_wav_pair = bin_extract(bin_dir_all, sound_dir, species_name)

# time_reso = 0.02
time_reso = 0.005

print('')
start_time_file_list = []
count = 0

# for ff in ['palmyra092007FS192-071011-232000']:
# for ff in ['Qx-Tt-SCI0608-N1-060814-123433']:
for ff in sorted(contour_train.keys()):
    print('\n'+ff+': '+contour_train[ff][0])

    file_contour = contour_train[ff][1]
    # sound_file = bin_wav_pair_train[
    #     os.path.join(bin_dir_train, contour_train[ff][0], ff + '.bin')]
    # samples, sample_rate = sf.read(sound_file)
    # sound_len = 1.0 * samples.shape[0] / sample_rate
    # print('Sound length is: ' + '{:f}'.format(sound_len)+' S')

    print('Reading in contours...')
    # time_contour_list = []
    # freq_contour_list = []

    # interpoltion
    contour_train_ff = []
    len_contour = len(file_contour)
    time_max = 0
    # read contours into the var contour_train_ff
    for cc in range(len_contour):
        if np.remainder(cc, 100) == 0:
            print('Contour: '+str(cc))
        time_contour = file_contour[cc]['Time']
        freq_contour = file_contour[cc]['Freq']

        # linear interpolation
        time_contour_interp = np.arange(time_contour[0], time_contour[-1], time_reso)
        freq_contour_interp = np.interp(time_contour_interp, time_contour,
                                        freq_contour)

        contour_train_ff_cc = dict()
        contour_train_ff_cc['Time'] = time_contour_interp
        contour_train_ff_cc['Freq'] = freq_contour_interp

        contour_train_ff.append(contour_train_ff_cc)
        # time_contour_list = time_contour_list + list(time_contour_interp)
        # freq_contour_list = freq_contour_list + list(freq_contour_interp)
    # contour_train[ff].append(contour_train_ff)

    # Exploratory analysis
    # pairwise comparison: check time overlap & harmonic relationship
    contour_count = 0
    std_norm_list = []
    contour_ind = np.ones(len_contour)
    contour_sqr = np.zeros((len_contour, len_contour))
    for cc1 in range(len_contour):
        # print("cc1: "+str(cc1)+" length " + str(contour_train_ff[cc1]['Time'].shape[0]))
        for cc2 in range(cc1+1, len_contour):
            a1 = contour_train_ff[cc1]['Time'][0]
            b1 = contour_train_ff[cc1]['Time'][-1]
            a2 = contour_train_ff[cc2]['Time'][0]
            b2 = contour_train_ff[cc2]['Time'][-1]
            if (b1-a2>0) & (b2-a1>0):  # there is overlap
                print("cc1: " + str(cc1) + " length " + str(
                    contour_train_ff[cc1]['Time'].shape[0]))
                print("cc2: " + str(cc2) + " length " + str(
                    contour_train_ff[cc2]['Time'].shape[0]))
                if a1 <= a2:
                    time_start = a2
                    seq_ind = 2
                else:
                    time_start = a1
                    seq_ind = 1
                if b1 <= b2:
                    time_end = b1
                else:
                    time_end = b2

                if seq_ind is 1:
                    ind_list = list(range(0, int(round((time_end-a1)/time_reso))))
                    freq_seq_1 = [contour_train_ff[cc1]['Freq'][tt] for tt in
                                  ind_list]
                    freq_seq_2 = [contour_train_ff[cc2]['Freq'][tt+int(round((time_start-a2)/time_reso))] for tt in
                                  ind_list]
                else:  # seq_ind 2
                    ind_list = list(range(0, int(round((time_end-a2)/time_reso))))
                    freq_seq_2 = [contour_train_ff[cc2]['Freq'][tt] for tt in
                                  ind_list]
                    freq_seq_1 = [contour_train_ff[cc1]['Freq'][tt+int(round((time_start-a1)/time_reso))] for tt in
                                  ind_list]
                print('Start time: '+str(time_start))
                print('End time: ' + str(time_end))
                print('Freq seq cc1: ')
                print(*freq_seq_1, sep=',')
                print('Freq seq cc2: ')
                print(*freq_seq_2, sep=',')

                # # correlation coefficient
                # corr_coeff = np.corrcoef(freq_seq_1, freq_seq_2)[0,1]
                # print('Correlation coefficient: '+str(corr_coeff))

                ratio_seq = np.array(freq_seq_1) / np.array(freq_seq_2)
                std_norm = ratio_seq.std() / ratio_seq.mean()
                print('Ratio sequence: ')
                print(*ratio_seq.tolist())
                print('std_norm: '+str(std_norm))
                std_norm_list.append(std_norm)
                if std_norm <= 0.05: # small freq one will lose a point
                    if np.array(freq_seq_1).mean() <= np.array(freq_seq_2).mean():
                        contour_ind[cc2] -= 1
                    else:
                        contour_ind[cc1] -= 1

                print('Duration steps: '+str(int(round((time_end-time_start)/time_reso+1.0))))
                print('')
                contour_count += 1
    print('Total contours: ' + str(len_contour))
    print('Remaining contours: '+str((contour_ind>=1.0).sum()))
    print('')

    # # find the indices of contours through the matrix contour_sqr
    # ind_list = np.zeros(len_contour)
    # x_list, y_list = np.where(contour_sqr == 1.0)
