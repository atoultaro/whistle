#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
whislte classification using audio signals
4-fold cross-validation

Created on 12/9/19
@author: atoultaro
"""
import os
import numpy as np
import pickle
import random
import matplotlib.pyplot as plt
from math import floor, ceil
import gc
import timeit

from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix, balanced_accuracy_score, \
    classification_report, f1_score
from sklearn.model_selection import train_test_split
import librosa
from keras import backend
from keras.losses import categorical_crossentropy
from keras.utils import to_categorical
from keras.optimizers import Adam, Adadelta
from keras import regularizers

from keras.layers import Dense, Flatten, Dropout, BatchNormalization, \
                        GlobalMaxPooling2D, ZeroPadding2D, AveragePooling2D, \
                        Activation, add, GlobalAveragePooling2D, ConvLSTM2D
from keras.layers import Conv2D, MaxPooling2D, Conv1D, MaxPooling1D, Input, \
    TimeDistributed, LSTM
from keras.models import Sequential, load_model, Model
from keras.callbacks import ModelCheckpoint, TensorBoard, EarlyStopping
import cv2

# from classifier.recurrent import train_and_evaluate
# from classifier.buildmodels import build_model
# from classifier.batchgenerator import PaddedBatchGenerator
# from keras.preprocessing.image import ImageDataGenerator


from cape_cod_whale.preprocess import bin_extract, fea_context_base_generate, \
    fea_label_shuffle, contour_retrieve, contour_target_retrieve, \
    data_generator

from cape_cod_whale.classifier import two_fold_cross_validate_random_forest, metrics_two_fold
from cape_cod_whale.load_feature_model import load_fea_model, find_best_model

from contextlib import redirect_stdout

from keras.callbacks import Callback
import matplotlib.patches as mpatches
import itertools


def lenet_dropout_input_conv(conf):
    """
    Buidling the model of LeNet with dropout on the input, both
    convolutional layers and full-connected layer

    Args:
        conf: configuration class object

        input_shape: (conf.IMG_F, conf.IMG_T, 1)
        conf.num_class: numbwer of classes
        conf.RATE_DROPOUT_INPUT: dropout rate
        conf.RATE_DROPOUT_CONV
        conf.RATE_DROPOUT_FC
    Returns:
        model: built model
    """
    model = Sequential()
    model.add(Dropout(conf['dropout'],
                      input_shape=(conf['IMG_F'], conf['IMG_T'], 1)))

    # conv group 1
    model.add(
        Conv2D(8, kernel_size=(4, 2), strides=(1, 1),
               kernel_regularizer=regularizers.l2(conf['l2_regu']),
               activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    model.add(Dropout(conf['dropout']))

    # conv group 2
    model.add(Conv2D(8, kernel_size=(4, 2), strides=(1, 1),
                     kernel_regularizer=regularizers.l2(conf['l2_regu']),
                     activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    model.add(Dropout(conf['dropout']))

    # conv group 3
    model.add(Conv2D(8, kernel_size=(4, 2), strides=(1, 1),
                     kernel_regularizer=regularizers.l2(conf['l2_regu']),
                     activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    model.add(Dropout(conf['dropout']))

    # conv group 4
    model.add(Conv2D(8, kernel_size=(4, 2), strides=(1, 1),
                     kernel_regularizer=regularizers.l2(conf['l2_regu']),
                     activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    model.add(Dropout(conf['dropout']))

    model.add(Flatten())
    model.add(Dense(32, activation='relu'))
    model.add(Dropout(conf['dropout']))
    model.add(Dense(conf['num_class'], activation='softmax'))

    return model


def convnet_long_time(conf):
    model = Sequential()
    # Group 1
    model.add(Conv2D(8, kernel_size=(3, 3), strides=(1, 1), activation='relu',
                     kernel_initializer='he_normal', padding='same',
                     kernel_regularizer=regularizers.l2(conf['l2_regu']),
                     input_shape=(conf['IMG_F'], conf['IMG_T'], 1)))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 2
    model.add(Conv2D(8, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
                     kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 3
    model.add(
        Conv2D(8, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 4
    model.add(
        Conv2D(8, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 5
    model.add(
        Conv2D(8, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 6
    model.add(
        Conv2D(8, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))

    # 1x1 convolution
    model.add(
        Conv2D(8, kernel_size=(1, 1), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same'))
    model.add(GlobalMaxPooling2D())
    # model.add(Dropout(conf['dropout']))
    model.add(Dense(conf['num_class'], activation='softmax'))

    return model


def birdnet(conf):
    """
    Birdnet
    """
    model = Sequential()
    # Group 1
    model.add(Conv2D(32, kernel_size=(7, 7), strides=(1, 1), activation='relu',
                     kernel_initializer='he_normal', padding='same',
                     kernel_regularizer=regularizers.l2(conf['l2_regu']),
                     input_shape=(conf['IMG_F'], conf['IMG_T'], 1)))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 2
    model.add(Conv2D(32, kernel_size=(5, 5), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
                     kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 3
    model.add(
        Conv2D(64, kernel_size=(5, 5), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 4
    model.add(
        Conv2D(128, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 5
    model.add(
        Conv2D(256, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 6
    model.add(
        Conv2D(512, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))

    model.add(Flatten())
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.5))

    # model.add(Dropout(conf['dropout']))
    model.add(Dense(conf['num_class'], activation='softmax'))

    return model


def birdnet_7layers(conf):
    """
    Birdnet
    """
    model = Sequential()
    # Group 1
    model.add(Conv2D(32, kernel_size=(7, 7), strides=(1, 1), activation='relu',
                     kernel_initializer='he_normal', padding='same',
                     kernel_regularizer=regularizers.l2(conf['l2_regu']),
                     input_shape=(conf['IMG_F'], conf['IMG_T'], 1)))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 2
    model.add(Conv2D(32, kernel_size=(5, 5), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
                     kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 3
    model.add(
        Conv2D(64, kernel_size=(5, 5), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 4
    model.add(
        Conv2D(128, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 5
    model.add(
        Conv2D(256, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 6
    model.add(
        Conv2D(512, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 7
    model.add(
        Conv2D(1024, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))

    model.add(Flatten())
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.5))

    # model.add(Dropout(conf['dropout']))
    model.add(Dense(conf['num_class'], activation='softmax'))

    return model


def birdnet_8layers(conf):
    """
    Birdnet
    """
    model = Sequential()
    # Group 1
    model.add(Conv2D(32, kernel_size=(7, 7), strides=(1, 1), activation='relu',
                     kernel_initializer='he_normal', padding='same',
                     kernel_regularizer=regularizers.l2(conf['l2_regu']),
                     input_shape=(conf['IMG_F'], conf['IMG_T'], 1)))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 2
    model.add(Conv2D(32, kernel_size=(5, 5), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
                     kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 3
    model.add(
        Conv2D(64, kernel_size=(5, 5), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 4
    model.add(
        Conv2D(128, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 5
    model.add(
        Conv2D(256, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 6
    model.add(
        Conv2D(512, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 7
    model.add(
        Conv2D(1024, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 8
    model.add(
        Conv2D(1024, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(1, 2)))
    model.add(Dropout(conf['dropout']))

    model.add(Flatten())
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.5))

    # model.add(Dropout(conf['dropout']))
    model.add(Dense(conf['num_class'], activation='softmax'))

    return model


def birdnet_freq(conf):
    """
    Birdnet
    """
    model = Sequential()
    # Group 1
    model.add(Conv2D(32, kernel_size=(7, 7), strides=(1, 1), activation='relu',
                     kernel_initializer='he_normal', padding='same',
                     kernel_regularizer=regularizers.l2(conf['l2_regu']),
                     input_shape=(conf['IMG_F'], conf['IMG_T'], 1)))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 2
    model.add(Conv2D(32, kernel_size=(5, 5), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
                     kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 3
    model.add(
        Conv2D(64, kernel_size=(5, 5), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 4
    model.add(
        Conv2D(128, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 5
    model.add(
        Conv2D(256, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(1, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 6
    model.add(
        Conv2D(512, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(1, 2)))
    model.add(Dropout(conf['dropout']))

    model.add(Flatten())
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.5))

    # model.add(Dropout(conf['dropout']))
    model.add(Dense(conf['num_class'], activation='softmax'))

    return model

# Resnet
from keras.layers.advanced_activations import PReLU


# def name_builder(type, stage, block, name):
#     return "{}{}{}_branch{}".format(type, stage, block, name)
#
#
# def identity_block(input_tensor, kernel_size, filters, stage, block):
#     F1, F2, F3 = filters
#     def name_fn(type, name):
#         return name_builder(type, stage, block, name)
#
#     x = Conv2D(F1, (1, 1), name=name_fn('res', '2a'))(input_tensor)
#     x = BatchNormalization(name=name_fn('bn', '2a'))(x)
#     x = PReLU()(x)
#
#     x = Conv2D(F2, kernel_size, padding='same', name=name_fn('res', '2b'))(x)
#     x = BatchNormalization(name=name_fn('bn', '2b'))(x)
#     x = PReLU()(x)
#
#     x = Conv2D(F3, (1, 1), name=name_fn('res', '2c'))(x)
#     x = BatchNormalization(name=name_fn('bn', '2c'))(x)
#     x = PReLU()(x)
#
#     x = add([x, input_tensor])
#     x = PReLU()(x)
#
#     return x
#
#
# def conv_block(input_tensor, kernel_size, filters, stage, block,
#                strides=(2, 2)):
#     def name_fn(type, name):
#         return name_builder(type, stage, block, name)
#
#     F1, F2, F3 = filters
#
#     x = Conv2D(F1, (1, 1), strides=strides, name=name_fn("res", "2a"))(
#         input_tensor)
#     x = BatchNormalization(name=name_fn("bn", "2a"))(x)
#     x = PReLU()(x)
#
#     x = Conv2D(F2, kernel_size, padding='same', name=name_fn("res", "2b"))(x)
#     x = BatchNormalization(name=name_fn("bn", "2b"))(x)
#     x = PReLU()(x)
#
#     x = Conv2D(F3, (1, 1), name=name_fn("res", "2c"))(x)
#     x = BatchNormalization(name=name_fn("bn", "2c"))(x)
#
#     sc = Conv2D(F3, (1, 1), strides=strides, name=name_fn("res", "1"))(
#         input_tensor)
#     sc = BatchNormalization(name=name_fn("bn", "1"))(sc)
#
#     x = add([x, sc])
#     x = PReLU()(x)
#
#     return x
#
#
# # resnet
# def resnet(conf):
#     input_tensor = Input(shape=(conf['IMG_F'], conf['IMG_T'], 1))
#     net = ZeroPadding2D((3, 3))(input_tensor)
#     net = Conv2D(64, (7, 7), strides=(2, 2), name="conv1")(net)
#     net = BatchNormalization(name="bn_conv1")(net)
#     net = PReLU()(net)
#     net = MaxPooling2D((3, 3), strides=(2, 2))(net)
#
#     net = conv_block(net, 3, [64, 64, 256], stage=2, block='a', strides=(1, 1))
#     net = identity_block(net, 3, [64, 64, 256], stage=2, block='b')
#     net = identity_block(net, 3, [64, 64, 256], stage=2, block='c')
#
#     net = conv_block(net, 3, [128, 128, 512], stage=3, block='a')
#     net = identity_block(net, 3, [128, 128, 512], stage=3, block='b')
#     net = identity_block(net, 3, [128, 128, 512], stage=3, block='c')
#     net = identity_block(net, 3, [128, 128, 512], stage=3, block='d')
#
#     net = conv_block(net, 3, [256, 256, 1024], stage=4, block='a')
#     net = identity_block(net, 3, [256, 256, 1024], stage=4, block='b')
#     net = identity_block(net, 3, [256, 256, 1024], stage=4, block='c')
#     net = identity_block(net, 3, [256, 256, 1024], stage=4, block='d')
#     net = identity_block(net, 3, [256, 256, 1024], stage=4, block='e')
#     net = identity_block(net, 3, [256, 256, 1024], stage=4, block='f')
#     net = AveragePooling2D((2, 2))(net)
#
#     net = Flatten()(net)
#     net = Dense(conf['num_class'], activation="softmax", name="softmax")(net)
#
#     model = Model(inputs=input_tensor, outputs=net)
#
#     return model


def bn(x, name, zero_init=False):
    return BatchNormalization()(x)

    # return BatchNormalization(
    #     axis=1, name=name,
    #     momentum=0.9, epsilon=1e-5,
    #     gamma_initializer='zeros' if zero_init else 'ones')(x)

    # return BatchNormalization(
    #     axis=1, name=name, fused=True,
    #     momentum=0.9, epsilon=1e-5,
    #     gamma_initializer='zeros' if zero_init else 'ones')(x)


def conv(x, filters, kernel, strides=1, name=None):
    return Conv2D(filters, kernel, name=name,
                  strides=strides, use_bias=False, padding='same',
                  kernel_regularizer=regularizers.l2(1e-4))(x)


def conv_block(input_tensor, kernel_size, filters, stage, block, strides=(2, 2)):
    filters1, filters2, filters3 = filters
    conv_name_base = 'res' + str(stage) + block + '_branch'
    bn_name_base = 'bn' + str(stage) + block + '_branch'

    x = conv(input_tensor, filters1, (1, 1), name=conv_name_base + '2a')
    x = bn(x, name=bn_name_base + '2a')
    x = Activation('relu')(x)

    x = conv(x, filters2, kernel_size, strides=strides, name=conv_name_base + '2b')
    x = bn(x, name=bn_name_base + '2b')
    x = Activation('relu')(x)

    x = conv(x, filters3, (1, 1), name=conv_name_base + '2c')
    x = bn(x, name=bn_name_base + '2c', zero_init=True)

    shortcut = conv(
        input_tensor,
        filters3, (1, 1), strides=strides,
        name=conv_name_base + '1')
    shortcut = bn(shortcut, name=bn_name_base + '1')

    # x = tf.keras.layers.add([x, shortcut])
    x = add([x, shortcut])
    x = Activation('relu')(x)
    return x


def identity_block(input_tensor, kernel_size, filters, stage, block):
    filters1, filters2, filters3 = filters
    conv_name_base = 'res' + str(stage) + block + '_branch'
    bn_name_base = 'bn' + str(stage) + block + '_branch'

    x = conv(input_tensor, filters1, 1, name=conv_name_base + '2a')
    x = bn(x, name=bn_name_base + '2a')
    x = Activation('relu')(x)

    x = conv(x, filters2, kernel_size, name=conv_name_base + '2b')
    x = bn(x, name=bn_name_base + '2b')
    x = Activation('relu')(x)

    x = conv(x, filters3, (1, 1), name=conv_name_base + '2c')
    x = bn(x, name=bn_name_base + '2c', zero_init=True)

    # x = tf.keras.layers.add([x, input_tensor])
    x = add([x, input_tensor])
    x = Activation('relu')(x)
    return x


def conv_block_simple(input_tensor, kernel_size, filters, stage, block, strides=(2, 2)):
    filters1, filters2 = filters
    conv_name_base = 'res' + str(stage) + block + '_branch'
    bn_name_base = 'bn' + str(stage) + block + '_branch'

    x = conv(input_tensor, filters1, kernel_size, name=conv_name_base + '2a')
    x = bn(x, name=bn_name_base + '2a')
    x = Activation('relu')(x)

    x = conv(x, filters2, kernel_size, strides=strides, name=conv_name_base + '2b')
    x = bn(x, name=bn_name_base + '2b', zero_init=True)

    shortcut = conv(
        input_tensor,
        filters2, (1, 1), strides=strides,
        name=conv_name_base + '1')
    shortcut = bn(shortcut, name=bn_name_base + '1')

    # x = tf.keras.layers.add([x, shortcut])
    x = add([x, shortcut])
    x = Activation('relu')(x)
    return x


def identity_block_simple(input_tensor, kernel_size, filters, stage, block):
    filters1, filters2 = filters
    conv_name_base = 'res' + str(stage) + block + '_branch'
    bn_name_base = 'bn' + str(stage) + block + '_branch'

    x = conv(input_tensor, filters1, kernel_size, name=conv_name_base + '2a')
    x = bn(x, name=bn_name_base + '2a')
    x = Activation('relu')(x)

    x = conv(x, filters2, kernel_size, name=conv_name_base + '2b')
    x = bn(x, name=bn_name_base + '2c', zero_init=True)

    # x = tf.keras.layers.add([x, input_tensor])
    x = add([x, input_tensor])
    x = Activation('relu')(x)
    return x


def conv_block_2_no_direct(input_tensor, kernel_size, filters, stage, block, strides=(1, 1)):
    filters1, filters2 = filters
    conv_name_base = 'res' + str(stage) + block + '_branch'
    bn_name_base = 'bn' + str(stage) + block + '_branch'

    x = conv(input_tensor, filters1, kernel_size, name=conv_name_base + '2a')
    x = bn(x, name=bn_name_base + '2a')
    x = Activation('relu')(x)

    x = conv(x, filters2, kernel_size, strides=strides, name=conv_name_base + '2b')
    x = bn(x, name=bn_name_base + '2b', zero_init=True)
    x = Activation('relu')(x)
    return x


def resnet18(conf):
    input_shape = (conf['IMG_F'], conf['IMG_T'], 1)
    num_class = conf['num_class']

    inputs = Input(shape=input_shape)

    x = conv(inputs, 64, (7, 7), strides=2, name='conv0')
    x = bn(x, name='bn_conv1')
    x = Activation('relu')(x)
    x = MaxPooling2D((3, 3), strides=(2, 2), padding='same')(x)

    x = conv_block_simple(x, 3, [64, 64], stage=2, block='a', strides=(1, 1))
    x = identity_block_simple(x, 3, [64, 64], stage=2, block='b')

    x = conv_block_simple(x, 3, [128, 128], stage=3, block='a')
    x = identity_block_simple(x, 3, [128, 128], stage=3, block='b')

    x = conv_block_simple(x, 3, [256, 256], stage=4, block='a')
    x = identity_block_simple(x, 3, [256, 256], stage=4, block='b')

    x = conv_block_simple(x, 3, [512, 512], stage=5, block='a')
    x = identity_block_simple(x, 3, [512, 512], stage=5, block='b')

    x = GlobalAveragePooling2D(name='avg_pool')(x)
    # x = Flatten()(x)
    outputs = Dense(num_class,
                    activation='softmax',
                    kernel_initializer='he_normal')(x)

    model = Model(inputs=inputs, outputs=outputs)
    return model


def resnet34(conf):
    input_shape = (conf['IMG_F'], conf['IMG_T'], 1)
    num_class = conf['num_class']

    inputs = Input(shape=input_shape)

    x = conv(inputs, 64, (7, 7), strides=2, name='conv0')
    x = bn(x, name='bn_conv1')
    x = Activation('relu')(x)
    x = MaxPooling2D((3, 3), strides=(2, 2), padding='same')(x)

    x = conv_block_simple(x, 3, [64, 64], stage=2, block='a', strides=(1, 1))
    x = identity_block_simple(x, 3, [64, 64], stage=2, block='b')
    x = identity_block_simple(x, 3, [64, 64], stage=2, block='c')

    x = conv_block_simple(x, 3, [128, 128], stage=3, block='a')
    x = identity_block_simple(x, 3, [128, 128], stage=3, block='b')
    x = identity_block_simple(x, 3, [128, 128], stage=3, block='c')
    x = identity_block_simple(x, 3, [128, 128], stage=3, block='d')

    x = conv_block_simple(x, 3, [256, 256], stage=4, block='a')
    x = identity_block_simple(x, 3, [256, 256], stage=4, block='b')
    x = identity_block_simple(x, 3, [256, 256], stage=4, block='c')
    x = identity_block_simple(x, 3, [256, 256], stage=4, block='d')
    x = identity_block_simple(x, 3, [256, 256], stage=4, block='e')
    x = identity_block_simple(x, 3, [256, 256], stage=4, block='f')

    x = conv_block_simple(x, 3, [512, 512], stage=5, block='a')
    x = identity_block_simple(x, 3, [512, 512], stage=5, block='b')
    x = identity_block_simple(x, 3, [512, 512], stage=5, block='c')

    x = GlobalAveragePooling2D(name='avg_pool')(x)
    # x = Flatten()(x)
    outputs = Dense(num_class,
                    activation='softmax',
                    kernel_initializer='he_normal')(x)

    model = Model(inputs=inputs, outputs=outputs)
    return model


def resnet50(conf):
    input_shape = (conf['IMG_F'], conf['IMG_T'], 1)
    num_class = conf['num_class']

    inputs = Input(shape=input_shape)

    x = conv(inputs, 64, (7, 7), strides=2, name='conv0')
    x = bn(x, name='bn_conv1')
    x = Activation('relu')(x)
    x = MaxPooling2D((3, 3), strides=(2, 2), padding='same')(x)

    x = conv_block(x, 3, [64, 64, 256], stage=2, block='a', strides=(1, 1))
    x = identity_block(x, 3, [64, 64, 256], stage=2, block='b')
    x = identity_block(x, 3, [64, 64, 256], stage=2, block='c')

    x = conv_block(x, 3, [128, 128, 512], stage=3, block='a')
    x = identity_block(x, 3, [128, 128, 512], stage=3, block='b')
    x = identity_block(x, 3, [128, 128, 512], stage=3, block='c')
    x = identity_block(x, 3, [128, 128, 512], stage=3, block='d')

    x = conv_block(x, 3, [256, 256, 1024], stage=4, block='a')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='b')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='c')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='d')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='e')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='f')

    x = conv_block(x, 3, [512, 512, 2048], stage=5, block='a')
    x = identity_block(x, 3, [512, 512, 2048], stage=5, block='b')
    x = identity_block(x, 3, [512, 512, 2048], stage=5, block='c')

    x = GlobalAveragePooling2D(name='avg_pool')(x)
    # x = Flatten()(x)
    outputs = Dense(num_class,
                    activation='softmax',
                    kernel_initializer='he_normal')(x)

    model = Model(inputs=inputs, outputs=outputs)
    return model


def resnet_cifar10_expt(conf):
    # resemble resnet_v1 using conv_block, identity_block
    # stack=4, filter_num=16
    input_shape = (conf['IMG_F'], conf['IMG_T'], 1)
    num_class = conf['num_class']
    num_filters = conf['num_filters']

    inputs = Input(shape=input_shape)
    x = conv(inputs, num_filters, (3, 3), strides=1, name='conv0')
    x = bn(x, name='bn_conv1')
    x = Activation('relu')(x)

    x = conv_block_simple(x, 3, [num_filters, num_filters], stage=2, block='a', strides=(1, 1))
    x = identity_block_simple(x, 3, [num_filters, num_filters], stage=2, block='b')
    x = identity_block_simple(x, 3, [num_filters, num_filters], stage=2, block='c')

    x = conv_block_simple(x, 3, [num_filters*2, num_filters*2], stage=3, block='a')
    x = identity_block_simple(x, 3, [num_filters*2, num_filters*2], stage=3, block='b')
    x = identity_block_simple(x, 3, [num_filters*2, num_filters*2], stage=3, block='c')

    x = conv_block_simple(x, 3, [num_filters*4, num_filters*4], stage=4, block='a')
    x = identity_block_simple(x, 3, [num_filters*4, num_filters*4], stage=4, block='b')
    x = identity_block_simple(x, 3, [num_filters*4, num_filters*4], stage=4, block='c')

    x = conv_block_simple(x, 3, [num_filters*8, num_filters*8], stage=5, block='a')
    x = identity_block_simple(x, 3, [num_filters*8, num_filters*8], stage=5, block='b')
    x = identity_block_simple(x, 3, [num_filters*8, num_filters*8], stage=5, block='c')

    x = GlobalAveragePooling2D(name='avg_pool')(x)
    # x = AveragePooling2D(pool_size=(8, 8), name='avg_pool')(x)
    # x = Flatten()(x)
    outputs = Dense(num_class,
                    activation='softmax',
                    kernel_initializer='he_normal')(x)

    model = Model(inputs=inputs, outputs=outputs)
    return model


def resnet_cifar10_expt_maxpool(conf):
    # resemble resnet_v1 using conv_block, identity_block
    # stack=4, filter_num=16
    input_shape = (conf['IMG_F'], conf['IMG_T'], 1)
    num_class = conf['num_class']
    num_filters = conf['num_filters']

    inputs = Input(shape=input_shape)
    x = conv(inputs, num_filters, (3, 3), strides=1, name='conv0')
    x = bn(x, name='bn_conv1')
    x = Activation('relu')(x)
    x = MaxPooling2D((3, 3), strides=(2, 2), padding='same')(x)

    x = conv_block_simple(x, 3, [num_filters, num_filters], stage=2, block='a', strides=(1, 1))
    x = identity_block_simple(x, 3, [num_filters, num_filters], stage=2, block='b')
    x = identity_block_simple(x, 3, [num_filters, num_filters], stage=2, block='c')

    x = conv_block_simple(x, 3, [num_filters*2, num_filters*2], stage=3, block='a')
    x = identity_block_simple(x, 3, [num_filters*2, num_filters*2], stage=3, block='b')
    x = identity_block_simple(x, 3, [num_filters*2, num_filters*2], stage=3, block='c')

    x = conv_block_simple(x, 3, [num_filters*4, num_filters*4], stage=4, block='a')
    x = identity_block_simple(x, 3, [num_filters*4, num_filters*4], stage=4, block='b')
    x = identity_block_simple(x, 3, [num_filters*4, num_filters*4], stage=4, block='c')

    x = conv_block_simple(x, 3, [num_filters*8, num_filters*8], stage=5, block='a')
    x = identity_block_simple(x, 3, [num_filters*8, num_filters*8], stage=5, block='b')
    x = identity_block_simple(x, 3, [num_filters*8, num_filters*8], stage=5, block='c')

    x = GlobalAveragePooling2D(name='avg_pool')(x)
    # x = AveragePooling2D(pool_size=(8, 8), name='avg_pool')(x)
    # x = Flatten()(x)
    outputs = Dense(num_class,
                    activation='softmax',
                    kernel_initializer='he_normal')(x)

    model = Model(inputs=inputs, outputs=outputs)
    return model


def resnet_cifar10_expt_no_direct(conf):
    # resemble resnet_v1 using conv_block, identity_block
    # stack=4, filter_num=16
    input_shape = (conf['IMG_F'], conf['IMG_T'], 1)
    num_class = conf['num_class']
    num_filters = conf['num_filters']

    inputs = Input(shape=input_shape)
    x = conv(inputs, num_filters, (3, 3), strides=1, name='conv0')
    x = bn(x, name='bn_conv1')
    x = Activation('relu')(x)

    x = conv_block_2_no_direct(x, 3, [num_filters, num_filters], stage=2, block='a')
    x = conv_block_2_no_direct(x, 3, [num_filters, num_filters], stage=2, block='b')
    x = conv_block_2_no_direct(x, 3, [num_filters, num_filters], stage=2, block='c')

    x = conv_block_2_no_direct(x, 3, [num_filters*2, num_filters*2], stage=3, block='a', strides=(2, 2))
    x = conv_block_2_no_direct(x, 3, [num_filters*2, num_filters*2], stage=3, block='b')
    x = conv_block_2_no_direct(x, 3, [num_filters*2, num_filters*2], stage=3, block='c')

    x = conv_block_2_no_direct(x, 3, [num_filters*4, num_filters*4], stage=4, block='a', strides=(2, 2))
    x = conv_block_2_no_direct(x, 3, [num_filters*4, num_filters*4], stage=4, block='b')
    x = conv_block_2_no_direct(x, 3, [num_filters*4, num_filters*4], stage=4, block='c')

    x = conv_block_2_no_direct(x, 3, [num_filters*8, num_filters*8], stage=5, block='a', strides=(2, 2))
    x = conv_block_2_no_direct(x, 3, [num_filters*8, num_filters*8], stage=5, block='b')
    x = conv_block_2_no_direct(x, 3, [num_filters*8, num_filters*8], stage=5, block='c')

    x = GlobalAveragePooling2D(name='avg_pool')(x)
    # x = AveragePooling2D(pool_size=(8, 8), name='avg_pool')(x)
    # x = Flatten()(x)
    outputs = Dense(num_class,
                    activation='softmax',
                    kernel_initializer='he_normal')(x)

    model = Model(inputs=inputs, outputs=outputs)
    return model


def resnet_cifar10_expt_not_global(conf):
    # resemble resnet_v1 using conv_block, identity_block
    # stack=4, filter_num=16
    input_shape = (conf['IMG_F'], conf['IMG_T'], 1)
    num_class = conf['num_class']
    num_filters = conf['num_filters']

    inputs = Input(shape=input_shape)
    x = conv(inputs, num_filters, (3, 3), strides=1, name='conv0')
    x = bn(x, name='bn_conv1')
    x = Activation('relu')(x)

    x = conv_block_simple(x, 3, [num_filters, num_filters], stage=2, block='a', strides=(1, 1))
    x = identity_block_simple(x, 3, [num_filters, num_filters], stage=2, block='b')
    x = identity_block_simple(x, 3, [num_filters, num_filters], stage=2, block='c')

    x = conv_block_simple(x, 3, [num_filters*2, num_filters*2], stage=3, block='a')
    x = identity_block_simple(x, 3, [num_filters*2, num_filters*2], stage=3, block='b')
    x = identity_block_simple(x, 3, [num_filters*2, num_filters*2], stage=3, block='c')

    x = conv_block_simple(x, 3, [num_filters*4, num_filters*4], stage=4, block='a')
    x = identity_block_simple(x, 3, [num_filters*4, num_filters*4], stage=4, block='b')
    x = identity_block_simple(x, 3, [num_filters*4, num_filters*4], stage=4, block='c')

    x = conv_block_simple(x, 3, [num_filters*8, num_filters*8], stage=5, block='a')
    x = identity_block_simple(x, 3, [num_filters*8, num_filters*8], stage=5, block='b')
    x = identity_block_simple(x, 3, [num_filters*8, num_filters*8], stage=5, block='c')

    # x = GlobalAveragePooling2D(name='avg_pool')(x)
    x = AveragePooling2D(pool_size=(int(num_filters/2), int(num_filters/2)), name='avg_pool')(x)
    x = Flatten()(x)
    outputs = Dense(num_class,
                    activation='softmax',
                    kernel_initializer='he_normal')(x)

    model = Model(inputs=inputs, outputs=outputs)
    return model


def resnet_cifar10_expt_deep3(conf):
    # resemble resnet_v1 using conv_block, identity_block
    # stack=4, filter_num=16
    input_shape = (conf['IMG_F'], conf['IMG_T'], 1)
    num_class = conf['num_class']
    num_filters = conf['num_filters']

    inputs = Input(shape=input_shape)
    x = conv(inputs, num_filters, (3, 3), strides=1, name='conv0')
    x = bn(x, name='bn_conv1')
    x = Activation('relu')(x)

    x = conv_block_simple(x, 3, [num_filters, num_filters], stage=2, block='a', strides=(1, 1))
    x = identity_block_simple(x, 3, [num_filters, num_filters], stage=2, block='b')
    x = identity_block_simple(x, 3, [num_filters, num_filters], stage=2, block='c')

    x = conv_block_simple(x, 3, [num_filters*2, num_filters*2], stage=3, block='a')
    x = identity_block_simple(x, 3, [num_filters*2, num_filters*2], stage=3, block='b')
    x = identity_block_simple(x, 3, [num_filters*2, num_filters*2], stage=3, block='c')

    x = conv_block_simple(x, 3, [num_filters*4, num_filters*4], stage=4, block='a')
    x = identity_block_simple(x, 3, [num_filters*4, num_filters*4], stage=4, block='b')
    x = identity_block_simple(x, 3, [num_filters*4, num_filters*4], stage=4, block='c')

    x = GlobalAveragePooling2D(name='avg_pool')(x)
    # x = AveragePooling2D(pool_size=(int(num_filters/2), int(num_filters/2)), name='avg_pool')(x)
    # x = Flatten()(x)
    outputs = Dense(num_class,
                    activation='softmax',
                    kernel_initializer='he_normal')(x)

    model = Model(inputs=inputs, outputs=outputs)
    return model


def resnet_cifar10_expt_deep5(conf):
    # resemble resnet_v1 using conv_block, identity_block
    # stack=4, filter_num=16
    input_shape = (conf['IMG_F'], conf['IMG_T'], 1)
    num_class = conf['num_class']
    num_filters = conf['num_filters']

    inputs = Input(shape=input_shape)
    x = conv(inputs, num_filters, (3, 3), strides=1, name='conv0')
    x = bn(x, name='bn_conv1')
    x = Activation('relu')(x)

    x = conv_block_simple(x, 3, [num_filters, num_filters], stage=2, block='a', strides=(1, 1))
    x = identity_block_simple(x, 3, [num_filters, num_filters], stage=2, block='b')
    x = identity_block_simple(x, 3, [num_filters, num_filters], stage=2, block='c')

    x = conv_block_simple(x, 3, [num_filters*2, num_filters*2], stage=3, block='a')
    x = identity_block_simple(x, 3, [num_filters*2, num_filters*2], stage=3, block='b')
    x = identity_block_simple(x, 3, [num_filters*2, num_filters*2], stage=3, block='c')

    x = conv_block_simple(x, 3, [num_filters*4, num_filters*4], stage=4, block='a')
    x = identity_block_simple(x, 3, [num_filters*4, num_filters*4], stage=4, block='b')
    x = identity_block_simple(x, 3, [num_filters*4, num_filters*4], stage=4, block='c')

    x = conv_block_simple(x, 3, [num_filters*8, num_filters*8], stage=5, block='a')
    x = identity_block_simple(x, 3, [num_filters*8, num_filters*8], stage=5, block='b')
    x = identity_block_simple(x, 3, [num_filters*8, num_filters*8], stage=5, block='c')

    x = conv_block_simple(x, 3, [num_filters*16, num_filters*16], stage=6, block='a')
    x = identity_block_simple(x, 3, [num_filters*16, num_filters*16], stage=6, block='b')
    x = identity_block_simple(x, 3, [num_filters*16, num_filters*16], stage=6, block='c')

    x = GlobalAveragePooling2D(name='avg_pool')(x)
    # x = AveragePooling2D(pool_size=(int(num_filters/2), int(num_filters/2)), name='avg_pool')(x)
    # x = Flatten()(x)
    outputs = Dense(num_class,
                    activation='softmax',
                    kernel_initializer='he_normal')(x)

    model = Model(inputs=inputs, outputs=outputs)
    return model


def resnet_cifar10_expt_deep6(conf):
    # resemble resnet_v1 using conv_block, identity_block
    # stack=4, filter_num=16
    input_shape = (conf['IMG_F'], conf['IMG_T'], 1)
    num_class = conf['num_class']
    num_filters = conf['num_filters']

    inputs = Input(shape=input_shape)
    x = conv(inputs, num_filters, (3, 3), strides=1, name='conv0')
    x = bn(x, name='bn_conv1')
    x = Activation('relu')(x)

    x = conv_block_simple(x, 3, [num_filters, num_filters], stage=2, block='a', strides=(1, 1))
    x = identity_block_simple(x, 3, [num_filters, num_filters], stage=2, block='b')
    x = identity_block_simple(x, 3, [num_filters, num_filters], stage=2, block='c')

    x = conv_block_simple(x, 3, [num_filters*2, num_filters*2], stage=3, block='a')
    x = identity_block_simple(x, 3, [num_filters*2, num_filters*2], stage=3, block='b')
    x = identity_block_simple(x, 3, [num_filters*2, num_filters*2], stage=3, block='c')

    x = conv_block_simple(x, 3, [num_filters*4, num_filters*4], stage=4, block='a')
    x = identity_block_simple(x, 3, [num_filters*4, num_filters*4], stage=4, block='b')
    x = identity_block_simple(x, 3, [num_filters*4, num_filters*4], stage=4, block='c')

    x = conv_block_simple(x, 3, [num_filters*8, num_filters*8], stage=5, block='a')
    x = identity_block_simple(x, 3, [num_filters*8, num_filters*8], stage=5, block='b')
    x = identity_block_simple(x, 3, [num_filters*8, num_filters*8], stage=5, block='c')

    x = conv_block_simple(x, 3, [num_filters*16, num_filters*16], stage=6, block='a')
    x = identity_block_simple(x, 3, [num_filters*16, num_filters*16], stage=6, block='b')
    x = identity_block_simple(x, 3, [num_filters*16, num_filters*16], stage=6, block='c')

    x = conv_block_simple(x, 3, [num_filters*32, num_filters*32], stage=7, block='a')
    x = identity_block_simple(x, 3, [num_filters*32, num_filters*32], stage=7, block='b')
    x = identity_block_simple(x, 3, [num_filters*32, num_filters*32], stage=7, block='c')

    x = GlobalAveragePooling2D(name='avg_pool')(x)
    # x = AveragePooling2D(pool_size=(int(num_filters/2), int(num_filters/2)), name='avg_pool')(x)
    # x = Flatten()(x)
    outputs = Dense(num_class,
                    activation='softmax',
                    kernel_initializer='he_normal')(x)

    model = Model(inputs=inputs, outputs=outputs)
    return model


def resnet18_expt(conf):
    input_shape = (conf['IMG_F'], conf['IMG_T'], 1)
    num_class = conf['num_class']
    num_filters = conf['num_filters']

    inputs = Input(shape=input_shape)
    x = conv(inputs, num_filters, (3, 3), strides=1, name='conv0')
    x = bn(x, name='bn_conv1')
    x = Activation('relu')(x)

    # x = identity_block_simple(x, 3, [num_filters, num_filters], stage=2, block='a')
    x = conv_block_simple(x, 3, [num_filters, num_filters], stage=2, block='a', strides=(1, 1))
    x = identity_block_simple(x, 3, [num_filters, num_filters], stage=2, block='b')

    x = conv_block_simple(x, 3, [num_filters*2, num_filters*2], stage=3, block='a')
    x = identity_block_simple(x, 3, [num_filters*2, num_filters*2], stage=3, block='b')

    x = conv_block_simple(x, 3, [num_filters*4, num_filters*4], stage=4, block='a')
    x = identity_block_simple(x, 3, [num_filters*4, num_filters*4], stage=4, block='b')

    x = conv_block_simple(x, 3, [num_filters*8, num_filters*8], stage=5, block='a')
    x = identity_block_simple(x, 3, [num_filters*8, num_filters*8], stage=5, block='b')

    x = GlobalAveragePooling2D(name='avg_pool')(x)
    # x = AveragePooling2D(pool_size=(8, 8), name='avg_pool')(x)
    # x = Flatten()(x)
    outputs = Dense(num_class,
                    activation='softmax',
                    kernel_initializer='he_normal')(x)

    model = Model(inputs=inputs, outputs=outputs)
    return model


def resnet34_expt(conf):
    input_shape = (conf['IMG_F'], conf['IMG_T'], 1)
    num_class = conf['num_class']
    num_filters = conf['num_filters']

    inputs = Input(shape=input_shape)
    x = conv(inputs, num_filters, (3, 3), strides=1, name='conv0')
    x = bn(x, name='bn_conv1')
    x = Activation('relu')(x)

    x = conv_block_simple(x, 3, [num_filters, num_filters], stage=2, block='a', strides=(1, 1))
    x = identity_block_simple(x, 3, [num_filters, num_filters], stage=2, block='b')
    x = identity_block_simple(x, 3, [num_filters, num_filters], stage=2, block='c')

    x = conv_block_simple(x, 3, [num_filters*2, num_filters*2], stage=3, block='a')
    x = identity_block_simple(x, 3, [num_filters*2, num_filters*2], stage=3, block='b')
    x = identity_block_simple(x, 3, [num_filters*2, num_filters*2], stage=3, block='c')
    x = identity_block_simple(x, 3, [num_filters * 2, num_filters * 2], stage=3, block='d')

    x = conv_block_simple(x, 3, [num_filters*4, num_filters*4], stage=4, block='a')
    x = identity_block_simple(x, 3, [num_filters*4, num_filters*4], stage=4, block='b')
    x = identity_block_simple(x, 3, [num_filters*4, num_filters*4], stage=4, block='c')
    x = identity_block_simple(x, 3, [num_filters*4, num_filters*4], stage=4, block='d')
    x = identity_block_simple(x, 3, [num_filters*4, num_filters*4], stage=4, block='e')
    x = identity_block_simple(x, 3, [num_filters*4, num_filters*4], stage=4, block='f')

    x = conv_block_simple(x, 3, [num_filters*8, num_filters*8], stage=5, block='a')
    x = identity_block_simple(x, 3, [num_filters*8, num_filters*8], stage=5, block='b')
    x = identity_block_simple(x, 3, [num_filters*8, num_filters*8], stage=5, block='c')

    x = GlobalAveragePooling2D(name='avg_pool')(x)
    # x = AveragePooling2D(pool_size=(8, 8), name='avg_pool')(x)
    # x = Flatten()(x)
    outputs = Dense(num_class,
                    activation='softmax',
                    kernel_initializer='he_normal')(x)

    model = Model(inputs=inputs, outputs=outputs)
    return model


def resnet34_expt_maxpool(conf):
    input_shape = (conf['IMG_F'], conf['IMG_T'], 1)
    num_class = conf['num_class']
    num_filters = conf['num_filters']

    inputs = Input(shape=input_shape)
    x = conv(inputs, num_filters, (3, 3), strides=1, name='conv0')
    x = bn(x, name='bn_conv1')
    x = Activation('relu')(x)
    x = MaxPooling2D((3, 3), strides=(2, 2), padding='same')(x)

    x = conv_block_simple(x, 3, [num_filters, num_filters], stage=2, block='a', strides=(1, 1))
    x = identity_block_simple(x, 3, [num_filters, num_filters], stage=2, block='b')
    x = identity_block_simple(x, 3, [num_filters, num_filters], stage=2, block='c')

    x = conv_block_simple(x, 3, [num_filters*2, num_filters*2], stage=3, block='a')
    x = identity_block_simple(x, 3, [num_filters*2, num_filters*2], stage=3, block='b')
    x = identity_block_simple(x, 3, [num_filters*2, num_filters*2], stage=3, block='c')

    x = conv_block_simple(x, 3, [num_filters*4, num_filters*4], stage=4, block='a')
    x = identity_block_simple(x, 3, [num_filters*4, num_filters*4], stage=4, block='b')
    x = identity_block_simple(x, 3, [num_filters*4, num_filters*4], stage=4, block='c')

    x = conv_block_simple(x, 3, [num_filters*8, num_filters*8], stage=5, block='a')
    x = identity_block_simple(x, 3, [num_filters*8, num_filters*8], stage=5, block='b')
    x = identity_block_simple(x, 3, [num_filters*8, num_filters*8], stage=5, block='c')

    x = GlobalAveragePooling2D(name='avg_pool')(x)
    outputs = Dense(num_class,
                    activation='softmax',
                    kernel_initializer='he_normal')(x)

    model = Model(inputs=inputs, outputs=outputs)
    return model


def resnet50_expt(conf):
    # resemble resnet_v1 using conv_block, identity_block
    input_shape = (conf['IMG_F'], conf['IMG_T'], 1)
    num_class = conf['num_class']
    num_filters = conf['num_filters']

    inputs = Input(shape=input_shape)
    x = conv(inputs, num_filters, (3, 3), strides=1, name='conv0')
    x = bn(x, name='bn_conv1')
    x = Activation('relu')(x)

    x = conv_block(x, 3, [num_filters, num_filters, num_filters*4], stage=2, block='a', strides=(1, 1))
    x = identity_block(x, 3, [num_filters, num_filters, num_filters*4], stage=2, block='b')
    x = identity_block(x, 3, [num_filters, num_filters, num_filters*4], stage=2, block='c')

    x = conv_block(x, 3, [num_filters*2, num_filters*2, num_filters*8], stage=3, block='a')
    x = identity_block(x, 3, [num_filters*2, num_filters*2, num_filters*8], stage=3, block='b')
    x = identity_block(x, 3, [num_filters*2, num_filters*2, num_filters*8], stage=3, block='c')
    x = identity_block(x, 3, [num_filters*2, num_filters*2, num_filters*8], stage=3, block='d')

    x = conv_block(x, 3, [num_filters*4, num_filters*4, num_filters*16], stage=4, block='a')
    x = identity_block(x, 3, [num_filters*4, num_filters*4, num_filters*16], stage=4, block='b')
    x = identity_block(x, 3, [num_filters*4, num_filters*4, num_filters*16], stage=4, block='c')
    x = identity_block(x, 3, [num_filters*4, num_filters*4, num_filters*16], stage=4, block='d')
    x = identity_block(x, 3, [num_filters*4, num_filters*4, num_filters*16], stage=4, block='e')
    x = identity_block(x, 3, [num_filters*4, num_filters*4, num_filters*16], stage=4, block='f')

    x = conv_block(x, 3, [num_filters*8, num_filters*8, num_filters*32], stage=5, block='a')
    x = identity_block(x, 3, [num_filters*8, num_filters*8, num_filters*32], stage=5, block='b')
    x = identity_block(x, 3, [num_filters*8, num_filters*8, num_filters*32], stage=5, block='c')

    x = GlobalAveragePooling2D(name='avg_pool')(x)
    # x = AveragePooling2D(pool_size=(8, 8), name='avg_pool')(x)
    # x = Flatten()(x)
    outputs = Dense(num_class,
                    activation='softmax',
                    kernel_initializer='he_normal')(x)

    model = Model(inputs=inputs, outputs=outputs)
    return model


def resnet50_expt_nonglobal(conf):
    # resemble resnet_v1 using conv_block, identity_block
    input_shape = (conf['IMG_F'], conf['IMG_T'], 1)
    num_class = conf['num_class']
    num_filters = conf['num_filters']

    inputs = Input(shape=input_shape)
    x = conv(inputs, num_filters, (3, 3), strides=1, name='conv0')
    x = bn(x, name='bn_conv1')
    x = Activation('relu')(x)

    x = conv_block(x, 3, [num_filters, num_filters, num_filters*4], stage=2, block='a', strides=(1, 1))
    x = identity_block(x, 3, [num_filters, num_filters, num_filters*4], stage=2, block='b')
    x = identity_block(x, 3, [num_filters, num_filters, num_filters*4], stage=2, block='c')

    x = conv_block(x, 3, [num_filters*2, num_filters*2, num_filters*8], stage=3, block='a')
    x = identity_block(x, 3, [num_filters*2, num_filters*2, num_filters*8], stage=3, block='b')
    x = identity_block(x, 3, [num_filters*2, num_filters*2, num_filters*8], stage=3, block='c')
    x = identity_block(x, 3, [num_filters*2, num_filters*2, num_filters*8], stage=3, block='d')

    x = conv_block(x, 3, [num_filters*4, num_filters*4, num_filters*16], stage=4, block='a')
    x = identity_block(x, 3, [num_filters*4, num_filters*4, num_filters*16], stage=4, block='b')
    x = identity_block(x, 3, [num_filters*4, num_filters*4, num_filters*16], stage=4, block='c')
    x = identity_block(x, 3, [num_filters*4, num_filters*4, num_filters*16], stage=4, block='d')
    x = identity_block(x, 3, [num_filters*4, num_filters*4, num_filters*16], stage=4, block='e')
    x = identity_block(x, 3, [num_filters*4, num_filters*4, num_filters*16], stage=4, block='f')

    x = conv_block(x, 3, [num_filters*8, num_filters*8, num_filters*32], stage=5, block='a')
    x = identity_block(x, 3, [num_filters*8, num_filters*8, num_filters*32], stage=5, block='b')
    x = identity_block(x, 3, [num_filters*8, num_filters*8, num_filters*32], stage=5, block='c')

    # x = GlobalAveragePooling2D(name='avg_pool')(x)
    x = AveragePooling2D(pool_size=(8, 8), name='avg_pool')(x)
    x = Flatten()(x)
    outputs = Dense(num_class,
                    activation='softmax',
                    kernel_initializer='he_normal')(x)

    model = Model(inputs=inputs, outputs=outputs)
    return model


def resnet101_expt(conf):
    # resemble resnet_v1 using conv_block, identity_block
    input_shape = (conf['IMG_F'], conf['IMG_T'], 1)
    num_class = conf['num_class']
    num_filters = conf['num_filters']

    inputs = Input(shape=input_shape)
    x = conv(inputs, num_filters, (3, 3), strides=1, name='conv0')
    x = bn(x, name='bn_conv1')
    x = Activation('relu')(x)

    x = conv_block(x, 3, [num_filters, num_filters, num_filters*4], stage=2, block='a', strides=(1, 1))
    x = identity_block(x, 3, [num_filters, num_filters, num_filters*4], stage=2, block='b')
    x = identity_block(x, 3, [num_filters, num_filters, num_filters*4], stage=2, block='c')

    x = conv_block(x, 3, [num_filters*2, num_filters*2, num_filters*8], stage=3, block='a')
    x = identity_block(x, 3, [num_filters*2, num_filters*2, num_filters*8], stage=3, block='b')
    x = identity_block(x, 3, [num_filters*2, num_filters*2, num_filters*8], stage=3, block='c')
    x = identity_block(x, 3, [num_filters*2, num_filters*2, num_filters*8], stage=3, block='d')

    x = conv_block(x, 3, [num_filters*4, num_filters*4, num_filters*16], stage=4, block='_block1')
    for ii in range(1, 23):
        x = identity_block(x, 3, [num_filters*4, num_filters*4, num_filters*16], stage=4, block='_block'+str(ii+1))

    x = conv_block(x, 3, [num_filters*8, num_filters*8, num_filters*32], stage=5, block='a')
    x = identity_block(x, 3, [num_filters*8, num_filters*8, num_filters*32], stage=5, block='b')
    x = identity_block(x, 3, [num_filters*8, num_filters*8, num_filters*32], stage=5, block='c')

    x = GlobalAveragePooling2D(name='avg_pool')(x)
    # x = AveragePooling2D(pool_size=(8, 8), name='avg_pool')(x)
    # x = Flatten()(x)
    outputs = Dense(num_class,
                    activation='softmax',
                    kernel_initializer='he_normal')(x)

    model = Model(inputs=inputs, outputs=outputs)
    return model


def resnet152_expt(conf):
    # resemble resnet_v1 using conv_block, identity_block
    input_shape = (conf['IMG_F'], conf['IMG_T'], 1)
    num_class = conf['num_class']
    num_filters = conf['num_filters']

    inputs = Input(shape=input_shape)
    x = conv(inputs, num_filters, (3, 3), strides=1, name='conv0')
    x = bn(x, name='bn_conv1')
    x = Activation('relu')(x)

    x = conv_block(x, 3, [num_filters, num_filters, num_filters*4], stage=2, block='a', strides=(1, 1))
    x = identity_block(x, 3, [num_filters, num_filters, num_filters*4], stage=2, block='b')
    x = identity_block(x, 3, [num_filters, num_filters, num_filters*4], stage=2, block='c')

    x = conv_block(x, 3, [num_filters*2, num_filters*2, num_filters*8], stage=3, block='_block1')
    for ii in range(1, 8):
        x = identity_block(x, 3, [num_filters * 2, num_filters * 2, num_filters * 8], stage=3, block='_block'+str(ii+1))
        # x = identity_block(x, 3, [num_filters*2, num_filters*2, num_filters*8], stage=3, block='b')
        # x = identity_block(x, 3, [num_filters*2, num_filters*2, num_filters*8], stage=3, block='c')
        # x = identity_block(x, 3, [num_filters*2, num_filters*2, num_filters*8], stage=3, block='d')

    x = conv_block(x, 3, [num_filters*4, num_filters*4, num_filters*16], stage=4, block='_block1')
    for ii in range(1, 36):
        x = identity_block(x, 3, [num_filters*4, num_filters*4, num_filters*16], stage=4, block='_block'+str(ii+1))

    x = conv_block(x, 3, [num_filters*8, num_filters*8, num_filters*32], stage=5, block='a')
    x = identity_block(x, 3, [num_filters*8, num_filters*8, num_filters*32], stage=5, block='b')
    x = identity_block(x, 3, [num_filters*8, num_filters*8, num_filters*32], stage=5, block='c')

    x = GlobalAveragePooling2D(name='avg_pool')(x)
    # x = AveragePooling2D(pool_size=(8, 8), name='avg_pool')(x)
    # x = Flatten()(x)
    outputs = Dense(num_class,
                    activation='softmax',
                    kernel_initializer='he_normal')(x)

    model = Model(inputs=inputs, outputs=outputs)
    return model


def resnet_layer(inputs,
                 num_filters=16,
                 kernel_size=3,
                 strides=1,
                 activation='relu',
                 batch_normalization=True,
                 conv_first=True):
    """2D Convolution-Batch Normalization-Activation stack builder

    # Arguments
        inputs (tensor): input tensor from input image or previous layer
        num_filters (int): Conv2D number of filters
        kernel_size (int): Conv2D square kernel dimensions
        strides (int): Conv2D square stride dimensions
        activation (string): activation name
        batch_normalization (bool): whether to include batch normalization
        conv_first (bool): conv-bn-activation (True) or
            bn-activation-conv (False)

    # Returns
        x (tensor): tensor as input to the next layer
    """
    conv = Conv2D(num_filters,
                  kernel_size=kernel_size,
                  strides=strides,
                  padding='same',
                  kernel_initializer='he_normal',
                  kernel_regularizer=regularizers.l2(1e-4))

    x = inputs
    if conv_first:
        x = conv(x)
        if batch_normalization:
            x = BatchNormalization()(x)
        if activation is not None:
            x = Activation(activation)(x)
    else:
        if batch_normalization:
            x = BatchNormalization()(x)
        if activation is not None:
            x = Activation(activation)(x)
        x = conv(x)
    return x


def resnet_v1(conf):
    """ResNet Version 1 Model builder [a]

    Stacks of 2 x (3 x 3) Conv2D-BN-ReLU
    Last ReLU is after the shortcut connection.
    At the beginning of each stage, the feature map size is halved (downsampled)
    by a convolutional layer with strides=2, while the number of filters is
    doubled. Within each stage, the layers have the same number filters and the
    same number of filters.
    Features maps sizes:
    stage 0: 32x32, 16
    stage 1: 16x16, 32
    stage 2:  8x8,  64
    The Number of parameters is approx the same as Table 6 of [a]:
    ResNet20 0.27M
    ResNet32 0.46M
    ResNet44 0.66M
    ResNet56 0.85M
    ResNet110 1.7M

    # Arguments
        input_shape (tensor): shape of input image tensor
        depth (int): number of core convolutional layers
        num_class (int): number of classes (CIFAR10 has 10)

    # Returns
        model (Model): Keras model instance
    """
    input_shape = (conf['IMG_F'], conf['IMG_T'], 1)
    depth = conf['depth']
    num_class = conf['num_class']
    num_stack = conf['num_stack']
    num_filters = conf['num_filters']
    kernel_size = conf['kernel_size']

    if (depth - 2) % 6 != 0:
        raise ValueError('depth should be 6n+2 (eg 20, 32, 44 in [a])')
    # Start model definition.
    # num_filters = 16
    num_res_blocks = int((depth - 2) / 6)

    inputs = Input(shape=input_shape)
    x = resnet_layer(inputs=inputs, num_filters=num_filters)
    # Instantiate the stack of residual units
    # for stack in range(3):
    for stack in range(num_stack):
        for res_block in range(num_res_blocks):
            strides = 1
            if stack > 0 and res_block == 0:  # first layer but not first stack
                strides = 2  # downsample
            y = resnet_layer(inputs=x,
                             num_filters=num_filters,
                             kernel_size=kernel_size,
                             strides=strides)
            y = resnet_layer(inputs=y,
                             num_filters=num_filters,
                             kernel_size=kernel_size,
                             activation=None)
            if stack > 0 and res_block == 0:  # first layer but not first stack
                # linear projection residual shortcut connection to match
                # changed dims
                x = resnet_layer(inputs=x,
                                 num_filters=num_filters,
                                 kernel_size=1,
                                 strides=strides,
                                 activation=None,
                                 batch_normalization=False)
            x = add([x, y])
            x = Activation('relu')(x)
        num_filters *= 2

    # Add classifier on top.
    # v1 does not use BN after last shortcut connection-ReLU
    x = AveragePooling2D(pool_size=(8, 8))(x)
    y = Flatten()(x)
    outputs = Dense(num_class,
                    activation='softmax',
                    kernel_initializer='he_normal')(y)

    # Instantiate model.
    model = Model(inputs=inputs, outputs=outputs)
    return model


def resnet_v2(input_shape, depth, num_class=10):
    """ResNet Version 2 Model builder [b]

    Stacks of (1 x 1)-(3 x 3)-(1 x 1) BN-ReLU-Conv2D or also known as
    bottleneck layer
    First shortcut connection per layer is 1 x 1 Conv2D.
    Second and onwards shortcut connection is identity.
    At the beginning of each stage, the feature map size is halved (downsampled)
    by a convolutional layer with strides=2, while the number of filter maps is
    doubled. Within each stage, the layers have the same number filters and the
    same filter map sizes.
    Features maps sizes:
    conv1  : 32x32,  16
    stage 0: 32x32,  64
    stage 1: 16x16, 128
    stage 2:  8x8,  256

    # Arguments
        input_shape (tensor): shape of input image tensor
        depth (int): number of core convolutional layers
        num_class (int): number of classes (CIFAR10 has 10)

    # Returns
        model (Model): Keras model instance
    """
    if (depth - 2) % 9 != 0:
        raise ValueError('depth should be 9n+2 (eg 56 or 110 in [b])')
    # Start model definition.
    num_filters_in = 16
    num_res_blocks = int((depth - 2) / 9)

    inputs = Input(shape=input_shape)
    # v2 performs Conv2D with BN-ReLU on input before splitting into 2 paths
    x = resnet_layer(inputs=inputs,
                     num_filters=num_filters_in,
                     conv_first=True)

    # Instantiate the stack of residual units
    for stage in range(3):
        for res_block in range(num_res_blocks):
            activation = 'relu'
            batch_normalization = True
            strides = 1
            if stage == 0:
                num_filters_out = num_filters_in * 4
                if res_block == 0:  # first layer and first stage
                    activation = None
                    batch_normalization = False
            else:
                num_filters_out = num_filters_in * 2
                if res_block == 0:  # first layer but not first stage
                    strides = 2    # downsample

            # bottleneck residual unit
            y = resnet_layer(inputs=x,
                             num_filters=num_filters_in,
                             kernel_size=1,
                             strides=strides,
                             activation=activation,
                             batch_normalization=batch_normalization,
                             conv_first=False)
            y = resnet_layer(inputs=y,
                             num_filters=num_filters_in,
                             conv_first=False)
            y = resnet_layer(inputs=y,
                             num_filters=num_filters_out,
                             kernel_size=1,
                             conv_first=False)
            if res_block == 0:
                # linear projection residual shortcut connection to match
                # changed dims
                x = resnet_layer(inputs=x,
                                 num_filters=num_filters_out,
                                 kernel_size=1,
                                 strides=strides,
                                 activation=None,
                                 batch_normalization=False)
            x = add([x, y])

        num_filters_in = num_filters_out

    # Add classifier on top.
    # v2 has BN-ReLU before Pooling
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = AveragePooling2D(pool_size=(8, 8))(x)
    y = Flatten()(x)
    outputs = Dense(num_class,
                    activation='softmax',
                    kernel_initializer='he_normal')(y)

    # Instantiate model.
    model = Model(inputs=inputs, outputs=outputs)
    return model


def lstm_2lay(conf):
    """
    LSTM
    :param data_shape:
    :param conf:
    :return:
    """
    l2_regu = conf["l2_regu"]
    dropout_rate = conf["dropout"]
    recurr_dropout_rate = conf["recurrent_dropout"]
    data_shape = (conf['IMG_T'], conf['IMG_F'])
    num_chan1 = conf["lstm1"]
    num_chan2 = conf["lstm2"]
    num_class = conf['num_class']
    dense_size = conf["dense_size"]

    model_rnn = Sequential()
    model_rnn.add(LSTM(num_chan1,
                       input_shape=data_shape,
                       return_sequences=True,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       dropout=dropout_rate,
                       recurrent_dropout=recurr_dropout_rate
                       ))
    model_rnn.add(BatchNormalization(center=False, scale=False))

    model_rnn.add(LSTM(num_chan2,
                       return_sequences=False,
                       kernel_regularizer=regularizers.l2(l2_regu),
                       recurrent_regularizer=regularizers.l2(l2_regu),
                       dropout=dropout_rate,
                       recurrent_dropout=recurr_dropout_rate))
    model_rnn.add(BatchNormalization(center=False, scale=False))

    # model_rnn.add(Dense(dense_size))
    model_rnn.add(Dense(num_class, activation='softmax'))
    return model_rnn


def conv2d_lstm(conf):  # expect input shape 5
    l2_regu = conf["l2_regu"]
    dropout_rate = conf["dropout"]
    recurr_dropout_rate = conf["recurrent_dropout"]
    data_shape = (conf['IMG_T'], conf['IMG_F'], 1, 1)  # last two dims are image's y-axis/freq and num of rbg
    num_chan1 = conf["lstm1"]
    num_chan2 = conf["lstm2"]
    dense_size = conf["dense_size"]

    model = Sequential()
    model.add(ConvLSTM2D(filters=num_chan1, kernel_size=(4, 1),
                         strides=(2, 1),
                         input_shape=data_shape, padding='valid',
                         kernel_regularizer=regularizers.l2(l2_regu),
                         recurrent_regularizer=regularizers.l2(l2_regu),
                         dropout=dropout_rate,
                         recurrent_dropout=recurr_dropout_rate,
                         return_sequences=True))
    model.add(BatchNormalization())
    model.add(ConvLSTM2D(filters=num_chan2, kernel_size=(4, 1),
                         strides=(2, 1),
                         padding='valid',
                         kernel_regularizer=regularizers.l2(l2_regu),
                         recurrent_regularizer=regularizers.l2(l2_regu),
                         dropout=dropout_rate,
                         recurrent_dropout=recurr_dropout_rate,
                         return_sequences=False))
    model.add(BatchNormalization())

    model.add(Flatten())
    model.add(Dense(dense_size))
    model.add(Dense(conf["num_class"], activation='softmax'))

    return model


def contour_data(file_contour, time_reso):
    print('Retrieving contours...')
    contour_target_ff = []
    len_contour = len(file_contour)
    print('len_contour: '+str(len_contour))
    time_min = 86400.0
    time_max = 0.0
    freq_high = 0.0
    freq_low = 192000.0
    # read contours into the var contour_target_ff
    for cc in range(len_contour):
        time_contour = file_contour[cc]['Time']
        freq_contour = file_contour[cc]['Freq']

        if time_contour.shape[0] > 1:
            # linear interpolation
            # time_contour_interp = np.arange(time_contour[0], time_contour[-1],
            #                                 time_reso)
            new_start_time = round(time_contour[0]/time_reso)*time_reso
            new_step = ceil((time_contour[-1] - time_contour[0])/time_reso)
            time_contour_interp = np.arange(new_start_time, new_start_time+new_step*time_reso, time_reso)

            time_min = np.min((time_contour_interp[0], time_min))
            time_max = np.max((time_contour_interp[-1], time_max))

            freq_contour_interp = np.interp(time_contour_interp, time_contour,
                                            freq_contour)
            freq_high = np.max((np.max(freq_contour_interp), freq_high))
            freq_low = np.min((np.min(freq_contour_interp), freq_low))

            contour_target_ff_cc = dict()
            contour_target_ff_cc['Time'] = time_contour_interp
            contour_target_ff_cc['Freq'] = freq_contour_interp

            contour_target_ff.append(contour_target_ff_cc)

    return contour_target_ff, time_min, time_max, freq_low, freq_high


def prepare_data(contour_target_list, conf, img_folder, save_file, plot=False):
    '''
    Convert whistle contours into sequences of fixed length
    :param contour_target_list:
    time_reso = 0.1, context_winsize=10.0, ratio_thre=0.02
    :return:
    df_target:
    '''
    freq_low_all = 192000.0
    freq_high_all = 0.0
    whistle_image_list = []
    label_list = []

    if plot:
        plt.ion()
        fig = plt.figure()
        ax = fig.add_subplot(111)

        if not os.path.exists(img_folder):
            os.mkdir(img_folder)
            for ss in conf['species_name']:
                os.mkdir(os.path.join(img_folder, ss))

    data_count = 1
    for ff in range(len(contour_target_list)):
        filename = contour_target_list[ff][0]
        print('\n'+filename)
        label_contour = contour_target_list[ff][1]
        file_contour = contour_target_list[ff][2]

        contour_target_ff, start_time, end_time, freq_low, freq_high = \
            contour_data(file_contour, conf['time_reso'])
        freq_high_all = np.max((freq_high, freq_high_all))
        freq_low_all = np.min((freq_low, freq_low_all))

        timesteps = ceil((end_time - start_time)/conf['time_reso'])+1
        print("Start time: "+str(start_time))
        print("Stop time: " + str(end_time))

        # Binary spectrogram
        # convert whistle freq into into a 2d feature map: whistle_freq for each file
        whistle_freq = np.zeros((conf['fft_size'], int(timesteps)))
        for cc in contour_target_ff:
            time_ind_start = int(floor((cc['Time'][0]-start_time)/conf['time_reso']))
            freq_ind = (np.floor(cc['Freq']/conf['sample_rate']*conf['fft_size'])).astype('int')
            for ii in range(cc['Time'].shape[0]):
                try:
                    whistle_freq[freq_ind[ii], time_ind_start+ii] = 1.0
                except:
                    print('stop!')

        if conf['spectro_dilation']:
            # whistle_freq_smooth = cv2.GaussianBlur(whistle_freq, (3, 3), 0, 0, cv2.BORDER_DEFAULT)
            kernel = np.ones((3, 3)).astype(np.uint8)
            kernel[0, 0] = 0
            kernel[0, 2] = 0
            kernel[2, 0] = 0
            kernel[2, 2] = 0
            whistle_freq_smooth = cv2.dilate(whistle_freq, kernel, cv2.BORDER_DEFAULT)
            whistle_freq = whistle_freq_smooth

        # plt.matshow(whistle_freq); plt.show()
        # print('whistle_freq shape: '+str(whistle_freq.shape[0])+', '+str(whistle_freq.shape[1]) )

        # cut whistle_freq into segments for data samples
        size_time = int(conf['context_winsize']/conf['time_reso'])
        size_hop = int(conf['context_hopsize']/conf['time_reso'])
        freq_high = conf['freq_ind_high']
        freq_low = conf['freq_ind_low']
        for tt in range(floor((whistle_freq.shape[1]-size_time)/size_hop)):
            whistle_image = whistle_freq[freq_low:freq_high, tt*size_hop:tt*size_hop+size_time]
            # if whistle_image.sum()/whistle_image.shape[0]/whistle_image.shape[1] >= 0.01:
            # if whistle_image.sum() >= 0.1*conf['IMG_T']:
            # print('whistle_image.sum: '+str(whistle_image.sum()))
            # plt.draw()
            # plt.pause(0.0001)
            # plt.clf()
            # plt.show()
            if (whistle_image.sum(axis=0) > 0).sum() >= conf['contour_timethre']:
                whistle_image_list.append(whistle_image)
                label_list.append(conf['species_id'][label_contour])

                if plot is True:
                    ax.matshow(whistle_image, origin='lower')
                    ax.title.set_text(str(data_count)+': '+label_contour)
                    ax.xaxis.tick_bottom()
                    fig.canvas.draw()
                    plt.savefig(os.path.join(img_folder, label_contour, str(data_count)+'_'+label_contour+'.png'))
                data_count += 1
                # print('stop for images')

    whistle_image_arr = np.asarray(whistle_image_list)
    # save image array
    if conf['numpy_data_output']:
        np.savez(save_file, whistle_image=whistle_image_arr, label=label_list)

    return whistle_image_arr, label_list, freq_high_all, freq_low_all


def prepare_data_audio(contour_target_list, sound_dir, conf, img_folder, save_file, plot=False):
    '''
    Convert whistle contours into sequences of fixed length
    :param contour_target_list:
    time_reso = 0.1, context_winsize=10.0, ratio_thre=0.02
    :return:
    df_target:
    '''
    freq_low_all = 192000.0  # an realistic upperbound
    freq_high_all = 0.0
    whistle_image_list = []
    label_list = []

    if plot:
        plt.ion()
        fig = plt.figure()
        ax = fig.add_subplot(111)

        if not os.path.exists(img_folder):
            os.mkdir(img_folder)
            for ss in conf['species_name']:
                os.mkdir(os.path.join(img_folder, ss))

    data_count = 1
    for ff in range(len(contour_target_list)):
        filename = contour_target_list[ff][0]
        print('\n'+filename)
        label_contour = contour_target_list[ff][1]
        file_contour = contour_target_list[ff][2]

        contour_target_ff, start_time, end_time, freq_low, freq_high = \
            contour_data(file_contour, conf['time_reso'])
        freq_high_all = np.max((freq_high, freq_high_all))
        freq_low_all = np.min((freq_low, freq_low_all))

        timesteps = ceil((end_time - start_time)/conf['time_reso'])+1
        print("Start time: "+str(start_time))
        print("Stop time: " + str(end_time))

        # spectrogram named whistle_freq for each file
        sound_path = os.path.join(sound_dir, label_contour, filename+'.wav')
        samples, _ = librosa.load(sound_path, sr=conf['sample_rate'], offset=start_time, duration=end_time-start_time+2.*conf['time_reso'])
        whistle_freq = np.abs(librosa.pseudo_cqt(samples, sr=conf['sample_rate'],
                                          hop_length=conf['hop_size'],
                                          fmin=4000.0, bins_per_octave=36,
                                          n_bins=144))

        whistle_presence = np.zeros((int(timesteps)))
        for cc in contour_target_ff:
            time_ind_start = int(floor((cc['Time'][0]-start_time)/conf['time_reso']))
            for ii in range(cc['Time'].shape[0]):
                whistle_presence[time_ind_start+ii] = 1.0

        # cut whistle_freq into segments for data samples
        size_time = int(conf['context_winsize']/conf['time_reso'])
        size_hop = int(conf['context_hopsize']/conf['time_reso'])
        freq_high = conf['freq_ind_high']
        freq_low = conf['freq_ind_low']
        for tt in range(floor((whistle_freq.shape[1]-size_time)/size_hop)):
            whistle_image = whistle_freq[freq_low:freq_high, tt*size_hop:tt*size_hop+size_time]
            whistle_presence_seg = whistle_presence[tt * size_hop:tt * size_hop + size_time]
            # plt.draw()
            # plt.pause(0.0001)
            # plt.clf()
            # plt.show()
            # if (whistle_image.sum(axis=0) > 0).sum() >= conf['contour_timethre']:
            if whistle_presence_seg.sum() >= conf['contour_timethre']:
                whistle_image_list.append(whistle_image)
                label_list.append(conf['species_id'][label_contour])

                if plot is True:
                    ax.matshow(whistle_image, origin='lower')
                    ax.title.set_text(str(data_count)+': '+label_contour)
                    ax.xaxis.tick_bottom()
                    fig.canvas.draw()
                    plt.savefig(os.path.join(img_folder, label_contour, str(data_count)+'_'+label_contour+'.png'))
                data_count += 1  # ? complex when having noise class
                # print('stop for images')
            elif conf['class_noise'] & (whistle_presence_seg.sum() == 0.0):  # no labels here!
                whistle_image_list.append(whistle_image)
                label_list.append(conf['species_id']['noise'])
                data_count += 1
                # noise class image here!
    whistle_image = np.asarray(whistle_image_list)
    # save image array
    if conf['numpy_data_output']:
        np.savez(save_file, whistle_image=whistle_image, label=label_list)

    # Shuffle: working on
    # label_idx = [ii for ii in range(len(label_list))]
    # random.shuffle(label_idx)
    # whistle_image_new = [whistle_image[tt] for tt in label_idx]
    # label_list_new = to_categorical(label_list)[label_idx, :]

    return whistle_image, label_list, freq_high_all, freq_low_all


def prepare_data_mask(contour_target_list, sound_dir, conf, img_folder, save_file, plot=False):
    '''
    Convert whistle contours into sequences of fixed length
    :param contour_target_list:
    time_reso = 0.1, context_winsize=10.0, ratio_thre=0.02
    :return:
    df_target:
    '''
    freq_low_all = 192000.0  # an realistic upperbound
    freq_high_all = 0.0
    whistle_image_list = []
    label_list = []

    if plot:
        plt.ion()
        fig = plt.figure()
        ax = fig.add_subplot(111)

        if not os.path.exists(img_folder):
            os.mkdir(img_folder)
            for ss in conf['species_name']:
                os.mkdir(os.path.join(img_folder, ss))

    data_count = 1
    for ff in range(len(contour_target_list)):
        filename = contour_target_list[ff][0]
        print('\n'+filename)
        label_contour = contour_target_list[ff][1]
        file_contour = contour_target_list[ff][2]

        contour_target_ff, start_time, end_time, freq_low, freq_high = \
            contour_data(file_contour, conf['time_reso'])
        freq_high_all = np.max((freq_high, freq_high_all))
        freq_low_all = np.min((freq_low, freq_low_all))

        timesteps = ceil((end_time - start_time)/conf['time_reso'])+1
        print("Start time: "+str(start_time))
        print("Stop time: " + str(end_time))

        # spectrogram named whistle_freq for each file
        sound_path = os.path.join(sound_dir, label_contour, filename+'.wav')
        samples, _ = librosa.load(sound_path, sr=conf['sample_rate'], offset=start_time, duration=end_time-start_time+2.*conf['time_reso'])
        whistle_freq0 = np.abs(librosa.pseudo_cqt(samples, sr=conf['sample_rate'],
                                          hop_length=conf['hop_size'],
                                          fmin=4000.0, bins_per_octave=36,
                                          n_bins=144))

        # Masked spectrogram
        whistle_freq = np.zeros((whistle_freq0.shape[0], whistle_freq0.shape[1]))
        for cc in contour_target_ff:
            time_ind_start = int(floor((cc['Time'][0]-start_time)/conf['time_reso']))
            # freq_ind = (np.floor(cc['Freq']/conf['sample_rate']*conf['fft_size'])).astype('int')
            freq_ind = (np.log2(cc['Freq']/4000.0)*36.).astype('int')
            freq_ind[freq_ind<0] = 0.0
            freq_ind[freq_ind >= 144] = 143

            if False: # width-1 mask
                for ii in range(cc['Time'].shape[0]):
                    whistle_freq[freq_ind[ii]-1:freq_ind[ii]+1+1, time_ind_start+ii] = whistle_freq0[freq_ind[ii]-1:freq_ind[ii]+1+1, time_ind_start+ii]

            # rasterization mask
            for ii in range(1, cc['Time'].shape[0]):
                if freq_ind[ii] > freq_ind[ii-1]:
                    whistle_freq[freq_ind[ii-1]-1:freq_ind[ii]+1+1, time_ind_start+ii-1:time_ind_start+ii+1] = whistle_freq0[freq_ind[ii-1]-1:freq_ind[ii]+1+1, time_ind_start+ii-1:time_ind_start+ii+1]
                elif freq_ind[ii] < freq_ind[ii-1]:
                    whistle_freq[freq_ind[ii]-1:freq_ind[ii-1]+1+1, time_ind_start+ii-1:time_ind_start+ii+1] = whistle_freq0[freq_ind[ii]-1:freq_ind[ii-1]+1+1, time_ind_start+ii-1:time_ind_start+ii+1]
                else:  # freq_ind[ii] == freq_ind[ii-1]
                    whistle_freq[freq_ind[ii]-1:freq_ind[ii]+1+1, time_ind_start+ii-1:time_ind_start+ii+1] = whistle_freq0[freq_ind[ii]-1:freq_ind[ii]+1+1, time_ind_start+ii-1:time_ind_start+ii+1]

        whistle_presence = np.zeros((int(timesteps)))
        for cc in contour_target_ff:
            time_ind_start = int(floor((cc['Time'][0]-start_time)/conf['time_reso']))
            for ii in range(cc['Time'].shape[0]):
                whistle_presence[time_ind_start+ii] = 1.0

        # cut whistle_freq into segments for data samples
        size_time = int(conf['context_winsize']/conf['time_reso'])
        size_hop = int(conf['context_hopsize']/conf['time_reso'])
        freq_high = conf['freq_ind_high']
        freq_low = conf['freq_ind_low']
        for tt in range(floor((whistle_freq.shape[1]-size_time)/size_hop)):
            whistle_image = whistle_freq[freq_low:freq_high, tt*size_hop:tt*size_hop+size_time]
            whistle_presence_seg = whistle_presence[tt * size_hop:tt * size_hop + size_time]
            # plt.draw()
            # plt.pause(0.0001)
            # plt.clf()
            # plt.show()
            # if (whistle_image.sum(axis=0) > 0).sum() >= conf['contour_timethre']:
            if whistle_presence_seg.sum() >= conf['contour_timethre']:
                whistle_image_list.append(whistle_image)
                label_list.append(conf['species_id'][label_contour])

                if plot is True:
                    ax.matshow(whistle_image, origin='lower')
                    ax.title.set_text(str(data_count)+': '+label_contour)
                    ax.xaxis.tick_bottom()
                    fig.canvas.draw()
                    plt.savefig(os.path.join(img_folder, label_contour, str(data_count)+'_'+label_contour+'.png'))
                    # plt.clf()
                data_count += 1  # ? complex when having noise class
                # print('stop for images')
            elif conf['class_noise'] & (whistle_presence_seg.sum() == 0.0):  # no labels here!
                whistle_image_list.append(whistle_image)
                label_list.append(conf['species_id']['noise'])
                data_count += 1
                # noise class image here!
    whistle_image = np.asarray(whistle_image_list)
    # save image array
    if conf['numpy_data_output']:
        np.savez(save_file, whistle_image=whistle_image, label=label_list)

    # Shuffle: working on
    # label_idx = [ii for ii in range(len(label_list))]
    # random.shuffle(label_idx)
    # whistle_image_new = [whistle_image[tt] for tt in label_idx]
    # label_list_new = to_categorical(label_list)[label_idx, :]

    return whistle_image, label_list, freq_high_all, freq_low_all


class ConfusionMatrixPlotter(Callback):
    """Plot the confusion matrix on a graph and update after each epoch
    # Arguments
        X_val: The input values
        Y_val: The expected output values
        classes: The categories as a list of string names
        normalize: True - normalize to [0,1], False - keep as is
        cmap: Specify matplotlib colour map
        title: Graph Title
    """
    def __init__(self, X_val, Y_val, classes, normalize=False,
                 cmap=plt.cm.Blues, title='Confusion Matrix'):
        self.X_val = X_val
        self.Y_val = Y_val
        self.title = title
        self.classes = classes
        self.normalize = normalize
        self.cmap = cmap
        plt.ion()
        # plt.show()
        plt.figure()

        plt.title(self.title)

    def on_train_begin(self, logs={}):
        pass

    def on_epoch_end(self, epoch, logs={}):
        plt.clf()
        pred = self.model.predict(self.X_val)
        max_pred = np.argmax(pred, axis=1)
        max_y = np.argmax(self.Y_val, axis=1)
        cnf_mat = confusion_matrix(max_y, max_pred)

        if self.normalize:
            cnf_mat = cnf_mat.astype('float') / cnf_mat.sum(axis=1)[:,
                                                np.newaxis]

        thresh = cnf_mat.max() / 2.
        for i, j in itertools.product(range(cnf_mat.shape[0]),
                                      range(cnf_mat.shape[1])):
            plt.text(j, i, cnf_mat[i, j], horizontalalignment="center",
                     color="white" if cnf_mat[i, j] > thresh else "black")

        plt.imshow(cnf_mat, interpolation='nearest', cmap=self.cmap)

        # Labels
        tick_marks = np.arange(len(self.classes))
        plt.xticks(tick_marks, self.classes, rotation=45)
        plt.yticks(tick_marks, self.classes)

        plt.colorbar()

        plt.tight_layout()
        plt.ylabel('True label')
        plt.xlabel('Predicted label')
        # plt.draw()
        plt.show()
        plt.pause(0.001)


def one_fold_validate(model_name, whistle_image_target_4d, label_target,
                  whistle_image_validate_4d, label_validate,
                  conf, fold_id=1):
    label_target_cat = to_categorical(label_target)
    label_validate_cat = to_categorical(label_validate)

    model_name_func = globals()[model_name]
    # model = model_name_func((conf['IMG_F'], conf['IMG_T'], 1), depth=20, num_class=4, num_stack=3, num_filters=32)
    model = model_name_func(conf)
    model_name_format = 'epoch_{epoch:02d}_valloss_{val_loss:.4f}_valacc_{val_acc:.4f}.hdf5'
    log_dir1 = os.path.join(conf['log_dir'], 'fold'+str(fold_id))
    if not os.path.exists(log_dir1):
        os.mkdir(log_dir1)
    check_path = os.path.join(log_dir1, model_name_format)

    if fold_id is 1:
        with open(os.path.join(conf['log_dir'], 'architecture.txt'), 'w') as f:
            with redirect_stdout(f):
                # print('')
                for kk in sorted(list(conf.keys())):
                    print(kk + ' ==>> ' + str(conf[kk]))
                model.summary()

    # checkpoint
    checkpoint = ModelCheckpoint(check_path, monitor='val_loss', verbose=0,
                                 save_best_only=True)
    early_stop = EarlyStopping(monitor='val_loss', mode='min', verbose=1,
                               patience=50)
    if conf['confusion_callback']:
        cm_plot = ConfusionMatrixPlotter(whistle_image_validate_4d,
                                         label_validate_cat, species_name)
    # model compile
    model.compile(loss=categorical_crossentropy,
                  optimizer=Adadelta(lr=conf['learning_rate']),
                  # optimizer=Adam(lr=conf['learning_rate']),
                  metrics=['accuracy'])
    model.summary()

    count_species1 = label_target_cat.sum(axis=0).tolist()
    conf["class_weight"] = (
                max(count_species1) / np.array(count_species1)).tolist()

    if conf['confusion_callback']:
        callback_list = [checkpoint, TensorBoard(log_dir=log_dir1), cm_plot,
                         early_stop]
    else:
        callback_list = [checkpoint, TensorBoard(log_dir=log_dir1), early_stop]

    model.fit(whistle_image_target_4d, label_target_cat,
              batch_size=conf['batch_size'], epochs=conf['epoch'],
              verbose=1, validation_split=0.2,
              callbacks=callback_list, class_weight=conf["class_weight"])
    re_model_name_format = 'epoch_\d+_valloss_(\d+.\d{4})_valacc_\d+.\d{4}.hdf5'
    best_model_path, _ = find_best_model(log_dir1, re_model_name_format,
                                         is_max=False, purge=True)
    conf['best_model'] = best_model_path
    model = load_model(best_model_path)
    y_pred_pro = model.predict(whistle_image_validate_4d)
    y_pred2 = np.argmax(y_pred_pro, axis=1)
    metrics_two_fold(label_validate, y_pred2, log_dir1, 'accuracy_fold.txt',
                     conf, mode='fold')

    np.savetxt(os.path.join(log_dir1, 'pred_label.txt'), y_pred2, delimiter=',', fmt='%d')
    np.savetxt(os.path.join(log_dir1, 'pred_prob.txt'), y_pred_pro, delimiter=',', fmt='%.6f')

    del model
    gc.collect()
    backend.clear_session()

    return y_pred2, y_pred_pro, best_model_path


def one_fold_validate_generator(model_name, whistle_image_target_4d,
                                label_target, whistle_image_test_4d,
                                label_test, conf, fold_id=1):
    label_target_cat = to_categorical(label_target)
    label_test_cat = to_categorical(label_test)

    whistle_image_train_4d, whistle_image_validate_4d, label_train_cat, \
    label_validate_cat = train_test_split(whistle_image_target_4d,
                                          label_target_cat, test_size=0.2)
    if conf['network_type'] is 'rnn':
        gen_train = data_generator(whistle_image_train_4d, label_train_cat,
                                   batch_size=conf['batch_size'], network_type='rnn')
    elif conf['network_type'] is 'conv2d_lstm':
        gen_train = data_generator(whistle_image_train_4d, label_train_cat,
                                   batch_size=conf['batch_size'], network_type='conv2d_lstm')
    else:  # cnn
        gen_train = data_generator(whistle_image_train_4d, label_train_cat,
                                    batch_size=conf['batch_size'])
        # gen_validate = data_generator(whistle_image_validate_4d, label_validate_cat,
        #                           batch_size=conf['batch_size'])


    model_name_func = globals()[model_name]
    model = model_name_func(conf)
    model_name_format = 'epoch_{epoch:02d}_valloss_{val_loss:.4f}_valacc_{val_acc:.4f}.hdf5'
    log_dir1 = os.path.join(conf['log_dir'], 'fold'+str(fold_id))
    if not os.path.exists(log_dir1):
        os.mkdir(log_dir1)
    check_path = os.path.join(log_dir1, model_name_format)

    if fold_id is 1:
        with open(os.path.join(conf['log_dir'], 'architecture.txt'), 'w') as f:
            with redirect_stdout(f):
                # print('')
                for kk in sorted(list(conf.keys())):
                    print(kk + ' ==>> ' + str(conf[kk]))
                model.summary()

    # checkpoint
    checkpoint = ModelCheckpoint(check_path, monitor='val_loss', verbose=0,
                                 save_best_only=True)
    early_stop = EarlyStopping(monitor='val_loss', mode='min', verbose=1,
                               patience=50)
    if conf['confusion_callback']:
        cm_plot = ConfusionMatrixPlotter(whistle_image_validate_4d,
                                         label_validate_cat, species_name)
    # model compile
    model.compile(loss=categorical_crossentropy,
                  optimizer=Adadelta(lr=conf['learning_rate']),
                  # optimizer=Adam(lr=conf['learning_rate']),
                  metrics=['accuracy'])
    model.summary()

    count_species1 = label_target_cat.sum(axis=0).tolist()
    conf["class_weight"] = (
                max(count_species1) / np.array(count_species1)).tolist()

    if conf['confusion_callback']:
        callback_list = [checkpoint, TensorBoard(log_dir=log_dir1), cm_plot,
                         early_stop]
    else:
        callback_list = [checkpoint, TensorBoard(log_dir=log_dir1), early_stop]

    steps = int(floor(whistle_image_train_4d.shape[0]/conf['batch_size']))
    model.fit_generator(gen_train,
                        epochs=conf['epoch'], verbose=1,
                        # samples_per_epoch=conf['batch_size']*steps,
                        # validation_data=(x_test, y_test_onehot),
                        validation_data=(whistle_image_validate_4d, label_validate_cat),
                        # validation_data=gen_validate,
                        # validation_steps=label_validate_cat.shape[0],
                        # validation_steps=steps_validate,
                        steps_per_epoch=steps, callbacks=callback_list,
                        class_weight=conf["class_weight"])

    re_model_name_format = 'epoch_\d+_valloss_(\d+.\d{4})_valacc_\d+.\d{4}.hdf5'
    best_model_path, _ = find_best_model(log_dir1, re_model_name_format,
                                         is_max=False, purge=True)
    conf['best_model'] = best_model_path
    model = load_model(best_model_path)
    y_pred_pro = model.predict(whistle_image_test_4d)
    y_pred2 = np.argmax(y_pred_pro, axis=1)
    metrics_two_fold(label_test, y_pred2, log_dir1, 'accuracy_fold.txt',
                     conf, mode='fold')

    np.savetxt(os.path.join(log_dir1, 'pred_label.txt'), y_pred2, delimiter=',', fmt='%d')
    np.savetxt(os.path.join(log_dir1, 'pred_prob.txt'), y_pred_pro, delimiter=',', fmt='%.6f')

    del model
    gc.collect()
    backend.clear_session()

    return y_pred2, y_pred_pro, best_model_path


def four_fold_validate(model_type, whistle_image_pie1_4d,
                       whistle_image_pie2_4d,
                       whistle_image_pie3_4d,
                       whistle_image_pie4_4d, label_pie1, label_pie2,
                       label_pie3, label_pie4, conf):
    start_time = timeit.default_timer()

    # fold 1: pie 2, 3, 4 as training and pie 1 as testing
    whistle_image_train_fold1_4d = np.vstack(
        (whistle_image_pie2_4d, whistle_image_pie3_4d, whistle_image_pie4_4d))
    label_train_fold1 = label_pie2 + label_pie3 + label_pie4
    y_pred1, y_pred_prob1, best_model1 = one_fold_validate(model_type,
                                             whistle_image_train_fold1_4d,
                                             label_train_fold1,
                                             whistle_image_pie1_4d, label_pie1,
                                             conf, fold_id=1)
    # fold 2: pie 1, 3, 4 as training and pie 2 as testing
    whistle_image_train_fold2_4d = np.vstack(
        (whistle_image_pie1_4d, whistle_image_pie3_4d, whistle_image_pie4_4d))
    label_train_fold2 = label_pie1 + label_pie3 + label_pie4
    y_pred2, y_pred_prob2, best_model2 = one_fold_validate(model_type,
                                             whistle_image_train_fold2_4d,
                                             label_train_fold2,
                                             whistle_image_pie2_4d, label_pie2,
                                             conf, fold_id=2)
    # fold 3: pie 1, 2, 4 as training and pie 3 as testing
    whistle_image_train_fold3_4d = np.vstack(
        (whistle_image_pie1_4d, whistle_image_pie2_4d, whistle_image_pie4_4d))
    label_train_fold3 = label_pie1 + label_pie2 + label_pie4
    y_pred3, y_pred_prob3, best_model3 = one_fold_validate(model_type,
                                             whistle_image_train_fold3_4d,
                                             label_train_fold3,
                                             whistle_image_pie3_4d, label_pie3,
                                             conf, fold_id=3)
    # fold 4: pie 1, 2, 3 as training and pie 4 as testing
    whistle_image_train_fold4_4d = np.vstack(
        (whistle_image_pie1_4d, whistle_image_pie2_4d, whistle_image_pie3_4d))
    label_train_fold4 = label_pie1 + label_pie2 + label_pie3
    y_pred4, y_pred_prob4, best_model4 = one_fold_validate(model_type,
                                             whistle_image_train_fold4_4d,
                                             label_train_fold4,
                                             whistle_image_pie4_4d, label_pie4,
                                             conf, fold_id=4)

    # collect all
    y_pred_tot = np.concatenate((y_pred1, y_pred2, y_pred3, y_pred4))
    label_total = label_pie1 + label_pie2 + label_pie3 + label_pie4
    metrics_two_fold(label_total, y_pred_tot, conf['log_dir'],
                     'accuracy_total.txt', conf, mode='total')

    stop_time = timeit.default_timer()
    with open(os.path.join(conf['log_dir'], 'run_time.txt'), 'w') as f:
        run_time = stop_time-start_time
        with redirect_stdout(f):
            print("Run time is: {0:.3f} s.".format(run_time))
            print("Run time is: {0:.3f} m.".format(run_time/60.0))

    return y_pred1, y_pred2, y_pred3, y_pred4, y_pred_prob1, y_pred_prob2, \
           y_pred_prob3, y_pred_prob4


def four_fold_validate_generator(model_type, whistle_image_pie1_4d,
                       whistle_image_pie2_4d,
                       whistle_image_pie3_4d,
                       whistle_image_pie4_4d, label_pie1, label_pie2,
                       label_pie3, label_pie4, conf):
    start_time = timeit.default_timer()

    # fold 1: pie 2, 3, 4 as training and pie 1 as testing
    whistle_image_train_fold1_4d = np.vstack(
        (whistle_image_pie2_4d, whistle_image_pie3_4d, whistle_image_pie4_4d))
    label_train_fold1 = label_pie2 + label_pie3 + label_pie4
    y_pred1, y_pred_prob1, best_model1 = one_fold_validate_generator(model_type,
                                             whistle_image_train_fold1_4d,
                                             label_train_fold1,
                                             whistle_image_pie1_4d, label_pie1,
                                             conf, fold_id=1)
    # fold 2: pie 1, 3, 4 as training and pie 2 as testing
    whistle_image_train_fold2_4d = np.vstack(
        (whistle_image_pie1_4d, whistle_image_pie3_4d, whistle_image_pie4_4d))
    label_train_fold2 = label_pie1 + label_pie3 + label_pie4
    y_pred2, y_pred_prob2, best_model2 = one_fold_validate_generator(model_type,
                                             whistle_image_train_fold2_4d,
                                             label_train_fold2,
                                             whistle_image_pie2_4d, label_pie2,
                                             conf, fold_id=2)
    # fold 3: pie 1, 2, 4 as training and pie 3 as testing
    whistle_image_train_fold3_4d = np.vstack(
        (whistle_image_pie1_4d, whistle_image_pie2_4d, whistle_image_pie4_4d))
    label_train_fold3 = label_pie1 + label_pie2 + label_pie4
    y_pred3, y_pred_prob3, best_model3 = one_fold_validate_generator(model_type,
                                             whistle_image_train_fold3_4d,
                                             label_train_fold3,
                                             whistle_image_pie3_4d, label_pie3,
                                             conf, fold_id=3)
    # fold 4: pie 1, 2, 3 as training and pie 4 as testing
    whistle_image_train_fold4_4d = np.vstack(
        (whistle_image_pie1_4d, whistle_image_pie2_4d, whistle_image_pie3_4d))
    label_train_fold4 = label_pie1 + label_pie2 + label_pie3
    y_pred4, y_pred_prob4, best_model4 = one_fold_validate_generator(model_type,
                                             whistle_image_train_fold4_4d,
                                             label_train_fold4,
                                             whistle_image_pie4_4d, label_pie4,
                                             conf, fold_id=4)

    # collect all
    y_pred_tot = np.concatenate((y_pred1, y_pred2, y_pred3, y_pred4))
    label_total = label_pie1 + label_pie2 + label_pie3 + label_pie4
    metrics_two_fold(label_total, y_pred_tot, conf['log_dir'],
                     'accuracy_total.txt', conf, mode='total')

    stop_time = timeit.default_timer()
    with open(os.path.join(conf['log_dir'], 'run_time.txt'), 'w') as f:
        run_time = stop_time-start_time
        with redirect_stdout(f):
            print("Run time is: {0:.3f} s.".format(run_time))
            print("Run time is: {0:.3f} m.".format(run_time/60.0))

    return y_pred1, y_pred2, y_pred3, y_pred4, y_pred_prob1, y_pred_prob2, \
           y_pred_prob3, y_pred_prob4


def main():
    # bin files for training & testing
    bin_dir = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/cv4'
    # bin_dir_train = os.path.join(bin_dir, 'cv2/__first_pie')
    # bin_dir_test = os.path.join(bin_dir, 'cv2/__second_pie')
    bin_dir_fold = dict()
    for pp in range(4):  # 'bin_dir_fold['pie1']'
        bin_dir_fold['pie'+str(pp+1)] = os.path.join(bin_dir, 'pie'+str(pp+1))

    sound_dir = '/home/ys587/__Data/__whistle/__sound_species/'
    species_name = ['bottlenose', 'common', 'spinner', 'melon-headed']
    species_id = {'bottlenose': 0, 'common': 1, 'spinner': 2, 'melon-headed': 3}

    conf_gen = dict()
    conf_gen['log_dir'] = "/home/ys587/__Data/__whistle/__log_dir_context/contour_temp_audio"
    conf_gen['save_dir'] = "/home/ys587/__Data/__whistle/__log_dir_context/audio_data_store_temp"
    conf_gen['data_store'] = "/home/ys587/__Data/__whistle/__log_dir_context/audio_data_store/__four_class"
    conf_gen['bin_dir'] = bin_dir

    conf_gen['species_name'] = species_name
    conf_gen['species_id'] = species_id
    # conf_gen["duration_thre"] = 0.05
    # conf_gen['time_reso'] = 0.01  # 10 ms
    # conf_gen['time_reso'] = 0.05  # 50 ms
    conf_gen['time_reso'] = 0.02  # 20 ms
    # conf_gen['time_reso'] = 0.1  # 100 ms

    # cepstral coefficient
    conf_gen['sample_rate'] = 192000
    conf_gen["num_class"] = len(species_name)

    conf_gen['context_winsize'] = 1.0  # sec
    # conf_gen['context_winsize'] = 5.0  # sec
    # conf_gen['context_hopsize'] = 5.0  # sec
    # conf_gen['context_hopsize'] = 2.5  # sec
    conf_gen['context_hopsize'] = 1.0  # sec
    # conf_gen['fft_size'] = 512
    # conf_gen['fft_size'] = 1024
    conf_gen['fft_size'] = 4096
    conf_gen['hop_size'] = int(conf_gen['time_reso']*conf_gen['sample_rate'])
    # audio
    conf_gen['freq_ind_low'] = 0
    # conf_gen['freq_ind_low'] = 20
    conf_gen['freq_ind_high'] = 144
    # Freq contours
    # conf_gen['freq_ind_low'] = floor(5000./(conf_gen['sample_rate']/conf_gen['fft_size']))
    # conf_gen['freq_ind_high'] = ceil(50000./(conf_gen['sample_rate']/conf_gen['fft_size']))
    # conf_gen['freq_ind_high'] = 267  # 50 kHz
    # conf_gen['freq_ind_low'] = 26  # 5 kHz

    # conf_gen['IMG_T'] = 200
    conf_gen['IMG_T'] = int(floor((conf_gen['context_winsize'] / conf_gen['time_reso'])))
    # conf_gen['IMG_F'] = 250
    conf_gen['IMG_F'] = conf_gen['freq_ind_high'] - conf_gen['freq_ind_low']
    conf_gen['input_shape'] = (conf_gen['IMG_F'], conf_gen['IMG_T'], 1)
    # conf_gen['contour_timethre'] = 100
    # conf_gen['contour_timethre'] = 50  # 1 s. used for 10-s sound clips with 5-s overlap
    conf_gen['contour_timethre'] = 25  # 0.5 s.
    # conf_gen['contour_timethre'] = 50  # 1 s
    # conf_gen['contour_timethre'] = 100  # 2 s.
    # conf_gen['contour_timethre'] = 150  # 3 s.
    # conf_gen['contour_timethre'] = 200  # 4 s.

    conf_gen['l2_regu'] = 0.01
    # conf_gen['l2_regu'] = 0.001
    # conf_gen['l2_regu'] = 0.2
    conf_gen['dropout'] = 0.1
    # conf_gen['batch_size'] = 128  # lstm_2lay
    conf_gen['batch_size'] = 32  # resnet 18, 34
    # conf_gen['batch_size'] = 16
    conf_gen['epoch'] = 200
    # conf_gen['epoch'] = 1  # debug
    conf_gen['learning_rate'] = 1.0

    conf_gen['confusion_callback'] = False
    conf_gen['spectro_dilation'] = False

    conf_gen['img_data_output'] = False  # output image of spectrogram data
    conf_gen['numpy_data_output'] = True
    conf_gen['numpy_data_use'] = not conf_gen['numpy_data_output']

    # add one more class 'noise': 4 species class + 1 noise class
    conf_gen['class_noise'] = True  # add the fifth class: noise

    # Use masked spectrogram of whistle contours, instead of original spectrograms
    # conf_gen['mask_spec_contour'] = True
    conf_gen['mask_spec_contour'] = False

    for pp in range(4):  # 'pie1_data.npz'
        conf_gen['save_file_pie'+str(pp+1)] = os.path.join(conf_gen['save_dir'],
                                                   'pie'+str(pp+1)+'_data.npz')
    for pp in range(4):
        conf_gen['image_pie' + str(pp + 1)] = os.path.join(conf_gen['save_dir'],
                                                               'pie' + str(pp + 1))

    if not os.path.exists(conf_gen['log_dir']):
        os.mkdir(conf_gen['log_dir'])

    # RNN
    conf_gen['network_type'] = 'conv2d_lstm'  # rnn, cnn or conv2d_lstm
    # conf_gen['network_type'] = 'rnn'
    # conf_gen['l2_regu'] = 0.1
    conf_gen['l2_regu'] = 0.01
    # conf_gen['dropout'] = 0.1
    conf_gen['dropout'] = 0.01
    # conf_gen['recurrent_dropout'] = 0.1
    conf_gen['recurrent_dropout'] = 0.01
    conf_gen['dense_size'] = 128

    # Read species labels, filenames & extract time and frequency sequences
    contour_pie1, bin_wav_pair_pie1 = bin_extract(bin_dir_fold['pie1'], sound_dir,
                                                    species_name)
    contour_pie2, bin_wav_pair_pie2 = bin_extract(bin_dir_fold['pie2'], sound_dir,
                                                    species_name)
    contour_pie3, bin_wav_pair_pie3 = bin_extract(bin_dir_fold['pie3'], sound_dir,
                                                    species_name)
    contour_pie4, bin_wav_pair_pie4 = bin_extract(bin_dir_fold['pie4'], sound_dir,
                                                    species_name)

    # read contours from bin files
    contour_pie1_list = contour_target_retrieve(contour_pie1,
                                                 bin_wav_pair_pie1,
                                                 bin_dir_fold['pie1'],
                                                 conf_gen)
    contour_pie2_list = contour_target_retrieve(contour_pie2,
                                                 bin_wav_pair_pie2,
                                                 bin_dir_fold['pie2'],
                                                 conf_gen)
    contour_pie3_list = contour_target_retrieve(contour_pie3,
                                                 bin_wav_pair_pie3,
                                                 bin_dir_fold['pie3'],
                                                 conf_gen)
    contour_pie4_list = contour_target_retrieve(contour_pie4,
                                                 bin_wav_pair_pie4,
                                                 bin_dir_fold['pie4'],
                                                 conf_gen)

    # prepare training & testing data
    if conf_gen['class_noise']:
        conf_gen['species_name'].append('noise')
        conf_gen['species_id'].update({'noise': 4})
        conf_gen["num_class"] += 1
        if not conf_gen['mask_spec_contour']:
            conf_gen[
                'data_store'] = "/home/ys587/__Data/__whistle/__log_dir_context/audio_data_store/__five_class"
        else:  # Masked-spectrogram-contour
            conf_gen['data_store'] = \
                "/home/ys587/__Data/__whistle/__log_dir_context/audio_data_store/__five_class_mask_spec_pasterization"

    # Pie 1
    pie1_data_path = os.path.join(conf_gen['data_store'], 'pie1_data.npz')
    if os.path.exists(pie1_data_path) & conf_gen['numpy_data_use']:
        print('Loading pie1 data...')
        data_temp = np.load(pie1_data_path)
        whistle_image_pie1 = data_temp['whistle_image']
        label_pie1 = data_temp['label'].tolist()
    elif conf_gen['mask_spec_contour']:
        whistle_image_pie1, label_pie1, freq_high_pie1, freq_low_pie1 = \
            prepare_data_mask(contour_pie1_list, sound_dir, conf_gen,
                              conf_gen['image_pie1'],
                              conf_gen['save_file_pie1'],
                              plot=conf_gen['img_data_output'])
    else:
        whistle_image_pie1, label_pie1, freq_high_pie1, freq_low_pie1 = \
            prepare_data_audio(contour_pie1_list, sound_dir, conf_gen,
                         conf_gen['image_pie1'],
                         conf_gen['save_file_pie1'],
                         plot=conf_gen['img_data_output'])

    # Pie 2
    pie2_data_path = os.path.join(conf_gen['data_store'], 'pie2_data.npz')
    if os.path.exists(pie2_data_path) & conf_gen['numpy_data_use']:
        print('Loading pie2 data...')
        data_temp = np.load(pie2_data_path)
        whistle_image_pie2 = data_temp['whistle_image']
        label_pie2 = data_temp['label'].tolist()
    elif conf_gen['mask_spec_contour']:
        whistle_image_pie2, label_pie2, freq_high_pie2, freq_low_pie2 = \
            prepare_data_mask(
            contour_pie2_list, sound_dir, conf_gen, conf_gen['image_pie2'],
            conf_gen['save_file_pie2'], plot=conf_gen['img_data_output'])
    else:
        whistle_image_pie2, label_pie2, freq_high_pie2, freq_low_pie2 = \
            prepare_data_audio(contour_pie2_list, sound_dir, conf_gen,
                         conf_gen['image_pie2'],
                         conf_gen['save_file_pie2'],
                         plot=conf_gen['img_data_output'])

    # Pie 3
    pie3_data_path = os.path.join(conf_gen['data_store'], 'pie3_data.npz')
    if os.path.exists(pie3_data_path) & conf_gen['numpy_data_use']:
        print('Loading pie3 data...')
        data_temp = np.load(pie3_data_path)
        whistle_image_pie3 = data_temp['whistle_image']
        label_pie3 = data_temp['label'].tolist()
    elif conf_gen['mask_spec_contour']:
        whistle_image_pie3, label_pie3, freq_high_pie3, freq_low_pie3 = \
            prepare_data_mask(
            contour_pie3_list, sound_dir, conf_gen, conf_gen['image_pie3'],
            conf_gen['save_file_pie3'], plot=conf_gen['img_data_output'])
    else:
        whistle_image_pie3, label_pie3, freq_high_pie3, freq_low_pie3 = \
            prepare_data_audio(contour_pie3_list, sound_dir, conf_gen,
                         conf_gen['image_pie3'],
                         conf_gen['save_file_pie3'],
                         plot=conf_gen['img_data_output'])

    # Pie 4
    pie4_data_path = os.path.join(conf_gen['data_store'], 'pie4_data.npz')
    if os.path.exists(pie4_data_path) & conf_gen['numpy_data_use']:
        print('Loading pie4 data...')
        data_temp = np.load(pie4_data_path)
        whistle_image_pie4 = data_temp['whistle_image']
        label_pie4 = data_temp['label'].tolist()
    elif conf_gen['mask_spec_contour']:
        whistle_image_pie4, label_pie4, freq_high_pie4, freq_low_pie4 = \
            prepare_data_mask(
            contour_pie4_list, sound_dir, conf_gen, conf_gen['image_pie4'],
            conf_gen['save_file_pie4'], plot=conf_gen['img_data_output'])
    else:
        whistle_image_pie4, label_pie4, freq_high_pie4, freq_low_pie4 = \
            prepare_data_audio(contour_pie4_list, sound_dir, conf_gen,
                         conf_gen['image_pie4'],
                         conf_gen['save_file_pie4'],
                         plot=conf_gen['img_data_output'])

    # Change the dimensions
    if conf_gen['network_type'] is 'cnn':
        whistle_image_pie1_4d = np.expand_dims(whistle_image_pie1, axis=3)
        whistle_image_pie2_4d = np.expand_dims(whistle_image_pie2, axis=3)
        whistle_image_pie3_4d = np.expand_dims(whistle_image_pie3, axis=3)
        whistle_image_pie4_4d = np.expand_dims(whistle_image_pie4, axis=3)
    elif conf_gen['network_type'] is 'rnn':
        whistle_image_pie1_4d = np.transpose(whistle_image_pie1, (0, 2, 1))
        whistle_image_pie2_4d = np.transpose(whistle_image_pie2, (0, 2, 1))
        whistle_image_pie3_4d = np.transpose(whistle_image_pie3, (0, 2, 1))
        whistle_image_pie4_4d = np.transpose(whistle_image_pie4, (0, 2, 1))
    elif conf_gen['network_type'] is 'conv2d_lstm':
        whistle_image_pie1_0 = np.expand_dims(whistle_image_pie1, axis=(3, 4))
        whistle_image_pie1_4d = np.transpose(whistle_image_pie1_0, (0, 2, 1, 3, 4))
        whistle_image_pie2_0 = np.expand_dims(whistle_image_pie2, axis=(3, 4))
        whistle_image_pie2_4d = np.transpose(whistle_image_pie2_0, (0, 2, 1, 3, 4))
        whistle_image_pie3_0 = np.expand_dims(whistle_image_pie3, axis=(3, 4))
        whistle_image_pie3_4d = np.transpose(whistle_image_pie3_0, (0, 2, 1, 3, 4))
        whistle_image_pie4_0 = np.expand_dims(whistle_image_pie4, axis=(3, 4))
        whistle_image_pie4_4d = np.transpose(whistle_image_pie4_0, (0, 2, 1, 3, 4))

    log_dir = '/home/ys587/__Data/__whistle/__log_dir_context/__new_results'

    # for rr in range(2):
    # # for rr in range(2, -1, -1):
    #     print('Run round: ' + str(rr))
    #
    #     model_type = 'conv2d_lstm'  # conf_gen['network_type'] is 'conv2d_lstm'
    #     conf_gen['lstm1'] = 32*(2**rr)
    #     conf_gen['lstm2'] = 32*(2**rr)
    #     proj_name = model_type + '_chan' + str(conf_gen['lstm1']) + '_' + \
    #                 str(conf_gen['lstm2']) + '_f1'
    #     print(proj_name)
    #
    #     conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
    #     if not os.path.exists(conf_gen['log_dir']):
    #         os.mkdir(conf_gen['log_dir'])
    #     conf_gen['learning_rate'] = 2.0
    #     conf_gen['batch_size'] = 32
    #     label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, \
    #     pred_prob2, pred_prob3, pred_prob4 = four_fold_validate_generator(
    #         model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
    #         whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
    #         label_pie3, label_pie4, conf_gen)

    # for rr in range(3):
    for rr in range(1):
    # # for rr in range(3, -1, -1):
    #     print('Run round: ' + str(rr))
    #
    #     model_type = 'lstm_2lay'  # conf_gen['network_type'] is 'rnn'
    #     conf_gen['lstm1'] = 64*(2**rr)
    #     conf_gen['lstm2'] = 64*(2**rr)
    #     # proj_name = model_type + '_paster_chan' + str(conf_gen['lstm1']) + '_' + \
    #     #             str(conf_gen['lstm2']) + '_f1'
    #     proj_name = model_type + '_chan_mask' + str(conf_gen['lstm1']) + '_' + \
    #               str(conf_gen['lstm2']) + '_f1'
    #     print(proj_name)
    #
    #     conf_gen['learning_rate'] = 2.0
    #     conf_gen['batch_size'] = 64
    #     conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
    #     if not os.path.exists(conf_gen['log_dir']):
    #         os.mkdir(conf_gen['log_dir'])
    #     label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, \
    #     pred_prob2, pred_prob3, pred_prob4 = four_fold_validate_generator(
    #         model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
    #         whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
    #         label_pie3, label_pie4, conf_gen)

        # model_type = 'resnet_cifar10_expt'
        # proj_name = model_type + '_run' + str(rr) + '_f1'
        # print(proj_name)
        # conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
        # if not os.path.exists(conf_gen['log_dir']):
        #     os.mkdir(conf_gen['log_dir'])
        # conf_gen['num_filters'] = 16
        # label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, \
        # pred_prob2, pred_prob3, pred_prob4 = four_fold_validate_generator(
        #     model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
        #     whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
        #     label_pie3, label_pie4, conf_gen)
        #
        model_type = 'resnet34_expt'
        proj_name = model_type + '_run' + str(rr) + '_f1'
        print(proj_name)

        conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
        if not os.path.exists(conf_gen['log_dir']):
            os.mkdir(conf_gen['log_dir'])
        conf_gen['num_filters'] = 16
        label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, \
        pred_prob2, pred_prob3, pred_prob4 = four_fold_validate_generator(
            model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
            whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
            label_pie3, label_pie4, conf_gen)

    # for rr in range(0, 5):
    #     print('Run round: ' + str(rr))
    #
    #     model_type = 'resnet101_expt'
    #     proj_name = model_type + '_run' + str(rr) + '_f1'
    #     print(proj_name)
    #     conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
    #     if not os.path.exists(conf_gen['log_dir']):
    #         os.mkdir(conf_gen['log_dir'])
    #     conf_gen['num_filters'] = 16
    #     conf_gen['batch_size'] = 8
    #     label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, \
    #     pred_prob2, pred_prob3, pred_prob4 = four_fold_validate_generator(
    #         model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
    #         whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
    #         label_pie3, label_pie4, conf_gen)

    # for rr in range(0, 5):
    #     print('Run round: ' + str(rr))
    #
    #     model_type = 'resnet50_expt'
    #     proj_name = model_type + '_filter_32_run' + str(rr) + '_f1'
    #     print(proj_name)
    #     conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
    #     if not os.path.exists(conf_gen['log_dir']):
    #         os.mkdir(conf_gen['log_dir'])
    #     conf_gen['num_filters'] = 32
    #     conf_gen['batch_size'] = 8
    #     label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, \
    #     pred_prob2, pred_prob3, pred_prob4 = four_fold_validate_generator(
    #         model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
    #         whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
    #         label_pie3, label_pie4, conf_gen)

    # for rr in range(0, 5):
    #     print('Run round: ' + str(rr))
    #
    #     model_type = 'resnet152_expt'
    #     proj_name = model_type + '_run' + str(rr) + '_f1'
    #     print(proj_name)
    #     conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
    #     if not os.path.exists(conf_gen['log_dir']):
    #         os.mkdir(conf_gen['log_dir'])
    #     conf_gen['num_filters'] = 16
    #     conf_gen['batch_size'] = 8
    #     label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, \
    #     pred_prob2, pred_prob3, pred_prob4 = four_fold_validate_generator(
    #         model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
    #         whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
    #         label_pie3, label_pie4, conf_gen)

    # for rr in range(0, 5):
    #     print('Run round: ' + str(rr))
        # model_type = 'resnet50_expt_nonglobal'
        # proj_name = model_type + '_run' + str(rr) + '_f1'
        # print(proj_name)
        # conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
        # if not os.path.exists(conf_gen['log_dir']):
        #     os.mkdir(conf_gen['log_dir'])
        # conf_gen['num_filters'] = 16
        # conf_gen['batch_size'] = 16
        # label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, pred_prob2, pred_prob3, pred_prob4 = four_fold_validate_generator(
        #     model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
        #     whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1,
        #     label_pie2, label_pie3, label_pie4, conf_gen)

        # model_type = 'resnet34_expt'
        # # model_type = 'resnet34_dummy_expt'
        # proj_name = model_type + '_run' + str(rr) + '_f1'
        # print(proj_name)
        # conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
        # if not os.path.exists(conf_gen['log_dir']):
        #     os.mkdir(conf_gen['log_dir'])
        # conf_gen['num_filters'] = 16
        # label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, \
        # pred_prob2, pred_prob3, pred_prob4 = four_fold_validate_generator(
        #     model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
        #     whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
        #     label_pie3, label_pie4, conf_gen)
        #
        # model_type = 'resnet_cifar10_expt_maxpool'
        # proj_name = model_type + '_run' + str(rr) + '_f1'
        # print(proj_name)
        # conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
        # if not os.path.exists(conf_gen['log_dir']):
        #     os.mkdir(conf_gen['log_dir'])
        # conf_gen['num_filters'] = 16
        # label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, \
        # pred_prob2, pred_prob3, pred_prob4 = four_fold_validate_generator(
        #     model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
        #     whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
        #     label_pie3, label_pie4, conf_gen)
        #
        # model_type = 'resnet50_expt'
        # proj_name = model_type + '_run' + str(rr) + '_f1'
        # print(proj_name)
        # conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
        # if not os.path.exists(conf_gen['log_dir']):
        #     os.mkdir(conf_gen['log_dir'])
        # conf_gen['num_filters'] = 16
        # label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, \
        # pred_prob2, pred_prob3, pred_prob4 = four_fold_validate_generator(
        #     model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
        #     whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
        #     label_pie3, label_pie4, conf_gen)
        #
        # model_type = 'resnet34_expt_maxpool'
        # # model_type = 'resnet34_dummy_expt'
        # proj_name = model_type + '_run' + str(rr) + '_f1'
        # print(proj_name)
        # conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
        # if not os.path.exists(conf_gen['log_dir']):
        #     os.mkdir(conf_gen['log_dir'])
        # conf_gen['num_filters'] = 16
        # label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, \
        # pred_prob2, pred_prob3, pred_prob4 = four_fold_validate_generator(
        #     model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
        #     whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
        #     label_pie3, label_pie4, conf_gen)

        # model_type = 'resnet_cifar10_expt_deep3'
        # proj_name = model_type + '_run' + str(rr) + '_f1'
        # print(proj_name)
        # conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
        # if not os.path.exists(conf_gen['log_dir']):
        #     os.mkdir(conf_gen['log_dir'])
        # conf_gen['num_filters'] = 16
        # label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, pred_prob2, pred_prob3, pred_prob4 = four_fold_validate_generator(
        #     model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
        #     whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
        #     label_pie3, label_pie4, conf_gen)

        # model_type = 'resnet_cifar10_expt_no_direct'
        # proj_name = model_type + '_run' + str(rr) + '_f1'
        # print(proj_name)
        # conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
        # if not os.path.exists(conf_gen['log_dir']):
        #     os.mkdir(conf_gen['log_dir'])
        # conf_gen['num_filters'] = 16
        # label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, \
        # pred_prob2, pred_prob3, pred_prob4 = four_fold_validate_generator(
        #     model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
        #     whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
        #     label_pie3, label_pie4, conf_gen)

        # model_type = 'resnet_cifar10_expt_deep5'
        # proj_name = model_type + '_run' + str(rr) + '_f1'
        # print(proj_name)
        # conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
        # if not os.path.exists(conf_gen['log_dir']):
        #     os.mkdir(conf_gen['log_dir'])
        # conf_gen['num_filters'] = 16
        # label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, \
        # pred_prob2, pred_prob3, pred_prob4 = four_fold_validate_generator(
        #     model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
        #     whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
        #     label_pie3, label_pie4, conf_gen)
        #
        # model_type = 'resnet_cifar10_expt_deep6'
        # proj_name = model_type + '_run' + str(rr) + '_f1'
        # print(proj_name)
        # conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
        # if not os.path.exists(conf_gen['log_dir']):
        #     os.mkdir(conf_gen['log_dir'])
        # conf_gen['num_filters'] = 16
        # label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, \
        # pred_prob2, pred_prob3, pred_prob4 = four_fold_validate_generator(
        #     model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
        #     whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
        #     label_pie3, label_pie4, conf_gen)

        # model_type = 'resnet18_expt'
        # proj_name = model_type + '_run' + str(rr) + '_f1'
        # print(proj_name)
        # conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
        # if not os.path.exists(conf_gen['log_dir']):
        #     os.mkdir(conf_gen['log_dir'])
        # conf_gen['num_filters'] = 16
        # label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, \
        # pred_prob2, pred_prob3, pred_prob4 = four_fold_validate_generator(
        #     model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
        #     whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
        #     label_pie3, label_pie4, conf_gen)

        # model_type = 'resnet_v1'
        # proj_name = model_type + '_depth20_stack4_filter32_run' + str(rr) + '_f1'
        # print(proj_name)
        # conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
        # if not os.path.exists(conf_gen['log_dir']):
        #     os.mkdir(conf_gen['log_dir'])
        # conf_gen['depth'] = 20  # 20, 32, 44  =>  n*6+2, as n=3, 5, 7
        # conf_gen['num_stack'] = 4
        # conf_gen['num_filters'] = 32
        # conf_gen['kernel_size'] = 3
        # label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1,
        # pred_prob2, pred_prob3, pred_prob4 = four_fold_validate_generator(
        #     model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
        #     whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
        #     label_pie3, label_pie4, conf_gen)

        # model_type = 'resnet_v1'
        # proj_name = model_type + '_depth20_stack4_filter16_kernel5_run' + str(rr) + '_f1'
        # print(proj_name)
        # conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
        # if not os.path.exists(conf_gen['log_dir']):
        #     os.mkdir(conf_gen['log_dir'])
        # conf_gen['depth'] = 20  # 20, 32, 44  =>  n*6+2, as n=3, 5, 7
        # conf_gen['num_stack'] = 4
        # conf_gen['num_filters'] = 16
        # conf_gen['kernel_size'] = 5
        # label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1,
        # pred_prob2, pred_prob3, pred_prob4 = four_fold_validate_generator(
        #     model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
        #     whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
        #     label_pie3, label_pie4, conf_gen)
        #
        # proj_name = model_type + '_depth20_stack3_filter32_kernel5__run' + str(rr) + '_f1'
        # print(proj_name)
        # conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
        # if not os.path.exists(conf_gen['log_dir']):
        #     os.mkdir(conf_gen['log_dir'])
        # conf_gen['depth'] = 20  # 20, 32, 44  =>  n*6+2, as n=3, 5, 7
        # conf_gen['num_stack'] = 3
        # conf_gen['num_filters'] = 32
        # conf_gen['kernel_size'] = 5
        # label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1,
        # pred_prob2, pred_prob3, pred_prob4 = four_fold_validate_generator(
        #     model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
        #     whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
        #     label_pie3, label_pie4, conf_gen)

        # proj_name = model_type + '_depth20_stack4_filter16_kernel7_run' + str(rr) + '_f1'
        # print(proj_name)
        # conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
        # if not os.path.exists(conf_gen['log_dir']):
        #     os.mkdir(conf_gen['log_dir'])
        # conf_gen['depth'] = 20  # 20, 32, 44  =>  n*6+2, as n=3, 5, 7
        # conf_gen['num_stack'] = 4
        # conf_gen['num_filters'] = 16
        # conf_gen['kernel_size'] = 7
        # label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1,
        # pred_prob2, pred_prob3, pred_prob4 = four_fold_validate_generator(
        #     model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
        #     whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
        #     label_pie3, label_pie4, conf_gen)

        # proj_name = model_type + '_depth20_stack3_filter32_kernel7__run' + str(rr) + '_f1'
        # print(proj_name)
        # conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
        # if not os.path.exists(conf_gen['log_dir']):
        #     os.mkdir(conf_gen['log_dir'])
        # conf_gen['depth'] = 20  # 20, 32, 44  =>  n*6+2, as n=3, 5, 7
        # conf_gen['num_stack'] = 3
        # conf_gen['num_filters'] = 32
        # conf_gen['kernel_size'] = 7
        # label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1,
        # pred_prob2, pred_prob3, pred_prob4 = four_fold_validate_generator(
        #     model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
        #     whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
        #     label_pie3, label_pie4, conf_gen)


    if False:
        for rr in range(1, 10):
            model_type = 'resnet_v1'
            proj_name = model_type+'_depth20_stack4_filter16_run' + str(rr) + '_f1'
            print(proj_name)
            conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
            if not os.path.exists(conf_gen['log_dir']):
                os.mkdir(conf_gen['log_dir'])
            conf_gen['depth'] = 20  # 20, 32, 44  =>  n*6+2, as n=3, 5, 7
            conf_gen['num_stack'] = 4
            conf_gen['num_filters'] = 16
            label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, pred_prob2, pred_prob3, pred_prob4 = four_fold_validate(
                model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
                whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
                label_pie3, label_pie4, conf_gen)

            proj_name = model_type+'_depth20_stack3_filter32_run' + str(rr) + '_f1'
            print(proj_name)
            conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
            if not os.path.exists(conf_gen['log_dir']):
                os.mkdir(conf_gen['log_dir'])
            conf_gen['depth'] = 20  # 20, 32, 44  =>  n*6+2, as n=3, 5, 7
            conf_gen['num_stack'] = 3
            conf_gen['num_filters'] = 32
            label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, pred_prob2, pred_prob3, pred_prob4 = four_fold_validate(
                model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
                whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
                label_pie3, label_pie4, conf_gen)

            model_type = 'resnet18'
            proj_name = model_type+'_run' + str(rr) + '_f1'
            print(proj_name)
            conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
            if not os.path.exists(conf_gen['log_dir']):
                os.mkdir(conf_gen['log_dir'])
            label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, pred_prob2, pred_prob3, pred_prob4 = four_fold_validate(
                model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
                whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
                label_pie3, label_pie4, conf_gen)

            model_type = 'resnet34'
            proj_name = model_type+'_run' + str(rr) + '_f1'
            print(proj_name)
            conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
            if not os.path.exists(conf_gen['log_dir']):
                os.mkdir(conf_gen['log_dir'])
            label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, pred_prob2, pred_prob3, pred_prob4 = four_fold_validate(
                model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
                whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
                label_pie3, label_pie4, conf_gen)

            model_type = 'resnet50'
            proj_name = model_type+'_run' + str(rr) + '_f1'
            print(proj_name)
            conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
            if not os.path.exists(conf_gen['log_dir']):
                os.mkdir(conf_gen['log_dir'])
            label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, pred_prob2, pred_prob3, pred_prob4 = four_fold_validate(
                model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
                whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
                label_pie3, label_pie4, conf_gen)

    if False:
        model_type = 'resnet_v1'
        for rr in range(1, 5):
            proj_name = 'resnet_v1_depth20_stack4_filter32_run'+str(rr)+'_f1'
            print(proj_name)
            conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
            if not os.path.exists(conf_gen['log_dir']):
                os.mkdir(conf_gen['log_dir'])
            conf_gen['depth'] = 20  # 20, 32, 44  =>  n*6+2, as n=3, 5, 7
            conf_gen['num_stack'] = 4
            conf_gen['num_filters'] = 32
            label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, pred_prob2,\
                pred_prob3, pred_prob4 = four_fold_validate(model_type,
                                                         whistle_image_pie1_4d,
                                                         whistle_image_pie2_4d,
                                                         whistle_image_pie3_4d,
                                                         whistle_image_pie4_4d,
                                                         label_pie1,
                                                         label_pie2,
                                                         label_pie3,
                                                         label_pie4, conf_gen)

        for rr in [0]:
            proj_name = 'resnet_v1_depth32_stack4_filter16_run' + str(rr) + '_f1'
            print(proj_name)
            conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
            if not os.path.exists(conf_gen['log_dir']):
                os.mkdir(conf_gen['log_dir'])
            conf_gen['depth'] = 32  # 20, 32, 44  =>  n*6+2, as n=3, 5, 7
            conf_gen['num_stack'] = 4
            conf_gen['num_filters'] = 16
            label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, pred_prob2, pred_prob3, pred_prob4 = four_fold_validate(
                model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
                whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
                label_pie3, label_pie4, conf_gen)

        for rr in [0]:
            proj_name = 'resnet_v1_depth32_stack3_filter32_run' + str(rr) + '_f1'
            print(proj_name)
            conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
            if not os.path.exists(conf_gen['log_dir']):
                os.mkdir(conf_gen['log_dir'])
            conf_gen['depth'] = 32  # 20, 32, 44  =>  n*6+2, as n=3, 5, 7
            conf_gen['num_stack'] = 3
            conf_gen['num_filters'] = 32
            label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, pred_prob2, pred_prob3, pred_prob4 = four_fold_validate(
                model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
                whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
                label_pie3, label_pie4, conf_gen)


    if False:
        # 5 classes
        proj_name = 'resnet_v1_class5_depth20_stack3_filter32_run0_f1'
        print(proj_name)
        conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
        if not os.path.exists(conf_gen['log_dir']):
            os.mkdir(conf_gen['log_dir'])
        conf_gen['depth'] = 20  # 20, 32, 44  =>  n*6+2, as n=3, 5, 7
        conf_gen['num_stack'] = 3
        conf_gen['num_filters'] = 32
        label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, pred_prob2, pred_prob3, pred_prob4 = four_fold_validate(
            model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
            whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
            label_pie3, label_pie4, conf_gen)

        proj_name = 'resnet_v1_class5_depth20_stack4_filter32_run0_f1'
        print(proj_name)
        conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
        if not os.path.exists(conf_gen['log_dir']):
            os.mkdir(conf_gen['log_dir'])
        conf_gen['depth'] = 20  # 20, 32, 44  =>  n*6+2, as n=3, 5, 7
        conf_gen['num_stack'] = 4
        conf_gen['num_filters'] = 32
        label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, pred_prob2, pred_prob3, pred_prob4 = four_fold_validate(
            model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
            whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
            label_pie3, label_pie4, conf_gen)

        for rr in range(1, 5):
            proj_name = 'resnet_v1_class5_depth20_stack3_filter16_run'+str(rr)+'_f1'
            print(proj_name)
            conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
            if not os.path.exists(conf_gen['log_dir']):
                os.mkdir(conf_gen['log_dir'])
            conf_gen['depth'] = 20  # 20, 32, 44  =>  n*6+2, as n=3, 5, 7
            conf_gen['num_stack'] = 3
            conf_gen['num_filters'] = 16
            label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, pred_prob2, pred_prob3, pred_prob4 = four_fold_validate(
                model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
                whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
                label_pie3, label_pie4, conf_gen)

            proj_name = 'resnet_v1_class5_depth20_stack4_filter16_run'+str(rr)+'_f1'
            print(proj_name)
            conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
            if not os.path.exists(conf_gen['log_dir']):
                os.mkdir(conf_gen['log_dir'])
            conf_gen['depth'] = 20  # 20, 32, 44  =>  n*6+2, as n=3, 5, 7
            conf_gen['num_stack'] = 4
            conf_gen['num_filters'] = 16
            label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, pred_prob2, pred_prob3, pred_prob4 = four_fold_validate(
                model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
                whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
                label_pie3, label_pie4, conf_gen)

            proj_name = 'resnet_v1_class5_depth20_stack3_filter32_run'+str(rr)+'_f1'
            print(proj_name)
            conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
            if not os.path.exists(conf_gen['log_dir']):
                os.mkdir(conf_gen['log_dir'])
            conf_gen['depth'] = 20  # 20, 32, 44  =>  n*6+2, as n=3, 5, 7
            conf_gen['num_stack'] = 3
            conf_gen['num_filters'] = 32
            label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, pred_prob2, pred_prob3, pred_prob4 = four_fold_validate(
                model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
                whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
                label_pie3, label_pie4, conf_gen)

            proj_name = 'resnet_v1_class5_depth20_stack4_filter32_run'+str(rr)+'_f1'
            print(proj_name)
            conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
            if not os.path.exists(conf_gen['log_dir']):
                os.mkdir(conf_gen['log_dir'])
            conf_gen['depth'] = 20  # 20, 32, 44  =>  n*6+2, as n=3, 5, 7
            conf_gen['num_stack'] = 4
            conf_gen['num_filters'] = 32
            label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, pred_prob2, pred_prob3, pred_prob4 = four_fold_validate(
                model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
                whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
                label_pie3, label_pie4, conf_gen)


    # for rr in range(5, 10):
    #     proj_name = 'resnet_v1_depth20_stack3_filter32_run'+str(rr)+'_f1'
    #     print(proj_name)
    #     conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
    #     if not os.path.exists(conf_gen['log_dir']):
    #         os.mkdir(conf_gen['log_dir'])
    #     conf_gen['depth'] = 20  # 20, 32, 44  =>  n*6+2, as n=3, 5, 7
    #     conf_gen['num_stack'] = 3
    #     conf_gen['num_filters'] = 32
    #     label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, pred_prob2,\
    #         pred_prob3, pred_prob4 = four_fold_validate(model_type,
    #                                                  whistle_image_pie1_4d,
    #                                                  whistle_image_pie2_4d,
    #                                                  whistle_image_pie3_4d,
    #                                                  whistle_image_pie4_4d,
    #                                                  label_pie1,
    #                                                  label_pie2,
    #                                                  label_pie3,
    #                                                  label_pie4, conf_gen)

    # proj_name = 'resnet_v1_class5_depth20_stack4_filter16_run0_f1'
    # print(proj_name)
    # conf_gen['log_dir'] = os.path.join(log_dir, proj_name)
    # if not os.path.exists(conf_gen['log_dir']):
    #     os.mkdir(conf_gen['log_dir'])
    # conf_gen['depth'] = 20  # 20, 32, 44  =>  n*6+2, as n=3, 5, 7
    # conf_gen['num_stack'] = 4
    # conf_gen['num_filters'] = 16
    # label_pred1, label_pred2, label_pred3, label_pred4, pred_prob1, pred_prob2, pred_prob3, pred_prob4 = four_fold_validate(
    #     model_type, whistle_image_pie1_4d, whistle_image_pie2_4d,
    #     whistle_image_pie3_4d, whistle_image_pie4_4d, label_pie1, label_pie2,
    #     label_pie3, label_pie4, conf_gen)


    # to-do
    # Done - save classification output labels
    # Done - save images for faster training/testing
    # Done - increase the numbers of filters in Resnet
    # Done - write an interface for multiple models' running
    # Done - make one more noise class!

    # Later: how to number label, image, features & predicted classes?
    # image is both the data storage format and

    # 1. image normalization, mean-removing, between -1 and 1; scale layer
    # 4. validation data: fix the validation data to be 50% overlap only whereas training data can be as dense possible
    # (Cont.) the context window size needs to be integer multiple of the hop size. E.g., 4 s vs. 1 s.
    # How to save the raw probabilities (not normalized via softmax)
    # how to make notification! A SMS or email

    # EXPT: amplitude on the contour: fixed bandwidth 3 pts
    # DATA: folds based on location, deployment and dates
    # EXPT: FCN layer after pooling?
    # EXPT: filter 64 & stack 5

if __name__ == '__main__':
    main()

