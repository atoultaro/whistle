#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Created on 11/13/19
@author: atoultaro
"""
import os
import numpy as np
import pickle
import random
import matplotlib.pyplot as plt
from math import floor, ceil
import pandas as pd

from contextlib import redirect_stdout

from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix, balanced_accuracy_score, \
    classification_report, f1_score
# import librosa
from keras.utils import to_categorical
from keras.layers import Input, Masking, Dense, Layer, Lambda, Flatten, Reshape, LSTM, RepeatVector, Dropout, TimeDistributed
from keras.models import Sequential, Model
from keras.optimizers import Adam
from keras.callbacks import TensorBoard
from keras.preprocessing.sequence import pad_sequences
from keras import backend as K

from classifier.recurrent import train_and_evaluate
# from classifier.buildmodels import build_model
from classifier.batchgenerator import PaddedBatchGenerator

from cape_cod_whale.preprocess import bin_extract, fea_context_base_generate, \
    fea_context_ae_generate, freq_seq_label_generate_no_harmonics, fea_label_shuffle


from cape_cod_whale.classifier import two_fold_cross_validate_random_forest, two_fold_cross_validate
from cape_cod_whale.load_feature_model import load_fea_model


def max_min_from_list(fea_list):
    max_fea = 0.0
    min_fea = 100000
    for ff in fea_list:
        max_fea = np.max((max_fea, ff.max()))
        min_fea = np.min((min_fea, ff.min()))
    return max_fea, min_fea

# bin files for training & testing
# data_dir = '/Users/ys587/__Data/whistle_Contours/tonals_20190210' # bin files
bin_dir = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files'
bin_dir_train = os.path.join(bin_dir, '__first_pie')
bin_dir_test = os.path.join(bin_dir, '__second_pie')
sound_dir = '/home/ys587/__Data/__whistle/__sound_species/'
species_name = ['bottlenose', 'common', 'spinner', 'melon-headed']
species_id = {'bottlenose': 0, 'common': 1, 'spinner': 2, 'melon-headed': 3}

conf_gen = dict()
conf_gen['bin_dir'] = bin_dir
conf_gen['species_name'] = species_name
conf_gen['species_id'] = species_id
# conf_gen["duration_thre"] = 0.80  # DEBUG
conf_gen["duration_thre"] = 0.05
# conf_gen['time_reso'] = 0.01  # 10 ms
conf_gen['time_reso'] = 0.1  # 100 ms
conf_gen['transform'] = "frq_contour"
# conf_gen['transform'] = "rocca"  # rocca, cep or both
# conf_gen['transform'] = "rocca_cep"
# conf_gen['transform'] = "cep"

# cepstral coefficient
conf_gen['sample_rate'] = 192000
conf_gen['input_size'] = 4096  # input size for deep LSTM features
# 21.33 msec given sampling rate 192,000 Hz
conf_gen['filt_num'] = 64  # number of filterbanks in cepstral coefficients
conf_gen['low_freq'] = 5000.
conf_gen['high_freq'] = 23500.
conf_gen["num_class"] = len(species_name)

# random forest
# conf_gen['n_est'] = 200
conf_gen['n_est'] = 1000

# contextual features
conf_gen['context_win'] = 15.0  # sec
# conf_gen['pca_components'] = None
conf_gen['pca_components'] = 20
conf_gen['n_jobs'] = 6

# autoencoder
# conf_gen['ae_type'] = 'vae'
conf_gen['ae_type'] = 'ae'
conf_gen['ae_lstm'] = 8
conf_gen['ae_latent_dim'] = 32
conf_gen['epoch'] = 100
# conf_gen['epoch'] = 5  # DEBUG
log_dir = "/home/ys587/__Data/__whistle/__log_dir_context/lstm_temp"
conf_gen['log_dir'] = log_dir

# conf_model = dict()
conf_gen['lstm1'] = 16
conf_gen['lstm2'] = 16
conf_gen['batch_size'] = 256
conf_gen['learning_rate'] = 0.1
# conf_gen["l2_regu"] = 5.e-2
conf_gen["l2_regu"] = 0.2
conf_gen["dropout"] = 0.2
conf_gen["recurrent_dropout"] = 0.2
conf_gen["dense_size"] = 64
conf_gen["class_weight"] = {0: 1.0, 1: 1.0, 2: 1.0, 3: 1.0} # default
conf_gen["mod"] = "sum"
# num_dolphins = len(species_name)
conf_gen["num_class"] = len(species_name)
conf_gen["fea_dim"] = 1  # default

if not os.path.exists(conf_gen['log_dir']):
    os.mkdir(conf_gen['log_dir'])

contour_train, bin_wav_pair_train = bin_extract(bin_dir_train, sound_dir,
                                                species_name)
contour_test, bin_wav_pair_test = bin_extract(bin_dir_test, sound_dir,
                                              species_name)

# fit autoencoder & generate encoder
# features & labels
# train data
fea_list_train, label_list_train, dur_train_max, count_long_train, \
count_all_train, fea_name = freq_seq_label_generate_no_harmonics(contour_train,
                                          conf_gen,
                                          species_id)
hist_dur = np.array([ff.shape[0] for ff in fea_list_train])

# test data
fea_list_test, label_list_test, dur_test_max, count_long_test, \
count_all_test, fea_name = freq_seq_label_generate_no_harmonics(contour_test,
                                          conf_gen,
                                          species_id)
dur_max_ind = ceil(max(dur_train_max, dur_test_max)/conf_gen['time_reso'])
conf_gen['dur_train_max'] = dur_train_max
conf_gen['dur_test_max'] = dur_test_max

# find the max and min values ib both fea_train and fea_test
max_fea_train, min_fea_train = max_min_from_list(fea_list_train)
max_fea_test, min_fea_test = max_min_from_list(fea_list_test)
max_fea = np.max((max_fea_train, max_fea_test))
min_fea = np.min((min_fea_train, min_fea_test))
max_minus_min_fea = max_fea - min_fea
fea_list_train_norm = []
for ff in range(len(fea_list_train)):
    fea_list_train_norm.append((fea_list_train[ff]-min_fea)/max_minus_min_fea)
fea_list_test_norm = []
for ff in range(len(fea_list_test)):
    fea_list_test_norm.append((fea_list_test[ff]-min_fea)/max_minus_min_fea)

fea_arr_train = pad_sequences(fea_list_train_norm, maxlen=dur_max_ind, dtype='float')
feature_train, label_train = fea_label_shuffle(fea_arr_train, label_list_train)
# fea_arr_train_3d = np.expand_dims(fea_arr_train0, axis=2)

fea_arr_test = pad_sequences(fea_list_test_norm, maxlen=dur_max_ind, dtype='float')
feature_test, label_test = fea_label_shuffle(fea_arr_test, label_list_test)
# fea_arr_test_3d = np.expand_dims(fea_arr_test0, axis=2)

model_name = "lstm_phone_2_layers"
model_tag = model_name + "_8_8"
conf_gen.update({'lstm1': 8, 'lstm2': 8})
two_fold_cross_validate(model_name, feature_train, label_train, feature_test,
                        label_test, conf_gen, log_dir, model_tag)







