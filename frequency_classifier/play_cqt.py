#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Created on 3/4/20
@author: atoultaro
"""
import librosa
import numpy as np

sound_44kHz = '/home/ys587/__Data/__whistle/__whistle_mann/sound/2017-11-02T145000_0004e9e500057249_2.0~92~0.0051.wav'
sound_48kHz = '/home/ys587/__Data/__whistle/__whistle_gillespie/48kHz/CD/CD1.wav'
sound_96kHz = '/home/ys587/__Data/__whistle/__whistle_gillespie/96kHz/CD/CD15.wav'
time_reso = 0.01

sample_rate = 44100
hop_size = int(time_reso*sample_rate)
samples0, _ = librosa.load(sound_48kHz, sr=sample_rate, offset=2.0, duration=1.0)
whistle_freq0 = np.abs(librosa.pseudo_cqt(samples0, sr=sample_rate,
                                         hop_length=hop_size,
                                         fmin=3000.0, bins_per_octave=24,
                                         n_bins=64))

sample_rate = 48000
hop_size = int(time_reso*sample_rate)
samples1, _ = librosa.load(sound_48kHz, sr=sample_rate, offset=2.0, duration=1.0)
whistle_freq1 = np.abs(librosa.pseudo_cqt(samples1, sr=sample_rate,
                                         hop_length=hop_size,
                                         fmin=3000.0, bins_per_octave=24,
                                         n_bins=64))
sample_rate = 96000
hop_size = int(time_reso*sample_rate)
samples2, _ = librosa.load(sound_96kHz, sr=sample_rate, offset=2.0, duration=1.0)
whistle_freq2 = np.abs(librosa.pseudo_cqt(samples2, sr=sample_rate,
                                         hop_length=hop_size,
                                         fmin=2500.0, bins_per_octave=24,
                                         n_bins=64))
