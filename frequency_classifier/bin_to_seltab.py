#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Created on 11/18/19
@author: atoultaro
"""
import os
import numpy as np
import pickle
import random
import matplotlib.pyplot as plt
from math import floor, ceil
import gc
import pandas as pd

from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix, balanced_accuracy_score, \
    classification_report, f1_score
# import librosa
from keras import backend
from keras.losses import categorical_crossentropy
from keras.utils import to_categorical
from keras.optimizers import Adam, Adadelta
from keras import regularizers

from keras.layers import Dense, Flatten, Dropout, BatchNormalization, \
                        GlobalMaxPooling2D, ZeroPadding2D, AveragePooling2D, \
                        Activation, add
from keras.layers import Conv2D, MaxPooling2D, Conv1D, MaxPooling1D, Input, TimeDistributed
from keras.models import Sequential, load_model, Model
from keras.callbacks import ModelCheckpoint, TensorBoard

from classifier.recurrent import train_and_evaluate
# from classifier.buildmodels import build_model
from classifier.batchgenerator import PaddedBatchGenerator

from cape_cod_whale.preprocess import bin_extract, fea_context_base_generate, \
    fea_context_ae_generate, freq_seq_label_generate_no_harmonics, \
    fea_label_shuffle, contour_target_retrieve

from cape_cod_whale.classifier import two_fold_cross_validate_random_forest, metrics_two_fold
from cape_cod_whale.load_feature_model import load_fea_model, find_best_model

from contextlib import redirect_stdout


bin_dir = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files'
bin_dir_train = os.path.join(bin_dir, '__first_pie_test')
# bin_dir_test = os.path.join(bin_dir, '__second_pie')
sound_dir = '/home/ys587/__Data/__whistle/__sound_species/'
species_name = ['bottlenose', 'common', 'spinner', 'melon-headed']
species_id = {'bottlenose': 0, 'common': 1, 'spinner': 2, 'melon-headed': 3}

# Read species labels, filenames & extract time and frequency sequences
contour_train, bin_wav_pair_train = bin_extract(bin_dir_train, sound_dir,
                                                species_name)
# contour_test, bin_wav_pair_test = bin_extract(bin_dir_test, sound_dir,
#                                               species_name)

# create output folder
# make the file loc
seltab_out = os.path.join(bin_dir_train, '__seltab_out')
if not os.path.exists(seltab_out):
    os.mkdir(seltab_out)
    for ss in species_id.keys():
        os.mkdir(os.path.join(seltab_out, ss))

# read contours from bin files
for ff in contour_train.keys():
    # for ff in list(contour_train.keys())[0:1]:
    species_name = contour_train[ff][0]
    time_freq_info = contour_train[ff][1]
    sound_file = os.path.join(bin_dir_train, species_name, ff+'.bin')
    seltab_file = os.path.join(seltab_out, species_name, ff+'.txt')
    # print(os.path.exists(sound_file))

    event_id = []
    channel_id = []
    time_begin = []
    time_end = []
    freq_low = []
    freq_high = []
    time_dur = []
    num_event = len(time_freq_info)
    for cc in range(num_event):
        event_id.append(str(cc+1))
        channel_id.append(str(1))
        time1 = np.array(time_freq_info[cc]['Time']).min()
        time2 = np.array(time_freq_info[cc]['Time']).max()
        time_begin.append(time1)
        time_end.append(time2)
        time_dur.append(time2-time1)
        freq_low.append( np.array(time_freq_info[cc]['Freq']).min() )
        freq_high.append( np.array(time_freq_info[cc]['Freq']).max() )
    seltab_data = {'Selection': event_id,
                   'View': ['Spectrogram 1' for ii in range(num_event)],
                   'Channel': channel_id,
                   'Begin Time (s)': time_begin,
                   'End Time (s)': time_end,
                   'Low Freq (Hz)': freq_low,
                   'High Freq (Hz)': freq_high,
                   'Score': np.ones(num_event).tolist(),
                   'Duration': time_dur
                   }
    seltab_df = pd.DataFrame(seltab_data)

    # Change order to match what raven expects
    if len(seltab_df) > 0:
        seltab_df = seltab_df[
            ['Selection', 'View', 'Channel', 'Begin Time (s)', 'End Time (s)',
             'Low Freq (Hz)', 'High Freq (Hz)', 'Score', 'Duration']]

    # export
    seltab_df.to_csv(seltab_file, sep='\t', mode='a', index=False)



