#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
whislte classification using frequency contours
4-fold cross-validation
Created on 11/21/19
@author: atoultaro
"""
import os
import numpy as np
import pickle
import random
import matplotlib.pyplot as plt
from math import floor, ceil
import gc

from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix, balanced_accuracy_score, \
    classification_report, f1_score
# import librosa
from keras import backend
from keras.losses import categorical_crossentropy
from keras.utils import to_categorical
from keras.optimizers import Adam, Adadelta
from keras import regularizers

from keras.layers import Dense, Flatten, Dropout, BatchNormalization, \
                        GlobalMaxPooling2D, ZeroPadding2D, AveragePooling2D, \
                        Activation, add
from keras.layers import Conv2D, MaxPooling2D, Conv1D, MaxPooling1D, Input, TimeDistributed
from keras.models import Sequential, load_model, Model
from keras.callbacks import ModelCheckpoint, TensorBoard, EarlyStopping
import cv2

from classifier.recurrent import train_and_evaluate
# from classifier.buildmodels import build_model
from classifier.batchgenerator import PaddedBatchGenerator

from cape_cod_whale.preprocess import bin_extract, fea_context_base_generate, \
    fea_label_shuffle, contour_retrieve, contour_target_retrieve

from cape_cod_whale.classifier import two_fold_cross_validate_random_forest, metrics_two_fold
from cape_cod_whale.load_feature_model import load_fea_model, find_best_model

from contextlib import redirect_stdout

from keras.callbacks import Callback
import matplotlib.patches as mpatches
import itertools


def lenet_dropout_input_conv(conf):
    """
    Buidling the model of LeNet with dropout on the input, both
    convolutional layers and full-connected layer

    Args:
        conf: configuration class object

        input_shape: (conf.IMG_F, conf.IMG_T, 1)
        conf.NUM_CLASSES: numbwer of classes
        conf.RATE_DROPOUT_INPUT: dropout rate
        conf.RATE_DROPOUT_CONV
        conf.RATE_DROPOUT_FC
    Returns:
        model: built model
    """
    model = Sequential()
    model.add(Dropout(conf['dropout'],
                      input_shape=(conf['IMG_F'], conf['IMG_T'], 1)))

    # conv group 1
    model.add(
        Conv2D(8, kernel_size=(4, 2), strides=(1, 1),
               kernel_regularizer=regularizers.l2(conf['l2_regu']),
               activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    model.add(Dropout(conf['dropout']))

    # conv group 2
    model.add(Conv2D(8, kernel_size=(4, 2), strides=(1, 1),
                     kernel_regularizer=regularizers.l2(conf['l2_regu']),
                     activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    model.add(Dropout(conf['dropout']))

    # conv group 3
    model.add(Conv2D(8, kernel_size=(4, 2), strides=(1, 1),
                     kernel_regularizer=regularizers.l2(conf['l2_regu']),
                     activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    model.add(Dropout(conf['dropout']))

    # conv group 4
    model.add(Conv2D(8, kernel_size=(4, 2), strides=(1, 1),
                     kernel_regularizer=regularizers.l2(conf['l2_regu']),
                     activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    model.add(Dropout(conf['dropout']))

    model.add(Flatten())
    model.add(Dense(32, activation='relu'))
    model.add(Dropout(conf['dropout']))
    model.add(Dense(conf['NUM_CLASSES'], activation='softmax'))

    return model


def convnet_long_time(conf):
    model = Sequential()
    # Group 1
    model.add(Conv2D(8, kernel_size=(3, 3), strides=(1, 1), activation='relu',
                     kernel_initializer='he_normal', padding='same',
                     kernel_regularizer=regularizers.l2(conf['l2_regu']),
                     input_shape=(conf['IMG_F'], conf['IMG_T'], 1)))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 2
    model.add(Conv2D(8, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
                     kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 3
    model.add(
        Conv2D(8, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 4
    model.add(
        Conv2D(8, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 5
    model.add(
        Conv2D(8, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 6
    model.add(
        Conv2D(8, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))

    # 1x1 convolution
    model.add(
        Conv2D(8, kernel_size=(1, 1), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same'))
    model.add(GlobalMaxPooling2D())
    # model.add(Dropout(conf['dropout']))
    model.add(Dense(conf['NUM_CLASSES'], activation='softmax'))

    return model


def birdnet(conf):
    """
    Birdnet
    """
    model = Sequential()
    # Group 1
    model.add(Conv2D(32, kernel_size=(7, 7), strides=(1, 1), activation='relu',
                     kernel_initializer='he_normal', padding='same',
                     kernel_regularizer=regularizers.l2(conf['l2_regu']),
                     input_shape=(conf['IMG_F'], conf['IMG_T'], 1)))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 2
    model.add(Conv2D(32, kernel_size=(5, 5), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
                     kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 3
    model.add(
        Conv2D(64, kernel_size=(5, 5), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 4
    model.add(
        Conv2D(128, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 5
    model.add(
        Conv2D(256, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 6
    model.add(
        Conv2D(512, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))

    model.add(Flatten())
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.5))

    # model.add(Dropout(conf['dropout']))
    model.add(Dense(conf['NUM_CLASSES'], activation='softmax'))

    return model


def birdnet_7layers(conf):
    """
    Birdnet
    """
    model = Sequential()
    # Group 1
    model.add(Conv2D(32, kernel_size=(7, 7), strides=(1, 1), activation='relu',
                     kernel_initializer='he_normal', padding='same',
                     kernel_regularizer=regularizers.l2(conf['l2_regu']),
                     input_shape=(conf['IMG_F'], conf['IMG_T'], 1)))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 2
    model.add(Conv2D(32, kernel_size=(5, 5), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
                     kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 3
    model.add(
        Conv2D(64, kernel_size=(5, 5), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 4
    model.add(
        Conv2D(128, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 5
    model.add(
        Conv2D(256, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 6
    model.add(
        Conv2D(512, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 7
    model.add(
        Conv2D(1024, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))

    model.add(Flatten())
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.5))

    # model.add(Dropout(conf['dropout']))
    model.add(Dense(conf['NUM_CLASSES'], activation='softmax'))

    return model


def birdnet_8layers(conf):
    """
    Birdnet
    """
    model = Sequential()
    # Group 1
    model.add(Conv2D(32, kernel_size=(7, 7), strides=(1, 1), activation='relu',
                     kernel_initializer='he_normal', padding='same',
                     kernel_regularizer=regularizers.l2(conf['l2_regu']),
                     input_shape=(conf['IMG_F'], conf['IMG_T'], 1)))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 2
    model.add(Conv2D(32, kernel_size=(5, 5), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
                     kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 3
    model.add(
        Conv2D(64, kernel_size=(5, 5), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 4
    model.add(
        Conv2D(128, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 5
    model.add(
        Conv2D(256, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 6
    model.add(
        Conv2D(512, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 7
    model.add(
        Conv2D(1024, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 8
    model.add(
        Conv2D(1024, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(1, 2)))
    model.add(Dropout(conf['dropout']))

    model.add(Flatten())
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.5))

    # model.add(Dropout(conf['dropout']))
    model.add(Dense(conf['NUM_CLASSES'], activation='softmax'))

    return model


def birdnet_freq(conf):
    """
    Birdnet
    """
    model = Sequential()
    # Group 1
    model.add(Conv2D(32, kernel_size=(7, 7), strides=(1, 1), activation='relu',
                     kernel_initializer='he_normal', padding='same',
                     kernel_regularizer=regularizers.l2(conf['l2_regu']),
                     input_shape=(conf['IMG_F'], conf['IMG_T'], 1)))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 2
    model.add(Conv2D(32, kernel_size=(5, 5), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
                     kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 3
    model.add(
        Conv2D(64, kernel_size=(5, 5), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 4
    model.add(
        Conv2D(128, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 5
    model.add(
        Conv2D(256, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(1, 2)))
    model.add(Dropout(conf['dropout']))
    # Group 6
    model.add(
        Conv2D(512, kernel_size=(3, 3), strides=(1, 1), activation='relu',
               kernel_initializer='he_normal', padding='same',
               kernel_regularizer=regularizers.l2(conf['l2_regu']),))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(1, 2)))
    model.add(Dropout(conf['dropout']))

    model.add(Flatten())
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.5))

    # model.add(Dropout(conf['dropout']))
    model.add(Dense(conf['NUM_CLASSES'], activation='softmax'))

    return model


def prepare_data(contour_target_list, conf, img_folder, save_file, plot=False):
    '''
    Convert whistle contours into sequences of fixed length
    :param contour_target_list:
    time_reso = 0.1, context_winsize=10.0, ratio_thre=0.02
    :return:
    df_target:
    '''
    freq_low_all = 192000.0
    freq_high_all = 0.0
    whistle_image_list = []
    label_list = []

    if plot:
        plt.ion()
        fig = plt.figure()
        ax = fig.add_subplot(111)

        # img_dir = conf_gen['image_pie' + str(pp + 1)]
        if not os.path.exists(img_folder):
            os.mkdir(img_folder)
            for ss in conf['species_name']:
                os.mkdir(os.path.join(img_folder, ss))

    data_count = 1
    for ff in range(len(contour_target_list)):
        filename = contour_target_list[ff][0]
        print('\n'+filename)
        label_contour = contour_target_list[ff][1]
        file_contour = contour_target_list[ff][2]

        contour_target_ff, start_time, end_time, freq_low, freq_high = \
            contour_data(file_contour, conf['time_reso'])
        freq_high_all = np.max((freq_high, freq_high_all))
        freq_low_all = np.min((freq_low, freq_low_all))

        timesteps = ceil((end_time - start_time)/conf['time_reso'])+1
        print("Start time: "+str(start_time))
        print("Stop time: " + str(end_time))

        # convert whistle freq into into a 2d feature map: whistle_freq for each file
        whistle_freq = np.zeros((conf['fft_size'], int(timesteps)))
        for cc in contour_target_ff:
            time_ind_start = int(floor((cc['Time'][0]-start_time)/conf['time_reso']))
            freq_ind = (np.floor(cc['Freq']/conf['sample_rate']*conf['fft_size'])).astype('int')
            for ii in range(cc['Time'].shape[0]):
                try:
                    whistle_freq[freq_ind[ii], time_ind_start+ii] = 1.0
                except:
                    print('stop!')

        if conf['spectro_dilation']:
            # whistle_freq_smooth = cv2.GaussianBlur(whistle_freq, (3, 3), 0, 0, cv2.BORDER_DEFAULT)
            kernel = np.ones((3, 3)).astype(np.uint8)
            kernel[0, 0] = 0
            kernel[0, 2] = 0
            kernel[2, 0] = 0
            kernel[2, 2] = 0
            whistle_freq_smooth = cv2.dilate(whistle_freq, kernel, cv2.BORDER_DEFAULT)
            whistle_freq = whistle_freq_smooth

        # plt.matshow(whistle_freq); plt.show()
        # print('whistle_freq shape: '+str(whistle_freq.shape[0])+', '+str(whistle_freq.shape[1]) )

        # cut whistle_freq into segments for data samples
        size_time = int(conf['context_winsize']/conf['time_reso'])
        size_hop = int(conf['context_hopsize']/conf['time_reso'])
        freq_high = conf['freq_ind_high']
        freq_low = conf['freq_ind_low']
        for tt in range(floor((whistle_freq.shape[1]-size_time)/size_hop)):
            whistle_image = whistle_freq[freq_low:freq_high, tt*size_hop:tt*size_hop+size_time]
            # if whistle_image.sum()/whistle_image.shape[0]/whistle_image.shape[1] >= 0.01:
            # if whistle_image.sum() >= 0.1*conf['IMG_T']:
            # print('whistle_image.sum: '+str(whistle_image.sum()))
            # plt.draw()
            # plt.pause(0.0001)
            # plt.clf()
            # plt.show()
            if (whistle_image.sum(axis=0) > 0).sum() >= conf['contour_timethre']:
                whistle_image_list.append(whistle_image)
                label_list.append(species_id[label_contour])

                if plot is True:
                    ax.matshow(whistle_image, origin='lower')
                    ax.title.set_text(str(data_count)+': '+label_contour)
                    ax.xaxis.tick_bottom()
                    fig.canvas.draw()
                    plt.savefig(os.path.join(img_folder, label_contour, str(data_count)+'_'+label_contour+'.png'))
                data_count += 1
                # print('stop for images')

    whistle_image_arr = np.asarray(whistle_image_list)
    # save image array
    if conf_gen['numpy_data_output']:
        np.savez(save_file, whistle_image=whistle_image_arr, label=label_list)

    return whistle_image_arr, label_list, freq_high_all, freq_low_all


def contour_data(file_contour, time_reso):
    print('Retrieving contours...')
    contour_target_ff = []
    len_contour = len(file_contour)
    print('len_contour: '+str(len_contour))
    time_min = 86400.0
    time_max = 0.0
    freq_high = 0.0
    freq_low = 192000.0
    # read contours into the var contour_target_ff
    for cc in range(len_contour):
        time_contour = file_contour[cc]['Time']
        freq_contour = file_contour[cc]['Freq']

        if time_contour.shape[0] > 1:
            # linear interpolation
            # time_contour_interp = np.arange(time_contour[0], time_contour[-1],
            #                                 time_reso)
            new_start_time = round(time_contour[0]/time_reso)*time_reso
            new_step = ceil((time_contour[-1] - time_contour[0])/time_reso)
            time_contour_interp = np.arange(new_start_time, new_start_time+new_step*time_reso, time_reso)

            time_min = np.min((time_contour_interp[0], time_min))
            time_max = np.max((time_contour_interp[-1], time_max))

            freq_contour_interp = np.interp(time_contour_interp, time_contour,
                                            freq_contour)
            freq_high = np.max((np.max(freq_contour_interp), freq_high))
            freq_low = np.min((np.min(freq_contour_interp), freq_low))

            contour_target_ff_cc = dict()
            contour_target_ff_cc['Time'] = time_contour_interp
            contour_target_ff_cc['Freq'] = freq_contour_interp

            contour_target_ff.append(contour_target_ff_cc)

    return contour_target_ff, time_min, time_max, freq_low, freq_high


class ConfusionMatrixPlotter(Callback):
    """Plot the confusion matrix on a graph and update after each epoch
    # Arguments
        X_val: The input values
        Y_val: The expected output values
        classes: The categories as a list of string names
        normalize: True - normalize to [0,1], False - keep as is
        cmap: Specify matplotlib colour map
        title: Graph Title
    """
    def __init__(self, X_val, Y_val, classes, normalize=False,
                 cmap=plt.cm.Blues, title='Confusion Matrix'):
        self.X_val = X_val
        self.Y_val = Y_val
        self.title = title
        self.classes = classes
        self.normalize = normalize
        self.cmap = cmap
        plt.ion()
        # plt.show()
        plt.figure()

        plt.title(self.title)

    def on_train_begin(self, logs={}):
        pass

    def on_epoch_end(self, epoch, logs={}):
        plt.clf()
        pred = self.model.predict(self.X_val)
        max_pred = np.argmax(pred, axis=1)
        max_y = np.argmax(self.Y_val, axis=1)
        cnf_mat = confusion_matrix(max_y, max_pred)

        if self.normalize:
            cnf_mat = cnf_mat.astype('float') / cnf_mat.sum(axis=1)[:,
                                                np.newaxis]

        thresh = cnf_mat.max() / 2.
        for i, j in itertools.product(range(cnf_mat.shape[0]),
                                      range(cnf_mat.shape[1])):
            plt.text(j, i, cnf_mat[i, j], horizontalalignment="center",
                     color="white" if cnf_mat[i, j] > thresh else "black")

        plt.imshow(cnf_mat, interpolation='nearest', cmap=self.cmap)

        # Labels
        tick_marks = np.arange(len(self.classes))
        plt.xticks(tick_marks, self.classes, rotation=45)
        plt.yticks(tick_marks, self.classes)

        plt.colorbar()

        plt.tight_layout()
        plt.ylabel('True label')
        plt.xlabel('Predicted label')
        # plt.draw()
        plt.show()
        plt.pause(0.001)


def one_fold_validate(whistle_image_target_4d, label_target,
                  whistle_image_validate_4d, label_validate,
                  conf, fold_id=1):
    label_target_cat = to_categorical(label_target)
    label_validate_cat = to_categorical(label_validate)

    # model = lenet_dropout_input_conv(conf)
    # model = convnet_long_time(conf)
    # model = convnet_long_time2(conf)
    # model = birdnet(conf)  # 6 layers
    model = birdnet_7layers(conf)  # 7 layers
    # model = birdnet_8layers(conf)  # 8 layers
    # model = birdnet_freq(conf)
    model_name_format = 'epoch_{epoch:02d}_valloss_{val_loss:.4f}_valacc_{val_acc:.4f}.hdf5'
    log_dir1 = os.path.join(conf['log_dir'], 'fold'+str(fold_id))
    if not os.path.exists(log_dir1):
        os.mkdir(log_dir1)
    check_path = os.path.join(log_dir1, model_name_format)

    if fold_id is 1:
        with open(os.path.join(conf['log_dir'], 'architecture.txt'), 'w') as f:
            with redirect_stdout(f):
                # print('')
                for kk in sorted(list(conf.keys())):
                    print(kk + ' ==>> ' + str(conf[kk]))
                model.summary()

    # checkpoint
    checkpoint = ModelCheckpoint(check_path, monitor='val_loss', verbose=0,
                                 save_best_only=True)
    early_stop = EarlyStopping(monitor='val_loss', mode='min', verbose=1,
                               patience=50)
    if conf['confusion_callback']:
        cm_plot = ConfusionMatrixPlotter(whistle_image_validate_4d,
                                         label_validate_cat, species_name)
    # model compile
    model.compile(loss=categorical_crossentropy,
                  optimizer=Adadelta(lr=conf['learning_rate']),
                  # optimizer=Adam(lr=conf['learning_rate']),
                  metrics=['accuracy'])
    model.summary()

    count_species1 = label_target_cat.sum(axis=0).tolist()
    conf["class_weight"] = (
                max(count_species1) / np.array(count_species1)).tolist()
    # conf["class_weight"] = (1.0+(max(count_species1) / np.array(count_species1) - 1.0)*5.0).tolist()
    # conf["class_weight"] = ((max(count_species1) / np.array(count_species1))**2.0).tolist()

    if conf['confusion_callback']:
        callback_list = [checkpoint, TensorBoard(log_dir=log_dir1), cm_plot,
                         early_stop]
    else:
        callback_list = [checkpoint, TensorBoard(log_dir=log_dir1), early_stop]

    model.fit(whistle_image_target_4d, label_target_cat,
              batch_size=conf['batch_size'], epochs=conf['epoch'],
              verbose=1, validation_split=0.2,
              callbacks=callback_list, class_weight=conf["class_weight"])
    re_model_name_format = 'epoch_\d+_valloss_(\d+.\d{4})_valacc_\d+.\d{4}.hdf5'
    best_model_path, _ = find_best_model(log_dir1, re_model_name_format,
                                         is_max=False, purge=True)
    conf['best_model'] = best_model_path
    model = load_model(best_model_path)
    y_pred2 = model.predict(whistle_image_validate_4d)
    y_pred2 = np.argmax(y_pred2, axis=1)
    metrics_two_fold(label_validate, y_pred2, log_dir1, 'accuracy_fold.txt',
                     conf, mode='fold')

    del model
    gc.collect()
    backend.clear_session()

    return y_pred2, best_model_path


# bin files for training & testing
bin_dir = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/cv4'
# bin_dir_train = os.path.join(bin_dir, 'cv2/__first_pie')
# bin_dir_test = os.path.join(bin_dir, 'cv2/__second_pie')
bin_dir_fold = dict()
for pp in range(4):  # 'bin_dir_fold['pie1']'
    bin_dir_fold['pie'+str(pp+1)] = os.path.join(bin_dir, 'pie'+str(pp+1))

sound_dir = '/home/ys587/__Data/__whistle/__sound_species/'
species_name = ['bottlenose', 'common', 'spinner', 'melon-headed']
species_id = {'bottlenose': 0, 'common': 1, 'spinner': 2, 'melon-headed': 3}

conf_gen = dict()
conf_gen['log_dir'] = "/home/ys587/__Data/__whistle/__log_dir_context/contour_temp"
conf_gen['bin_dir'] = bin_dir

conf_gen['species_name'] = species_name
conf_gen['species_id'] = species_id
# conf_gen["duration_thre"] = 0.05
# conf_gen['time_reso'] = 0.01  # 10 ms
# conf_gen['time_reso'] = 0.05  # 50 ms
conf_gen['time_reso'] = 0.02  # 20 ms
# conf_gen['time_reso'] = 0.1  # 100 ms

# cepstral coefficient
conf_gen['sample_rate'] = 192000
conf_gen["num_class"] = len(species_name)

# conf_gen['context_winsize'] = 10.0  # sec
conf_gen['context_winsize'] = 5.0  # sec
# conf_gen['context_hopsize'] = 5.0  # sec
# conf_gen['context_hopsize'] = 2.5  # sec
conf_gen['context_hopsize'] = 1.0  # sec
# conf_gen['fft_size'] = 512
conf_gen['fft_size'] = 1024
conf_gen['freq_ind_low'] = floor(5000./(conf_gen['sample_rate']/conf_gen['fft_size']))
conf_gen['freq_ind_high'] = ceil(50000./(conf_gen['sample_rate']/conf_gen['fft_size']))
# conf_gen['freq_ind_high'] = 267  # 50 kHz
# conf_gen['freq_ind_low'] = 26  # 5 kHz

# conf_gen['IMG_T'] = 200
conf_gen['IMG_T'] = int(floor((conf_gen['context_winsize'] / conf_gen['time_reso'])))
# conf_gen['IMG_F'] = 250
conf_gen['IMG_F'] = conf_gen['freq_ind_high'] - conf_gen['freq_ind_low']
# conf_gen['contour_timethre'] = 100
# conf_gen['contour_timethre'] = 50  # 1 s. used for 10-s sound clips with 5-s overlap
# conf_gen['contour_timethre'] = 25  # 0.5 s.
conf_gen['contour_timethre'] = 50  # 1 s
# conf_gen['contour_timethre'] = 100  # 2 s.
# conf_gen['contour_timethre'] = 150  # 3 s.
# conf_gen['contour_timethre'] = 200  # 4 s.

conf_gen['l2_regu'] = 0.01
# conf_gen['l2_regu'] = 0.001
# conf_gen['l2_regu'] = 0.2
conf_gen['dropout'] = 0.1
conf_gen['NUM_CLASSES'] = 4
# conf_gen['batch_size'] = 128
conf_gen['batch_size'] = 64
conf_gen['epoch'] = 200
# conf_gen['epoch'] = 10  # debug
conf_gen['learning_rate'] = 1.0
conf_gen['img_data_output'] = False  # output image of spectrogram data
conf_gen['numpy_data_output'] = False
conf_gen['confusion_callback'] = False  # activate the call back of confusion matrix plot
conf_gen['spectro_dilation'] = False

for pp in range(4):  # 'pie1_data.npz'
    conf_gen['save_file_pie'+str(pp+1)] = os.path.join(conf_gen['log_dir'],
                                               'pie'+str(pp+1)+'_data.npz')
for pp in range(4):
    conf_gen['image_pie' + str(pp + 1)] = os.path.join(conf_gen['log_dir'],
                                                           'pie' + str(pp + 1))

if not os.path.exists(conf_gen['log_dir']):
    os.mkdir(conf_gen['log_dir'])

# Read species labels, filenames & extract time and frequency sequences
contour_pie1, bin_wav_pair_pie1 = bin_extract(bin_dir_fold['pie1'], sound_dir,
                                                species_name)
contour_pie2, bin_wav_pair_pie2 = bin_extract(bin_dir_fold['pie2'], sound_dir,
                                                species_name)
contour_pie3, bin_wav_pair_pie3 = bin_extract(bin_dir_fold['pie3'], sound_dir,
                                                species_name)
contour_pie4, bin_wav_pair_pie4 = bin_extract(bin_dir_fold['pie4'], sound_dir,
                                                species_name)

# read contours from bin files
contour_pie1_list = contour_target_retrieve(contour_pie1,
                                             bin_wav_pair_pie1,
                                             bin_dir_fold['pie1'],
                                             conf_gen)
contour_pie2_list = contour_target_retrieve(contour_pie2,
                                             bin_wav_pair_pie2,
                                             bin_dir_fold['pie2'],
                                             conf_gen)
contour_pie3_list = contour_target_retrieve(contour_pie3,
                                             bin_wav_pair_pie3,
                                             bin_dir_fold['pie3'],
                                             conf_gen)
contour_pie4_list = contour_target_retrieve(contour_pie4,
                                             bin_wav_pair_pie4,
                                             bin_dir_fold['pie4'],
                                             conf_gen)

# prepare training & testing data
whistle_image_pie1, label_pie1, freq_high_pie1, freq_low_pie1 = \
    prepare_data(contour_pie1_list, conf_gen,
                 conf_gen['image_pie1'],
                 conf_gen['save_file_pie1'],
                 plot=conf_gen['img_data_output'])
whistle_image_pie2, label_pie2, freq_high_pie2, freq_low_pie2 = \
    prepare_data(contour_pie2_list, conf_gen,
                 conf_gen['image_pie2'],
                 conf_gen['save_file_pie2'],
                 plot=conf_gen['img_data_output'])
whistle_image_pie3, label_pie3, freq_high_pie3, freq_low_pie3 = \
    prepare_data(contour_pie3_list, conf_gen,
                 conf_gen['image_pie3'],
                 conf_gen['save_file_pie3'],
                 plot=conf_gen['img_data_output'])
whistle_image_pie4, label_pie4, freq_high_pie4, freq_low_pie4 = \
    prepare_data(contour_pie4_list, conf_gen,
                 conf_gen['image_pie4'],
                 conf_gen['save_file_pie4'],
                 plot=conf_gen['img_data_output'])

whistle_image_pie1_4d = np.expand_dims(whistle_image_pie1, axis=3)
whistle_image_pie2_4d = np.expand_dims(whistle_image_pie2, axis=3)
whistle_image_pie3_4d = np.expand_dims(whistle_image_pie3, axis=3)
whistle_image_pie4_4d = np.expand_dims(whistle_image_pie4, axis=3)

# conv model
# fold 1: pie 2, 3, 4 as training and pie 1 as testing
whistle_image_train_fold1_4d = np.vstack((whistle_image_pie2_4d,
                                          whistle_image_pie3_4d,
                                          whistle_image_pie4_4d))
label_train_fold1 = label_pie2+label_pie3+label_pie4
y_pred1, best_model1 = one_fold_validate(whistle_image_train_fold1_4d, label_train_fold1,
                  whistle_image_pie1_4d, label_pie1,
                  conf_gen, fold_id=1)
# fold 2: pie 1, 3, 4 as training and pie 2 as testing
whistle_image_train_fold2_4d = np.vstack((whistle_image_pie1_4d,
                                          whistle_image_pie3_4d,
                                          whistle_image_pie4_4d))
label_train_fold2 = label_pie1+label_pie3+label_pie4
y_pred2, best_model2 = one_fold_validate(whistle_image_train_fold2_4d, label_train_fold2,
                  whistle_image_pie2_4d, label_pie2,
                  conf_gen, fold_id=2)
# fold 3: pie 1, 2, 4 as training and pie 3 as testing
whistle_image_train_fold3_4d = np.vstack((whistle_image_pie1_4d,
                                          whistle_image_pie2_4d,
                                          whistle_image_pie4_4d))
label_train_fold3 = label_pie1+label_pie2+label_pie4
y_pred3, best_model3 = one_fold_validate(whistle_image_train_fold3_4d, label_train_fold3,
                  whistle_image_pie3_4d, label_pie3,
                  conf_gen, fold_id=3)
# fold 4: pie 1, 2, 3 as training and pie 4 as testing
whistle_image_train_fold4_4d = np.vstack((whistle_image_pie1_4d,
                                          whistle_image_pie2_4d,
                                          whistle_image_pie3_4d))
label_train_fold4 = label_pie1+label_pie2+label_pie3
y_pred4, best_model4 = one_fold_validate(whistle_image_train_fold4_4d, label_train_fold4,
                  whistle_image_pie4_4d, label_pie4,
                  conf_gen, fold_id=4)

# collect all
y_pred_tot = np.concatenate((y_pred1, y_pred2, y_pred3, y_pred4))
label_total = label_pie1 + label_pie2 + label_pie3 + label_pie4
metrics_two_fold(label_total, y_pred_tot, conf_gen['log_dir'],
                 'accuracy_total.txt', conf_gen, mode='total')
