#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Evaluate performance after the training & testing are done.
Apply sklearn.metrics on the saved models in a batch modes

Created on 6/26/19
@author: atoultaro
"""
import os
import random
import numpy as np

from contextlib import redirect_stdout
import gc
from keras.utils import to_categorical
from keras.models import load_model
from keras import backend
from sklearn.metrics import confusion_matrix, balanced_accuracy_score, \
    classification_report, f1_score

from cape_cod_whale.preprocess import bin_extract, fea_label_generate, \
    save_fea, load_fea, freq_seq_label_generate
from cape_cod_whale.classifier import find_best_model
from classifier.batchgenerator import PaddedBatchGenerator


model_path = u'/home/ys587/__Data/__whistle/__log_dir_freq_contour/20190626/make_simple_lstm_mask_regu_128'


# Data
# bin files for training & testing
# data_dir = '/Users/ys587/__Data/whistle_Contours/tonals_20190210' # bin files
bin_dir_train = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__first_pie'
bin_dir_test = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__second_pie'
bin_dir_all = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__all'
sound_dir = '/home/ys587/__Data/__whistle/__sound_species/'

species_name = ['bottlenose', 'common', 'spinner', 'melon-headed']
species_id = {'bottlenose': 0, 'common': 1, 'spinner': 2, 'melon-headed': 3}
contour_train, bin_wav_pair_train = bin_extract(bin_dir_train, sound_dir,
                                                species_name)
contour_test, bin_wav_pair_test = bin_extract(bin_dir_test, sound_dir,
                                              species_name)

# find contours longer than a pre-defined duration and extract audio features
conf_general = dict()
conf_general["duration_thre"] = 0.20
# conf.duration_thre = 0.50
percentile_list = [0, 25, 50, 75, 100]
conf_general['time_reso'] = 0.005

conf_model = dict()
conf_model['batch_size'] = 128
conf_model['epochs'] = 25
conf_model['learning_rate'] = 0.005

# conf_model['dur_train_max'] = dur_train_max
# conf_model['dur_test_max'] = dur_test_max
conf_model["l2_regu"] = 5.e-2
# conf_model["class_weight"] = {0: 1.0, 1: 1.0, 2: 7.0, 3: 3.0}
conf_model["class_weight"] = {0: 1.0, 1: 1.0, 2: 1.0, 3: 1.0}
conf_model["mod"] = "sum"


# features & labels
print("data: features and labels")
fea_list_train, label_list_train, dur_train_max, count_long_train, \
count_all_train = freq_seq_label_generate(contour_train,
                                          conf_general['time_reso'],
                                          bin_dir_train, species_id)

fea_list_test, label_list_test, dur_test_max, count_long_test, count_all_test \
    = freq_seq_label_generate(contour_test,
                              conf_general['time_reso'],
                              bin_dir_train, species_id)


# Features & labels
train_idx = [ii for ii in range(len(label_list_train))]
random.shuffle(train_idx)
test_idx = [ii for ii in range(len(label_list_test))]
random.shuffle(test_idx)

# Use PaddedBatchGenerator to get features and labels
feature_train = [fea_list_train[tt] for tt in train_idx]
target_train = to_categorical(label_list_train)[train_idx, :]
feature_test = [fea_list_test[tt] for tt in test_idx]
target_test = to_categorical(label_list_test)[test_idx, :]


# method 1: save the model and weight
# model = model_from_json()
# model.load_weights(os.path.join(config.TRAIN_RESULT_PATH, 'Run'+str(run_ind), 'model_weights.h5'))

# method 2: save the model only


# 1st fold
best_model_path, best_accu = find_best_model(os.path.join(model_path, 'fold1'))
print('Best model for fold 1: ')
print(best_model_path)
model_lstm = load_model(best_model_path)
# model_lstm = load_model(os.path.join(model_path, 'fold1', 'epoch_23_0.5252_1.2586.hdf5'))

testgen1 = PaddedBatchGenerator(feature_test, target_test,
                               batch_size=target_test.shape[0]
                               )

(x_test1, y_test1_onehot) = next(testgen1)
class_prob = model_lstm.predict(x_test1)  # predict_proba the same as predict
y_pred1 = np.argmax(class_prob, axis=1)
y_test1 = np.argmax(y_test1_onehot, axis=1).astype(int)

del model_lstm
gc.collect()
backend.clear_session()


# 2nd fold: switch train and test data
best_model_path, best_accu= find_best_model(os.path.join(model_path, 'fold2'))
print('Best model for fold 2: ')
print(best_model_path)
model_lstm = load_model(best_model_path)
# model_lstm = load_model(os.path.join(model_path, 'fold2', 'epoch_16_0.3194_1.2887.hdf5'))

testgen2 = PaddedBatchGenerator(feature_train, target_train,
                                batch_size=target_train.shape[0]
                                )

(x_test2, y_test2_onehot) = next(testgen2)
class_prob = model_lstm.predict(x_test2)  # predict_proba the same as predict
y_pred2 = np.argmax(class_prob, axis=1)
y_test2 = np.argmax(y_test2_onehot, axis=1).astype(int)

del model_lstm
gc.collect()
backend.clear_session()


# performance metrics of 2-fold cross-validation
y_pred = np.concatenate((y_pred1, y_pred2))
y_test = np.concatenate((y_test1, y_test2))

# metrics_two_fold(y_test, y_pred, log_dir, conf)
# confusion matrix
confu_mat = confusion_matrix(y_test, y_pred)

# balanced accuracy
balanced_accuracy = balanced_accuracy_score(y_test, y_pred)

# classification report
class_report = classification_report(y_test, y_pred)

# f1-score
f1_class_avg = f1_score(y_test, y_pred, average='macro')

with open(os.path.join(model_path, 'metrics.txt'), 'w') as f:
    with redirect_stdout(f):
        print("Confusion matrix:\n")
        print(confu_mat)
        print("\nBalanced accuracy:")
        print(balanced_accuracy)
        print("\nClassification_report:")
        print(class_report)
        print("\nf1 score:")
        print(f1_class_avg)

        print("\nHyper-parameters:")
        print("Batch size: " + str(conf_model['batch_size']))
        print("Epochs: " + str(conf_model['epochs']))
        print("L2_regularization: " + str(conf_model['l2_regu']))
        # print("Class weight: " + str(conf['class_weight']))




