#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Created on 1/22/20
@author: atoultaro
"""

from keras.models import load_model
from vis.utils import utils
from keras import activations
import os
import numpy as np
from matplotlib import pyplot as plt

import warnings
warnings.filterwarnings('ignore')

species_id = {'bottlenose': 0, 'common': 1, 'spinner': 2, 'melon-headed': 3}
resnet_model = '/home/ys587/__Data/__whistle/__log_dir_context/classification_audio/__cv5/resnet_cifar10_expt_run0_f1_p740_notGlobalAvg/fold1/epoch_165_valloss_0.0648_valacc_0.9822.hdf5'

model = load_model(resnet_model)
model.layers[-1].activation = activations.linear
model = utils.apply_modifications(model)


# load the data / image
data_store_five = "/home/ys587/__Data/__whistle/__log_dir_context/audio_data_store/__five_class"
pie1_data_path = os.path.join(data_store_five, 'pie1_data.npz')
data_temp = np.load(pie1_data_path)
whistle_image_pie1 = data_temp['whistle_image']
label_pie1 = data_temp['label'].tolist()

whistle_image_pie1_4d = np.expand_dims(whistle_image_pie1, axis=3)
