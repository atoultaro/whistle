#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Created on 6/28/19
@author: atoultaro
"""

import os
import numpy as np
import pickle
import random
import gc

from keras import backend
# import matplotlib.pyplot as plt
from sklearn.svm import SVC, LinearSVC

# import librosa
from keras.utils import to_categorical, plot_model
from keras.layers import LSTM, RepeatVector, TimeDistributed, Dense
from keras.models import Sequential, Model

# from classifier.recurrent import train_and_evaluate
# from classifier.buildmodels import build_model
# from classifier.batchgenerator import PaddedBatchGenerator

from cape_cod_whale.preprocess import bin_extract, fea_label_generate, \
    save_fea, load_fea, freq_seq_label_generate
from cape_cod_whale.classifier import metrics_two_fold


def model_autoencoder_lstm(data_shape, conf):
    '''
    lstm autoencoder model
    :param data_shape:
    :param conf:
    :return:
    '''
    model_auto = Sequential()
    model_auto.add(LSTM(conf['lstm_dim'], activation='relu', input_shape=data_shape))
    model_auto.add(RepeatVector(data_shape[0]))
    model_auto.add(LSTM(conf['lstm_dim'], activation='relu', return_sequences=True))
    model_auto.add(TimeDistributed(Dense(1)))

    return model_auto


def autoencoder_train_pred(sequence, seq_len, conf):
    '''
    autoencoder fit and predict
    :param sequence:
    :param seq_len:
    :param conf:
    :return:
    '''
    model_lstm = model_autoencoder_lstm([seq_len, 1], conf)
    model_lstm.compile(optimizer='adam', loss='mse')
    # fit model
    model_lstm.fit(sequence, sequence, epochs=conf["epochs"], verbose=0)

    # connect the encoder LSTM as the output layer
    model = Model(inputs=model_lstm.inputs, outputs=model_lstm.layers[0].output)
    # get the feature vector for the input sequence
    yhat0 = model.predict(sequence)
    yhat = yhat0.squeeze()

    del model_lstm
    gc.collect()
    backend.clear_session()

    return yhat


def encode_lstm_fea(fea, conf):
    '''
    Apply autoencoder on extracting features
    :param fea:
    :param conf:
    :return:
    '''
    auto_fea = []
    for ii in range(len(fea)):
        print("Feature: " + str(ii))
        sequence = fea[ii]  # 1-d array
        seq_len = sequence.shape[0]
        print("Input dimension: " + str(seq_len))
        sequence = sequence.reshape((1, seq_len, 1))

        yhat = autoencoder_train_pred(sequence, seq_len, conf)
        print("yhat's dimension: "+str(yhat.shape[0]))
        auto_fea.append(yhat)
    return auto_fea


# bin files for training & testing
# data_dir = '/Users/ys587/__Data/whistle_Contours/tonals_20190210' # bin files
bin_dir_train = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__first_pie'
bin_dir_test = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__second_pie'
bin_dir_all = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__all'
sound_dir = '/home/ys587/__Data/__whistle/__sound_species/'
# seltab_out_path = '/home/ys587/__Data/__whistle/__seltab'

species_name = ['bottlenose', 'common', 'spinner', 'melon-headed']
species_id = {'bottlenose': 0, 'common': 1, 'spinner': 2, 'melon-headed': 3}
contour_train, bin_wav_pair_train = bin_extract(bin_dir_train, sound_dir,
                                                species_name)
contour_test, bin_wav_pair_test = bin_extract(bin_dir_test, sound_dir,
                                              species_name)
# contour_all, bin_wav_pair = bin_extract(bin_dir_all, sound_dir, species_name)


# find contours longer than a pre-defined duration and extract audio features
conf_general = dict()
conf_general["duration_thre"] = 1.0
# conf.duration_thre = 0.50
percentile_list = [0, 25, 50, 75, 100]
conf_general['time_reso'] = 0.005  # 5 msec

# features & labels
fea_list_train, label_list_train, dur_train_max, count_long_train, \
count_all_train = freq_seq_label_generate(contour_train,
                                          conf_general['time_reso'],
                                          conf_general["duration_thre"],
                                          species_id)

fea_list_test, label_list_test, dur_test_max, count_long_test, count_all_test \
    = freq_seq_label_generate(contour_test,
                              conf_general['time_reso'],
                              conf_general["duration_thre"],
                              species_id)

conf_model = dict()
# conf_model['batch_size'] = 128
# conf_model['epochs'] = 1 # debug
# conf_model['learning_rate'] = 0.005
# conf_model["l2_regu"] = 5.e-2

conf_model['epochs'] = 100
conf_model['Num_temp'] = 0
conf_model['lstm_dim'] = 20 # dim of lstm
# conf_model.update({"fea_dim": 5})

# log_dir = "/home/ys587/__Data/__whistle/__log_dir/"
log_dir = "/home/ys587/__Data/__whistle/__log_dir_freq_contour"

# num_dolphins = len(species_name)
# conf_model["num_class"] = len(species_name)
# conf_model["fea_dim"] = 1

# Features & labels
train_idx = [ii for ii in range(len(label_list_train))]
random.shuffle(train_idx)
test_idx = [ii for ii in range(len(label_list_test))]
random.shuffle(test_idx)

feature_train = [fea_list_train[tt] for tt in train_idx]
target_train = to_categorical(label_list_train)[train_idx, :]
feature_test = [fea_list_test[tt] for tt in test_idx]
target_test = to_categorical(label_list_test)[test_idx, :]

# First N_num features or all features
N_num = conf_model['Num_temp']
if N_num == 0:
    # feature extraction using autoencoder_lstm
    print("Feature extraction for train data...")
    fea_auto_train = encode_lstm_fea(feature_train, conf_model)
    print("Feature extraction for test data...")
    fea_auto_test = encode_lstm_fea(feature_test, conf_model)

    target_train_list = np.argmax(target_train, axis=1)
    target_test_list = np.argmax(target_test, axis=1)
else:
    # feature extraction using autoencoder_lstm
    print("Feature extraction for train data...")
    fea_auto_train = encode_lstm_fea(feature_train[:N_num], conf_model)
    print("Feature extraction for test data...")
    fea_auto_test = encode_lstm_fea(feature_test[:N_num], conf_model)

    target_train_list = np.argmax(target_train[:N_num, :], axis=1)
    print("Train label:")
    print(target_train_list)
    target_test_list = np.argmax(target_test[:N_num, :], axis=1)
    print("Test label:")
    print(target_test_list)


count_species1 = target_train.sum(axis=0).tolist()
class_weight_temp = (max(count_species1) / np.array(count_species1)).tolist()
# class_weight_dict = dict()
class_weight_dict = {0 : class_weight_temp[0], 1 : class_weight_temp[1], 2 : class_weight_temp[2], 3 : class_weight_temp[3]}

# clf = SVC(gamma='auto', class_weight="balanced")
clf = SVC(gamma='auto', class_weight=class_weight_dict)
# clf = LinearSVC(random_state=0, tol='1e-5')
clf.fit(fea_auto_train, target_train_list)
target_pred = clf.predict(fea_auto_test)

log_dir0 = os.path.join(log_dir, 'autoencoder_lstm')
os.mkdir(log_dir0)

# save features in the drive
pkl_path_train = os.path.join(log_dir0, 'train.pkl')
with open(pkl_path_train, 'wb') as f:
    pickle.dump([fea_auto_train, target_train_list], f)
pkl_path_test = os.path.join(log_dir0, 'test.pkl')
with open(pkl_path_test, 'wb') as f:
    pickle.dump([fea_auto_test, target_test_list], f)

metrics_two_fold(target_test_list, target_pred, log_dir0, 'info.txt', None, conf_model)



