#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

code structure of training and validation mimics those in rocca
random forest as classifier
use functions of autoencoder in capde_cod_whale

Created on 11/8/19
@author: atoultaro
"""

import os
import numpy as np
import pickle
import random
import matplotlib.pyplot as plt
from math import floor, ceil
import pandas as pd

from contextlib import redirect_stdout

from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix, balanced_accuracy_score, \
    classification_report, f1_score
# import librosa
from keras.utils import to_categorical
from keras.layers import Input, Masking, Dense, Layer, Lambda, Flatten, Reshape, LSTM, RepeatVector, Dropout, TimeDistributed
from keras.models import Sequential, Model
from keras.optimizers import Adam
from keras.callbacks import TensorBoard
from keras.preprocessing.sequence import pad_sequences
from keras import backend as K

from classifier.recurrent import train_and_evaluate
# from classifier.buildmodels import build_model
from classifier.batchgenerator import PaddedBatchGenerator

from cape_cod_whale.preprocess import bin_extract, fea_context_base_generate, \
    fea_context_ae_generate, freq_seq_label_generate_no_harmonics, \
    fea_label_shuffle, contour_target_retrieve


from cape_cod_whale.classifier import two_fold_cross_validate_random_forest, two_fold_cross_validate
from cape_cod_whale.load_feature_model import load_fea_model


def autoencoder_lstm(x_train, conf):
    # x_train = np.expand_dims(x_train, axis=2)
    input_seq = Input(shape=(x_train.shape[1], 1))  # timestep

    # Image needs to be flattened in order to train with Dense layers
    # x = Flatten()(input_seq)
    x = Masking(mask_value=0.0)(input_seq)

    # Encoder
    x = LSTM(conf['ae_lstm'])(x)
    encoded = Dense(conf['ae_latent_dim'], activation='relu')(x)
    encoder = Model(input_seq, encoded)
    encoder.summary()

    # Decoder
    decoder_input = Input(shape=(conf['ae_latent_dim'],))
    x = Dense(conf['ae_lstm'], activation='relu')(decoder_input)
    input_repeat = RepeatVector(x_train.shape[1])(x)
    decoded = LSTM(1, return_sequences=True)(input_repeat)
    decoder = Model(decoder_input, decoded)
    decoder.summary()

    # Full Autoencoder
    autoencoder = Model(input_seq, decoder(encoder(input_seq)))
    autoencoder.summary()
    # adam = Adam(lr=0.0005)
    adam = Adam(lr=0.005)
    autoencoder.compile(optimizer=adam, loss='mse')
    autoencoder.fit(x_train, x_train,
                    callbacks=[TensorBoard(log_dir=conf['log_dir'])],
                    epochs=conf['epoch'], shuffle=True, batch_size=256)

    return encoder, decoder, autoencoder


def autoencoder_lstm_v2(x_train, conf):
    # x_train = np.expand_dims(x_train, axis=2)
    n_seq = x_train.shape[1]
    n_features = x_train.shape[2]
    input_seq = Input(shape=(n_seq, 1))  # timestep

    # Image needs to be flattened in order to train with Dense layers
    # x = Flatten()(input_seq)
    x = Masking(mask_value=0.0)(input_seq)

    # Encoder
    x = LSTM(conf['ae_lstm'], return_sequences=True)(x)
    encoded = LSTM(conf['ae_latent_dim'], return_sequences=False, activation='relu')(x)
    encoder = Model(input_seq, encoded)
    encoder.summary()

    # Decoder
    decoder_input = Input(shape=(conf['ae_latent_dim'],))
    # x = Dense(conf['ae_lstm'], activation='relu')(decoder_input)
    x = RepeatVector(n_seq)(decoder_input)
    x = LSTM(conf['ae_latent_dim'], return_sequences=True)(x)
    x = LSTM(conf['ae_lstm'], return_sequences=True)(x)
    decoded = TimeDistributed(Dense(n_features))(x)
    decoder = Model(decoder_input, decoded)
    decoder.summary()

    # Full Autoencoder
    autoencoder = Model(input_seq, decoder(encoder(input_seq)))
    autoencoder.summary()
    # adam = Adam(lr=0.0005)
    adam = Adam(lr=0.005)
    autoencoder.compile(optimizer=adam, loss='mse')
    autoencoder.fit(x_train, x_train,
                    callbacks=[TensorBoard(log_dir=conf['log_dir'])],
                    epochs=conf['epoch'], shuffle=True, batch_size=256)

    return encoder, decoder, autoencoder


def max_min_from_list(fea_list):
    max_fea = 0.0
    min_fea = 100000
    for ff in fea_list:
        max_fea = np.max((max_fea, ff.max()))
        min_fea = np.min((min_fea, ff.min()))
    return max_fea, min_fea


def sampling(args):
    z_mean, z_log_var = args
    batch, dim = K.shape(z_mean)[0], K.int_shape(z_mean)[1]
    epsilon = K.random_normal(shape=(batch, dim), mean=0.0, stddev=0.1)

    return z_mean + K.exp(0.5 * z_log_var) * epsilon


class CustomVariationalLayer(Layer):
    def vae_loss(self, z_mean, z_sigma, inputs, outputs):
        reconstruction_loss = K.sum(K.binary_crossentropy(K.batch_flatten(inputs), K.batch_flatten(outputs)), axis=-1)
        kl_loss = - 0.5 * K.sum(1.0 + z_sigma - K.square(z_mean) - K.exp(z_sigma), axis=-1)
        return K.mean(reconstruction_loss + kl_loss)

    def call(self, inputs):
        z_mean, z_sigma, inp, out = inputs
        loss = self.vae_loss(z_mean, z_sigma, inp, out)
        self.add_loss(loss, inputs=inputs)
        return out


def var_autoencoder_lstm(x_train, conf):
    input_seq  = Input(shape=(x_train.shape[1],1))
    x = Masking(mask_value=0.0)(input_seq)

    # encoder
    x = LSTM(conf['ae_lstm'])(x)
    x = Dense(conf['ae_latent_dim']*2, activation='relu')(x)
    dim_sampling = conf['ae_latent_dim']
    z_mean, z_sigma = Dense(dim_sampling)(x), Dense(dim_sampling)(x)
    z = Lambda(sampling, output_shape=(dim_sampling,))([z_mean, z_sigma])
    encoder = Model(input_seq, [z_mean, z_sigma, z])

    # Decoder
    decoder_input = Input(shape=(dim_sampling,))
    x = Dense(conf['ae_lstm'], activation='relu')(decoder_input)
    x = RepeatVector(x_train.shape[1])(x)
    outputs = LSTM(x_train.shape[2], return_sequences=True)(x)
    decoder = Model(decoder_input, outputs)

    # Full Autoencoder
    autoencoder_out = decoder(encoder(input_seq)[2])
    out = CustomVariationalLayer()(
        [z_mean, z_sigma, input_seq, autoencoder_out])
    autoencoder = Model(input_seq, out)

    adam = Adam(lr=0.0005)
    autoencoder.compile(optimizer=adam, loss=None)
    # autoencoder.fit(x_train, shuffle=True, epochs=50, batch_size=32)
    autoencoder.fit(x_train,
                    callbacks=[TensorBoard(log_dir=conf['log_dir'])],
                    epochs=conf['epoch'], shuffle=True, batch_size=256)

    return encoder, decoder, autoencoder


# bin files for training & testing
# data_dir = '/Users/ys587/__Data/whistle_Contours/tonals_20190210' # bin files
bin_dir = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files'
bin_dir_train = os.path.join(bin_dir, '__first_pie')
bin_dir_test = os.path.join(bin_dir, '__second_pie')
sound_dir = '/home/ys587/__Data/__whistle/__sound_species/'
species_name = ['bottlenose', 'common', 'spinner', 'melon-headed']
species_id = {'bottlenose': 0, 'common': 1, 'spinner': 2, 'melon-headed': 3}

conf_gen = dict()
conf_gen['bin_dir'] = bin_dir
conf_gen['species_name'] = species_name
conf_gen['species_id'] = species_id
# conf_gen["duration_thre"] a= 0.80  # DEBUG
conf_gen["duration_thre"] = 0.05
conf_gen['time_reso'] = 0.01  # 10 ms
conf_gen['transform'] = "frq_contour"
# conf_gen['transform'] = "rocca"  # rocca, cep or both
# conf_gen['transform'] = "rocca_cep"
# conf_gen['transform'] = "cep"

# cepstral coefficient
conf_gen['sample_rate'] = 192000
conf_gen['input_size'] = 4096  # input size for deep LSTM features
# 21.33 msec given sampling rate 192,000 Hz
conf_gen['filt_num'] = 64  # number of filterbanks in cepstral coefficients
conf_gen['low_freq'] = 5000.
conf_gen['high_freq'] = 23500.
conf_gen["num_class"] = len(species_name)

# random forest
# conf_gen['n_est'] = 200
conf_gen['n_est'] = 1000

# contextual features
conf_gen['context_win'] = 15.0  # sec
# conf_gen['pca_components'] = None
conf_gen['pca_components'] = 20
conf_gen['n_jobs'] = 6

# autoencoder
# conf_gen['ae_type'] = 'vae'
conf_gen['ae_type'] = 'ae'
conf_gen['ae_lstm'] = 8
conf_gen['ae_latent_dim'] = 32
conf_gen['epoch'] = 50
# conf_gen['epoch'] = 5  # DEBUG
conf_gen['log_dir'] = r'/home/ys587/__Data/__whistle/__log_dir_context/autoencoder_temp'
# conf_gen['log_dir'] = r'/home/ys587/__Data/__whistle/__log_dir_context/autoencoder_vae_temp'

if not os.path.exists(conf_gen['log_dir']):
    os.mkdir(conf_gen['log_dir'])

contour_train, bin_wav_pair_train = bin_extract(bin_dir_train, sound_dir,
                                                species_name)
contour_test, bin_wav_pair_test = bin_extract(bin_dir_test, sound_dir,
                                              species_name)

# fit autoencoder & generate encoder
# features & labels
# train data
fea_list_train, label_list_train, dur_train_max, count_long_train, \
count_all_train, fea_name = freq_seq_label_generate_no_harmonics(contour_train,
                                          conf_gen,
                                          species_id)
hist_dur = np.array([ff.shape[0] for ff in fea_list_train])

# test data
fea_list_test, label_list_test, dur_test_max, count_long_test, \
count_all_test, fea_name = freq_seq_label_generate_no_harmonics(contour_test,
                                          conf_gen,
                                          species_id)
dur_max_ind = ceil(max(dur_train_max, dur_test_max)/conf_gen['time_reso'])

if True:
    # find the max and min values ib both fea_train and fea_test
    max_fea_train, min_fea_train = max_min_from_list(fea_list_train)
    max_fea_test, min_fea_test = max_min_from_list(fea_list_test)
    max_fea = np.max((max_fea_train, max_fea_test))
    min_fea = np.min((min_fea_train, min_fea_test))
    max_minus_min_fea = max_fea - min_fea
    fea_list_train_norm = []
    for ff in range(len(fea_list_train)):
        fea_list_train_norm.append((fea_list_train[ff]-min_fea)/max_minus_min_fea)
    fea_list_test_norm = []
    for ff in range(len(fea_list_test)):
        fea_list_test_norm.append((fea_list_test[ff]-min_fea)/max_minus_min_fea)

    fea_arr_train = pad_sequences(fea_list_train_norm, maxlen=dur_max_ind, dtype='float')
    fea_arr_train_3d = np.expand_dims(fea_arr_train, axis=2)
    fea_arr_test = pad_sequences(fea_list_test_norm, maxlen=dur_max_ind, dtype='float')
    fea_arr_test_3d = np.expand_dims(fea_arr_test, axis=2)

    # autoencoder fit
    # autoencoder
    if conf_gen['ae_type'] is 'ae':
        encoder_train, decoder_train, autoencoder_train = autoencoder_lstm(fea_arr_train_3d, conf_gen)
        # encoder_train, decoder_train, autoencoder_train = autoencoder_lstm_v2(fea_arr_train_3d, conf_gen)
    elif conf_gen['ae_type'] is 'vae':
        # varitional autoencoder
        encoder_train, decoder_train, autoencoder_train = var_autoencoder_lstm(fea_arr_train_3d, conf_gen)
    else:
        print('No specified ae is available.')

    # for each contour, find features in its neighborhood window
    with open(os.path.join(conf_gen['log_dir'], 'architecture.txt'), 'w') as f:
        with redirect_stdout(f):
            print('Encoder:')
            encoder_train.summary()
            print('\nDecoder:')
            decoder_train.summary()
            print('\nAutoencoder:')
            autoencoder_train.summary()

    df_train, duration_max_train, count_long_train, count_all_train = \
        fea_context_ae_generate(contour_train, bin_wav_pair_train, bin_dir_train,
                                  conf_gen, encoder_train, min_fea, max_fea,
                                dur_max_ind, fea=conf_gen['ae_type'])
    df_test, duration_max_test, count_long_test, count_all_test = \
        fea_context_ae_generate(contour_test, bin_wav_pair_test, bin_dir_test,
                                  conf_gen, encoder_train, min_fea, max_fea,
                                dur_max_ind, fea=conf_gen['ae_type'])

    label_list_train = list(df_train['label'])
    label_list_test = list(df_test['label'])

    # feature ae + context ae
    fea_train = np.array(df_train.iloc[:, 4:])
    fea_test = np.array(df_test.iloc[:, 4:])
    feature_train, label_train = fea_label_shuffle(fea_train, label_list_train)
    feature_test, label_test = fea_label_shuffle(fea_test, label_list_test)

    log_dir = os.path.join(conf_gen['log_dir'], 'ae_contextae')
    y_pred, y_test = two_fold_cross_validate_random_forest(feature_train,
                                                           feature_test,
                                                           label_train,
                                                           label_test,
                                                           log_dir,
                                                           conf_gen)
    # feature ae:
    fea_train = np.array(df_train.iloc[:, 4:4+conf_gen['ae_latent_dim']])
    fea_test = np.array(df_test.iloc[:, 4:4+conf_gen['ae_latent_dim']])
    feature_train, label_train = fea_label_shuffle(fea_train, label_list_train)
    feature_test, label_test = fea_label_shuffle(fea_test, label_list_test)

    log_dir = os.path.join(conf_gen['log_dir'], 'ae')
    y_pred, y_test = two_fold_cross_validate_random_forest(feature_train,
                                                           feature_test,
                                                           label_train,
                                                           label_test,
                                                           log_dir,
                                                           conf_gen)

    # feature context ae
    fea_train = np.array(df_train.iloc[:, 4+conf_gen['ae_latent_dim']:])
    fea_test = np.array(df_test.iloc[:, 4+conf_gen['ae_latent_dim']:])
    feature_train, label_train = fea_label_shuffle(fea_train, label_list_train)
    feature_test, label_test = fea_label_shuffle(fea_test, label_list_test)

    log_dir = os.path.join(conf_gen['log_dir'], 'contextae')
    y_pred, y_test = two_fold_cross_validate_random_forest(feature_train,
                                                           feature_test,
                                                           label_train,
                                                           label_test,
                                                           log_dir,
                                                           conf_gen)
# feature rocca + context rocca
conf_gen['transform'] = "rocca"
df_train_rocca, duration_max_train_rocca, count_long_train_rocca, count_all_train_rocca = \
    fea_context_base_generate(contour_train, bin_wav_pair_train, bin_dir_train,
                              conf_gen)
df_test_rocca, duration_max_test_rocca, count_long_test_rocca, count_all_test_rocca = \
    fea_context_base_generate(contour_test, bin_wav_pair_test, bin_dir_test,
                              conf_gen)

fea_train = np.array(df_train_rocca.iloc[:, 4:])
fea_test = np.array(df_test_rocca.iloc[:, 4:])
feature_train, label_train = fea_label_shuffle(fea_train, label_list_train)
feature_test, label_test = fea_label_shuffle(fea_test, label_list_test)

log_dir = os.path.join(conf_gen['log_dir'], 'rocca_contextrocca')
y_pred, y_test = two_fold_cross_validate_random_forest(feature_train,
                                                       feature_test,
                                                       label_train,
                                                       label_test,
                                                       log_dir,
                                                       conf_gen)
# feature context rocca
fea_train = np.array(df_train_rocca.iloc[:, 4+41:])
fea_test = np.array(df_test_rocca.iloc[:, 4+41:])
feature_train, label_train = fea_label_shuffle(fea_train, label_list_train)
feature_test, label_test = fea_label_shuffle(fea_test, label_list_test)

log_dir = os.path.join(conf_gen['log_dir'], 'contextrocca')
y_pred, y_test = two_fold_cross_validate_random_forest(feature_train,
                                                       feature_test,
                                                       label_train,
                                                       label_test,
                                                       log_dir,
                                                       conf_gen)
# feature rocca
fea_train = np.array(df_train_rocca.iloc[:, 4:41])
fea_test = np.array(df_test_rocca.iloc[:, 4:41])
feature_train, label_train = fea_label_shuffle(fea_train, label_list_train)
feature_test, label_test = fea_label_shuffle(fea_test, label_list_test)

log_dir = os.path.join(conf_gen['log_dir'], 'rocca')
y_pred, y_test = two_fold_cross_validate_random_forest(feature_train,
                                                       feature_test,
                                                       label_train,
                                                       label_test,
                                                       log_dir,
                                                       conf_gen)

# feature all (ae + context ae + rocca + context rocca)
df_train_tot = pd.concat([df_train, df_train_rocca.iloc[:, 4:]], axis=1)
df_test_tot = pd.concat([df_test, df_test_rocca.iloc[:, 4:]], axis=1)
fea_train = np.array(df_train_tot.iloc[:, 4:])
fea_test = np.array(df_test_tot.iloc[:, 4:])
feature_train, label_train = fea_label_shuffle(fea_train, label_list_train)
feature_test, label_test = fea_label_shuffle(fea_test, label_list_test)

log_dir = os.path.join(conf_gen['log_dir'], 'all_fea')
y_pred, y_test = two_fold_cross_validate_random_forest(feature_train,
                                                       feature_test,
                                                       label_train,
                                                       label_test,
                                                       log_dir,
                                                       conf_gen)

with open(os.path.join(conf_gen['log_dir'], 'configuration.txt'), 'w') as f:
    with redirect_stdout(f):
        for key in list(conf_gen.keys()):
            print(key+'->'+str(conf_gen[key]))

# # apply trained autoencoder to both train and test data
# encoded_fea_test = encoder_train.predict(fea_arr_test_3d)
# encoded_fea_train = encoder_train.predict(fea_arr_train_3d)
#
# encoded_fea_all = np.vstack((encoded_fea_train, encoded_fea_test))  # ae
# # encoded_fea_all = np.vstack((encoded_fea_train[0], encoded_fea_test[0]))  # vae
# label_list_all = label_list_train + [ll+4 for ll in label_list_test]
# tsne_dir='/home/ys587/PycharmProjects/dash-sample-apps-master/apps/dash-tsne/data/fea_autoencoder'
# np.savetxt(os.path.join(tsne_dir, 'fea_ae_input.csv'), encoded_fea_all, fmt='%.8f', delimiter=',')
# np.savetxt(os.path.join(tsne_dir, 'fea_ae_labels.csv'), label_list_all, fmt='%d', delimiter=',')

# random forest
# feature_train, label_train = fea_label_shuffle(encoded_fea_train, label_list_train)
# feature_test, label_test = fea_label_shuffle(encoded_fea_test, label_list_test)

# vae
# log_dir = r'/home/ys587/__Data/__whistle/__log_dir_context/fea_context'
# y_pred3, y_test3 = two_fold_cross_validate_random_forest(encoded_fea_train[0],
#                                                        encoded_fea_test[0],
#                                                        label_list_train,
#                                                        label_list_test,
#                                                        conf_gen['log_dir'],
#                                                        conf_gen)

# y_pred3, y_test3 = two_fold_cross_validate_random_forest(encoded_fea_train,
#                                                        encoded_fea_test,
#                                                        label_list_train,
#                                                        label_list_test,
#                                                        conf_gen['log_dir'],
#                                                        conf_gen)

# ([3342., 1155.,  499.,  213.,   87.,   31.,   10.,    7.,    0., 5.]),
# for bins ([  6.,  28.,  50.,  72.,  94., 116., 138., 160., 182., 204., 226.])
# lots of very short contours
# 1159 (21.7%) is 10 or shorter; 1440 (26.9%) is (10,20]; 941 (17.6%) is (20,30]
# 568 is (30,40]; s1205 is larger than 40






# fea_list_test, label_list_test, dur_test_max, count_long_test, count_all_test, \
# _ = freq_seq_label_generate_no_harmonics(contour_test,
#                               conf_gen,
#                               species_id)





# feature extraction, including rocca features & autoencoder features





# # for each contour, find features in its neighborhood window
# df_train, duration_max_train, count_long_train, count_all_train = \
#     fea_context_base_generate(contour_train, bin_wav_pair_train, bin_dir_train,
#                               conf_gen)
#
# df_test, duration_max_test, count_long_test, count_all_test = \
#     fea_context_base_generate(contour_test, bin_wav_pair_test, bin_dir_test,
#                               conf_gen)
#
# # extract context features
# # df_train_context = fea_context_generate(df_train, conf_gen)
#
# label_list_train = list(df_train['label'])
# fea_train = np.array(df_train.iloc[:, 4:])
# label_list_test = list(df_test['label'])
# fea_test = np.array(df_test.iloc[:, 4:])
# feature_train, label_train = fea_label_shuffle(fea_train, label_list_train)
# feature_test, label_test = fea_label_shuffle(fea_test, label_list_test)
#
# log_dir = r'/home/ys587/__Data/__whistle/__log_dir_context/fea_ind_context'
# y_pred, y_test = two_fold_cross_validate_random_forest(feature_train,
#                                                        feature_test,
#                                                        label_train,
#                                                        label_test,
#                                                        log_dir,
#                                                        conf_gen)
#
# # feature rocca
# fea_train = np.array(df_train.iloc[:, 4:4+41])
# fea_test = np.array(df_test.iloc[:, 4:4+41])
# feature_train, label_train = fea_label_shuffle(fea_train, label_list_train)
# feature_test, label_test = fea_label_shuffle(fea_test, label_list_test)
#
# log_dir = r'/home/ys587/__Data/__whistle/__log_dir_context/fea_ind'
# y_pred2, y_test2 = two_fold_cross_validate_random_forest(feature_train,
#                                                        feature_test,
#                                                        label_train,
#                                                        label_test,
#                                                        log_dir,
#                                                        conf_gen)
#
# # feature context
# fea_train = np.array(df_train.iloc[:, 4+41:])
# fea_test = np.array(df_test.iloc[:, 4+41:])
# feature_train, label_train = fea_label_shuffle(fea_train, label_list_train)
# feature_test, label_test = fea_label_shuffle(fea_test, label_list_test)
#
# log_dir = r'/home/ys587/__Data/__whistle/__log_dir_context/fea_context'
# y_pred3, y_test3 = two_fold_cross_validate_random_forest(feature_train,
#                                                        feature_test,
#                                                        label_train,
#                                                        label_test,
#                                                        log_dir,
#                                                        conf_gen)