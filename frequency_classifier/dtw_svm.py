#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
How to visualize all the whistle contours?
Is dtw a viable option?
Can SVM handle the large sample size?

Created on 7/9/19
@author: atoultaro
"""

import os
import numpy as np
import pickle
import random
import matplotlib.pyplot as plt

# import librosa
from keras.utils import to_categorical

from classifier.recurrent import train_and_evaluate
# from classifier.buildmodels import build_model
from classifier.batchgenerator import PaddedBatchGenerator


from cape_cod_whale.preprocess import bin_extract, fea_label_generate, \
    save_fea, load_fea, freq_seq_label_generate, fea_label_shuffle
from cape_cod_whale.classifier import train_validation, \
    two_fold_cross_validate

# bin files for training & testing
# data_dir = '/Users/ys587/__Data/whistle_Contours/tonals_20190210' # bin files
bin_dir_train = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__first_pie'
bin_dir_test = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__second_pie'
bin_dir_all = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__all'
sound_dir = '/home/ys587/__Data/__whistle/__sound_species/'
# seltab_out_path = '/home/ys587/__Data/__whistle/__seltab'

species_name = ['bottlenose', 'common', 'spinner', 'melon-headed']
species_id = {'bottlenose': 0, 'common': 1, 'spinner': 2, 'melon-headed': 3}
contour_train, bin_wav_pair_train = bin_extract(bin_dir_train, sound_dir,
                                                species_name)
contour_test, bin_wav_pair_test = bin_extract(bin_dir_test, sound_dir,
                                              species_name)
# contour_all, bin_wav_pair = bin_extract(bin_dir_all, sound_dir, species_name)


# find contours longer than a pre-defined duration and extract audio features
conf_general = dict()
conf_general["duration_thre"] = 0.20
# conf.duration_thre = 0.50
percentile_list = [0, 25, 50, 75, 100]
conf_general['time_reso'] = 0.02
conf_general['transform'] = "zscorederiv"

# features & labels
fea_list_train, label_list_train, dur_train_max, count_long_train, \
count_all_train = freq_seq_label_generate(contour_train,
                                          conf_general,
                                          species_id)
def dist_seq(v1, v2):
    if v1.shape[0] >= v2.shape[0]:
        seq_long = v1
        seq_short = v2
    else:
        seq_long = v2
        seq_short = v1
    len_long = seq_long.shape[0]
    len_short = seq_short.shape[0]
    cost_length = len_long - len_short
    cost_diff = np.zeros()


num_species = [(np.array(label_list_train)==ii).sum() for ii in range(4)]
min_num = np.min(num_species)
num_data = len(label_list_train)
dist_mat = np.zeros((num_data, num_data))
for ii in range(num_data):
    for jj in range(num_data):
        dist_mat[ii, jj] =


