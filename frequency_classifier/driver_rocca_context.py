#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Context-based whistle classification

Not contour based, which means that features will be taken a look at within a
neighborhood window around the center of the contour

Created on 10/23/19
@author: atoultaro
"""
import os
import numpy as np
import pickle
import random
import matplotlib.pyplot as plt

from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix, balanced_accuracy_score, \
    classification_report, f1_score
# import librosa
from keras.utils import to_categorical
from classifier.recurrent import train_and_evaluate
# from classifier.buildmodels import build_model
from classifier.batchgenerator import PaddedBatchGenerator

from cape_cod_whale.preprocess import bin_extract, fea_context_base_generate, \
    fea_label_shuffle

from cape_cod_whale.classifier import two_fold_cross_validate_random_forest
from cape_cod_whale.load_feature_model import load_fea_model

# bin files for training & testing
# data_dir = '/Users/ys587/__Data/whistle_Contours/tonals_20190210' # bin files
bin_dir = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files'
bin_dir_train = os.path.join(bin_dir, '__first_pie')
bin_dir_test = os.path.join(bin_dir, '__second_pie')
sound_dir = '/home/ys587/__Data/__whistle/__sound_species/'
species_name = ['bottlenose', 'common', 'spinner', 'melon-headed']
species_id = {'bottlenose': 0, 'common': 1, 'spinner': 2, 'melon-headed': 3}

conf_gen = dict()
conf_gen['bin_dir'] = bin_dir
conf_gen['species_name'] = species_name
conf_gen['species_id'] = species_id
# conf_gen["duration_thre"] = 0.10
# conf_gen["duration_thre"] = 0.50
# conf_gen["duration_thre"] = 0.20
# conf_gen["duration_thre"] = 0.80
conf_gen["duration_thre"] = 0.05
conf_gen['time_reso'] = 0.01  # 10 ms
conf_gen['transform'] = "rocca"  # rocca, cep or both
# conf_gen['transform'] = "rocca_cep"
# conf_gen['transform'] = "cep"

# cepstral coefficient
conf_gen['sample_rate'] = 192000
conf_gen['input_size'] = 4096  # input size for deep LSTM features
# 21.33 msec given sampling rate 192,000 Hz
conf_gen['filt_num'] = 64  # number of filterbanks in cepstral coefficients
conf_gen['low_freq'] = 5000.
conf_gen['high_freq'] = 23500.
conf_gen["num_class"] = len(species_name)

# random forest
# conf_gen['n_est'] = 200
conf_gen['n_est'] = 1000

# contextual features
conf_gen['context_win'] = 30.0  # sec
# conf_gen['pca_components'] = 20
conf_gen['pca_components'] = None
conf_gen['n_jobs'] = 6


contour_train, bin_wav_pair_train = bin_extract(bin_dir_train, sound_dir,
                                                species_name)
contour_test, bin_wav_pair_test = bin_extract(bin_dir_test, sound_dir,
                                              species_name)

# for each contour, find features in its neighborhood window
df_train, duration_max_train, count_long_train, count_all_train = \
    fea_context_base_generate(contour_train, bin_wav_pair_train, bin_dir_train,
                              conf_gen)

df_test, duration_max_test, count_long_test, count_all_test = \
    fea_context_base_generate(contour_test, bin_wav_pair_test, bin_dir_test,
                              conf_gen)

label_list_train = list(df_train['label'])
fea_train = np.array(df_train.iloc[:, 4:])
label_list_test = list(df_test['label'])
fea_test = np.array(df_test.iloc[:, 4:])
feature_train, label_train = fea_label_shuffle(fea_train, label_list_train)
feature_test, label_test = fea_label_shuffle(fea_test, label_list_test)

log_dir = r'/home/ys587/__Data/__whistle/__log_dir_context/fea_ind_context'
y_pred, y_test = two_fold_cross_validate_random_forest(feature_train,
                                                       feature_test,
                                                       label_train,
                                                       label_test,
                                                       log_dir,
                                                       conf_gen)

# feature rocca
fea_train = np.array(df_train.iloc[:, 4:4+41])
fea_test = np.array(df_test.iloc[:, 4:4+41])
feature_train, label_train = fea_label_shuffle(fea_train, label_list_train)
feature_test, label_test = fea_label_shuffle(fea_test, label_list_test)

log_dir = r'/home/ys587/__Data/__whistle/__log_dir_context/fea_ind'
y_pred2, y_test2 = two_fold_cross_validate_random_forest(feature_train,
                                                       feature_test,
                                                       label_train,
                                                       label_test,
                                                       log_dir,
                                                       conf_gen)

# feature context
fea_train = np.array(df_train.iloc[:, 4+41:])
fea_test = np.array(df_test.iloc[:, 4+41:])
feature_train, label_train = fea_label_shuffle(fea_train, label_list_train)
feature_test, label_test = fea_label_shuffle(fea_test, label_list_test)

log_dir = r'/home/ys587/__Data/__whistle/__log_dir_context/fea_context'
y_pred3, y_test3 = two_fold_cross_validate_random_forest(feature_train,
                                                       feature_test,
                                                       label_train,
                                                       label_test,
                                                       log_dir,
                                                       conf_gen)

