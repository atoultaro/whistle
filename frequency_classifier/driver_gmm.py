#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
whistle classification using Gaussian Mixture Model (GMM)

Created on 8/30/19
@author: atoultaro
"""

import os
import pandas as pd
import numpy as np
import pickle
import random
import matplotlib.pyplot as plt
from sklearn.mixture import GaussianMixture
from sklearn.metrics import confusion_matrix, classification_report

# import librosa
from keras.utils import to_categorical

from classifier.recurrent import train_and_evaluate
# from classifier.buildmodels import build_model
from classifier.batchgenerator import PaddedBatchGenerator

from cape_cod_whale.preprocess import bin_extract, fea_label_generate, \
    save_fea, load_fea, freq_seq_label_generate, fea_label_shuffle, \
    freq_seq_label_generate_no_harmonics, fea_label_generate_no_harmonics
from cape_cod_whale.classifier import train_validation, \
    two_fold_cross_validate


def gmms_pred(gmm_list, feature_test, label_test, num_species):
    score_list = []
    for species in range(num_species):
        score = gmm_list[species].score_samples(feature_test)
        score_list.append(score)
    score_tot = np.array(score_list)
    label_pred = np.argmax(score_tot, axis=0)
    print('Confusion matrix for training/testing: ')
    print(confusion_matrix(label_test, label_pred))
    report = classification_report(label_test, label_pred, target_names=species_name)
    print(report)

    return label_pred, report


# def two_fold_cross_validate_gmm(fea_train, label_train, fea_test, label_test, log_dir, model_name):
# # (model_func_name, fea_train, label_train, fea_test, label_test, conf, log_dir, model_name):
#     log_dir = os.path.join(log_dir, model_name)
#     os.mkdir(log_dir)
#
#     # 1st fold
#     # y_pred0, y_test0 = train_validation(model_lstm, traingen, testgen, conf, log_dir1, model_name)
#
#
#     # 2nd fold
#     #
#     # y_pred = np.concatenate((y_pred0, y_pred1))
#     # y_test = np.concatenate((y_test0, y_test1))
#     #
#     # return y_pred, y_test

# bin files for training & testing
# data_dir = '/Users/ys587/__Data/whistle_Contours/tonals_20190210' # bin files
bin_dir_train = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__first_pie'
bin_dir_test = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__second_pie'
bin_dir_all = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__all'
sound_dir = '/home/ys587/__Data/__whistle/__sound_species/'
# seltab_out_path = '/home/ys587/__Data/__whistle/__seltab'

species_name = ['bottlenose', 'common', 'spinner', 'melon-headed']
species_id = {'bottlenose': 0, 'common': 1, 'spinner': 2, 'melon-headed': 3}
num_species = max(species_id.values())+1
contour_train, bin_wav_pair_train = bin_extract(bin_dir_train, sound_dir,
                                                species_name)
contour_test, bin_wav_pair_test = bin_extract(bin_dir_test, sound_dir,
                                              species_name)

# find contours longer than a pre-defined duration and extract audio features
conf_gen = dict()
# conf_gen["duration_thre"] = 1.0
conf_gen["duration_thre"] = 0.5
percentile_list = [0, 25, 50, 75, 100]
conf_gen['time_reso'] = 0.02
# conf_gen['transform'] = "zscorederiv"
conf_gen['transform'] = "non_z"
conf_gen['n_components'] = 64
conf_gen['tol'] = 1e-3
conf_gen['max_iter'] = 200
conf_gen['covar'] = 'diag'
conf_gen['shuffle'] = True

# features & labels
train_data_file = u'/home/ys587/__Data/__whistle/__cepstrum/__data/df_train.pkl'
if os.path.exists(train_data_file) is True:
    df_train = pd.read_pickle(train_data_file)
else:
    df_train,  dur_train_max, count_long_train, count_all_train = \
        fea_label_generate_no_harmonics(contour_train,
                                        bin_wav_pair_train,
                                        bin_dir_train,
                                        species_id,
                                        conf_gen)
    df_train.to_pickle(train_data_file)

test_data_file = u'/home/ys587/__Data/__whistle/__cepstrum/__data/df_test.pkl'
if os.path.exists(test_data_file) is True:
    df_test = pd.read_pickle(test_data_file)
else:
    df_test,  dur_test_max, count_long_test, count_all_test = \
        fea_label_generate_no_harmonics(contour_test,
                                        bin_wav_pair_test,
                                        bin_dir_test,
                                        species_id,
                                        conf_gen)
    df_test.to_pickle(test_data_file)

conf_model = dict()
# conf_model['num_mix'] = 3
log_dir = "/home/ys587/__Data/__whistle/__cepstrum/__log/"

fea_train = df_train.iloc[:, 3:].to_numpy()
label_train0 = df_train.iloc[:, 2].astype(int).to_numpy()
fea_test = df_test.iloc[:, 3:].to_numpy()
label_test0 = df_test.iloc[:, 2].astype(int).to_numpy()

# shuffle
if conf_gen['shuffle'] is True:
    feature_train, label_train = fea_label_shuffle(fea_train.tolist(), label_train0)
    feature_test, label_test = fea_label_shuffle(fea_test.tolist(), label_test0)
else:
    feature_train = fea_train
    label_train = label_train0
    feature_test = fea_test
    label_test = label_test0

# training
num_species = 4
gmm_list = []
for species in range(num_species):
    print('Training a GMM classifier for '+species_name[species])
    # data for gmm
    spec_ind = np.where(label_train == species)[0]
    # spec_ind = np.where(label_test == species)[0]
    fea_gmm_train_list = []
    for ind in spec_ind:
        fea_gmm_train_list.append(feature_train[ind, :])
        # fea_gmm_train_list.append(feature_test[ind, :])
    fea_gmm_train = np.vstack(fea_gmm_train_list)

    # gmm model
    gmm = GaussianMixture(covariance_type=conf_gen['covar'], n_components=conf_gen['n_components'], tol=conf_gen['tol'], max_iter=conf_gen['max_iter'], verbose=1)
    gmm.fit(fea_gmm_train)
    gmm_list.append(gmm)

# validation on training data
# score_list = []
# for species in range(num_species):
#     score = gmm_list[species].score_samples(feature_train)
#     score_list.append(score)
# score_tot = np.array(score_list)
# label_pred = np.argmax(score_tot, axis=0)
# print('Confusion matrix for training/training: ')
# print(confusion_matrix(label_train, label_pred))
# print(classification_report(label_train, label_pred, target_names=species_name))
label_pred0, report = gmms_pred(gmm_list, feature_train, label_train, num_species)

# testing
label_pred, report = gmms_pred(gmm_list, feature_test, label_test, num_species)
# score_list = []
# for species in range(num_species):
#     score = gmm_list[species].score_samples(feature_test)
#     score_list.append(score)
# score_tot = np.array(score_list)
# label_pred = np.argmax(score_tot, axis=0)
# print('Confusion matrix for training/testing: ')
# print(confusion_matrix(label_test, label_pred))
# print(classification_report(label_test, label_pred, target_names=species_name))
