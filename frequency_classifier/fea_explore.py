#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Explore features

Created on 7/1/19
@author: atoultaro
"""

import os
import numpy as np
import pickle
import random

# import matplotlib.pyplot as plt
from sklearn.svm import SVC, LinearSVC
from keras.utils import to_categorical
# import librosa
import seaborn as sns # west wing
import pandas as pd
import matplotlib.pyplot as plt

# from classifier.recurrent import train_and_evaluate
# from classifier.buildmodels import build_model
# from classifier.batchgenerator import PaddedBatchGenerator

from cape_cod_whale.preprocess import bin_extract, fea_label_generate, \
    save_fea, load_fea, freq_seq_label_generate
from cape_cod_whale.classifier import metrics_two_fold

# bin files for training & testing
# data_dir = '/Users/ys587/__Data/whistle_Contours/tonals_20190210' # bin files
bin_dir_train = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__first_pie'
bin_dir_test = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__second_pie'
bin_dir_all = '/home/ys587/__Data/__whistle/tonals_20190210/label_bin_files/__all'
sound_dir = '/home/ys587/__Data/__whistle/__sound_species/'
# seltab_out_path = '/home/ys587/__Data/__whistle/__seltab'

species_name = ['bottlenose', 'common', 'spinner', 'melon-headed']
species_id = {'bottlenose': 0, 'common': 1, 'spinner': 2, 'melon-headed': 3}
species_name_by_id = {0 : 'bottlenose', 1 : 'common', 2 : 'spinner', 3 :'melon-headed'}
contour_train, bin_wav_pair_train = bin_extract(bin_dir_train, sound_dir,
                                                species_name)
# contour_test, bin_wav_pair_test = bin_extract(bin_dir_test, sound_dir,
#                                               species_name)

# find contours longer than a pre-defined duration and extract audio features
conf_general = dict()
conf_general["duration_thre"] = 0.2
# conf.duration_thre = 0.50
percentile_list = [0, 25, 50, 75, 100]
conf_general['time_reso'] = 0.005

# features & labels
print("data: features and labels")
fea_list_train, label_list_train, dur_train_max, count_long_train, \
count_all_train = freq_seq_label_generate(contour_train,
                                          conf_general['time_reso'],
                                          conf_general["duration_thre"],
                                          species_id)

# fea_list_test, label_list_test, dur_test_max, count_long_test, count_all_test \
#     = freq_seq_label_generate(contour_test,
#                               conf_general['time_reso'],
#                               conf_general["duration_thre"],
#                               species_id)

# Features & labels
# feature_train, label_list_train
train_num = len(fea_list_train)

# whistle duration
fea_dur = np.zeros(train_num)
fea_freq_median = np.zeros(train_num)
fea_freq_mean = np.zeros(train_num)
fea_freq_std_norm = np.zeros(train_num)
fea_freq_diff_ratio = np.zeros(train_num)
for ii in range(train_num):
    fea_dur[ii] = fea_list_train[ii].shape[0]*conf_general['time_reso']
    fea_freq_median[ii] = np.median(fea_list_train[ii])
    fea_freq_mean[ii] = np.mean(fea_list_train[ii])
    fea_freq_std_norm[ii] = np.std(fea_list_train[ii])/fea_freq_mean[ii]

    # about freq diff
    freq_diff = np.diff(fea_list_train[ii])
    fea_freq_diff_ratio[ii] = np.mean(freq_diff)
    # if (fea_list_train[ii]).sum() is 0.0:
    #     print("ii: "+str(ii))
    # freq_diff = np.diff(fea_list_train[ii])
    # freq_diff_pos = (freq_diff*(freq_diff >= 0)).sum()
    # freq_diff_neg = (freq_diff * -1.0 * (freq_diff <= 0)).sum()
    # fea_freq_diff_ratio[ii] = freq_diff_pos / (freq_diff_pos + freq_diff_neg)
    # if np.isnan(fea_freq_diff_ratio[ii]):
    #     print("nan => ii: " + str(ii))

label_train_name = []
for ii in range(train_num):
    label_train_name.append(species_name_by_id[label_list_train[ii]])

fea_mat = np.vstack((fea_dur, fea_freq_median, fea_freq_mean, fea_freq_std_norm, fea_freq_diff_ratio))
df_fea = pd.DataFrame(fea_mat.T, columns=['duration', 'freq_median','freq_mean','freq_std_norm', 'freq_diff_ratio'])
df_fea['species_id'] =  label_train_name
df_fea['species_id'] = df_fea['species_id'].astype('category')


fea_dir = r'/home/ys587/__Data/__whistle/__log_dir_freq_contour/__fea_explore/'

tips = sns.load_dataset("tips")
# freq
plt.figure()
ax0 = sns.boxplot(x="species_id", y="duration", data=df_fea)
plt.savefig(os.path.join(fea_dir, 'duration.png'), dpi=300)
plt.figure()
ax1 = sns.boxplot(x="species_id", y="freq_mean", data=df_fea)
plt.savefig(os.path.join(fea_dir, 'freq_mean.png'), dpi=300)
plt.figure()
ax2 = sns.boxplot(x="species_id", y="freq_median", data=df_fea)
plt.savefig(os.path.join(fea_dir, 'freq_median.png'), dpi=300)
plt.figure()
ax3 = sns.boxplot(x="species_id", y="freq_std_norm", data=df_fea)
plt.savefig(os.path.join(fea_dir, 'freq_std_norm.png'), dpi=300)
plt.figure()
ax5 = sns.boxplot(x="species_id", y="freq_diff_ratio", data=df_fea)
plt.savefig(os.path.join(fea_dir, 'freq_diff_ratio.png'), dpi=300)

# sns.boxplot()
plt.figure()
ax4 = sns.scatterplot(x="duration", y="freq_std_norm", hue="species_id", style="species_id", data=df_fea)
plt.savefig(os.path.join(fea_dir, 'freq_std_norm_vs_duration.png'), dpi=300)
plt.figure()
ax6 = sns.scatterplot(x="freq_mean", y="freq_std_norm", hue="species_id", style="species_id", data=df_fea)
plt.savefig(os.path.join(fea_dir, 'freq_std_norm_vs_freq_mean.png'), dpi=300)

