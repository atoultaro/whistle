#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
whistle contour classification FM whistles
running script for RNN experiments
To replace run_classification.py

Created on 6/3/19
@author: atoultaro
"""
import os
import pickle
import random
import argparse
from collections import namedtuple

# add-on imports
import numpy as np
from keras.utils import to_categorical
import matplotlib.pyplot as plt


from classifier.recurrent import train_and_evaluate
from classifier.buildmodels import build_model
from classifier.crossvalidator import CrossValidator
from utilities import xmltodict_typed, annotations, features
from classifier import specifications

def crossval(params):

    np.seterr(all='raise')
    corpus = annotations.get_files(params["contourdir"])

    # Fairly small, let's load everybody in
    # Side effect:  files with no whistles matching criteria
    # are removed from corpus to ensure more balanced cross validation
    feats = features.read(corpus, params["features"])
    # Some files may have zero whistles after selection criteria.
    # Remove the files and empty feature lists

    print("Whistle report")
    for k,v in feats.items():
        print("%s %d"%(k,sum([len(f) for f in v])))

    # todo: support other stratifications
    annotations.stratify_byfile(corpus, feats)

    # Move features into groups that should not be split across
    # the train/test barrier.
    # classes - Dictionary of class names and number to which they are assigned
    # ids - List of group numbers
    # group_examples - List where each item is a list of examples in the
    #    group.
    # group_labels - Category of each group
    group_type = namedtuple("group_type",
                            ('classes', 'ids', 'examples', 'labels'))
    groups = group_type(classes={}, ids=[], examples=[], labels=[])
    grpidx = 0
    classidx = 0
    classlist = []
    for catname in corpus.keys():
        classlist.append(catname)
        groups.classes[catname] = classidx
        for exgroup in feats[catname]:
            groups.ids.append(grpidx)
            groups.labels.append(classidx)
            groups.examples.append(exgroup)
            grpidx += 1
        classidx += 1

    # Construct the model --------------------------------
    # Determine arguments to feed to model
    args = params['model']['parameters']  # arguments specified by user
    # Features are time X dim features
    args['input_nodes'] = groups.examples[0][0].shape[1]  # example dimension
    args['output_nodes'] = len(groups.classes)  # output dimension
    model_spec = specifications.build_model(params['model']['name'], args)

    cv = CrossValidator(groups, model_spec, train_and_evaluate,
                        n_folds = params['folds_n'],
                        batch_size = params['model']['optimization']['batch_size'],
                        epochs = params['model']['optimization']['epochs'],
                        saveroot=params['saveroot'],
                        classnames=classlist)


if __name__ == '__main__':
    plt.ion()

    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--method",
                        default=None,
                        choices=["crossval", "postprocess"],
                        help=""""
Processing method:

  Most methods produce or use HDF5 files containing experimental results.
  All files are stored in a directory that is the same name as the experiment
  specification and placed as a sibling of the specification.

  postprocess - Used for post-processing analysis.

  Can be used to override /model/method in the XML configuration file 
""")
    parser.add_argument("--xml", help="XML specification file")
    args = parser.parse_args()

    # Read configuration
    print("Method %s, specification file:  %s" % (args.method, args.xml))
    spec = xmltodict_typed.parse(open(args.xml, 'rb'))

    # Save location based on configuration file name
    [fname, ext] = os.path.splitext(args.xml)
    spec['experiment']['saveroot'] = fname
    if not os.path.isdir(spec['experiment']['saveroot']):
        os.mkdir(spec['experiment']['saveroot'])
    fname = fname
    spec['experiment']['save_to'] = fname
    spec['experiment']['name'] = os.path.basename(fname)
    spec['experiment']['metadata'] = open(args.xml, 'r').read()

    if args.method is None:
        try:
            method = spec['experiment']['method']
        except KeyError:
            method = "crossval"
    else:
        method = args.method
    # parser.parse_args("-m", method)   # validate method

    if method == "postprocess":
        postprocess(spec['experiment'])
    else:
        crossval(spec['experiment'])

    plt.show()
